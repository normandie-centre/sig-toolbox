# SIG-Toolbox

Dépôt pour les outils SIG du CEREMA Normandie Centre, partitionné par logiciel utilisé.

Arborescence du dépôt :
```
├── Arcgis : des scripts arcpy, toolbox arcgis pro, ...
├── Excel : les macros Excel
├── Matlab : les scripts Matlab
├── PG-linker : un outil nodejs pour faire des foreign tables entre serveurs postgres
├── PostgreSQL : les outils liés à postgres
├── Python : les scripts, outils et tableaux de bord en Python
├── QGIS : les models builders, toolbox, plugins et styles QGIS
├── R : les dashboards Shiny et autres scripts R
├── Stage : Dossier contenant tous les scripts des stages géomatiques
└── utils : des petits scripts qui peuvent faciliter la vie
```

