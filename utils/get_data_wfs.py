# -*- coding: utf-8 -*-

"""
Script permettant le téléchargement de données WFS avec filtrages géographique et attributaire. Fonctionne par défaut avec les flux Géoportail et la clef Cerema.

Il faut modifier à la main les paramètres dans le script.

Usage :
-------
    python get_data_wfs.py

"""

__author__ = "Thomas Escrihuela"
__contact__ = "Thomas.Escrihuela@cerema.fr"
__date__ = "2021/05"


import requests
from xml.etree import ElementTree
import zipfile
import os


############################
### Fonctions  #############
############################

def download_url(url, save_path, params = None, auth = None, chunk_size=128):
    """Téléchargement d'un zip avec requests"""
    r = requests.get(url, params=params, auth=auth, stream=True)
    # r = requests.get(url, params=params, auth=auth, proxies = proxies, stream=True)   # <proxy>
    with open(save_path, 'wb') as fd:
        for chunk in r.iter_content(chunk_size=chunk_size):
            fd.write(chunk)

def create_directory(absolute_path_to_directory):
    """Création de répertoire si non existe"""
    if not os.path.exists(absolute_path_to_directory):
        os.makedirs(absolute_path_to_directory)


############################
### Paramètres  ############
############################

### WFS Parameters

# # Extraction GPU
base_request = 'https://data.geopf.fr/wfs/ows'
layer = "wfs_du:zone_urba"
# bbox = "bbox(the_geom,47.9194,-0.8166,48.9196,1.6526)"
bbox="bbox(the_geom,-58,-120,55,60)" # La France avec DOM-TOM
# bbox="bbox(the_geom,-21.486034,55.99726,-20.812584,55.056386)"   # La Réunion
# srsname = "EPSG:2975"

## Extraction des routes depuis le flux WFS de Magellium exposant le réseau routier
# base_request = 'https://magosm.magellium.com/geoserver/ows'
# layer = "magosm:france_highways_line"
# # bbox = "bbox(the_geom,47.9194,-0.8166,48.9196,1.6526)"
# bbox = "bbox(the_geom,37.600,-10.104,55.170,13.281)"

# Extraction BDTopo
# base_request = 'https://wxs.ign.fr/essentiels/geoportail/wfs'
# layer = "BDCARTO_BDD_WLD_WGS84G:troncon_route"
# bbox = "bbox(the_geom,47.9194,-0.8166,48.9196,1.6526)"
# filter = "vocation in ('Liaison régionale', 'Type autoroutier', 'Liaison principale')"

srsname = "EPSG:4326"
outputformat = "shape-zip"
pas = 5000

### Output parameters
output_directory_name = "output"
zip_directory_name = "zip"
shape_directory_name = "shape"


############################
### Constantes  ############
############################
static_params = (
    ('service', 'WFS'),
    ('request', 'GetFeature'),
    ('version', '2.0.0'),
    ('typename', layer),
    ('typenames', layer),
    ('sortBy', 'gid'),    # à utiliser avec le GPU de la GPF car pas de pk sur la table zone_urba (07/2024)
)

output_file_name = layer.replace(':', '_') + ".geojson"
output_shp_name = layer.replace(':', '_') + ".shp"

zip_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), output_directory_name, zip_directory_name)
shape_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), output_directory_name, shape_directory_name)

output_shapefile = os.path.join(shape_directory, output_shp_name)


############################
### MAIN ###################
############################

if __name__ == "__main__":

    ## Création des répertoires de sortie
    create_directory(zip_directory)
    create_directory(shape_directory)


    ## Détermination du nombre d'entités à télécharger
    print("Détermination du nombre d'entités à télécharger...")

    custom_params = (
        ('count', '1'),
        ('cql_filter', bbox)
        # ('cql_filter', bbox + ' and ' + filter)       # à activer si filtre attributaire
    )
    params = (static_params + custom_params)

    response = requests.get(base_request, params = params)
    # print(response.request.url)
    # response = requests.get(base_request, params = params, auth = (username, password), proxies = proxies)
    xml = ElementTree.fromstring(response.text)
    nb_entites = int(xml.get('numberMatched'))

    if (not isinstance(nb_entites, int)):
        print(f"Error : [{nb_entites}] n'est pas un nombre")
        exit

    ## Téléchargement des zips et décompression
    print(f"Téléchargement de {nb_entites} entités pour {layer} par WFS...")

    for i in range(0, nb_entites, pas):
        print(f"Récupération des objets {i+1} à {i+pas}")
        custom_params = (
            ('srsname', srsname),
            ('outputformat', outputformat),
            ('startindex', i),
            ('count', pas),
            ('cql_filter', bbox)
            # ('cql_filter', bbox + ' and ' + filter)   # à activer si filtre attributaire
        )
        params = (static_params + custom_params)
        

        # Téléchargement de l'archive
        current_entities = layer.replace(':', '_') + str(i)
        zip = os.path.join(zip_directory, current_entities + ".zip")
        download_url(base_request, zip, params=params)
        # download_url(base_request, zip, params=params, auth=(username, password))
        

        ## Extraction du zip
        with zipfile.ZipFile(zip, 'r') as zip_ref:
            shape_directory_current_entities = os.path.join(shape_directory, current_entities)
            create_directory(shape_directory_current_entities)
            zip_ref.extractall(shape_directory_current_entities)


    ## Fusion des shapefiles
    print("Fusion des shapefiles...")
    creation_shp = False
    for root, dirs, files in os.walk(shape_directory):
        for file in files:
            if file.endswith(".shp"):
                shp = os.path.join(root, file)
                # Si le shp fusionné n'existe pas, on le crée
                if not creation_shp:
                    creation_shp = True
                    command = f'ogr2ogr -overwrite -f "ESRI Shapefile" {output_shapefile} {shp}'
                    os.popen(command).read()
                # Si le shp fusionné existe, on ajoute des entités
                else:
                    command = f'ogr2ogr -f "ESRI Shapefile" -update -append {output_shapefile} {shp}'
                    os.popen(command).read()


    ## Conversion en Geojson dans le répertoire courant
    command = f'ogr2ogr -overwrite -f "Geojson" {output_file_name} {output_shapefile}'
    os.popen(command).read()
    print(f"Génération de {output_file_name} terminé")
