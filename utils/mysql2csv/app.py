"""
Outil qui crée une interface basique de conversion de dump MySQL en csv 

Usage :
    python convert_dump_to_csv.py
"""

__author__ = "Thomas Escrihuela <Thomas.Escrihuela@cerema.fr>"
__version__ = "1.0"


import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
import subprocess
import mysql_dump_to_csv

class Interface(tk.Tk):
    
    """Notre fenêtre principale.
    Tous les widgets sont stockés comme attributs de cette fenêtre."""
    
    def __init__(self):
        """Constructeur de la classe
        Crée les attributs de l'objets (fichier et informations de connexion à la bdd)"""
        tk.Tk.__init__(self)
        self.fichier = ""
        self.rep = ""

        self.creer_widgets()

    def creer_widgets(self):
        
        """Permet de créer tous les widgets de l'objet"""
        # Widgets du chargement de fichier source et de lancement du l'import
        self.load = tk.Button(self, text="Import du dump MySQL", command=self.load_file, width=20)
        self.fichier_source = tk.Label(width=100)

        # Widgets du chargement de fichier source et de lancement du l'import
        self.load_2 = tk.Button(self, text="Répertoire de sortie", command=self.load_directory, width=20)
        self.rep_sortie = tk.Label(width=100)

        self.run = tk.Button(self, text="Lancer la conversion", command=self.convert_mysql_to_csv)

        # Placement des widgets
        self.load.grid(row=0, column=0)
        self.fichier_source.grid(row=0, column=1)
        self.load_2.grid(row=1, column=0)
        self.rep_sortie.grid(row=1, column=1)
        self.run.grid(row=3, column=1)


    def load_file(self):
        """Charge un dump MySQL"""
        file = filedialog.askopenfilename(title = "Sélection du dump MySQL", filetypes=[("Tous les fichiers","*.*")])
        self.fichier = file
        self.fichier_source["text"] = file


    def load_directory(self):
        """Sélectionne le répertoire de sortie"""
        rep = filedialog.askdirectory(title = "Répertoire en sortie")
        self.rep = rep
        self.rep_sortie["text"] = rep


    def convert_mysql_to_csv(self):
        """Lance la conversion du dump MySQL en CSV"""

        # Teste l'existence du fichier
        if not self.fichier:
            messagebox.showerror(message="Veuillez sélectionner un dump MySQL en entrée",title='Absence de dump MySQL')
            return

        # Teste l'existence du fichier
        if not self.rep:
            messagebox.showerror(message="Veuillez sélectionner un répertoire de sortie",title='Absence de répertoire de sortie')
            return

        r = mysql_dump_to_csv.parse_file(self.fichier, self.rep)
        if r == 1:
            messagebox.showinfo(message="Le dump a bien été converti en csv disponibles dans le répertoire de sortie",title='Succès de la conversion !')
        else:
            messagebox.showerror(message="Erreur lors de la conversion du dump. Vérifiez que votre fichier est bien un dump MySQL",title='Erreur de la conversion')


if __name__ == "__main__":
    app = Interface()
    app.title("Conversion de dump MySQL en CSV")
    app.mainloop()
