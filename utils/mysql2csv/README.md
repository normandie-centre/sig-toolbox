# Conversion de dump MySQL en CSV

## Execution du script

Le script **app.py** peut être lancé par python pour créer une interface graphique afin de convertir un dump MySQL en csv.

L'interface propose de sélectionner un fichier dump au format SQL puis de choisir le dossier de sortie.

À chaque table du dump correspondra un fichier csv.

## Compilation en exécutable

Le script peut être également compilé en exécutable. Pour ceci, il faut avoir python3 et le module *pyinstaller* et lancer la commande suivante :
`pyinstaller.exe --clean --onefile --noconsole --add-data 'mysql_dump_to_csv;.' -n faq_rt app.py`

Le dossier *dist* est alors créé et contiendra l'exécutable **faq_rt.exe**.

## Lancer l'éxecutable

Pour une utilisation immédiate, l'éxecutable du dépôt peut directement être lancé (éxecutable dans **dist/**).

## Source

_Forked from https://github.com/mwildehahn/mysql-dump-to-csv_
