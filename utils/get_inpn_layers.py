import requests
import xml.etree.ElementTree as ET
import re
from pprint import pprint
import os
import zipfile


def download_url(url, save_path, chunk_size=128):
    r = requests.get(url, stream=True)
    with open(save_path, 'wb') as fd:
        for chunk in r.iter_content(chunk_size=chunk_size):
            fd.write(chunk)

def create_directory(absolute_path_to_directory):
    if not os.path.exists(absolute_path_to_directory):
        os.makedirs(absolute_path_to_directory)


bbox = '490000,6820000,513000,6850000,EPSG:2154'
output_directory_name = "output"
zip_directory_name = "zip"
shape_directory_name = "shape"
getcap_inpn_wfs = "http://ws.carmencarto.fr/WFS/119/fxx_inpn?request=GetCapabilities"
getfeature_inpn_wfs = "http://ws.carmencarto.fr/WFS/119/fxx_inpn?SERVICE=WFS&REQUEST=GetFeature&VERSION=2.0.0&TYPENAME=<layer>&SRSNAME=EPSG:2154&BBOX=" + bbox


## Création des répertoires de sortie
zip_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), output_directory_name, zip_directory_name)
shape_directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), output_directory_name, shape_directory_name)

create_directory(zip_directory)
create_directory(shape_directory)


## Get layers from GetCap
r = requests.get(getcap_inpn_wfs)
root = ET.fromstring(r.text)
layers = root.findall(".//{http://www.opengis.net/wfs}FeatureType//{http://www.opengis.net/wfs}Name")
layers = [l.text for l in layers]

## Get data
for l in layers:
    ## Get number of features in extent
    count_request = re.sub('<layer>',l,getfeature_inpn_wfs)
    r = requests.get(count_request)
    root = ET.fromstring(r.text)
    nb_features = root.get('numberReturned')

    if nb_features == '0':
        print("Pas d'entité trouvée pour " + l)
    else:
        print(nb_features + " entitées trouvées pour " + l)
        ## Get data in zip directory
        get_request = count_request + "&outputformat=shape"
        zip = os.path.join(zip_directory,l+".zip")
        download_url(get_request,zip)

        ## Extract data to shape directory
        with zipfile.ZipFile(zip, 'r') as zip_ref:
            zip_ref.extractall(shape_directory)