#! usr/bin/env python

from sys import argv
from os.path import exists
from numpy import correlate
import simplejson as json 

def compute(data):
    print(data)
    for key in data['ListeElementsMetierReleve']:

        data[correspondance[key['IdElement']]] = key['Valeur']
    print(data)
    return data

# script, in_file, out_file = argv

in_file = r"./Releves.json"
out_file = in_file.replace("json", "geojson")
worklist_file = r"WorkList.json"

longitude_champs = "Longitude"
latitude_champs = "Latitude"
data = json.load(open(in_file))

correspondance = {}
if exists(worklist_file):
    d = json.load(open(worklist_file))
    for a in d["ListeElementWorkList"]:
        for b in a["ListeSousElements"]:
            correspondance[b['Id']] = b['Label']

geojson = {
    "type": "FeatureCollection",
    "features": [
    {
        "type": "Feature",
        "geometry" : {
            "type": "Point",
            "coordinates": [d[longitude_champs], d[latitude_champs]],
            },
        "properties" : compute(d),
     } for d in data]
}

output = open(out_file, 'w')
json.dump(geojson, output)

# print(geojson)