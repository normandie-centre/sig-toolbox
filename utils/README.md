# Un zeste de scripting...

```
.
├── get_DPE_data.sh : télécharger tous les DPE en une commande
├── get_data_wfs.sh : récupérer des données par WFS avec filtres géographique et attributaire
├── get_data_wfs.py : récupérer des données par WFS avec filtres géographique et attributaire
├── get_inpn_layers.py : un script standalone faisant appel à l'API QGIS pour télécharger les données de l'INPN
├── json2csv : un utilitaire pour transformer un json en csv
├── json2geojson : un script pour transformer un json contenant des champs lon/lat en geojson. Utile pour Scout.
└── mysql2csv : un utilitaire pour transformer un dump mysql en csv
```
