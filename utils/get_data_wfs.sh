#!/bin/bash

## BDCARTO
#layer="BDCARTO_BDD_WLD_WGS84G:troncon_route"
#bbox="bbox(the_geom,47.9194,-0.8166,48.9196,1.6526)"
#filter="vocation in ('Liaison régionale', 'Type autoroutier', 'Liaison principale')"
#url="https://wxs.ign.fr/qn6ksdvxtfbt53g0c55dvvyy/geoportail/wfs"

## BDTOPO
#layer="BDTOPO_V3:commune"
#bbox="bbox(geometrie,-4.900,42.212,8.298,51.437)" #FXX
#filter="code_insee_de_la_region = '24'"
#url="https://wxs.ign.fr/qn6ksdvxtfbt53g0c55dvvyy/geoportail/wfs"

## GPU
url="https://wxs-gpu.mongeoportail.ign.fr/externe/39wtxmgtn23okfbbs1al2lz3/wfs"
layer="wfs_du:zone_urba"
# layer="wfs_du:prescription_surf"
bbox = "bbox(the_geom,48.971,-5.680,42.440,0.213)"
#bbox="bbox(the_geom,15.31278996684614846,-64.54487895100361072,17.69407667042260712,-57.21990220260368432)" #GLP
#filter="code_insee_de_la_region = '24'"

#layer="BDTOPO_V3:troncon_de_route"
#bbox="bbox(geometrie,15.31278996684614846,-64.54487895100361072,17.69407667042260712,-57.21990220260368432)"
#filter="code_insee_de_la_region = '24'"
#url="https://wxs.ign.fr/essentiels/geoportail/wfs"


pas="1000"
username='cerema@IGN'
password='20!cerema!21'

output_name=$(echo ${layer} | sed 's!:!-!g')
output_file="${output_name}.geojson"

rm -rf /tmp/${output_name}*

echo "Détermination du nombre d'entités à télécharger..."
#nb_entites=$(curl -s -G -u $username:$password "https://wxs.ign.fr/qn6ksdvxtfbt53g0c55dvvyy/geoportail/wfs?service=WFS&request=GetFeature&version=2.0.0&typename=${layer}&typenames=${layer}&count=1"\
#    --data-urlencode "cql_filter=${bbox} and ${filter}" | grep -o 'numberMatched="[^"][0-9]\+"' | cut -d'"' -f2)

nb_entites=$(curl -s -G $url"?service=WFS&request=GetFeature&version=2.0.0&typename=${layer}&typenames=${layer}&count=1"\
    --data-urlencode "cql_filter=${bbox}" | grep -o 'numberMatched="[^"][0-9]\+"' | cut -d'"' -f2)

if ! [[ $nb_entites =~ ^[0-9]+$ ]]
then
    echo "nb_entites vaut [$nb_entites] ce qui n'est pas un nombre. Arrêt du programme..."
    exit
fi

echo "Téléchargement de $nb_entites objets pour ${layer} par WFS..."
for i in $(seq 0 $pas $nb_entites)
do
    echo "Récupération des objets $(($i+1)) à $(($i+$pas))"
    curl -s -G -u $username:$password $url"?service=WFS&request=GetFeature&version=2.0.0"\
        --data-urlencode "SRSNAME=EPSG:2154" \
        --data-urlencode "outputformat=shape-zip" \
	--data-urlencode "TYPENAME=${layer}" \
	--data-urlencode "TYPENAMES=${layer}" \
	--data-urlencode "STARTINDEX=${i}" \
	--data-urlencode "COUNT=${pas}" \
	--data-urlencode "cql_filter=${bbox}" \
    > /tmp/${output_name}_$i.zip
    unzip -qq /tmp/${output_name}_$i.zip -d /tmp/${output_name}_$i
done

echo "Fusion des données téléchargées..."
creation_shp=false
for shp in $(find /tmp/${output_name}* -name '*.shp')
do
    if [ ! $creation_shp ]
    then
	creation_shp=true
        ogr2ogr -overwrite -f "ESRI Shapefile" /tmp/$output_name.shp $shp
    else
        ogr2ogr -f "ESRI Shapefile" -update -append /tmp/$output_name.shp $shp
    fi
done
ogr2ogr -f "GeoJSON" $output_file /tmp/$output_name.shp

echo "Génération de $output_file terminé"
