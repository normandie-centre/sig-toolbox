import requests
import json
import pprint
import pandas as pd
import os
import zipfile
import io

# Spécifier l'URL du fichier JSON à télécharger
url = "https://transport.data.gouv.fr/api/datasets"

# Faire la requête HTTP pour télécharger le fichier
reponse = requests.get(url)

donnees_json = reponse.json()

# On ne garde que ceux qui ont un aom
donnees_json = [entite for entite in donnees_json if entite['aom']['name'] is not None & if entite['resources']['title'] is not None]

resultats = []
for entite in donnees_json:
    lignes_gtfs = [(ligne['datagouv_id'],ligne['title'], ligne['url']) for ligne in entite['resources'] if 'format' in ligne and ligne['format'] == 'GTFS']
    resultats.extend(lignes_gtfs)

df = pd.DataFrame(resultats, columns=['id','Titre', 'URL'])
pprint.pprint(df)

def telecharger_zip(url):
    response = requests.get(url)
    if response.status_code == 200:
        return response.content
    else:
        print(f"Échec du téléchargement de {url}, ou alors ce n'est pas un zip")
        return None

for index, row in df.iterrows():
    titre = row['title']
    url = row['URL']
    contenu_zip = telecharger_zip(url)
    if contenu_zip:
        try:
            dossier_destination = f"./Desktop/depot/sig-toolbox/sig-toolbox/R/traitement sncf gtfs/gtfs_transports_data_gouv_v2/{titre}/"
            if not os.path.exists(dossier_destination):
                os.makedirs(dossier_destination)
            nom_fichier_zip = os.path.join(dossier_destination, 'archive.zip')
            with open(nom_fichier_zip, 'wb') as f:
                f.write(contenu_zip)
            with zipfile.ZipFile(nom_fichier_zip, 'r') as zip_ref:
                zip_ref.extractall(dossier_destination)
            os.remove(nom_fichier_zip)
            print(f"Fichier ZIP extrait avec succès dans {dossier_destination}.")
        except:
            pass
