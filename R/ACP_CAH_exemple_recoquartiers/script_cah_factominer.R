library(FactoMineR)
#http://factominer.free.fr/factomethods/classification-hierarchique-sur-composantes-principales.html
library(factoextra)
library('dplyr')
library(wordcloud)
library(ggplot2)

#------------------------------------------------------------
# LECTURE DU CSV ET QQS STATS
#------------------------------------------------------------
#modifier le répertoire par défaut wd = working directory
#setwd("F:/z-formation_R/CAH")
WD <- getwd()
if (!is.null(WD)) setwd(WD)

#charger les données avec un header et la première colonne comme nom d'observation
donnees <- read.csv2("indicateurs_v7_1er_mars.csv", header = TRUE, row.names=1, sep = ",", dec = ".", fileEncoding = "latin1", na.strings = c("N/A", "NULL", "missing"))
rownames(donnees) <- as.character(rownames(donnees))


#afficher les 6 premières lignes 
print(head(donnees)) 

#stat. descriptives 
print(summary(donnees))

#graphique -croisement deux à deux 
#pairs(donnees)

# ACP avec X axes
# !!!!!!!!!!! Bien choisir combien on veut d'axes nous donne l'info retenu !
resultats_acp <- PCA (X = donnees, ncp= 10, graph = FALSE)
str(resultats_acp)
summary(resultats_acp)
plot (resultats_acp)
dimdesc(resultats_acp, axes = 1:2)

# variable et individus dans les premiers et deuxièmes plans
plot.PCA (resultats_acp, choix = "var", col.var = "blue")
plot.PCA (resultats_acp, axes = c(3,4), choix = "var", col.var = "blue")

plot.PCA (resultats_acp, choix = "ind", col.ind = "black")
plot.PCA (resultats_acp, axes = c(3,4), choix = "ind", col.ind  = "blue")

# variable + individu
fviz_pca_biplot(resultats_acp, col.ind = "lightblue", col.var = "blue")
fviz_pca_biplot(resultats_acp, axes = c(3,4), col.ind = "lightblue", col.var = "blue")

# Nombre total de groupes
nombre_groupes <- 8

resultats_cah <- HCPC (resultats_acp, nb.clust = nombre_groupes, graph=T)
str(resultats_cah)

# intertie
barplot(resultats_cah$call$t$inert.gain[1:20],horiz = T,main="Gain d'inertie intra sur les 20 dernières agrégations ",
        col="lightblue",border = "grey")

# Pour une représentation des individus dans l'espace factoriel
plot(resultats_cah)
plot(resultats_cah, choice ="tree")
plot(resultats_cah,axes=1:2)
plot(resultats_cah,axes=3:4)
tableau_fid_clust <- resultats_cah$data.clust%>% 
  select(clust) %>% 
  arrange(clust)

# récupérer le résultat
write.csv(tableau_fid_clust, file = "groupes_cah.csv", row.names = TRUE)

# Description des classes
# nb individu par classe
table (resultats_cah$data.clust$clust)

resultats_cah$desc.axes


# description par les parangons : Les individus les plus proches du centre de classe
resultats_cah$desc.ind$para
#Descriptiondesclassesparlesindividus : Les individus les plus éloignés des centres des autres classes
resultats_cah$desc.ind$dist


# description par les variables avec un vtest :
resultats_cah$desc.var
# accéder à un groupe :
# groupe_1 <- resultats_cah$desc.var[[2]]$`1`
# groupe_2 <- resultats_cah$desc.var[[2]]$`2`
# groupe_3 <- resultats_cah$desc.var[[2]]$`3`
# groupe_4 <- resultats_cah$desc.var[[2]]$`4`
# groupe_5 <- resultats_cah$desc.var[[2]]$`5`
# groupe_6 <- resultats_cah$desc.var[[2]]$`6`
# groupe_7 <- resultats_cah$desc.var[[2]]$`7`
# groupe_8 <- resultats_cah$desc.var[[2]]$`8`


# Initialisation de la liste pour stocker les groupes
groupes <- list()

# Boucle pour créer les variables de groupe
for (i in 1:nombre_groupes) {
  nom_variable <- paste0("groupe_", i)
  groupes[[nom_variable]] <- resultats_cah$desc.var[[2]][[as.character(i)]]
}

#write.csv(test, file = "test", row.names = TRUE)

# # NUAGE DE MOTS POUR LES INDICATEURS
# Fixer la graine aléatoire pour la reproductibilité
set.seed(10)

# Boucle sur les groupes de 1 à 8
for (i in 1:nombre_groupes) {
  # Données du groupe i
  nom_groupe <- paste0("groupe_", i)
  donnees_nuage <- data.frame(
    indicateur = rownames(groupes[[nom_groupe]]),
    v_test = round(groupes[[nom_groupe]][,"v.test"])
  )
  
  # Ajouter une colonne pour la couleur en fonction de la valeur de v_test
  donnees_nuage$color <- ifelse(donnees_nuage$v_test > 0, "black", "red")
  
  # Sauvegarder en format PNG
  nom_fichier <- paste0("nuage_de_mots_groupe_", i, ".png")
  png(file = nom_fichier, width = 1200, height = 800, units = "px", res = 100)
  
  # Créer le nuage de mots
  wordcloud(
    words = donnees_nuage$indicateur, 
    freq = abs(donnees_nuage$v_test), 
    max.words = 20,
    min.freq = 1, 
    scale = c(2.5, 0.1), 
    ordered.colors = 1,
    colors = donnees_nuage$color, 
    rot.per = 0
  )
  
  # Ajouter le titre
  #title(main = paste("Groupe", i))
  
  dev.off()  # Fermer le périphérique graphique et sauvegarder l'image
}

##########################################################################
# CALCUL DES MOYENNES DE MOYENNE
##########################################################################
tableau_fid_clust$fid <- rownames(tableau_fid_clust)
donnees$fid <- rownames(donnees)
donnees_and_clust <- merge(tableau_fid_clust, donnees, by = "fid", all = TRUE)
donnees_and_clust <- subset(donnees_and_clust, select = -fid)

stats_selon_typo <- donnees_and_clust %>%
  group_by(clust) %>%
  summarise_all(list(mean = mean), na.rm = TRUE)

nb_logts_loc_com_region <- donnees_and_clust %>%
  summarise(count = n(), sum(nb_batiment_reco), sum(somme_nlogh_reco), sum(somme_nloccom_reco) )

nb_logts_loc_com_selon_typo <- donnees_and_clust %>%
  group_by(clust) %>%
  summarise(count = n(), sum(nb_batiment_reco), sum(somme_nlogh_reco), sum(somme_nloccom_reco) )

# ggplot(donnees_and_clust, aes(x = as.factor(clust), y = part_logh_rs, fill = as.factor(clust))) +
#   geom_violin(width = 5) +
#   geom_boxplot(width = 0.1, fill = "white", color = "black", alpha = 0.5) +
#   scale_fill_manual(values = c("black", "#e31a1c", "#1f78b4", "#b2df8a",  "#a6cee3", "#fb9a99","orange", "grey")) +
#   labs(title = "Violin Plot de part_logh_rs selon clust",
#        x = "Clust",
#        y = "part_logh_rs") +
#   theme_minimal()

# Violin plot
ggplot(donnees_and_clust, aes(x = as.factor(clust), y = part_logh_rs, fill = as.factor(clust))) +
  geom_violin(width = 3) +
  geom_boxplot(width = 0.1, fill = "white", color = "black", alpha = 0.5) +
  scale_fill_manual(values = c("black", "#e31a1c", "#1f78b4", "#b2df8a",  "#a6cee3", "#fb9a99","orange", "grey")) +
  labs(title = "Violin Plot de part_logh_rs selon clust",
       x = "Clust",
       y = "part_logh_rs") +
  theme_minimal()

# Version automatisées
# Liste des colonnes à exclure (si nécessaire)
colonnes_a_exclure <- c("clust")  # Ajoutez d'autres noms de colonnes si nécessaire

# Récupérer la liste des colonnes dans le tibble
colonnes <- setdiff(names(donnees_and_clust), colonnes_a_exclure)

# Parcourir chaque colonne et créer un violin plot
dir.create("img", showWarnings = FALSE)

#for (colonne in colonnes) {
for (i in seq_along(colonnes)) {
  colonne <- colonnes[i]
  plot <- ggplot(donnees_and_clust, aes(x = as.factor(clust), y = .data[[colonne]], fill = as.factor(clust))) +
    geom_violin() +
    geom_boxplot(width = 0.1, color = "black") +
    scale_fill_manual(values = c("#616161", "#e31a1c", "#1f78b4", "#b2df8a",  "#a6cee3", "#fb9a99", "orange", "#EEEEEE")) +
    labs(title = paste("Violin Plot de", colonne, "selon clust"),
         x = "Clust",
         y = colonne) +
    theme_minimal()
  # Enregistrer le graphique dans le répertoire "img" avec le nom de la colonne
  ggsave(paste0("img/", i, "_", colonne, "_plot.png"), plot, width = 8, height = 6)
  
  # Afficher ou enregistrer le graphe selon vos besoins
  print(paste("Graph pour", colonne))
}
