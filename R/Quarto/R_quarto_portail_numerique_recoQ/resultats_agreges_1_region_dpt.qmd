---
title: "Résultats agrégés aux échelles régionales et départementales"
image: "images/illustration/miniature_dpt.png"
description: "Synthèse et chiffres clefs pour la Normandie et ses 5 départements."
date: 2024-09-04
lang: fr
lightbox: true
---

::: {.callout-tip collapse="true"}
## Vous souhaitez télécharger toutes les fiches de l'étude ?

<a href="pdf/cerema_fiches_recoquartiers.zip" download><i class="bi bi-file-earmark-zip"></i> Télécharger toutes les fiches dans un fichier .zip</a>
:::

## Synthèse régionale
![](pdf/fiches_png/4-1_Typo_Region_page_1.png){fig-align="center"}

## Synthèses départementales

::::: panel-tabset
### Calvados
![](pdf/fiches_png/Typo_Departement_Calvados_page_1.png){fig-align="center"}

### Eure
![](pdf/fiches_png/Typo_Departement_Eure_page_1.png){fig-align="center"}

### Manche
![](pdf/fiches_png/Typo_Departement_Manche_page_1.png){fig-align="center"}

### Orne
![](pdf/fiches_png/Typo_Departement_OrneV2_page_1.png){fig-align="center"}

### Seine-maritime
![](pdf/fiches_png/Typo_Departement_Seine-MaritimeV2_page_1.png){fig-align="center"}


:::::