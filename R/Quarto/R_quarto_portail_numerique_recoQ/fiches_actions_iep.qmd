---
title: "Intervention sur l'espace public"
image: "images/illustration/exemple_fiche_action.png"
date: 2024-09-08
lang: fr
lightbox: true
---

::: {.callout-tip collapse="true"}
## Vous souhaitez télécharger toutes les fiches de l'étude ?

<a href="pdf/cerema_fiches_recoquartiers.zip" download><i class="bi bi-file-earmark-zip"></i> Télécharger toutes les fiches dans un fichier .zip</a>
:::

# Fiche chapeau

![](images/IEP - Fiche chapeau_page_1.png)

![](images/IEP - Fiche chapeau_page_2.png)

# Imperméabilisation et résilience climatique

## IEP 1.1 - Végétaliser pour lutter contre la surchauffe urbaine

![](images/IEP%201.1%20-%20VÉGÉTALISER%20POUR%20LUTTER_page_1.png)

![](images/IEP%201.1%20-%20VÉGÉTALISER%20POUR%20LUTTER_page_2.png)

![](images/IEP%201.1%20-%20VÉGÉTALISER%20POUR%20LUTTER_page_3.png)

## IEP 1.2 - Gérer l'eau sur l'espace public

![](images/IEP%201.2%20-%20GÉRER%20L’EAU%20SUR%20L’ESPACE%20PUBLIC_page_1.png)

![](images/IEP%201.2%20-%20GÉRER%20L’EAU%20SUR%20L’ESPACE%20PUBLIC_page_2.png)

# Place de la voiture, accessibilité et confort du piéton

## IEP 2.1 - La place de la voiture sur l'espace public

![](images/IEP%202.1%20-%20LA%20PLACE%20DE%20LA%20VOITURE%20SUR%20L’ESPACE%20PUBLIC_page_1.png)

![](images/IEP%202.1%20-%20LA%20PLACE%20DE%20LA%20VOITURE%20SUR%20L’ESPACE%20PUBLIC_page_2.png)

![](images/IEP%202.1%20-%20LA%20PLACE%20DE%20LA%20VOITURE%20SUR%20L’ESPACE%20PUBLIC_page_3.png)

## IEP 2.2 - Favoriser les modes actifs et l'accessibilité

![](images/IEP%202.2%20-%20FAVORISER%20LES%20MODES%20ACTIFS%20ET%20L’ACCESSIBILITÉ_page_1.png)

![](images/IEP%202.2%20-%20FAVORISER%20LES%20MODES%20ACTIFS%20ET%20L’ACCESSIBILITÉ_page_2.png)

# Confort d'usage des espaces publics et attractivité

## IEP 3.1 - Espace public, lien social et confort d'usage

![](images/IEP%203.1%20-%20ESPACE%20PUBLIC,%20LIEN%20SOCIAL%20ET%20CONFORT%20D’USAGE_page_1.png)

![](images/IEP%203.1%20-%20ESPACE%20PUBLIC,%20LIEN%20SOCIAL%20ET%20CONFORT%20D’USAGE_page_2.png)

## IEP 3.2 - Mettre en valeur le patrimoine et le paysage

![](images/IEP%203.2%20-%20METTRE%20EN%20VALEUR%20LE%20PATRIMOINE%20ET%20LE%20PAYSAGE_page_1.png)

![](images/IEP%203.2%20-%20METTRE%20EN%20VALEUR%20LE%20PATRIMOINE%20ET%20LE%20PAYSAGE_page_2.png)

## IEP 3.3 - Commerces et revitalisation urbaine

![](images/IEP%203.3%20-%20COMMERCES%20ET%20REVITALISATION%20URBAINE_page_1.png)

![](images/IEP%203.3%20-%20COMMERCES%20ET%20REVITALISATION%20URBAINE_page_2.png)
