---
title: "Méthodologie"
date: 2024-09-10
lang: fr
#lightbox: true
---

> Les principaux éléments de méthodologie pour identifier et qualifier les quartiers

## Identifier les quartiers de la reconstruction

Afin d’identifier les quartiers de la reconstruction, les sources disponibles à une échelle infra-communale ont été mobilisées. L’objectif est de distinguer des ensembles de bâtis collectifs de la période 1948-1958 suffisamment proches pour être qualifiés de quartier, ainsi la méthodologie décrite ci-après vise d’une part à mettre de côté le bâti isolé et d’autre part à élargir les ensembles de bâtis pour dégager des zones propices à des analyses transversales : des blocs ou quartiers. L’objectif est d’inviter à une réflexion ne se limitant pas à l’aspect bâtimentaire en intégrant les problématiques liées aux populations, à l’habitat, au cadre de vie et à l’attractivité d’un quartier.

![](images/schema_methodo_01_q_tache_bati.svg){fig-align="center"}

Le schéma ci-dessous présente les principales étapes de la méthodologie. Les principales sources mobilisées sont les fichiers fonciers (CEREMA), la base de données nationale des bâtiments (CSTB) et la BD TOPO (IGN). Ces données sont disponibles sur l’ensemble du territoire d’étude.

![](images/illustration/illustration_060_schema_methodo_identifier_recoq.png){fig-align="center" width="700px"}

## Proposition d'une typologie de quartier : une grille de lecture macro

Une quarantaine d’indicateurs ont été retenus pour qualifier ces quartiers selon 4 thématiques :

-   Bâtiment
-   Habitat
-   Socio-économie
-   Nature en ville et cadre de vie

Plusieurs sources de données ont été croisées. Certaines n’étant pas directement disponibles à l’échelle du bâtiment ou du quartier, des approximations ont été faites.

Afin de dégager une grille de lecture pour la compréhension de ces quartiers, il a été fait appel aux outils statistiques classiques de classification en groupe.

![](images/illustration/illustration_061_schema_methodo_acp_cah.png){fig-align="center" width="700px"}

::: {.callout-note appearance="minimal" collapse="true"}
### En savoir plus sur les outils statistiques...

[Analyse en Composante Principale (ACP)](https://fr.wikipedia.org/wiki/Analyse_en_composantes_principales)

[Classification Ascendante Hiéarchique](https://fr.wikipedia.org/wiki/Regroupement_hi%C3%A9rarchique)

::: columns
::: {.column width="45%"}
![](images/clipboard-1385993017.png)
:::

::: {.column width="10%"}
:::

::: {.column width="45%"}
![](images/clipboard-4232551297.png)
:::
:::
:::

## Les indicateurs

Le dictionnaire des indicateurs présenté ci-dessous détaille :

-   Le nom technique de l’indicateur dans le tableau excel
-   Le libellé de l’indicateur
-   La thématique
-   La source
-   Le millésime
-   L’utilisation de l’indicateur dans la classification statistique (CAH)

```{r}
#| echo: FALSE
#| warning: FALSE
library(dplyr)
library(DT)
library(arrow)
df_dico <- open_dataset("data/dico_des_indicateurs.parquet") %>% 
   collect()

datatable(df_dico, extensions = 'Buttons', filter = 'top',
              options = list(
                dom = 'Bfrtip',
                buttons = list('copy', 'csv', 'excel','print'),
                lengthMenu = list(c(10, 25, 50, -1), c('10', '25', '50', 'All')),
                pageLength = 8  # Affiche tous les enregistrements par défaut
              )
    )%>%
  DT::formatStyle(columns = c(1, 2, 3, 4, 5, 6, 7), fontSize = '14px')
```

## Limites et pistes d'améliorations

La méthodologie d’identification et de qualification des quartiers de la Reconstruction de Normandie est une approche infracommunale reproductible. Elle prend appui sur des hypothèses qui peuvent être adaptées sur d’autres territoires, d’autres périodes de construction ou d’autres problématiques.

Les résultats de la typologie de quartiers permettent une lecture éclairée du rôle et des problématiques au sein du maillage du territoire. Plusieurs limites sont à noter :

-   Certains quartiers sont bien au centre de leur groupe, d’autres sont proches des « frontières » entre différentes typologies et relèvent des problématiques de plusieurs groupes.
-   Chaque quartier a ses spécificités propres : le travail de typologie est par définition caricatural de la situation des quartiers, qui ne peuvent pas s’affranchir d’un travail de diagnostic personnalisé.  
-   Les thématiques retenues ne couvrent pas l’ensemble des problématiques que peuvent rencontrer un quartier.
-   Certains indicateurs peuvent être imprécis ou erronés. Notamment, la difficulté d’identifier les logements sociaux à l’échelle des bâtiments a pu amener une distinction entre les groupes 1 et 2 imparfaites (des logements du groupe 2 sont peut être des logements sociaux) : des reclassements manuels ou une fusion de ces groupes pourraient être envisagées si une confrontation avec les acteurs du territoires permet d’affiner les données.
-   Les espaces verts ont été reconstitués par le croisement des données recensées sur la BDTopo et OpenStreetMap. Ces bases de données sont très incomplètes et n’incluent pas certains éléments, par exemple les fronts de mer. De plus, les espaces engazonnés privés ne sont pas intégrés.

D'autres pistes d’améliorations sont identifiées comme affiner le travail sur la morphologie urbaine, intégrer des données prédictives sur les DPE, rajouter manuellement les parkings à ciel ouvert ou les données des bâtiments publics... Mais in fine, dans la masse d’indicateurs retenus (près d’une cinquantaine), les résultats sont robustes et permettent de cibler les actions à mener.

Enfin, une confrontation avec la connaissance fine des acteurs locaux (collectivités, bailleurs sociaux, professionnels du bâtiment, syndic etc..) doit permettre d’affiner les analyses pour des territoires qui souhaiteraient un accompagnement pour intervenir sur ces quartiers et ce bâti.

## Annexes

### Environnements et outils

La production et la mise en forme des résultats de travail partenarial a été faite avec les outils suivants :

-   [Postgres](https://www.postgresql.org/)
-   [QGIS](https://www.qgis.org/)
-   [R](https://www.r-project.org/) et [R Studio](https://posit.co/download/rstudio-desktop/)
-   [Quarto](https://quarto.org/)
-   [Observablehq](https://observablehq.com/)
-   [Maplibre](https://maplibre.org/)
-   [PM Tiles](https://docs.protomaps.com/pmtiles/)
-   [Ogr2ogr](https://gdal.org/programs/ogr2ogr.html)
-   [Visual Studio Code](https://code.visualstudio.com/)
