@echo off

REM Changer de répertoire vers le dossier de destination
cd "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers"

REM Exécuter les commandes pmtiles pour convertir les fichiers mbtiles en pmtiles
pmtiles convert batiment_bdnb_48_58_corrige_pour_map.mbtiles batiment_bdnb_48_58_corrige_pour_map.pmtiles
pmtiles convert batiment_bdnb_recoq_hors48_58_hors_parcelle_48_58.mbtiles bat_hors48_58.pmtiles
pmtiles convert communes.mbtiles communes.pmtiles
pmtiles convert parcelle_48_58.mbtiles parcelle_48_58.pmtiles
pmtiles convert quartiers.mbtiles quartiers.pmtiles


REM pmtiles convert batiment_bdnb_recoq_hors48_58_sur_parcelle_48_58.mbtiles bat_parcelle_48_58.pmtiles
REM pmtiles convert batiment_48_58.mbtiles bat_48_58.pmtiles

@echo on