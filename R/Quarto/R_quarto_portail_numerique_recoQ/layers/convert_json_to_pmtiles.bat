@echo off
REM Changer de répertoire vers le dossier de QGIS
cd "C:\Program Files\QGIS 3.28\bin"

REM Exécuter les commandes ogr2ogr pour convertir les fichiers geojson en mbtiles
ogr2ogr -f MBTILES "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers\communes.mbtiles" "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers\communes.geojson" -dsco MAXZOOM=18
ogr2ogr -f MBTILES "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers\batiment_48_58.mbtiles" "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers\batiment_48_58.geojson" -dsco MAXZOOM=18
ogr2ogr -f MBTILES "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers\batiment_bdnb_48_58_corrige_pour_map.mbtiles" "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers\batiment_bdnb_48_58_corrige_pour_map.geojson" -dsco MAXZOOM=18
ogr2ogr -f MBTILES "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers\batiment_bdnb_recoq_hors48_58_sur_parcelle_48_58.mbtiles" "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers\batiment_bdnb_recoq_hors48_58_sur_parcelle_48_58.geojson" -dsco MAXZOOM=18
ogr2ogr -f MBTILES "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers\batiment_bdnb_recoq_hors48_58_hors_parcelle_48_58.mbtiles" "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers\batiment_bdnb_recoq_hors48_58_hors_parcelle_48_58.geojson" -dsco MAXZOOM=18
ogr2ogr -f MBTILES "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers\quartiers.mbtiles" "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers\quartiers.geojson" -dsco MAXZOOM=18
ogr2ogr -f MBTILES "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers\parcelle_48_58.mbtiles" "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers\parcelle_48_58.geojson" -dsco MAXZOOM=18



REM Changer de répertoire vers le dossier de destination
cd "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers"

REM Exécuter les commandes pmtiles pour convertir les fichiers mbtiles en pmtiles
pmtiles convert communes.mbtiles communes.pmtiles
pmtiles convert batiment_48_58.mbtiles bat_48_58.pmtiles
pmtiles convert batiment_bdnb_48_58_corrige_pour_map.mbtiles batiment_bdnb_48_58_corrige_pour_map.pmtiles
pmtiles convert batiment_bdnb_recoq_hors48_58_sur_parcelle_48_58.mbtiles bat_parcelle_48_58.pmtiles
pmtiles convert batiment_bdnb_recoq_hors48_58_hors_parcelle_48_58.mbtiles bat_hors48_58.pmtiles
pmtiles convert quartiers.mbtiles quartiers.pmtiles
pmtiles convert parcelle_48_58.mbtiles parcelle_48_58.pmtiles

@echo on