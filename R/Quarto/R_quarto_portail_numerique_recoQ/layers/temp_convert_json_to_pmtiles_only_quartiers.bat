@echo off
REM Changer de répertoire vers le dossier de QGIS
cd "C:\Program Files\QGIS 3.28\bin"

REM Exécuter les commandes ogr2ogr pour convertir les fichiers geojson en mbtiles
ogr2ogr -f MBTILES "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers\quartiers.mbtiles" "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers\quartiers.geojson" -dsco MAXZOOM=18


REM Changer de répertoire vers le dossier de destination
cd "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_portail_numerique_recoQ\layers"

REM Exécuter les commandes pmtiles pour convertir les fichiers mbtiles en pmtiles
pmtiles convert quartiers.mbtiles quartiers.pmtiles

@echo on

pause