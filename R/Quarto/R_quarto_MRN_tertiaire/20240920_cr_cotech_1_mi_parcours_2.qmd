---
title: "COTECH phase 1 et réunion à mi-parcours phase 2"
lang: fr  
---

```{=html}
<!-- pour exporter au format word
à copier coller dans le terminal avec ctrl + shift + v
quarto render 20240920_cr_cotech_1_mi_parcours_2.qmd --to docx --reference-doc custom-reference-doc.docx

dans la console :
system("pandoc _site/20240920_cr_cotech_1_mi_parcours_2.qmd.docx -o _site/20240704_cr_reunion_lancement.pdf")
-->
```
le 19 septembre 2024

> **Présents :**
>
> -   Kevin Hibert, ALTERN (responsable pôle tertiaire et pôle ENR)
> -   Helene Lebourg-koulibaly, MRN
> -   Cedric Delahais, MRN
> -   Carine Coulet, MRN
> -   Charlotte Célestin, MRN
> -   Dimitri Guignon, MRN
> -   Aude Peyralbes, MRN
> -   Quentin Legros, CEREMA
> -   Noelie Carretero, CEREMA
> -   Guillaume Chrétien, CEREMA

# Introduction

la MRN rappelle les enjeux de l'étude et les attendus de la mission d'accompagnement du CEREMA qui s'inscrit dans la révision du SCOT AEC avec des liens à créer avec le [service public énergie métropole](https://energies.metropole-rouen-normandie.fr/qui-sommes-nous/) et les actions d'ALTERN. [En savoir plus en vidéo !](https://www.youtube.com/watch?v=0P0QVSq76AM)

## Suivi de la mission

Le planning prévisionnel présenté lors de la réunion de lancement est tenu. La phase 3 (portail numérique) est également bien avancé, elle sert de support de présentation.

La réunion se déroule en deux temps : **COTECH de la phase 1** et **réunion à mi-parcours de la phase 2**.

# COTECH phase 1

Le CEREMA présente la méthodologie déployée pour identifier les usages et surfaces du parc tertiaire ainsi que les sources mobilisées (principalement fichiers fonciers, BD TOPO, données internes de la métropole, base permanente des équipements de l'INSEE).

La méthodologie s'appuie sur le travail du réseau CEREMA notamment la cartographie [ENREZO](https://www.cerema.fr/fr/actualites/enrezo-cartographie-ligne-identifier-potentiel-developpement).

## Usages et surfaces

Le CEREMA présente deux points en détails :

-   les résultats au bâtiment sous forme de carte interactive
-   des analyses à l'échelle de la métropole

Deux mailles sont retenues :

-   le parcellaire unifié avec des bâtiments groupés
-   les locaux fiscaux

Les chiffres clefs, tableaux et graphes permettent aux agents de la métropole d'affiner leur connaissance macro de ce parc, des débats sur l'utilisation de ces résultats sont engagés et des idées d'actions sont esquissées.

Le cas particulier du centre commercial de St Sever est passé en revue pour illustrer la complexité de l'exercice et un exemple d'un bâtiment en copropriété mixte tertiaire résidentiel est détaillée pour montrer à la fois le niveau de détail et les limites des informations recueillies.

Des questions sont posées sur les bâtiments dont les usages ne sont pas identifiés. Le CEREMA, selon le temps disponible, essaiera d'analyser plus finement ces cas.

La Métropole serait intéressée pour une analyse par type de MOA (CD/CR, commune,privé) sur les surfaces et nombre de locaux, ainsi que locataire vs propriétaire.

La métropole serait intéressée pour avoir un focus tertiaire sur les monopropriétés, similaire au focus tertiaire sur les copropriétés qui a été présenté.

La métropole demande une estimation chiffrée du coût de mise à jour du portail cartographique lors des mises à jour des bases de données.

## Données à transmettre par la métropole

> **La MRN transférera les données disponibles de l'observatoire des bureaux et des activités.**

> **La MRN transférera le SIG des zones d'activités et commerciales pour améliorer l'analyse par parc**

> **La MRN transférera le SIG des réseaux de chaleur**

Certaines étant en opendata, le CEREMA essaiera de les retrouver et recontactera la métropole au besoin.

## Validation de la phase 1

Les réactions sur les résultats et le portail numérique répondent au cahier des charges et aux attentes des services. La métropole remercie le CEREMA d'avoir rajouté des analyses pour essayer de dégager du sens de cet ensemble très conséquent de données.

Le CEREMA n'a pas souhaité diffuser les résultats car des améliorations méthodologiques sont encore prévues et les chiffres pourraient évoluer de manière significative.

# Réunion à mi-parcours phase 2

Les différentes sources mobilisables pour estimer les consommations énergétiques sont présentées. Au vu des imbrications bâtiments / points de livraisons et de la difficulté à récupérer dans le temps de l'étude des données réelles des fournisseurs, la métropole valide le principe d'appliquer des ratios prenant appui sur les données disponibles :

-   CEREN
-   DPE
-   PROFEEL
-   BDNB
-   OPERAT
-   OID (observatoire de l'immobilier durable)

L'objectif premier de la mission est de traduire les surfaces en kWhef/m².an tout poste de consommation confondus.

Plusieurs approches sont possibles, les participants valident le principe de ratios modulés selon les caractéristiques suivantes issues de la phase 1 :

-   Type de secteur
-   Classe de surface
-   Période de construction

Le CEREMA s'attachera à fournir des "fourchettes" au niveau du bâtiment. Ce niveau de détail n'aura que peu de sens (estimation sommaire) au niveau de chaque de bâtiment mais permettra des agrégations par secteur, commune, type de propriétaires etc...

Il est à noter qu'en première approche le calcul simple avec seulement les ratio ceren par secteur multiplié par les surfaces régionales correspond exactement aux données des fournisseurs d'énergie en open data avec une hypothèse sur la part du fioul à 5%. Les recherches biblio sur la mixité énergétique tendent à minimiser la part du fioul par rapport aux précédentes données du PCAET (15%).

La métropole note dans ses pistes d'actions pour les années à venir une meilleure exploitation des données des fournisseurs d'énergie.

La métropole aimerait avoir des éléments de présentation (schéma de la méthodo, captures d’écran du portail etc…) pour présenter l’étude lors d’un atelier le 15 octobre.

------------------------------------------------------------------------

# Conclusion

Le prochain COTECH sera la présentation de l'ensemble des résultats.

Il se tiendra le vendredi 15 novembre de 9h30 à 10h15 puis sera prolongé par une séance de 10h15 à 11h pour prendre en compte les demandes d'ajustements de la métropole sur le portail numérique.

Le CEREMA enverra les éléments 15 jours à l'avance et présentera une ou plusieurs solutions d'hébergement de la plateforme, l'environnement de développement étant 100% logiciel libre.
