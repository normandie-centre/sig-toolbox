---
title: "Carte interactive"
---


::: callout-important
## Work in progress  
Le site est en construction, les données sont provisoires et la visualisation n'est pas optimisée !
:::

```{r setup, include=FALSE}
#| echo: false
#| warning: false
#library(tidyr)
library(dplyr)
library(DBI)
library(DT)
library(sf)
library(leaflet)
con <- dbConnect(RPostgres::Postgres(),dbname = 'w_mrn_tertiaire_logt',
                 host = 'postgresql-b56a04df-oc5758a8f.database.cloud.ovh.net',
                 port = 20184,
                 user = 'geomatique',
                 password = 'geomatique',
                 sslmode = 'require')

tups_ter <- st_read(con, query = "select idtup, methode, surface_ter,surface_res, typesecteur_retenu_tup
, geomtup from tertiaire.tups_ter")%>% 
  st_transform(tups_ter, crs = 4326)

# NB : leaflet n'aimerait pas les multipolygones ? ou il comprend une geometrycollection ? à creuser
batiments_groupes_ter <- st_read(con, query = "select (st_dump(geom)).geom from tertiaire.batiments_groupes_ter")%>% 
  st_transform(tups_ter, crs = 4326)

dbDisconnect(con)

```


## Parcelles et bâtiments
```{r}
#| echo: false
#| warning: false
# Créer la carte avec leaflet
leaflet() %>%
      addTiles(urlTemplate = "https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
               attribution = '© OpenStreetMap contributors',
               group = "OSM (default)") %>%
      addTiles("https://wxs.ign.fr/ortho/geoportail/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}",
               options = WMSTileOptions(tileSize = 256, maxZoom = 19),
               group = "Orthos") %>%
      addPolygons(
        data = tups_ter,
        fillColor = "yellow",  # Customize the fill color as needed
        fillOpacity = 0.5,   # Customize the fill opacity as needed
        color = "black",     # Customize the border color as needed
        stroke = TRUE,       # Show border
        weight = 2,
        group = "Parcelles",
        popup = paste0( "IdTUP : ", tups_ter$idtup , "<br>"
                 ,"Méthode : ", tups_ter$methode , "<br>"
                 ,"Surfaces tertiaires : ", tups_ter$surface_ter , "m² <br>"
                 ,"Surfaces résidentielles : ", tups_ter$surface_res , "m² <br>"
                 ,"Secteur retenu : ", tups_ter$typesecteur_retenu_tup , "<br>"
               )
      ) %>%
      addPolygons(
        data = batiments_groupes_ter,
        fillColor = "black",  # Customize the fill color as needed
        fillOpacity = 1,   # Customize the fill opacity as needed
        color = "white",     # Customize the border color as needed
        stroke = TRUE,       # Show border
        weight = 1,
        group = "Batiments",
        options = leafletOptions(interactive = FALSE)
      ) %>%
      addLayersControl(
        baseGroups = c("OSM (default)", "Orthos"),
        overlayGroups = c("Parcelles","Batiments"),
        options = layersControlOptions(collapsed = TRUE)
      ) #%>% 
      #setView(lng = 2.203749, lat = 46.227638, zoom = 5)  # Coordonnées de la France et niveau de zoom
  

```

