---
format: html
---

```{ojs}
//| name: choix du fichier ici
//| echo: false
//myData = FileAttachment("data_nrj.csv").csv({typed: true})
// Exemple 
gwhef_m2_an_elec=970
pertes_elec = gwhef_m2_an_elec * 2.3 - gwhef_m2_an_elec
gwhef_m2_an_gaz=730
gwhef_m2_an_reseaux=150
gwhef_m2_an_fioul_autres=113

// exemple complexe tertiaire
usages_elec=({
  "Chauffage":27.5/100,
  "Eau chaude sanitaire":6.4/100,
  "Refroidissement/climatisation":6.3/100,
  "Spécifique":53.8/100,
  "Cuisson":4/100,
  "Autres usages":2/100
})

usages_gaz=({
  "Chauffage":70.5/100,
  "Eau chaude sanitaire":16.7/100,
  "Cuisson":8.3/100,
  "Autres usages":4.5/100
})

usages_reseaux=({
  "Chauffage":82/100,
  "Eau chaude sanitaire":18/100,
})

usages_fioul_autres=({
  "Chauffage":62/100,
  "Eau chaude sanitaire":12.9/100,
  "Cuisson":5.2/100,
  "Autres usages":19.9/100
})

rendement_elec=({
  "Chauffage":92/100,
  "Eau chaude sanitaire":90/100,
  "Refroidissement/climatisation":100/100,
  "Spécifique":100/100,
  "Cuisson":92/100,
  "Autres usages":100/100
})

rendement_gaz=({
  "Chauffage":76/100,
  "Eau chaude sanitaire":70/100,
  "Cuisson":76/100,
  "Autres usages":100/100
})

rendement_reseaux=({
  "Chauffage":100/100,
  "Eau chaude sanitaire":80/100,
})

rendement_fioul_autres=({
  "Chauffage":64/100,
  "Eau chaude sanitaire":50/100,
  "Cuisson":64/100,
  "Autres usages":100/100
})

myData=[
//---------------------------------------------------------------------------------------------------------------
  {source: "Electricité (ep)",target: "Pertes",value: pertes_elec},
//---------------------------------------------------------------------------------------------------------------
  {source: "Electricité (ep)", target: "Bâtiments (ef)", value: gwhef_m2_an_elec},
//---------------------------------------------------------------------------------------------------------------
  {source: "Bâtiments (ef)", target: "Chauffage (ef)", value: gwhef_m2_an_elec*usages_elec["Chauffage"]},
  {source: "Bâtiments (ef)", target: "Eau chaude sanitaire (ef)", value: gwhef_m2_an_elec*usages_elec["Eau chaude sanitaire"]},
  {source: "Bâtiments (ef)", target: "Refroidissement/Climatisation (ef)", value: gwhef_m2_an_elec*usages_elec["Refroidissement/climatisation"]},
  {source: "Bâtiments (ef)", target: "Spécifique (ef)", value: gwhef_m2_an_elec*usages_elec["Spécifique"]},
  {source: "Bâtiments (ef)", target: "Autres usages (ef)", value: gwhef_m2_an_elec*usages_elec["Autres usages"]},
  {source: "Bâtiments (ef)", target: "Cuisson (ef)", value: gwhef_m2_an_elec*usages_elec["Cuisson"]},
//---------------------------------------------------------------------------------------------------------------
  {source: "Gaz (ep)", target: "Bâtiments (ef)", value: gwhef_m2_an_gaz},
//---------------------------------------------------------------------------------------------------------------
 {source: "Bâtiments (ef)", target: "Chauffage (ef)", value: gwhef_m2_an_gaz*usages_gaz["Chauffage"]},
  {source: "Bâtiments (ef)", target: "Eau chaude sanitaire (ef)", value: gwhef_m2_an_gaz*usages_gaz["Eau chaude sanitaire"]},
  {source: "Bâtiments (ef)", target: "Autres usages (ef)", value: gwhef_m2_an_gaz*usages_gaz["Autres usages"]},
  {source: "Bâtiments (ef)", target: "Cuisson (ef)", value: gwhef_m2_an_gaz*usages_gaz["Cuisson"]},
  
//---------------------------------------------------------------------------------------------------------------
  {source: "Réseaux de chaleur (ep)", target: "Bâtiments (ef)", value: gwhef_m2_an_reseaux},
//---------------------------------------------------------------------------------------------------------------
 {source: "Bâtiments (ef)", target: "Chauffage (ef)", value: gwhef_m2_an_reseaux*usages_reseaux["Chauffage"]},
  {source: "Bâtiments (ef)", target: "Eau chaude sanitaire (ef)", value: gwhef_m2_an_reseaux*usages_reseaux["Eau chaude sanitaire"]},
//---------------------------------------------------------------------------------------------------------------  

//---------------------------------------------------------------------------------------------------------------
  {source: "Fioul et autres (ep)", target: "Bâtiments (ef)", value: gwhef_m2_an_fioul_autres},
//---------------------------------------------------------------------------------------------------------------
 {source: "Bâtiments (ef)", target: "Chauffage (ef)", value: gwhef_m2_an_fioul_autres*usages_fioul_autres["Chauffage"]},
  {source: "Bâtiments (ef)", target: "Eau chaude sanitaire (ef)", value: gwhef_m2_an_fioul_autres*usages_fioul_autres["Eau chaude sanitaire"]},
  {source: "Bâtiments (ef)", target: "Autres usages (ef)", value: gwhef_m2_an_fioul_autres*usages_fioul_autres["Autres usages"]},
  {source: "Bâtiments (ef)", target: "Cuisson (ef)", value: gwhef_m2_an_fioul_autres*usages_fioul_autres["Cuisson"]},
//---------------------------------------------------------------------------------------------------------------  

{source: "Chauffage (ef)", target: "Pertes", value: gwhef_m2_an_elec*usages_elec["Chauffage"]*(1-rendement_elec["Chauffage"])},
{source: "Chauffage (ef)", target: "Chauffage (utile)", value: gwhef_m2_an_elec*usages_elec["Chauffage"]*rendement_elec["Chauffage"]},
  {source: "Chauffage (ef)", target: "Pertes", value: gwhef_m2_an_gaz*usages_gaz["Chauffage"]*(1-rendement_gaz["Chauffage"])},
  {source: "Chauffage (ef)", target: "Chauffage (utile)", value: gwhef_m2_an_gaz*usages_gaz["Chauffage"]*rendement_gaz["Chauffage"]},
{source: "Chauffage (ef)", target: "Pertes", value: gwhef_m2_an_reseaux*usages_reseaux["Chauffage"]*(1-rendement_reseaux["Chauffage"])},
{source: "Chauffage (ef)", target: "Chauffage (utile)", value: gwhef_m2_an_reseaux*usages_reseaux["Chauffage"]*rendement_reseaux["Chauffage"]},
  {source: "Chauffage (ef)", target: "Pertes", value: gwhef_m2_an_fioul_autres*usages_fioul_autres["Chauffage"]*(1-rendement_fioul_autres["Chauffage"])},
  {source: "Chauffage (ef)", target: "Chauffage (utile)", value: gwhef_m2_an_fioul_autres*usages_fioul_autres["Chauffage"]*rendement_fioul_autres["Chauffage"]},
  
{source: "Eau chaude sanitaire (ef)", target: "Pertes", value: gwhef_m2_an_elec*usages_elec["Eau chaude sanitaire"]*(1-rendement_elec["Eau chaude sanitaire"])},
{source: "Eau chaude sanitaire (ef)", target: "Eau chaude sanitaire (utile)", value: gwhef_m2_an_elec*usages_elec["Eau chaude sanitaire"]*rendement_elec["Eau chaude sanitaire"]},
  {source: "Eau chaude sanitaire (ef)", target: "Pertes", value: gwhef_m2_an_gaz*usages_gaz["Eau chaude sanitaire"]*(1-rendement_gaz["Eau chaude sanitaire"])},
  {source: "Eau chaude sanitaire (ef)", target: "Eau chaude sanitaire (utile)", value: gwhef_m2_an_gaz*usages_gaz["Eau chaude sanitaire"]*rendement_gaz["Eau chaude sanitaire"]},
{source: "Eau chaude sanitaire (ef)", target: "Pertes", value: gwhef_m2_an_reseaux*usages_reseaux["Eau chaude sanitaire"]*(1-rendement_reseaux["Eau chaude sanitaire"])},
{source: "Eau chaude sanitaire (ef)", target: "Eau chaude sanitaire (utile)", value: gwhef_m2_an_reseaux*usages_reseaux["Eau chaude sanitaire"]*rendement_reseaux["Eau chaude sanitaire"]},
  {source: "Eau chaude sanitaire (ef)", target: "Pertes", value: gwhef_m2_an_fioul_autres*usages_fioul_autres["Eau chaude sanitaire"]*(1-rendement_fioul_autres["Eau chaude sanitaire"])},
  {source: "Eau chaude sanitaire (ef)", target: "Eau chaude sanitaire (utile)", value: gwhef_m2_an_fioul_autres*usages_fioul_autres["Eau chaude sanitaire"]*rendement_fioul_autres["Eau chaude sanitaire"]},  

  /*{source: "Autres usages (ef)", target: "Pertes", value: gwhef_m2_an_elec*usages_elec["Autres usages"]*(1-rendement_elec["Autres usages"])},*/
  {source: "Autres usages (ef)", target: "Autres usages (utile)", value: gwhef_m2_an_elec*usages_elec["Autres usages"]*rendement_elec["Autres usages"]},
  /*{source: "Autres usages (ef)", target: "Pertes", value: gwhef_m2_an_gaz*usages_gaz["Autres usages"]*(1-rendement_gaz["Autres usages"])},*/
  {source: "Autres usages (ef)", target: "Autres usages (utile)", value: gwhef_m2_an_gaz*usages_gaz["Autres usages"]*rendement_gaz["Autres usages"]},
  /*{source: "Autres usages (ef)", target: "Pertes", value: gwhef_m2_an_fioul_autres*usages_fioul_autres["Autres usages"]*(1-rendement_fioul_autres["Autres usages"])},*/
  {source: "Autres usages (ef)", target: "Autres usages (utile)", value: gwhef_m2_an_fioul_autres*usages_fioul_autres["Autres usages"]*rendement_fioul_autres["Autres usages"]},

  {source: "Cuisson (ef)", target: "Pertes", value: gwhef_m2_an_elec*usages_elec["Cuisson"]*(1-rendement_elec["Cuisson"])},
  {source: "Cuisson (ef)", target: "Cuisson (utile)", value: gwhef_m2_an_elec*usages_elec["Cuisson"]*rendement_elec["Cuisson"]},
  {source: "Cuisson (ef)", target: "Pertes", value: gwhef_m2_an_gaz*usages_gaz["Cuisson"]*(1-rendement_gaz["Cuisson"])},
  {source: "Cuisson (ef)", target: "Cuisson (utile)", value: gwhef_m2_an_gaz*usages_gaz["Cuisson"]*rendement_gaz["Cuisson"]},
  {source: "Cuisson (ef)", target: "Pertes", value: gwhef_m2_an_fioul_autres*usages_fioul_autres["Cuisson"]*(1-rendement_fioul_autres["Cuisson"])},
  {source: "Cuisson (ef)", target: "Cuisson (utile)", value: gwhef_m2_an_fioul_autres*usages_fioul_autres["Cuisson"]*rendement_fioul_autres["Cuisson"]},

  /*{source: "Spécifique (ef)", target: "Pertes", value: gwhef_m2_an_elec*usages_elec["Spécifique"]*(1-rendement_elec["Spécifique"])},*/
  {source: "Spécifique (ef)", target: "Spécifique (utile)", value: gwhef_m2_an_elec*usages_elec["Spécifique"]*rendement_elec["Spécifique"]},
  
  
  /*{source: "Refroidissement/Climatisation (ef)", target: "Pertes", value: gwhef_m2_an_elec*usages_elec["Refroidissement/climatisation"]*(1-rendement_elec["Refroidissement/climatisation"])},*/
  {source: "Refroidissement/Climatisation (ef)", target: "Refroidissement/Climatisation (utile)", value: gwhef_m2_an_elec*usages_elec["Refroidissement/climatisation"]*rendement_elec["Refroidissement/climatisation"]}
  
]



```


```{ojs}
//| echo: false

chart = SankeyChart(
  {
    links: myData
  }, 
  {
    nodeGroup: d => d.id, // un group par ligne
    nodeAlign, // e.g., d3.sankeyJustify; set by input above
    linkColor, // e.g., "source" or "target"; set by input above
    nodeLabel:d=>(`${d.id} : ${ Math.round( (d.value)).toLocaleString('fr-FR')} GWh/an`),
    // largeur des noeuds :
    nodeWidth:10,
    // largeur du graphe
    width:widthChart,
    // hauteur du graphe
    height: heightChart,
    // espace vertical entre les noeuds
    nodePadding:nodePadding
  }
)


// retour à la ligne
html`<br>`

// bouton pour télécharger un csv
DOM.download(() => serialize(chart), undefined, "Save as SVG")
//DOM.download(() => rasterize(chart), undefined, "Save as PNG")
```


```{ojs}
//| name: boîte de dialogue pour changer la mise en forme
//| echo: false
viewof linkColor = Inputs.select(new Map([
  ["static", "#aaa"],
  ["source-target", "source-target"],
  ["source", "source"],
  ["target", "target"],
]), {
  value: new URLSearchParams(html`<a href>`.search).get("color") || "source-target",
  label: "Link color"
})

viewof nodeAlign = Inputs.select(["left", "right", "center", "justify"], {
  value: "justify",
  label: "Node alignment"
})

viewof nodePadding = Inputs.range([0,75], {step:1,value:50, label: "Espace vertical entre noeuds"})

viewof widthChart = Inputs.range([250,1200], {step:50,value:1050, label: "Largeur du graphe"})

viewof heightChart = Inputs.range([250,1200], {step:50,value:600, label: "Hauteur du graphe"})

```


<!-- Partie purement technique, ne pas modifier... -->

```{ojs}
//| name: import des librairies
//| echo: false

//import {SankeyChart} from "@d3/sankey-component"
//https://observablehq.com/@d3/sankey-component

// je prend juste d3sankey et je redéfinis la fonction en dessous
import {d3Sankey} from "@d3/sankey-component"
import {serialize, rasterize} from "@mbostock/saving-svg"

```

```{ojs}
//| name: modification à la marge de la fonction SankeyChart pour changer la taille de la police
//| echo: false
// Copyright 2021-2023 Observable, Inc.
// Released under the ISC license.
// https://observablehq.com/@d3/sankey-diagram
function SankeyChart({
  nodes, // an iterable of node objects (typically [{id}, …]); implied by links if missing
  links // an iterable of link objects (typically [{source, target}, …])
}, {
  format = ",", // a function or format specifier for values in titles
  align = "justify", // convenience shorthand for nodeAlign
  nodeId = d => d.id, // given d in nodes, returns a unique identifier (string)
  nodeGroup, // given d in nodes, returns an (ordinal) value for color
  nodeGroups, // an array of ordinal values representing the node groups
  nodeLabel, // given d in (computed) nodes, text to label the associated rect
  nodeTitle = d => `${d.id}\n${format(d.value)}`, // given d in (computed) nodes, hover text
  nodeAlign = align, // Sankey node alignment strategy: left, right, justify, center
  nodeSort, // comparator function to order nodes
  nodeWidth = 15, // width of node rects
  nodePadding = 10, // vertical separation between adjacent nodes
  nodeLabelPadding = 6, // horizontal separation between node and label
  nodeStroke = "currentColor", // stroke around node rects
  nodeStrokeWidth, // width of stroke around node rects, in pixels
  nodeStrokeOpacity, // opacity of stroke around node rects
  nodeStrokeLinejoin, // line join for stroke around node rects
  linkSource = ({source}) => source, // given d in links, returns a node identifier string
  linkTarget = ({target}) => target, // given d in links, returns a node identifier string
  linkValue = ({value}) => value, // given d in links, returns the quantitative value
  linkPath = d3Sankey.sankeyLinkHorizontal(), // given d in (computed) links, returns the SVG path
  linkTitle = d => `${d.source.id} → ${d.target.id}\n${format(d.value)}`, // given d in (computed) links
  linkColor = "source-target", // source, target, source-target, or static color
  linkStrokeOpacity = 0.5, // link stroke opacity
  linkMixBlendMode = "multiply", // link blending mode
  colors = d3.schemeTableau10, // array of colors
  width = 640, // outer width, in pixels
  height = 400, // outer height, in pixels
  marginTop = 5, // top margin, in pixels
  marginRight = 1, // right margin, in pixels
  marginBottom = 5, // bottom margin, in pixels
  marginLeft = 1, // left margin, in pixels
} = {}) {
  // Convert nodeAlign from a name to a function (since d3-sankey is not part of core d3).
  if (typeof nodeAlign !== "function") nodeAlign = {
    left: d3Sankey.sankeyLeft,
    right: d3Sankey.sankeyRight,
    center: d3Sankey.sankeyCenter
  }[nodeAlign] ?? d3Sankey.sankeyJustify;

  // Compute values.
  const LS = d3.map(links, linkSource).map(intern);
  const LT = d3.map(links, linkTarget).map(intern);
  const LV = d3.map(links, linkValue);
  if (nodes === undefined) nodes = Array.from(d3.union(LS, LT), id => ({id}));
  const N = d3.map(nodes, nodeId).map(intern);
  const G = nodeGroup == null ? null : d3.map(nodes, nodeGroup).map(intern);

  // Replace the input nodes and links with mutable objects for the simulation.
  nodes = d3.map(nodes, (_, i) => ({id: N[i]}));
  links = d3.map(links, (_, i) => ({source: LS[i], target: LT[i], value: LV[i]}));

  // Ignore a group-based linkColor option if no groups are specified.
  if (!G && ["source", "target", "source-target"].includes(linkColor)) linkColor = "currentColor";

  // Compute default domains.
  if (G && nodeGroups === undefined) nodeGroups = G;

  // Construct the scales.
  const color = nodeGroup == null ? null : d3.scaleOrdinal(nodeGroups, colors);

  // Compute the Sankey layout.
  d3Sankey.sankey()
      .nodeId(({index: i}) => N[i])
      .nodeAlign(nodeAlign)
      .nodeWidth(nodeWidth)
      .nodePadding(nodePadding)
      .nodeSort(nodeSort)
      .extent([[marginLeft, marginTop], [width - marginRight, height - marginBottom]])
    ({nodes, links});

  // Compute titles and labels using layout nodes, so as to access aggregate values.
  if (typeof format !== "function") format = d3.format(format);
  const Tl = nodeLabel === undefined ? N : nodeLabel == null ? null : d3.map(nodes, nodeLabel);
  const Tt = nodeTitle == null ? null : d3.map(nodes, nodeTitle);
  const Lt = linkTitle == null ? null : d3.map(links, linkTitle);

  // A unique identifier for clip paths (to avoid conflicts).
  const uid = `O-${Math.random().toString(16).slice(2)}`;

  const svg = d3.create("svg")
      .attr("width", width)
      .attr("height", height)
      .attr("viewBox", [0, 0, width, height])
      .attr("style", "max-width: 100%; height: auto; height: intrinsic;");

  const node = svg.append("g")
      .attr("stroke", nodeStroke)
      .attr("stroke-width", nodeStrokeWidth)
      .attr("stroke-opacity", nodeStrokeOpacity)
      .attr("stroke-linejoin", nodeStrokeLinejoin)
    .selectAll("rect")
    .data(nodes)
    .join("rect")
      .attr("x", d => d.x0)
      .attr("y", d => d.y0)
      .attr("height", d => d.y1 - d.y0)
      .attr("width", d => d.x1 - d.x0);

  if (G) node.attr("fill", ({index: i}) => color(G[i]));
  if (Tt) node.append("title").text(({index: i}) => Tt[i]);

  const link = svg.append("g")
      .attr("fill", "none")
      .attr("stroke-opacity", linkStrokeOpacity)
    .selectAll("g")
    .data(links)
    .join("g")
      .style("mix-blend-mode", linkMixBlendMode);

  if (linkColor === "source-target") link.append("linearGradient")
      .attr("id", d => `${uid}-link-${d.index}`)
      .attr("gradientUnits", "userSpaceOnUse")
      .attr("x1", d => d.source.x1)
      .attr("x2", d => d.target.x0)
      .call(gradient => gradient.append("stop")
          .attr("offset", "0%")
          .attr("stop-color", ({source: {index: i}}) => color(G[i])))
      .call(gradient => gradient.append("stop")
          .attr("offset", "100%")
          .attr("stop-color", ({target: {index: i}}) => color(G[i])));

  link.append("path")
      .attr("d", linkPath)
      .attr("stroke", linkColor === "source-target" ? ({index: i}) => `url(#${uid}-link-${i})`
          : linkColor === "source" ? ({source: {index: i}}) => color(G[i])
          : linkColor === "target" ? ({target: {index: i}}) => color(G[i])
          : linkColor)
      .attr("stroke-width", ({width}) => Math.max(1, width))
      .call(Lt ? path => path.append("title").text(({index: i}) => Lt[i]) : () => {});

  if (Tl) svg.append("g")
      .attr("font-family", "sans-serif")
      .attr("font-size", 14)
    .selectAll("text")
    .data(nodes)
    .join("text")
      .attr("x", d => d.x0 < width / 2 ? d.x1 + nodeLabelPadding : d.x0 - nodeLabelPadding)
      .attr("y", d => (d.y1 + d.y0) / 2)
      .attr("dy", "0.35em")
      .attr("text-anchor", d => d.x0 < width / 2 ? "start" : "end")
      .text(({index: i}) => Tl[i]);

  function intern(value) {
    return value !== null && typeof value === "object" ? value.valueOf() : value;
  }

  return Object.assign(svg.node(), {scales: {color}});
}
```
