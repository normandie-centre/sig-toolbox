@echo off

REM Changer de répertoire vers le dossier de destination
cd "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_MRN_tertiaire\layers"

REM Exécuter les commandes pmtiles pour convertir les fichiers mbtiles en pmtiles
pmtiles convert communes.mbtiles communes.pmtiles
pmtiles convert batiments_groupes_ter.mbtiles batiments_groupes_ter.pmtiles
pmtiles convert tups_ter.mbtiles tups_ter.pmtiles

@echo on