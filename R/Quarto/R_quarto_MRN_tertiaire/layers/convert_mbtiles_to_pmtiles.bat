@echo off

REM Changer de répertoire vers le dossier de destination
cd "C:\Users\guillaume.chretien\Downloads\test_mbiles_mrn_tertiaire\mrn_tertiaire"

REM Exécuter les commandes pmtiles pour convertir les fichiers mbtiles en pmtiles
pmtiles convert communes.mbtiles communes.pmtiles
pmtiles convert batiments_groupes_ter.mbtiles batiments_groupes_ter.pmtiles
pmtiles convert batiments_groupes_ter_detail.mbtiles batiments_groupes_ter_detail.pmtiles
pmtiles convert tups_ter.mbtiles tups_ter.pmtiles

pmtiles convert reseau_chaleur_cana.mbtiles reseau_chaleur_cana.pmtiles
pmtiles convert reseau_chaleur_perimetre_classement_dsp_niveau_1.mbtiles reseau_chaleur_perimetre_classement_dsp_niveau_1.pmtiles
pmtiles convert reseau_chaleur_perimetre_classement_dsp_niveau_2.mbtiles reseau_chaleur_perimetre_classement_dsp_niveau_2.pmtiles
pmtiles convert zae.mbtiles zae.pmtiles


@echo on

pause