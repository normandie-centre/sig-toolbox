@echo off
REM Changer de répertoire vers le dossier de QGIS
cd "C:\Program Files\QGIS 3.28\bin"

REM Exécuter les commandes ogr2ogr pour convertir les fichiers geojson en mbtiles

ogr2ogr -f MBTILES "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_MRN_tertiaire\layers\batiments_groupes_ter_detail.mbtiles" "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_MRN_tertiaire\layers\batiments_groupes_ter_detail.geojson" -dsco MAXZOOM=18

REM Changer de répertoire vers le dossier de destination
cd "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_MRN_tertiaire\layers"

REM Exécuter les commandes pmtiles pour convertir les fichiers mbtiles en pmtiles

pmtiles convert batiments_groupes_ter_detail.mbtiles batiments_groupes_ter_detail.pmtiles

@echo on