@echo off
REM Changer de répertoire vers le dossier de QGIS
cd "C:\Program Files\QGIS 3.28\bin"

REM Exécuter les commandes ogr2ogr pour convertir les fichiers geojson en mbtiles
REM ogr2ogr -f MBTILES "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_barentin_verte_permeable\layers\barentin.mbtiles" "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_barentin_verte_permeable\layers\barentin.geojson" -dsco MAXZOOM=18
REM ogr2ogr -f MBTILES "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_barentin_verte_permeable\layers\barentin_mask.mbtiles" "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_barentin_verte_permeable\layers\barentin_mask.geojson" -dsco MAXZOOM=18
REM ogr2ogr -f MBTILES "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_barentin_verte_permeable\layers\ppr_debordement.mbtiles" "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_barentin_verte_permeable\layers\ppr_debordement.geojson" -dsco MAXZOOM=18
REM ogr2ogr -f MBTILES "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_barentin_verte_permeable\layers\ppr_remontee_nappe.mbtiles" "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_barentin_verte_permeable\layers\ppr_remontee_nappe.geojson" -dsco MAXZOOM=18
REM ogr2ogr -f MBTILES "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_barentin_verte_permeable\layers\ppr_ruissellement.mbtiles" "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_barentin_verte_permeable\layers\ppr_ruissellement.geojson" -dsco MAXZOOM=18

ogr2ogr -f MBTILES "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_barentin_verte_permeable\layers\batiments.mbtiles" "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_barentin_verte_permeable\layers\batiments.geojson" -dsco MAXZOOM=18


REM Changer de répertoire vers le dossier de destination
cd "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_barentin_verte_permeable\layers"

REM Exécuter les commandes pmtiles pour convertir les fichiers mbtiles en pmtiles
REM pmtiles convert barentin.mbtiles barentin.pmtiles
REM pmtiles convert barentin_mask.mbtiles barentin_mask.pmtiles

REM pmtiles convert ppr_debordement.mbtiles ppr_debordement.pmtiles
REM pmtiles convert ppr_remontee_nappe.mbtiles ppr_remontee_nappe.pmtiles
REM pmtiles convert ppr_ruissellement.mbtiles ppr_ruissellement.pmtiles

pmtiles convert batiments.mbtiles batiments.pmtiles

@echo on