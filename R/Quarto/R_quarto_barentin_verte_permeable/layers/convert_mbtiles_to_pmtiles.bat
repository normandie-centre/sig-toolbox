@echo off

REM Changer de répertoire vers le dossier de destination
cd "C:\Users\guillaume.chretien\Documents\sig-toolbox\R\Quarto\R_quarto_barentin_verte_permeable\layers"

REM Exécuter les commandes pmtiles pour convertir les fichiers mbtiles en pmtiles
pmtiles convert barentin.mbtiles barentin.pmtiles
pmtiles convert barentin_mask.mbtiles barentin_mask.pmtiles
pmtiles convert ppr_debordement.mbtiles ppr_debordement.pmtiles
pmtiles convert ppr_remontee_nappe.mbtiles ppr_remontee_nappe.pmtiles
pmtiles convert ppr_ruissellement.mbtiles ppr_ruissellement.pmtiles
pmtiles convert batiments.mbtiles batiments.pmtiles

pmtiles convert test_pour_visu_3d_4326.mbtiles test_pour_visu_3d.pmtiles


@echo on