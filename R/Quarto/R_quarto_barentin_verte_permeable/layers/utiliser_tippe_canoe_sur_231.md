se connecter au 231 sur gislab

ouvrir un terminal

tippe puis tab ça complète
-h pour help

cd + première lettre puis tab

tippecanoe -o gares.mbtiles -zg --drop-densest-as-needed --extend-zooms-if-still-dropping ~/gares.geojson           
  
tippecanoe -o ppr_ruissellement.mbtiles -zg --drop-densest-as-needed --extend-zooms-if-still-dropping --no-simplification-of-shared-nodes ppr_ruissellement_4326.geojson          

tippecanoe -o barentin.mbtiles -zg --drop-densest-as-needed --extend-zooms-if-still-dropping --no-simplification-of-shared-nodes barentin_4326.geojson          
tippecanoe -o barentin_mask.mbtiles -zg --drop-densest-as-needed --extend-zooms-if-still-dropping --no-simplification-of-shared-nodes barentin_mask_4326.geojson          
tippecanoe -o batiments.mbtiles -zg --drop-densest-as-needed --extend-zooms-if-still-dropping --no-simplification-of-shared-nodes batiments_4326.geojson          
tippecanoe -o ppr_debordement.mbtiles -zg --drop-densest-as-needed --extend-zooms-if-still-dropping --no-simplification-of-shared-nodes ppr_debordement_4326.geojson          
tippecanoe -o ppr_remontee_nappe.mbtiles -zg --drop-densest-as-needed --extend-zooms-if-still-dropping --no-simplification-of-shared-nodes ppr_remontee_nappe_4326.geojson          

tippecanoe -o test_pour_visu_3d_4326.mbtiles -zg --drop-densest-as-needed --extend-zooms-if-still-dropping --no-simplification-of-shared-nodes test_pour_visu_3d_4326.geojson          

taper ll dans le terminal pour lister des fichiers et voir leur taille