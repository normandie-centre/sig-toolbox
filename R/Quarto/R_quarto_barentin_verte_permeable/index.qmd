---
description: "Explorez les données SIG"
date: 2024-09-18
lang: fr
format: dashboard
resources:
  - layers/*.pmtiles
  - images/icons/*.svg

back-to-top-navigation: false

include-in-header:
  text: |
    <script src="js/pmtiles.js"></script>
    <script src="js/maplibre.js"></script>
    <link href="js/maplibre.css" rel="stylesheet">
    <script src="js/turf.js"></script>
---


```{r}
#| echo: false
#| output: false
LAYERS_PATH <- Sys.getenv("LAYERS_PATH")
ojs_define(LAYERS_PATH)
```


## {.sidebar width="250px"}

```{ojs}
html`
<div style="font-size:14px">
        <div style="margin-bottom:3px;"><strong>Niveau d'aléas</strong></div>

        <div>
            <label for="opacitySliderAlea">Opacité :</label>
            <input id="opacitySliderAlea" type="range" min="0" max="1" step="0.1" value="0.7">
        </div>
        
        <span class="badge" style="background:green;opacity:1">    </span> Faible<br>
        <span class="badge" style="background:orange;opacity:1">    </span> Moyen<br>
        <span class="badge" style="background:red;opacity:1">    </span> Fort<br>
</div>
`
```


## Column 


```{ojs}
//| echo: false
//| padding: 0px
//| expandable: false
//| fill: false

// DIFFICULTES AVEC LES MBTILES DES PPR... comme d'hab !
// idée : json pour ces couches et pmtiles pour les autres ?
/*
barentin_4326 = FileAttachment("layers/barentin_4326.geojson").json({typed: false})
barentin_mask_4326 = FileAttachment("layers/barentin_mask_4326.geojson").json({typed: false})
ppr_debordement_4326 = FileAttachment("layers/ppr_debordement_4326.geojson").json({typed: false})
ppr_remontee_nappe_4326 = FileAttachment("layers/ppr_remontee_nappe_4326.geojson").json({typed: false})
ppr_ruissellement_4326 = FileAttachment("layers/ppr_ruissellement_4326.geojson").json({typed: false})
*/

myMap = {

  // on définit le container de la carte et on l'attend  , on mettant fill: false la card prend la taille de la map et en réglant le height en vh en tatonnant on trouve la bonne proportion
  const container = html`<div style="height:100vh;">`;
  yield container;

    // add the PMTiles plugin to the maplibregl global.
    const protocol = new pmtiles.Protocol();
    maplibregl.addProtocol('pmtiles', (request) => {
        return new Promise((resolve, reject) => {
            const callback = (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve({data});
                }
            };
            protocol.tile(request, callback);
        });
    }); 

    
    const PMTILES_URL_barentin = `${LAYERS_PATH}/barentin.pmtiles`;
    const barentin = new pmtiles.PMTiles(PMTILES_URL_barentin);
    protocol.add(barentin);
    
    const PMTILES_URL_barentin_mask = `${LAYERS_PATH}/barentin_mask.pmtiles`;
    const barentin_mask = new pmtiles.PMTiles(PMTILES_URL_barentin_mask);
    protocol.add(barentin_mask);

    const PMTILES_URL_ppr_debordement =`${LAYERS_PATH}/ppr_debordement.pmtiles`;
    const ppr_debordement = new pmtiles.PMTiles(PMTILES_URL_ppr_debordement);
    protocol.add(ppr_debordement);

    const PMTILES_URL_ppr_remontee_nappe =`${LAYERS_PATH}/ppr_remontee_nappe.pmtiles`;
    const remontee_nappe = new pmtiles.PMTiles(PMTILES_URL_ppr_remontee_nappe);
    protocol.add(remontee_nappe);

    const PMTILES_URL_ppr_ruissellement =`${LAYERS_PATH}/ppr_ruissellement.pmtiles`;
    const ppr_ruissellement = new pmtiles.PMTiles(PMTILES_URL_ppr_ruissellement);
    protocol.add(ppr_ruissellement);
    
    const PMTILES_URL_batiments =`${LAYERS_PATH}/batiments.pmtiles`;
    const batiments = new pmtiles.PMTiles(PMTILES_URL_batiments);
    protocol.add(batiments);
    
    const PMTILES_URL_test_pour_visu_3d =`${LAYERS_PATH}/test_pour_visu_3d.pmtiles`;
    const test_pour_visu_3d = new pmtiles.PMTiles(PMTILES_URL_test_pour_visu_3d);
    protocol.add(test_pour_visu_3d);
    
    const sourceOrtho = {style:'normal', format:'image/jpeg',layer:'HR.ORTHOIMAGERY.ORTHOPHOTOS'};
    const sourcePlanIGN = {style:'normal', format:'image/png',layer:'GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2'};
    const sourceHillShade = {style:'estompage_grayscale',format:'image/png',layer:'ELEVATION.ELEVATIONGRIDCOVERAGE.SHADOW'};
    const MAPTILER_TOKEN='o85gF994l9IDgPUYoRH0';

  const map = new maplibregl.Map({
    container,
    center: [0.96, 49.535],
            zoom: 12,
            pitch: 0,
            maxPitch: 80,
            style: {
                version: 8,
                sources: {
                    'raster-planign': {
                        type: 'raster',
                        //'tiles': ['https://tile.openstreetmap.org/{z}/{x}/{y}.png'],
                        tiles: [
                           `https://data.geopf.fr/wmts?SERVICE=WMTS&style=${sourcePlanIGN.style}&VERSION=1.0.0&REQUEST=GetTile&format=${sourcePlanIGN.format}&layer=${sourcePlanIGN.layer}&tilematrixset=PM&TileMatrix={z}&TileCol={x}&TileRow={y}`
                        ],
                        tileSize: 256,
                        attribution: '© <a href="https://www.ign.fr/">IGN</a>',
                        minzoom: 0,
                        maxzoom: 22
                    },
                    'raster-ortho': {
                        type: 'raster',
                        //'tiles': ['https://tile.openstreetmap.org/{z}/{x}/{y}.png'],
                        tiles: [
                            `https://data.geopf.fr/wmts?SERVICE=WMTS&style=${sourceOrtho.style}&VERSION=1.0.0&REQUEST=GetTile&format=${sourceOrtho.format}&layer=${sourceOrtho.layer}&tilematrixset=PM&TileMatrix={z}&TileCol={x}&TileRow={y}`
                        ],
                        tileSize: 256,
                        attribution: '© <a href="https://www.ign.fr/">IGN</a>',
                        minzoom: 0,
                        maxzoom: 22
                    },
                    'hillshadeSource': {
                        type: 'raster',
                        //'tiles': ['https://tile.openstreetmap.org/{z}/{x}/{y}.png'],
                        tiles: [
                            `https://data.geopf.fr/wmts?SERVICE=WMTS&style=${sourceHillShade.style}&VERSION=1.0.0&REQUEST=GetTile&format=${sourceHillShade.format}&layer=${sourceHillShade.layer}&tilematrixset=PM&TileMatrix={z}&TileCol={x}&TileRow={y}`
                        ],
                        tileSize: 256,
                        attribution: '© <a href="https://www.ign.fr/">IGN</a>',
                        minzoom: 0,
                        maxzoom: 22
                    },
                    'terrainSource': {
                        type: 'raster-dem',
                        url: `https://api.maptiler.com/tiles/terrain-rgb-v2/tiles.json?key=${MAPTILER_TOKEN}`,
                        tileSize: 256
                    },
                    'source_barentin': {
                        type: 'vector',
                        url: `pmtiles://${PMTILES_URL_barentin}`
                    },
                    'source_ppr_debordement': {
                        type: 'vector',
                        url: `pmtiles://${PMTILES_URL_ppr_debordement}`,
                        attribution: '© <a href="https://www.cerema.fr/fr">CEREMA</a>',
                    },
                    'source_remontee_nappe': {
                        type: 'vector',
                        url: `pmtiles://${PMTILES_URL_ppr_remontee_nappe}`
                    },
                    'source_ppr_ruissellement': {
                        type: 'vector',
                        url: `pmtiles://${PMTILES_URL_ppr_ruissellement}`
                    },
                    'source_batiments': {
                        type: 'vector',
                        url: `pmtiles://${PMTILES_URL_batiments}`
                    },
                    'source_test_pour_visu_3d': {
                        type: 'vector',
                        url: `pmtiles://${PMTILES_URL_test_pour_visu_3d}`
                    },
                    'source_barentin_mask': {
                        type: 'vector',
                        url: `pmtiles://${PMTILES_URL_barentin_mask}`
                    }

                    /*
                    'source_barentin_4326': {
                        'type': 'geojson',
                        data: barentin_4326
                    },
                    'source_barentin_mask_4326': {
                        'type': 'geojson',
                        data: barentin_mask_4326
                    },
                    'source_ppr_debordement_4326': {
                        'type': 'geojson',
                        data: ppr_debordement_4326
                    },
                    'source_ppr_remontee_nappe_4326': {
                        'type': 'geojson',
                        data: ppr_remontee_nappe_4326
                    },
                    'source_ppr_ruissellement_4326': {
                        'type': 'geojson',
                        data: ppr_ruissellement_4326
                    }
                    */
                },
                layers: [ 
                    {
                        id: 'raster-planign',
                        type: 'raster',
                        source: 'raster-planign',
                        layout: {
                            // Make the layer visible by default.
                            visibility: 'visible' //ou none
                        },
                    },    
                    {
                        id: 'raster-ortho',
                        type: 'raster',
                        source: 'raster-ortho',
                        layout: {
                            // Make the layer visible by default.
                            visibility: 'none' //ou none
                        },
                    },
                    {
                        id: 'hillshadeSource',
                        type: 'raster',
                        source: 'hillshadeSource',
                        layout: {
                            // Make the layer visible by default.
                            visibility: 'visible' //ou none
                        },
                    },
                    // pmtiles
                    {
                        id: "barentin",
                        source: "source_barentin",
                        'source-layer': "barentin_4326",
                        type: "line", 
                        paint: {
                            'line-color': '#e619e5',
                            'line-width': 2
                        }
                    },
                    {
                        id: "ppr_debordement",
                        source: "source_ppr_debordement",
                        'source-layer': 'ppr_debordement_4326',
                        type: "fill", 
                        paint: {
                            'fill-color': [
                                'match',
                                ['get', 'nivalea'], 
                                'Fai', 'green', 
                                'M', 'orange', 
                                'F', 'red',
                                // Définissez une couleur par défaut si nécessaire :
                                '#808080' // Couleur grise par défaut pour les valeurs non spécifiées
                            ],
                            "fill-opacity": 0.75//,
                            //'fill-outline-color': '#000000'       
                        }
                    },
                    {
                        id: "remontee_nappe",
                        source: "source_remontee_nappe",
                        'source-layer': 'remontee_nappe_4326',
                        type: "fill", 
                        paint: {
                            "fill-color": "blue"
                        },
                        "fill-opacity": 1
                    },
                    { 
                        id: "ppr_ruissellement",
                        source: "source_ppr_ruissellement",
                        'source-layer': 'ppr_ruissellement_4326',
                        type: "fill", 
                        paint: {
                            'fill-color': [
                                'match',
                                ['get', 'nivalea'], 
                                'Fai', 'green', 
                                'M', 'orange', 
                                'F', 'red',
                                // Définissez une couleur par défaut si nécessaire :
                                '#808080' // Couleur grise par défaut pour les valeurs non spécifiées
                            ],
                            "fill-opacity": 0.75//,
                            //'fill-outline-color': '#000000'       
                        }
                    },
                    {
                        id: 'batiments',
                        source: 'source_batiments',
                        'source-layer': 'batiments_4326',
                        type: "fill-extrusion", // Changer le type de couche en "fill-extrusion"
                        paint: {                            
                            "fill-extrusion-color": "white", // Couleur de remplissage extrudée
                            "fill-extrusion-height": ["get", "hauteur"], // Propriété de hauteur à utiliser pour extruder les polygones
                            "fill-extrusion-base": 0, // Hauteur de base de l'extrusion
                            "fill-extrusion-opacity": 0.6
                        }
                    },
                    {
                        id: 'test_pour_visu_3d',
                        source: 'source_test_pour_visu_3d',
                        'source-layer': 'test_pour_visu_3d_4326',
                        type: "fill",
                        paint: {                            
                            'fill-color': 'blue',
                            "fill-opacity": 0.75
                        }
                    },
                    {
                        id: "barentin_mask",
                        source: "source_barentin_mask",
                        'source-layer': 'barentin_mask_4326',
                        type: "fill", 
                        paint: {
                            'fill-color': 'white',
                            "fill-opacity": 0.9
                        }
                    }
                    // test geojson
                    /*
                    {
                        id: "barentin_4326",
                        source: "source_barentin_4326",
                        type: "line", 
                        paint: {
                            'line-color': '#e619e5',
                            'line-width': 2
                        },
                    },
                    { 
                        id: "ppr_debordement_4326",
                        source: "source_ppr_debordement_4326",
                        type: "fill", 
                        paint: {
                            'fill-color': [
                                'match',
                                ['get', 'nivalea'], 
                                'Fai', 'green', 
                                'M', 'orange', 
                                'F', 'red',
                                // Définissez une couleur par défaut si nécessaire :
                                '#808080' // Couleur grise par défaut pour les valeurs non spécifiées
                            ],
                            "fill-opacity": 0.75//,
                            //'fill-outline-color': '#000000'       
                        }
                    },
                    { 
                        id: "ppr_ruissellement_4326",
                        source: "source_ppr_ruissellement_4326",
                        type: "fill", 
                        paint: {
                            'fill-color': [
                                'match',
                                ['get', 'nivalea'], 
                                'Fai', 'green', 
                                'M', 'orange', 
                                'F', 'red',
                                // Définissez une couleur par défaut si nécessaire :
                                '#808080' // Couleur grise par défaut pour les valeurs non spécifiées
                            ],
                            "fill-opacity": 0.75//,
                            //'fill-outline-color': '#000000'       
                        }
                    },
                    {
                        id: "ppr_remontee_nappe_4326",
                        source: "source_ppr_remontee_nappe_4326",
                        type: "fill", 
                        paint: {
                            'fill-color': 'blue',
                            "fill-opacity": 0.75
                        }
                    },
                    {
                        id: "barentin_mask_4326",
                        source: "source_barentin_mask_4326",
                        type: "fill", 
                        paint: {
                            'fill-color': 'white',
                            "fill-opacity": 0.9
                        }
                    },
                    {
                        id: 'batiments',
                        source: 'source_batiments',
                        'source-layer': 'batiments',
                        type: "fill-extrusion", // Changer le type de couche en "fill-extrusion"
                        paint: {                            
                            "fill-extrusion-color": "white", // Couleur de remplissage extrudée
                            "fill-extrusion-height": ["get", "hauteur"], // Propriété de hauteur à utiliser pour extruder les polygones
                            "fill-extrusion-base": 0, // Hauteur de base de l'extrusion
                            "fill-extrusion-opacity": 0.6
                        }
                    }
                    */
                    
                ],
                terrain: {
                    source: 'terrainSource',
                    exaggeration: 1.1
                }
            }
  });

  //------------------------------------------------------------------------------------------------      
  // test interactivité avec Inputs d'observable ?
  const opacitySliderAlea = document.getElementById('opacitySliderAlea');
  opacitySliderAlea.addEventListener('input', function () {
        const opacityValue = parseFloat(opacitySliderAlea.value);
        map.setPaintProperty('ppr_ruissellement', 'fill-opacity', opacityValue);
        map.setPaintProperty('ppr_debordement', 'fill-opacity', opacityValue);
  });
  
  //------------------------------------------------------------------------------------------------      
  map.addControl(new maplibregl.NavigationControl(
    {
      visualizePitch: true,
      //showZoom: true,
      //showCompass: true
    }), 
    'top-left'
  );
  
  map.addControl(new maplibregl.FullscreenControl()), 'top-right';

  const scale = new maplibregl.ScaleControl({
    maxWidth: 80,
    unit: 'metric'
  });
  map.addControl(scale);


  //------------------------------------------------------------------------------------------------      
  
  map.on('click', 'barentin', (e) => {
    // Center the map on the coordinates of any clicked symbol from the 'symbols' layer.
    const centroid = turf.centroid(e.features[0].geometry);
    map.flyTo({
        center: centroid.geometry.coordinates,
        zoom: 15,
        pitch: 45
    });
    
    // POP UP on click
    new maplibregl.Popup()
        .setLngLat(e.lngLat)
        .setHTML(`
          Commune de ${e.features[0].properties.nom}
        `)
        .addTo(map);
  });
  
  
  //------------------------------------------------------------------------------------------------      
  // Change the cursor to a pointer when the it enters a feature in the 'symbols' layer.
  map.on('mouseenter', 'barentin', () => {
      map.getCanvas().style.cursor = 'pointer';
  });
  map.on('mouseleave', 'barentin', () => {
      map.getCanvas().style.cursor = '';
  });

  //------------------------------------------------------------------------------------------------      
  // pris sur un exemple, permet de refaire un coup de propre
  invalidation.then(() => map.remove());
  
  //------------------------------------------------------------------------------------------------
  // Ebauche de menu sur mesure

  const mapMenuDiv = document.createElement('div');
  mapMenuDiv.classList.add('mapFondDePlan');
  //mapMenuDiv.innerHTML = '<strong>Fond de plan :</strong>';
  map.getContainer().appendChild(mapMenuDiv);
  // Ajouter un style pour le z-index

  mapMenuDiv.style.position = 'absolute'; // new
  mapMenuDiv.style.zIndex = '1000'; // new

  const customDivHTML = `
          <strong>Fond de plan :</strong>
          <div class="form-check form-switch">
              <input class="form-check-input" type="checkbox" role="switch" id="orthoCheckbox">
              <label class="form-check-label" for="orthoCheckbox">Photos aériennes</label>
          </div>                    
      `;
  mapMenuDiv.innerHTML += customDivHTML;

  // doc à creuser
  // https://docs.mapbox.com/mapbox-gl-js/example/toggle-layers/
  document.getElementById('orthoCheckbox').addEventListener('change', function (e) {
      e.preventDefault();
      e.stopPropagation();

      const valeurCheckbox = document.getElementById('orthoCheckbox').checked;
      if (valeurCheckbox) {
          map.setLayoutProperty('raster-ortho', 'visibility', 'visible');
          map.setLayoutProperty('raster-planign', 'visibility', 'none');
      } else {
          map.setLayoutProperty('raster-ortho', 'visibility', 'none');
          map.setLayoutProperty('raster-planign', 'visibility', 'visible');
      }
  });

  // Brouillon pour ajouter mon menu layer !
  const mapLayersDiv = document.createElement('div');
  mapLayersDiv.classList.add('mapLayers');
  //mapMenuDiv.innerHTML = '<strong>Fond de plan :</strong>';
  map.getContainer().appendChild(mapLayersDiv);
  // Ajouter un style pour le z-index

  mapLayersDiv.style.position = 'absolute'; // new
  mapLayersDiv.style.zIndex = '1000'; // new
  const customDivHTML_2 = `
          <button id="toggleMapMenuButton" class="btn btn-light">
              <img src="images/icons/layers-half.svg" alt="Icône SVG" width="25" height="25">
          </button>                    
          <div id="layersList" ></div>
          `
  mapLayersDiv.innerHTML += customDivHTML_2;

  const toggleableLayerIds= [
      "barentin",
      'barentin_mask',
      "batiments",
      "ppr_debordement",
      "ppr_remontee_nappe",
      "ppr_ruissellement",
      "test_pour_visu_3d"
      ];

  const aliasLayerIds = {
      "barentin":"Barentin",
      "barentin_mask":"Masque",
      "batiments":"Batiments",
      "ppr_debordement":"PPR débordement",
      "ppr_remontee_nappe":"PPR remontée de nappe",
      "ppr_ruissellement":"PPR ruissellement",
      "test_pour_visu_3d":"Test Rémi 1cm d'eau"
  };

  // https://docs.mapbox.com/mapbox-gl-js/example/toggle-layers/
  // Set up the corresponding toggle button for each layer.                
  for (const id of toggleableLayerIds) {
      // Skip layers that already have a button set up.
      if (document.getElementById(id)) {
          continue;
      }

      // Create a link.
      const link = document.createElement('a');
      link.id = id;
      link.href = '#';
      //link.textContent = id;
      link.textContent = aliasLayerIds[id] || id;
      link.className = 'active';

      // Show or hide layer when the toggle is clicked.
      link.onclick = function (e) {
          const clickedLayer = this.id;
          e.preventDefault();
          e.stopPropagation();

          const visibility = map.getLayoutProperty(
              clickedLayer,
              'visibility'
          ) || 'visible';
          // NB : je rajoute ou visible car la première c'est undefined ? ? ?
          // Toggle layer visibility by changing the layout object's visibility property.
          if (visibility === 'visible') {
              map.setLayoutProperty(clickedLayer, 'visibility', 'none');
              this.className = '';
          } else {
              this.className = 'active';
              map.setLayoutProperty(
                  clickedLayer,
                  'visibility',
                  'visible'
              );
          }
      };

      const layers = document.getElementById('layersList');
      layers.appendChild(link);
  }


}

```