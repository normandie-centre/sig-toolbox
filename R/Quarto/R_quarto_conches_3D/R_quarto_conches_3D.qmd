---
include-in-header:
  text: |
    <script src="https://unpkg.com/pmtiles@2.11.0/dist/index.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/maplibre-gl@4.2.0/dist/maplibre-gl.min.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/maplibre-gl@4.2.0/dist/maplibre-gl.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/@turf/turf@6.5.0/turf.min.js"></script>
theme:
  - flatly
  - custom.scss
---

# Représentation du centre Bourg de Conches en Ouches

source des bâtiments : BD Topo 2023

```{ojs}
//| echo: false
commune_4326 = FileAttachment("data/commune_4326.geojson").json({typed: false})
//commune_4326

centre_bourg_4326 = FileAttachment("data/centre_bourg_4326.geojson").json({typed: false})
//centre_bourg_4326

batiment_centre_bourg_bdtopo_2023_4326 = FileAttachment("data/batiment_centre_bourg_bdtopo_2023_4326.geojson").json({typed: false})
//batiment_centre_bourg_bdtopo_2023_4326
```

```{ojs}
//| echo: false
//| padding: 0px
//| expandable: false
//| fill: false

// interessant, un viewer plus simple basé sur maplibre ? ? ? ma couche commune est bonne
// https://observablehq.com/@neocartocnrs/geoverview

myMap = {

  // on définit le container de la carte et on l'attend  , on mettant fill: false la card prend la taille de la map et en réglant le height en vh en tatonnant on trouve la bonne proportion
  const container = html`<div style="height:80vh;">`;
  yield container;
/*
    // add the PMTiles plugin to the maplibregl global.
    const protocol = new pmtiles.Protocol();
    maplibregl.addProtocol('pmtiles', (request) => {
        return new Promise((resolve, reject) => {
            const callback = (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve({data});
                }
            };
            protocol.tile(request, callback);
        });
    });

    const PMTILES_URL_B1 = 'https://guillaumechretiencerema.github.io/mydatas/recoquartiers/bat_48_58.pmtiles?raw=true';
    const b1 = new pmtiles.PMTiles(PMTILES_URL_B1);
    // this is so we share one instance across the JS code and the map renderer
    protocol.add(b1);

   
    const PMTILES_URL_C = 'https://guillaumechretiencerema.github.io/mydatas/recoquartiers/communes.pmtiles?raw=true';
    const c = new pmtiles.PMTiles(PMTILES_URL_C);
    protocol.add(c);

    const PMTILES_URL_Q = 'https://guillaumechretiencerema.github.io/mydatas/recoquartiers/quartiers.pmtiles?raw=true';
    const q = new pmtiles.PMTiles(PMTILES_URL_Q);
    protocol.add(q);
*/

    const sourceOrtho = {style:'normal', format:'image/jpeg',layer:'HR.ORTHOIMAGERY.ORTHOPHOTOS'};
    const sourcePlanIGN = {style:'normal', format:'image/png',layer:'GEOGRAPHICALGRIDSYSTEMS.PLANIGNV2'};
    const sourceHillShade = {style:'estompage_grayscale',format:'image/png',layer:'ELEVATION.ELEVATIONGRIDCOVERAGE.SHADOW'};
    const MAPTILER_TOKEN='o85gF994l9IDgPUYoRH0';


  const map = new maplibregl.Map({
    container,
    center: [0.94, 48.96],
            zoom: 16,
            pitch: 45,
            maxPitch:85,
            //maxPitch: 75,
            style: {
                version: 8,
                sources: {
                    'raster-planign': {
                        type: 'raster',
                        tiles: [
                           `https://data.geopf.fr/wmts?SERVICE=WMTS&style=${sourcePlanIGN.style}&VERSION=1.0.0&REQUEST=GetTile&format=${sourcePlanIGN.format}&layer=${sourcePlanIGN.layer}&tilematrixset=PM&TileMatrix={z}&TileCol={x}&TileRow={y}`
                        ],
                        tileSize: 256,
                        attribution: '© <a href="https://www.ign.fr/">IGN</a>',
                        minzoom: 0,
                        maxzoom: 22
                    },
                    'raster-ortho': {
                        type: 'raster',
                        tiles: [
                            `https://data.geopf.fr/wmts?SERVICE=WMTS&style=${sourceOrtho.style}&VERSION=1.0.0&REQUEST=GetTile&format=${sourceOrtho.format}&layer=${sourceOrtho.layer}&tilematrixset=PM&TileMatrix={z}&TileCol={x}&TileRow={y}`
                        ],
                        tileSize: 256,
                        attribution: '© <a href="https://www.ign.fr/">IGN</a>',
                        minzoom: 0,
                        maxzoom: 22
                    },
                    'hillshadeSource': {
                        type: 'raster',
                        tiles: [
                            `https://data.geopf.fr/wmts?SERVICE=WMTS&style=${sourceHillShade.style}&VERSION=1.0.0&REQUEST=GetTile&format=${sourceHillShade.format}&layer=${sourceHillShade.layer}&tilematrixset=PM&TileMatrix={z}&TileCol={x}&TileRow={y}`
                        ],
                        tileSize: 256,
                        attribution: '© <a href="https://www.ign.fr/">IGN</a>',
                        minzoom: 0,
                        maxzoom: 22
                    },
                    'terrainSource': {
                        type: 'raster-dem',
                        url: `https://api.maptiler.com/tiles/terrain-rgb-v2/tiles.json?key=${MAPTILER_TOKEN}`,
                        tileSize: 256
                    },
                    'commune_4326': {
                        'type': 'geojson',
                        data: commune_4326
                    },
                    'centre_bourg_4326': {
                        'type': 'geojson',
                        data: centre_bourg_4326
                    },
                    'batiment_centre_bourg_bdtopo_2023_4326': {
                        'type': 'geojson',
                        data: batiment_centre_bourg_bdtopo_2023_4326
                    }
                },
                layers: [ 
                    {
                        id: 'raster-planign',
                        type: 'raster',
                        source: 'raster-planign',
                        layout: {
                            // Make the layer visible by default.
                            visibility: 'none' //ou none
                        },
                    },    
                    {
                        id: 'raster-ortho',
                        type: 'raster',
                        source: 'raster-ortho',
                        layout: {
                            // Make the layer visible by default.
                            visibility: 'visible' //ou none
                        },
                    },
                    {
                        id: 'hillshadeSource',
                        type: 'raster',
                        source: 'hillshadeSource',
                        layout: {
                            // Make the layer visible by default.
                            visibility: 'visible' //ou none
                        },
                    },
                    {
                        id: "commune_4326",
                        source: "commune_4326",
                        //'source-layer': "commune_4326",
                        type: "line", 
                        paint: {
                            'line-color': "violet",
                            'line-width': 2
                        }
                    },
                    {
                        id: "centre_bourg_4326_line",
                        source: "centre_bourg_4326",
                        //'source-layer': 'centre_bourg_4326',
                        type: "line", 
                        paint: {
                            'line-color': "lightblue",
                            'line-width': 2,
                            'line-opacity':0.5
                        }
                    },
                    {
                        id: 'batiment_centre_bourg_bdtopo_2023_4326',
                        source: 'batiment_centre_bourg_bdtopo_2023_4326',
                        //'source-layer': 'batiment_centre_bourg_bdtopo_2023_4326',
                        type: "fill-extrusion", // Changer le type de couche en "fill-extrusion"
                        paint: {                            
                            "fill-extrusion-color": "#ffffff", // Couleur de remplissage extrudée
                            "fill-extrusion-height": ["get", "hauteur"], // Propriété de hauteur à utiliser pour extruder les polygones
                            "fill-extrusion-base": 0, // Hauteur de base de l'extrusion
                            "fill-extrusion-opacity": 0.8
                        }
                    }
                ],
                terrain: {
                    source: 'terrainSource',
                    exaggeration: 1
                }
            }
  });


  //------------------------------------------------------------------------------------------------      
  map.addControl(new maplibregl.NavigationControl(
    {
      visualizePitch: true,
      //showZoom: true,
      //showCompass: true
    }), 
    'top-left'
  );
  
  map.addControl(new maplibregl.FullscreenControl()), 'top-right';

  const scale = new maplibregl.ScaleControl({
    maxWidth: 80,
    unit: 'metric'
  });
  map.addControl(scale);

  
  //------------------------------------------------------------------------------------------------      
  // pris sur un exemple, permet de refaire un coup de propre
  invalidation.then(() => map.remove());
  
  //------------------------------------------------------------------------------------------------
  // Ebauche de menu sur mesure
  
  const mapMenuDiv = document.createElement('div');
  mapMenuDiv.classList.add('map-menu');
  //mapMenuDiv.innerHTML = '<strong>Fond de plan :</strong>';
  map.getContainer().appendChild(mapMenuDiv);
  
  /*const customDivHTML = `
    <div>
        <label for="orthoCheckbox">Orthophoto</label>
        <input type="checkbox" id="orthoCheckbox" unchecked>
    </div>
  `;*/
  const customDivHTML = `
    <strong>Fond de plan :</strong>
    <div class="form-check form-switch">
      <input class="form-check-input" type="checkbox" role="switch" id="orthoCheckbox" checked>
      <label class="form-check-label" for="orthoCheckbox">Photos aériennes</label>
    </div>
  `;
  mapMenuDiv.innerHTML += customDivHTML;
  
  // doc à creuser
  // https://docs.mapbox.com/mapbox-gl-js/example/toggle-layers/
  document.getElementById('orthoCheckbox').addEventListener('change', function(e) {
    e.preventDefault();
    e.stopPropagation();
    
    const valeurCheckbox = document.getElementById('orthoCheckbox').checked;
    if (valeurCheckbox) {
      map.setLayoutProperty('raster-ortho', 'visibility', 'visible');
      map.setLayoutProperty('raster-planign', 'visibility', 'none');
    } else {
      map.setLayoutProperty('raster-ortho', 'visibility', 'none');
      map.setLayoutProperty('raster-planign', 'visibility', 'visible');
    }
  });

  // le map.on load ne marche pas et le yield fait foirer la map...
  // yield map;
  // console.log(map.getStyle().layers);
 
}

```