@echo off
:: Changer le répertoire de travail au dossier contenant _site
cd /d %~dp0_site

:: Démarrer le serveur HTTP avec Python
start python -m http.server 8000

:: Attendre un court instant pour s'assurer que le serveur est démarré
timeout /t 2 /nobreak >nul

:: Ouvrir le navigateur par défaut avec l'URL du site
start http://localhost:8000

:: Attendre que l'utilisateur appuie sur une touche pour fermer
pause