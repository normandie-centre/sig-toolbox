Pour exporter au format word un .qmd (quarto markdown) avec pan doc, coller le fichier custom-reference-doc.docx à la racine de votre projet R
Puis (adapter et) copier cette ligne de commande dans le TERMINAL de Rstudio (ctrl + shift + v pour copier) :

quarto render 20240704_cr_reunion_lancement.qmd --to docx --reference-doc custom-reference-doc.docx

En remplaçant 20240704_cr_reunion_lancement.qmd par votre nom de fichier