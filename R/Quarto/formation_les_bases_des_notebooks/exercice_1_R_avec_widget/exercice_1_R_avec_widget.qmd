---
title: "Atelier Quarto - Exercice 1"
subtitle: "R avec WidgetHTML : tableau, graphe, carte, inline code"
toc: true
format:
  html:
    code-fold: true # les blocs de code seront repliés, NB : on n'est pas obligé de mettre de code, on peut juste afficher les résultats si l'on veut
    code-summary: "Afficher le code" # message pour le symbole de flèche permettant de dérouler le code
    code-tools: true # un bouton pour afficher tout le .qmd
execute: 
  warning: false # ne pas afficher les messages d'avertissement
embed-resources: true # cela permet d'avoir un fichier html qui intègre toutes les ressources
---

## Chargement des librairies

Un premier bloc de code (chunk en anglais) est utilisé pour charger les librairies R qui seront utilisées dans les blocs de code suivant.

```{r}
library(dplyr) # package pour manipuler les données
library(ggplot2) # package pour les graphes
library(plotly) # package pour rendre un ggplot interactif
library(DT) # package pour afficher les résultats dans des tableaux HTML
library(sf) # package géographique de R
library(leaflet) # package pour faire des cartes interactives
# https://github.com/spyrales/gouvdown?tab=readme-ov-file
# pour l'installer taper dans la console :
# install.packages("remotes")
# library(gouvdown) # mise en forme de l'état pour les graphes ggplot
```

## Chargement d'un json et sélection de colonnes

R peut lire tous les formats, le geojson est un classique. R permet ensuite de manipuler ce fichier geojson, par exemple, sélectionner quelques colonnes et supprimer la colonne géométrie.

```{r}
#| output: false

# Fonction st_read du package sf pour lire un geojson
communes_mrn <- st_read("data/communes_mrn.geojson")

# Utilisation du vocabulaire de dplyr pour sélectionner quelques colonnes 
communes_mrn_qqs_colonnes <- communes_mrn %>% 
  st_drop_geometry() %>% 
  select(nom, population, surface_ha)
```

## Affichage d'un tableau

Et voici un affichage sous forme de tableau HTML dans lequel on peut faire des recherches et des tris. Une seule ligne de code est nécessaire !

```{r}
# Affichage du tableau de donnée, oui c'est juste une ligne...
datatable(communes_mrn_qqs_colonnes)
```

## Un premier graphe

Ensuite, il est assez simple de faire un graphe avec la grammaire de base de ggplot.  
Si la syntaxe vous paraît complexe au début, il existe une extension "esquisse" qui propose une interface graphique.  

```{r}
# Utilisation de la librairie ggplot pour faire un graphe 
g <- ggplot(communes_mrn, aes(x = reorder(nom, population), y = population)) +
  geom_bar(stat = "identity", fill = "purple", color = "white") + # identity ou count pour les stats
  coord_flip() +
  labs(
    x = "Région",
    y = "Communes"
  ) + 
  theme_minimal()

# transformation en un graphe interactif
ggplotly(g,width = 600, height = 1200)

```

## Une première carte pour vérifier les géométries

```{r}
# Au préalable, il est nécessaire de reprojeter la couche dans CRS attendu par leaflet
communes_mrn <- st_transform(communes_mrn, crs = 4326)

# Ajout d'une carte leaflet
leaflet(communes_mrn) %>% # on indique la donné
  addTiles() %>% # on charge le fond de plan de base (osm)
  addPolygons()  # on ajoute les polygones de la donnée
```

## Une carte en aplat de couleurs

```{r}
# Calcul de la densité de population avec les fonctions de R
communes_mrn$densite_population = round( communes_mrn$population / communes_mrn$surface_ha )

# Création d'une palette de couleur
palette2 <- colorBin(palette = "Blues", domain = communes_mrn$densite_population, bins = 10)

# Le code ci-dessous est un copier collé de la documentation avec quelques adaptations, cela peut sembler fastidieux mais une fois la logique intégrée, faire des cartes prend le temps de changer le nom de l'indicateur...
leaflet(communes_mrn) %>%
  addTiles() %>% 
  addPolygons(
    fillColor = ~palette2(densite_population),
    weight = 1,
    color = "black",
    fillOpacity = 0.7,
    highlightOptions = highlightOptions(
      weight = 3,
      color = "#666",
      fillOpacity = 0.9,
      bringToFront = TRUE
    ),
    label = ~paste("Commune:", nom, " Densité:", densite_population," habitants par hectare")
  ) %>%
  addLegend(
    pal = palette2,
    values = ~densite_population,
    opacity = 0.7,
    title = "Population",
    position = "bottomright"
  )
```

## Mélanger texte et résultats avec du inline

```{r}
pop_rouen <- communes_mrn  %>% 
  st_drop_geometry() %>% 
  filter(insee_com=='76540')  %>% 
  select(population) %>% 
  format(big.mark = " ")
```

Rouen compte `{r} pop_rouen` habitants.\
Dans cette phrase, le nombre d'habitants est issus directement du code grace à la syntaxe [inline code](https://quarto.org/docs/computations/inline-code.html).
