---
title: "Les bases des notebooks avec QUARTO"
title-slide-attributes:
  data-background-image: images/image_accueil.jpg
  data-background-size: contain
  data-background-opacity: "0.25"
author: "Guillaume CHRETIEN"
date: 2025-01-31
format: 
  revealjs:
    width: 1920
    height: 1080
    #theme: dark
    theme: [default, custom.scss]
    logo: images/BlocMarque_RF-Cerema_horizontal_R.png
    footer: "DTERNC/GVUUB"
    transition: fade
    transition-speed: fast
    background-transition: fade
    controls: true
    slide-number: true
    show-slide-number: all
    embed-resources: true
    #mouse-wheel: true
editor_options: 
  chunk_output_type: inline
lang: fr
---

# Pourquoi un atelier sur les notebooks ?

-   Le format d'atelier permet de partager des techniques rapidement sans la lourdeur et l'investissement d'une formation

-   QUARTO est polyglotte : R, python, observable javascript, html, css, javascript etc...

-   Les notebooks permettent de mélanger facilement du code (traitements, tableaux, graphes, cartes etc...) et du texte (commentaires de chargé d'études !)

-   Un notebook peut être vu comme le chaînon manquant entre un environnement tableur/traitement de texte et une appli complexe avec du bigdata (appli web, Rshiny, python Dash etc...), mais c'est aussi un moyen simple pour faire de la documentation au format web.

-   Et surtout un notebook est :

    -   Dynamique
    -   Reproductible
    -   Multiformat (article, blog, book, website, dashboard en html mais également possibilité de créer des word, pdf, présentation)
    -   Publiable (et responsive design !) notamment chez des hébergeurs gratuit (github pages, quartopub) ou sur un serveur ultrabasique sans aucun paramétrage ni environnement (ex. un simple nginx sur une VM linux)

## Proposition d'utiliser R studio

-   Quarto est un outil en ligne de commande (CLI) qui peut être intégré dans différents environnements
-   R studio intègre quarto avec une interface userfriendly
-   Au préalable, penser à installer R !
-   Aucun paramétrage à faire...

[Télécharger R et R studio](https://posit.co/download/rstudio-desktop/){target="_blank"}

::: callout-tip
Pour l'utiliser avec Visual Studio Code

-   Télécharger R

-   Pour QUARTO, deux possibilités :

    -   Vous avez installé R studio : rajouter simplement le chemin suivant dans les variables d'environnement `C:\Program Files\RStudio\resources\app\bin\quarto\bin` *(à adapter)*

    -   Vous ne comptez pas installer R studio : télécharger et installer [quarto](https://quarto.org/docs/get-started/){target="_blank"}

-   Enfin, dans VSC téléchargez les extensions R et QUARTO
:::

## Atelier : créer son premier notebook

::::: columns
::: {.column width="50%"}
### Au programme :

-   Quelques exemples aboutis\
-   L'interface visuelle de R studio\
-   Le site ressource Quarto\
-   Les bases du markdown et quelques possibilités avancées\
-   Mise en forme (theme, custom css)\
-   Premiers chunks de code (lire une donnée, faire un tableau téléchargeable, un graphe, une carte)\
:::

::: {.column width="50%"}
### Selon le temps et vos envies :

-   Export en html, pdf, docx\
-   Intégration continue\
-   Reveal, dashboard, website, book ou la puissance du yaml pour composer à façon\
-   Possibilités de diffusion (standalone, gitpage, quartopublish etc...)\
-   Exemples avancés de chunk en R, en python, en observable javascript (ojs)\
-   Interactivité en ObservableJavaScript (ojs) ou R shiny\
-   Se connecter à postgres\
-   Démos de sites complets\
:::
:::::

# Le site ressource QUARTO

Le site ressource [Quarto](https://quarto.org/){target="_blank"} est très complet avec plusieurs approches possible pour l'apprentissage (get starded, guide et reference). C'est un outil de travail à ingurgiter en fonction des besoins. Pour les plus curieux, le [forum de discussion](https://github.com/quarto-dev/quarto-cli/discussions){target="_blank"} permet également d'interagir avec d'autres utilisateurs et parfois même l'équipe de développement.

# Création du projet et édition en mode visual

## Créer un nouveau projet

R studio propose des boîtes de dialogue pour créer un nouveau projet quarto :\
- `file\new project`

![](images/paste-1.png){fig-align="center"}

## Créer un nouveau projet (suite)

Pour commencer, un `Quarto project` permet de se familiariser avec les principes de bases.

![](images/paste-2.png){fig-align="center"}

## L'interface R studio

![](images/paste-3.png)

## L'interface R studio (suite) {.scrollable}

+--------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ![](images/paste-5.png){width="144"} | 3 fichiers ont été créé :                                                                                                                                                                                                                                                                                                                                                                  |
|                                      |                                                                                                                                                                                                                                                                                                                                                                                            |
|                                      | \- \_quarto.yml : c'est un fichier de configuration, il va s'appliquer à tous les documents quarto présents dans le répertoire, il est surtout important pour les websites, book, blogs. Le format [**YAML**](https://fr.wikipedia.org/wiki/YAML){target="_blank"} est un standard plutôt lisible pour des fichiers de configuration notamment avec une indentation pour les arboresences. |
|                                      |                                                                                                                                                                                                                                                                                                                                                                                            |
|                                      | \- exercice_1.qmd : c'est le fichier **QuartoMarkDown**, c'est là que tout va se passer et il contient déjà un en-tête en yaml, du texte, un bloc de code.                                                                                                                                                                                                                                 |
|                                      |                                                                                                                                                                                                                                                                                                                                                                                            |
|                                      | -   exercice_1.Rproj : c'est le fichier de sauvegarde du projet Rstudio                                                                                                                                                                                                                                                                                                                    |
+--------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ![](images/paste-4.png)              | Un bouton `render`permet de visualiser le rendu ("avec une option render on save"). Les plus aguerris préfereront taper dans le terminal : `quarto preview exercice_1.qmd` pour prévisualiser et `quarto render exercice_1.qmd` pour faire un rendu complet.                                                                                                                               |
+--------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ![](images/paste-6.png)              | Le terminal permet de taper directement des lignes de commande, les lignes de commandes de Quarto offrent beaucoup de possibilités mais on peut s'en passer pour débuter.                                                                                                                                                                                                                  |
+--------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ![](images/paste-7.png)              | Rstudio propose un onglet environnement très pratique pour visualiser les données ou connexions pour les codes en R                                                                                                                                                                                                                                                                        |
+--------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+
| ![](images/paste-8.png)              | Deux modes d'édition sont possibles : **Source** et **Visual**. Le mode Visual ressemble à un petit traitement de texte et permet de faire des mises en pages sans connaître le markdown. C'est un bon point de départ mais les possibilités de mises en page sont bien plus grandes en mode Source que celles proposées dans ce mode Visual.                                              |
+--------------------------------------+--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+

: Interface Rstudio {tbl-colwidths="\[15,85\]"}

# Les bases du markdown

## Edition en mode visual

Le mode visuel permet de gérer les titres, les puces, les numérotations, gras/italique, tableaux, images, hyperliens et propose quelques mises en forme couramment utilisées mais moins classique comme les blockquote, lineblock, callout, tabset, emoji.

![](images/paste-9.png){fig-align="center"}

::: callout-tip
# Un raccourci bien pratique

taper / pour faire apparaître toutes les possibilités et vous éviter d'aller dans le menu !
:::

## Session de travail

-   Utiliser le mode **visual** et écrire un article en utilisant les possibilités proposées dans les menus.

-   Repasser en mode **Source** et observer la syntaxe, c'est un bon moyen d'apprendre mais le mieux est de vous lancer avec <https://quarto.org/docs/authoring/markdown-basics.html>

-   Pour aller plus loin, essayez de reproduire des parties de ce [notebook démos des possibilités de mises en page](https://app-nc.cerema.fr/atelier_quarto/exercice_0_mises_en_page_no_code.html){target="_blank"}

:::::::::: columns
:::::: {.column width="50%"}
::: callout-tip
# Copier coller des images, oui c'est possible !

En mode visual, vous pouvez copier coller des images à la volée, elles s'enregistreront automatiquement dans un répertoire images (qui se crééra automatiquement si besoin)
:::

::: {.callout-tip collapse="true"}
# le callout Collapsable

Avec la propriété collapse = true, il est possible de rendre le callout collapsable, très pratique pour les encarts *en savoir plus...*

``` markdown
::: {.callout-tip collapse="true"} 
blabla
:::
```
:::

::: callout-tip
# et le HTML ?

Et oui c'est possible aussi, si les possibilités du markdown ne vous suffisent pas... écrivez directement en html !
:::
::::::

::::: {.column width="50%"}
::: callout-tip
# Plusieurs manières de faire des colonnes...

mais la plus simple est d'encapsuler son text dans des blocs de colonnes et de régler la largeur relative des colonnes.

``` markdown
::: columns

::: {.column width="50%"}
blabla
:::

::: {.column width="50%"}
blabla
:::

:::
```
:::

::: callout-tip
# des icons prêt à l'emploi

Quarto intègre bootstrap et [bsicons](https://icons.getbootstrap.com/){target="_blank"}, pour mettre un icon, on peut écrire

``` html
<i class="bi bi-bug"></i>
```

Le résultat sera un joli icon : <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-bug" viewBox="0 0 16 16"> <path d="M4.355.522a.5.5 0 0 1 .623.333l.291.956A5 5 0 0 1 8 1c1.007 0 1.946.298 2.731.811l.29-.956a.5.5 0 1 1 .957.29l-.41 1.352A5 5 0 0 1 13 6h.5a.5.5 0 0 0 .5-.5V5a.5.5 0 0 1 1 0v.5A1.5 1.5 0 0 1 13.5 7H13v1h1.5a.5.5 0 0 1 0 1H13v1h.5a1.5 1.5 0 0 1 1.5 1.5v.5a.5.5 0 1 1-1 0v-.5a.5.5 0 0 0-.5-.5H13a5 5 0 0 1-10 0h-.5a.5.5 0 0 0-.5.5v.5a.5.5 0 1 1-1 0v-.5A1.5 1.5 0 0 1 2.5 10H3V9H1.5a.5.5 0 0 1 0-1H3V7h-.5A1.5 1.5 0 0 1 1 5.5V5a.5.5 0 0 1 1 0v.5a.5.5 0 0 0 .5.5H3c0-1.364.547-2.601 1.432-3.503l-.41-1.352a.5.5 0 0 1 .333-.623M4 7v4a4 4 0 0 0 3.5 3.97V7zm4.5 0v7.97A4 4 0 0 0 12 11V7zM12 6a4 4 0 0 0-1.334-2.982A3.98 3.98 0 0 0 8 2a3.98 3.98 0 0 0-2.667 1.018A4 4 0 0 0 4 6z"/> </svg>
:::
:::::
::::::::::

# Mise en forme

## Theme ou css maison ?

### Une multitude de *themes*

[Aperçu des mises en forme prêtes à l'emploi](https://bootswatch.com/){.external target="_blank"}\
Dans le fichier \_quarto.yaml, rajouter le code suivant pour préciser le theme choisi et expérimentez avec les mises en forme possibles (themes) et essayer en plusieurs (exemple : darkly, quartz, vapor )

``` md
project:
  title: "exercice_1"

format:
  html:
    theme: united
```

### Des mises en forme "maison" (exemple CEREMA).

Une mise en forme personnalisée peut s'appuyer sur un theme existant et ne modifier que le nécessaire, [exemple avec une ossature de site web](https://app-nc.cerema.fr/atelier_quarto/exercice_5_site_web_une_ossature/index.html){.external target="_blank"} :

``` md
project:
  title: "exercice_1"

format:
  html:
    theme: [default, custom.scss]
```

Le fichier custom.scss déposé à la racine du projet contient uniquement les personnalisations nécessaire. Un custom.scss aux couleurs du cerema est déjà prêt à l'emploi.  
*Cette ossature de site web est dispo [ici dans le zip des ressources téléchargeables](https://app-nc.cerema.fr/atelier_quarto/atelier_quarto.zip){.external target="_blank"}*

<!-- ## Le design de l'Etat {.scrollable} -->

<!-- Pour un design de l'Etat tout prêt à l'emploi, il est possible de rajouter ces blocs OJS (à adapter) dans votre .qmd. -->

<!-- -   Bloc ojs 1 : -->

<!-- ```qmd -->

<!-- design = require("https://cdn.observableusercontent.com/npm/@gouvfr/dsfr@1.8.5/dist/dsfr.module.min.js").catch(() => Window) -->

<!-- dsfr_css = require.resolve("https://cdn.observableusercontent.com/npm/@gouvfr/dsfr@1.8.5/dist/dsfr.min.css") -->

<!-- ``` -->

<!-- -   Bloc ojs 2 : -->

<!-- ```qmd -->

<!-- htl.html`<link rel="stylesheet" href="${dsfr_css}">` -->

<!-- ``` -->

<!-- -   Bloc ojs 3 : -->

<!-- ```qmd -->

<!-- htl.html` -->

<!-- <header role="banner" class="fr-header"> -->

<!-- <div class="fr-header__body"> -->

<!-- <div class="fr-container"> -->

<!-- <div class="fr-header__body-row"> -->

<!-- <div class="fr-header__brand fr-enlarge-link"> -->

<!-- <div class="fr-header__brand-top"> -->

<!-- <div class="fr-header__logo"> -->

<!-- <p class="fr-logo"> Préfecture<br>de la <br>Seine-Maritime </p> -->

<!-- </div> -->

<!-- </div> -->

<!-- <div class="fr-header__service">  -->

<!-- <p class="fr-header__service-title">Modèle de document Quarto déployé automatiquement en page Gitlab</p> -->

<!-- <p class="fr-header__service-tagline">Direction Départementale des Territoires et de la Mer de la Seine-Maritime</p> -->

<!-- </div> -->

<!-- </div> -->

<!-- </div> -->

<!-- </div> -->

<!-- </div> -->

<!-- </header> -->

<!-- ` -->

<!-- ``` -->

# Mes premiers CHUNKS de code

On va essayer de reproduire ce [notebook R (avec widget HTML)](https://app-nc.cerema.fr/atelier_quarto/exercice_1_R_avec_widget.html){target="_blank"}

🤯 ne paniquez pas...

## Les options d'exécution

De base, lorsque vous allez mettre vos bouts de code :

-   le code sera affiché (`output: true`)
-   le code s'exécutera (`eval: true`)
-   les messages d'erreurs et d'avertissements seront affichés (`warning: true`)
-   le résultat de votre code s'affichera (ex. un graphe, une carte) (`output: true`)

Deux options s'offrent à vous :

-   Indiquer dans chaque bloc de code le comportement que vous souhaitez avec `true` et `false`
-   Indiquer dans l'en-tête YAML du document comment vous gérer l'ensemble du document, exemple pour l'atelier\

``` md
---
title: "Atelier Quarto - Exercice 1"
subtitle: "R avec WidgetHTML : tableau, graphe, carte, inline code"
toc: true
format:
  html:
    code-fold: true # les blocs de code seront repliés, NB : on n'est pas obligé de mettre de code, on peut juste afficher les résultats si l'on veut
    code-summary: "Afficher le code" # message pour le symbole de flèche permettant de dérouler le code
execute: 
  warning: false # ne pas afficher les messages d'avertissement
---
```

... il y a beaucoup d'autres options plus subtiles dans [la doc Quarto sur les options d'exécution](https://quarto.org/docs/computations/execution-options.html){.external target="_blank"}

## Etape 1 : quelques librairies

:::: panel-tabset
### R

Si ce n'est pas déjà fait, installez les packages ci-dessous, R propose un menu dédié à la recherche et à l'installation des packages : ![](images/paste-12.png)

Utiliser le bouton pour ajouter un chunk de code (en R, python ou ojs) : ![](images/paste-11.png){fig-align="left" height="32px"} Et voilà, on va commencer à coder ⌨️ dans des petits chunks.

```{r}
#| echo: true
#| output: true
#| warning: false
#| output-location: column

library(dplyr) # package pour manipuler les données
library(ggplot2) # package pour les graphes
library(plotly) # package pour rendre un ggplot interactif
library(DT) # package pour afficher les résultats dans des tableaux HTML
library(sf) # package géographique de R
library(leaflet) # package pour les webmap
```

<!-- # pour installer gouvdown taper dans la console -->

<!-- # remotes::install_github("spyrales/gouvdown") -->

<!-- # library(gouvdown) # mise en forme de l'état pour les graphes ggplot -->

### Python

::: callout-important
## Etre admin ou ne pas être...

Une petite ligne de commande préalable pour installer Jupyter. Attention ! Il faut faire un clic droit `ouvrir l'invite de commande en tant qu'administrateur` sinon vous aurez surement une erreur liés aux droits d'écriture...

Et donc pour installer jupyter : `py -m pip install jupyter`\
Puis les librairies nécessaires\
`pip install plotly`\
`pip install geopandas`\
`pip install pandas`\
`pip install mapclassify`\
`pip install folium`
:::

Ajouter un chunk python et commencer à coder :

```{python}
#| echo: true
#| output: true
#| warning: false
#| output-location: column
import geopandas as gpd
import pandas as pd
import plotly.express as px
from IPython.display import display, HTML
import folium
from folium.features import GeoJsonTooltip
```

### ObservableJavascript (OJS)

```{ojs}
//| echo: true
//| warning: false

import { aq, op } from '@uwdata/arquero' // le dplyr ou pandas du javascript
bertin = require("bertin") // le ggplot de la carto en javascript
import {view} from "@neocartocnrs/geoverview" // une fonction pour afficher un geojson

// Pour en savoir plus :
// https://quarto.org/docs/interactive/ojs/examples/arquero.html
// https://riatelab.github.io/bertin/
// https://observablehq.com/d/5758f59e729bc58e
```
::::

## Etape 2 : Lecture et manipulation de la donnée

[Télécharger la donnée communes_mrn.geojson](https://app-nc.cerema.fr/atelier_quarto/data/communes_mrn.geojson)

::: panel-tabset
### R

```{r}
#| echo: true
#| output: false
#| warning: false

# Fonction pour lire un geojson
communes_mrn <- st_read("data/communes_mrn.geojson")

# Utilisation du vocabulaire de dplyr pour sélectionner quelques colonnes 
communes_mrn_qqs_colonnes <- communes_mrn %>% 
  st_drop_geometry() %>% 
  select(nom, population, surface_ha)
```

### Python

```{python}
#| echo: true
#| output: false
#| warning: false

# Fonction pour lire un geojson
communes_mrn = gpd.read_file("data/communes_mrn.geojson")

# Utilisation du vocabulaire de pandas pour sélectionner quelques colonnes 
communes_mrn_qqs_colonnes = communes_mrn.drop(columns='geometry')[['nom', 'population', 'surface_ha']]
```

### ObservableJavascript (OJS)

```{ojs}
//| echo: true
//| warning: false

// Fonction pour lire un geojson
communes_mrn = FileAttachment("data/communes_mrn_crs4326.geojson").json()
// on va manipuler la donnée differement en javascript... cf prochain chunk !
// On commence ici par créer le tableau de donnée correspondant aux propriétés du geojson avec une fonction bien pratique de la librairie bertin
communes_mrn_table = bertin.properties.table(communes_mrn)
```
:::

## Etape 3 : Affichage du tableau de données

::: panel-tabset
### R

```{r}
#| echo: true
#| output: true
#| warning: false
#| output-location: column

# Affichage du tableau de donnée
datatable(communes_mrn_qqs_colonnes)
```

### Python

```{python}
#| echo: true
#| output: true
#| warning: false
#| output-location: column
display(HTML(communes_mrn_qqs_colonnes.to_html()))
print(communes_mrn_qqs_colonnes)
```

### ObservableJavascript (OJS)

```{ojs}
//| echo: true
//| warning: false
//| output-location: column

Inputs.table( 
  communes_mrn_table,
  {
    height: 800,
    columns:["nom", "population"] // choix des colonnes à afficher
  }
)
```
:::

## Prenons un peu de recul !

Ecrire du code peut paraître un peu fastidieux au début... mais ces quelques lignes sont beaucoup plus logiques que :

-   Se demander avec quoi on ouvre un geojson...

-   Se demander comment convertir un geojson en excel

-   Ouvrir dans excel le fichier, sélectionner les colonnes à supprimer, cliquer sur supprimer les colonnes en se demandant si il faudrait pas faire une copie au cas où

-   Faire un copier coller de ce tableau dans une page A4 et se demander comment la mettre en page

-   Se rendre compte vous avez une donnée à corriger... et tout recommencer

L'environnement des notebooks, en plus d'être libre et gratuit, vous permet de gérer absolument tous les formats (xls, csv, json, geojson, gpkg, shp, postgres, parquet etc...) dans un même environnement. Une fois une technique apprise, un simple copier coller vous permet de réinvestir vos connaissances.

Et si vous modifiez la donnée (mise à jour annuelle par exemple)... il suffit de relancer votre notebook pour que tout se mette à jour 🤩.

## Etape 4 : Un premier graphe

::: panel-tabset
### R

```{r}
#| echo: true
#| output: true
#| warning: false
#| output-location: column

# Utilisation de la librairie ggplot pour faire un graphe 
g <- ggplot(communes_mrn, aes(x = reorder(nom, population), y = population)) +
  geom_bar(stat = "identity", fill = "purple", color = "white") + # identity ou count pour les stats
  coord_flip() +
  labs(
    x = "Région",
    y = "Communes"
  ) + 
  theme_minimal()

# transformation en un graphe interactif
ggplotly(g,width = 600, height = 1200)

```

### Python

```{python}
#| echo: true
#| output: true
#| warning: false
#| output-location: column

fig = px.bar(
    communes_mrn.sort_values(by='population', ascending=True),
    y='nom',  # Utiliser y pour les noms des communes
    x='population',  # Utiliser x pour les populations
    title='Population des Communes de la MRN',
    labels={'nom': 'Commune', 'population': 'Population'},
    text='population',
    orientation='h',  # Graphique en barres horizontales
    height=1200
)

fig
```

### ObservableJavascript (OJS)

```{ojs}
//| echo: true
//| warning: false
//| output-location: column

// La librairie plot est intégrée directement
Plot.plot({
  marginLeft: 180,
  x: {axis: "top", grid: true},
  marks: [
    Plot.barX(
      communes_mrn.features.map(d => d.properties), {
        x: "population", 
        y: "nom", 
        tip: "xy",
        sort: {y: "x", reverse: true}
    })
  ]
  
})
```
:::

## La grammaire de la Dataviz

En mal d'inspiration ? Besoin d'idées ?

-   [From data viz, découvrir tous les types de graphes](https://www.data-to-viz.com/){.external target="_blank"}
-   [R graph gallery, les créer en langage R](https://r-graph-gallery.com/index.html){.external target="_blank"}

![](images/clipboard-3323703848.png){fig-align="center" width="450"}

## R et l'extension esquisse

Il est possible de faire des graphes avec des `glisser déposer` et de générer le code correspondant... plus d'excuse pour ne pas se mettre à la dataviz ! [Comment installer et utiliser Esquisse](https://r-graph-gallery.com/package/esquisse.html){.external target="_blank"}

![](images/%7BE63659DF-BFE7-47DE-81CD-B0CB38F84187%7D.png){fig-align="center"}

## Etape 5 : Une première carte pour visualiser les géométries

::: panel-tabset
### R

```{r}
#| echo: true
#| output: true
#| warning: false
#| output-location: column

# Au préalable, il est nécessaire de reprojeter la couche dans CRS attendu par leaflet
# En général les geojson sont directement en 4326 mais ayez en tête que si vous ne voyez pas vos données, c'est surement un problème de projection !
communes_mrn <- st_transform(communes_mrn, crs = 4326)

# Ajout d'une carte leaflet
leaflet(communes_mrn) %>% # on indique la donné
  addTiles() %>% # on charge le fond de plan de base (osm)
  addPolygons()  # on ajoute les polygones de la donnée
```

### Python

```{python}
#| echo: true
#| output: true
#| warning: false
#| output-location: column

# Ajout d'une carte leaflet
communes_mrn.explore()
```

### ObservableJavascript (OJS)

```{ojs}
//| echo: true
//| warning: false
//| output-location: column

// la fonction pratique chargée dans les appels de librairie
view(communes_mrn)
```
:::

## Etape 6 : Carte en aplat de couleur

::: panel-tabset
### R

```{r}
#| echo: true
#| output: true
#| warning: false
#| output-location: column

# Calcul de la densité de population avec les fonctions de R
communes_mrn$densite_population = round( communes_mrn$population / communes_mrn$surface_ha )

# Création d'une palette de couleur
palette2 <- colorBin(palette = "Blues", domain = communes_mrn$densite_population, bins = 10)

# Le code ci-dessous est un copier collé de la documentation avec quelques adaptations, cela peut sembler fastidieux mais une fois la logique intégrée, faire des cartes prend le temps de changer le nom de l'indicateur...
leaflet(communes_mrn) %>%
  addTiles() %>% 
  addPolygons(
    fillColor = ~palette2(densite_population),
    weight = 1,
    color = "black",
    fillOpacity = 0.7,
    highlightOptions = highlightOptions(
      weight = 3,
      color = "#666",
      fillOpacity = 0.9,
      bringToFront = TRUE
    ),
    label = ~paste("Commune:", nom, " Densité:", densite_population," habitants par hectare")
  ) %>%
  addLegend(
    pal = palette2,
    values = ~densite_population,
    opacity = 0.7,
    title = "Population",
    position = "bottomright"
  )
```

### Python

```{python}
#| echo: true
#| output: true
#| warning: false
#| output-location: column

communes_mrn = communes_mrn.to_crs(epsg=4326)

# Créer une carte centrée sur le centroïde
m = folium.Map()

# se caler sur les limites de la couche
bbox = communes_mrn.total_bounds
minx, miny, maxx, maxy = bbox
bounds = [[miny, minx], [maxy, maxx]]
m.fit_bounds(bounds)



# Choroplèthe
chloropeth = folium.Choropleth(
    geo_data=communes_mrn,
    data=communes_mrn,
    columns=["nom", "population"],
    key_on="feature.properties.nom",
    fill_color="Reds",
    bins=10,
    fill_opacity=0.7,
    line_opacity=0.2,
    legend_name="Population / commune",
    highlight=True,
)

# Define the tooltips
tooltip = GeoJsonTooltip(
    fields=['nom', 'population', "insee_com"],
    aliases=['Commune: ', 'Population: ', "Code INSEE:"],
    localize=True,
    sticky=False,
    smooth_factor=0,
    labels=True,
    style="""
        background-color: #F0EFEF;
        border: 2px solid black;
        border-radius: 3px;
        box-shadow: 3px;
        font-size: 12px;
    """,
    max_width=750,
)

chloropeth.geojson.add_child(tooltip)
chloropeth.add_to(m)

# Ajouter un contrôle de couches
folium.LayerControl().add_to(m)

# Afficher la carte
m
```

### ObservableJavascript (OJS)

```{ojs}
//| echo: true
//| warning: false
//| output-location: column

// La librairie bertin est développé par un géographe français et très bien documentée
bertin.draw({
  params: { projection:"Mercator", margin: 50},
  layers: [
    {
      type: "layer",
      geojson: communes_mrn,
      stroke:"black",
      fill: {
        type: "choro",
        values: "population",
        nbreaks: 5,
        method: "jenks",
        colors: "Reds",
        leg_round: -1,
        leg_title: `Population`,
        leg_x: 10,
        leg_y: 10
      },
      fillOpacity: 0.5,
      tooltip: ["$nom","$population"]
    },
    {
      type:"tiles",
      style:"openstreetmap"
    },
  ]
})
```
:::

## Etape 7 : Intégrer des résultats dans une rédaction (Inline code)

::: panel-tabset
### R

```{r}
#| echo: true
#| output: true
#| warning: false
#| output-location: column

pop_rouen <- communes_mrn  %>% 
  st_drop_geometry() %>% 
  filter(insee_com=='76540')  %>% 
  select(population) %>% 
  format(big.mark = " ")

  print(pop_rouen)
```

Rouen compte `{r} pop_rouen` habitants.\
Dans cette phrase, le nombre d'habitants est issus directement du code grace à la syntaxe [inline code](https://quarto.org/docs/computations/inline-code.html){.external target="_blank"}.

![](images/clipboard-846281635.png)

### Python

```{python}
#| echo: true
#| output: true
#| warning: false
#| output-location: column

# Filtre
pop_rouen_df = communes_mrn[communes_mrn['insee_com'] == '76540'][['population']]

# et pour passer d'un dataframe à une simple valeur
pop_rouen_value = f"{pop_rouen_df.iloc[0, 0]:,}".replace(',', ' ')
print(pop_rouen_value)
```

Rouen compte `{python} pop_rouen_value` habitants.\
Dans cette phrase, le nombre d'habitants est issus directement du code grace à la syntaxe [inline code](https://quarto.org/docs/computations/inline-code.html){.external target="_blank"}.

![](images/paste-13.png){width="376"}

### ObservableJavascript (OJS)

```{ojs}
//| echo: true
//| warning: false
//| output-location: column
pop_rouen = bertin.properties.table(communes_mrn)
    .filter(commune => commune.insee_com === '76540')  // Filtrer par code_insee
    .map(commune => commune.population); // récupérer la propriété population
```

Rouen compte `{ojs} pop_rouen` habitants.\
Dans cette phrase, le nombre d'habitants est issus directement du code grace à la syntaxe [inline code](https://quarto.org/docs/computations/inline-code.html){.external target="_blank"}.

![](images/paste-14.png){width="410"}
:::

# Rendu et diffusion

## Rendu de votre projet

-   Le bouton render de Rstudio est un raccourci qui écrit pour vous dans le terminal `quarto preview quarto_presentation_reveal.qmd --to revealjs --presentation --no-watch-inputs --no-browse` selon les options sélectionnées\
-   Pour une prévisualisation qui se met à jour à chaque modification, tapez simplement `quarto preview` (+ préciser le nom du fichier si besoin) dans le terminal et hop votre projet va s'afficher et se mettre à jour à chaque sauvegarde\
-   Pour **exporter tout votre projet** tapez simplement **`quarto render`** (préciser le nom du fichier si besoin) dans le terminal et hop votre projet va être tricoté et prêt à être publié\
-   Nous avons travaillé en html (format par défaut aujourd'hui) mais il est possible de faire des sorties en docx ou pdf (*attention : il faut alors utiliser des librairies comme ggplot et non des widget html !*)

## Diffusion sur le web

Il y a plusieurs possibilités pour diffuser vos résultats sur le web :

-   Si vous avez accès un ftp, vous déposer simplement vos fichiers. La DTERNC a maintenant un serveur exposé internet pour diffuser vos notebooks !\
-   Si vos données sont publiques, des offres gratuites sont également disponibles
    -   Quartopub\
    -   Github pages\
-   Attention, si vos données sont sensibles ou non diffusables grand public, il faudra trouver une autre approche (exemple empaqueter votre site dans un shiny avec mot de passe ou utiliser htpassword sur le serveur de la DTERNC par exemple)

::: callout-tip
Et si vous n'avez pas mis de fonctionnalités avancées, votre html sera autonome lisible tel quel (mail, clef usb, simple fichier sur votre disque dur) !
:::

## Quarto Pub

Vos données sont publiques ? Votre projet fait moins de 100 Mo ? Quarto Pub vous permet de diffuser sur le web !

-   Créer vous un compte sur [Quarto Pub](https://quartopub.com/){.external target="_blank"}
-   Connecter vous à votre espace et laisser la page ouverte
-   Taper `quarto publish quarto-pub` dans le terminal
-   Répondez aux questions posées dans le terminal

Et maintenant, sur la page dans votre compte Quarto Pub vous voyez votre projet avec une adresse web pour le diffuser💓.

## GITLAB en CI/CD

La DDTM76 propose sur son dépôt Gitlab un template de repo pour pouvoir créer une gitlabpages et la publier (nécessité d'avoir un compte cerbérisé et les droits sur l'instance ministérielle) :\
[EZ Quarto GitPage](https://gitlab-forge.din.developpement-durable.gouv.fr/dd/ddtm76/bmcp/ez_quarto_gitpage)

Les principes essentiels :

-   Dans Gitlab : "Settings" \> "CI/CD" \> "Runners", activer la coche "Enable instance runners for this project".
-   Préciser la procédure dans un .gitlab-ci.yml en cohérence avec \_quarto.yaml

# Les ressources : exemples en ligne et codes sources téléchargeables

## Quelques exemples {.scrollable}

[**Téléchargez un .zip de toutes les ressources**](atelier_quarto.zip){target="_blank"}

-   Maîtriser le markdown : [Mises en page : les classiques et quelques bonus](https://app-nc.cerema.fr/atelier_quarto/exercice_0_mises_en_page_no_code.html){target="_blank"}

-   Tableau, graphe, carte, code intégré\
    [R (avec widget HTML)](https://app-nc.cerema.fr/atelier_quarto/exercice_1_R_avec_widget.html){target="_blank"}\
    [Python](https://app-nc.cerema.fr/atelier_quarto/exercice_1bis_python.html){target="_blank"}\
    [Observable JavaScript (ojs)](https://app-nc.cerema.fr/atelier_quarto/exercice_1ter_observable_javascript.html){target="_blank"}

-   Exemple avec interactivité en ojs\
    [Arquero, le dplyr du javascript](https://app-nc.cerema.fr/atelier_quarto/exercice_3_arquero_ojs_interactivite/exercice_3_arquero_ojs_interactivite.html){target="_blank"}\
    [Duckdb et le tout SQL](https://app-nc.cerema.fr/atelier_quarto/exercice_3bis_duckdb_ojs_interactivite/exercice_3bis_duckdb_ojs_interactivite.html){target="_blank"}

-   Multiformat html, docx, pdf :\
    [R et latex](https://app-nc.cerema.fr/atelier_quarto/exercice_2_R_sans_widget_docs_pdf_latex.html){target="_blank"}\
    [R et typst](https://app-nc.cerema.fr/atelier_quarto/exercice_2bis_R_sans_widget_docs_pdf_typst.html){target="_blank"}

-   Lire un .ods, graphe stackbar + dot en ojs : [.ods avec R et OJS](https://app-nc.cerema.fr/atelier_quarto/exercice_4_lire_un_ods_exemple_dpl.html){target="_blank"}

-   Ossature de website avec mise en forme personnalisée :

    -   [template website cerema](https://app-nc.cerema.fr/atelier_quarto/exercice_5_site_web_une_ossature/index.html){target="_blank"}\
    -   [template website dsfr](https://app-nc.cerema.fr/atelier_quarto/exercice_6_site_web_dsfr/index.html){target="_blank"}

-   Jouer avec les paramètres de bases d'Observable Plot pour tuner un graphe : [OJS Plot](https://app-nc.cerema.fr/atelier_quarto/exercice_7_ojs_plot_properties.html){target="_blank"}

# Conclusion

Les notebooks offrent de nombreuses possibilités et la courbe de progression est assez douce si l'on commence par faire de simples articles.

Ils sont particulièrement adaptés pour :

-   la documentation

-   les articles simples de type scientifiques (c'est le standard pour les publications web)

-   les rendus sous forme de sites web ou webbook

-   le travail en équipe aux profils variés (exemple : différentes personnes pour rédiger le contexte et les enjeux, préparer les données sous postgres, faire un traitement python, mettre en place une dataviz, commenter les résultats)

-   les proofs of concept (POC) de dataviz avant de passer sur une archi plus lourde

-   ... et les diaporamas comme celui ci, qui utilise le moteur [reveal.js](https://revealjs.com/){.external target="_blank"}
