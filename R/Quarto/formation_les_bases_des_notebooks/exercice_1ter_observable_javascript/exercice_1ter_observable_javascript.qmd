---
title: "Atelier Quarto - Exercice 1"
subtitle: "Observable JavaScript avec WidgetHTML : tableau, graphe, carte, inline code"
toc: true
format:
  html:
    code-fold: true # les blocs de code seront repliés, NB : on n'est pas obligé de mettre de code, on peut juste afficher les résultats si l'on veut
    code-summary: "Afficher le code" # message pour le symbole de flèche permettant de dérouler le code
    code-tools: true # un bouton pour afficher tout le .qmd
execute: 
  warning: false # ne pas afficher les messages d'avertissement
embed-resources: true # cela permet d'avoir un fichier html qui intègre toutes les ressources
---

## Chargement des librairies

Un premier bloc de code (chunk en anglais) est utilisé pour charger les librairies javascript qui seront utilisées dans les blocs de code suivant.

```{ojs}
import { aq, op } from '@uwdata/arquero' // le dplyr ou pandas du javascript
bertin = require("bertin") // le ggplot de la carto en javascript
import {view} from "@neocartocnrs/geoverview" // une fonction pratique pour visualiser un gejson
```

## Chargement d'un json

Le geojson est un classique du web. La librairie [bertin](https://github.com/riatelab/bertin) est très pratique pour faire des cartes équivalentes à celles de ggplot.

```{ojs}
// ici, on prend un geojson déjà en projection 4326
communes_mrn = FileAttachment("data/communes_mrn_crs4326.geojson").json()
// On commence ici par créer le tableau de donnée correspondant aux propriétés du geojson avec une fonction bien pratique de la librairie bertin
communes_mrn_table = bertin.properties.table(communes_mrn)
```

## Affichage d'un tableau

Et voici un affichage sous forme de tableau HTML.

```{ojs}
// NB : on aurait pu utiliser la librairie DataTables comme en R et python mais l'alternative en observable javascript est très adaptée au notebook et à l'interactivité (pour plus tard...)
// une case pour la fonction recherche
viewof search = Inputs.search(communes_mrn_table)
// le tableau avec les valeurs filtrées
Inputs.table( 
  search,
  {
    height: 800,
    rows: 71,
    layout: "fixed",
    sort: "population",
    reverse: true,
    columns:["nom", "population"] // choix des colonnes à afficher
  }
)
```

## Un premier graphe

La libraire plot d'observable est un peu l'équivalent de ggplot en R. La logique est un peu différente mais, une fois prise en main, elle permet de composer des graphes à façon.

```{ojs}
// La librairie plot est intégrée directement
Plot.plot({
  marginLeft: 180,
  x: {axis: "top", grid: true},
  marks: [
    Plot.barX(
      communes_mrn.features.map(d => d.properties), {
        x: "population", 
        y: "nom", 
        tip: "xy",
        sort: {y: "x", reverse: true}
    })
  ]
})
```

## Une première carte pour vérifier les géométries

```{ojs}
// la fonction pratique chargée dans les appels de librairie
view(communes_mrn)
```

## Une carte en aplat de couleurs

```{ojs}
// La librairie bertin est développé par un géographe français et très bien documentée
bertin.draw({
  params: { projection:"Mercator", margin: 50},
  layers: [
    {
      type: "layer",
      geojson: communes_mrn,
      stroke:"black",
      fill: {
        type: "choro",
        values: "population",
        nbreaks: 5,
        method: "jenks",
        colors: "Reds",
        leg_round: -1,
        leg_title: `Population`,
        leg_x: 10,
        leg_y: 10
      },
      fillOpacity: 0.5,
      tooltip: ["$nom","$population"]
    },
    {
      type:"tiles",
      style:"openstreetmap"
    },
  ]
})
```

## Mélanger texte et résultats avec du inline

```{ojs}
pop_rouen = bertin.properties.table(communes_mrn)
    .filter(commune => commune.insee_com === '76540')  // Filtrer par code_insee
    .map(commune => commune.population); // récupérer la propriété population
```

Rouen compte ${pop_rouen.toLocaleString("fr-FR")} habitants.\
Dans cette phrase, le nombre d'habitants est issus directement du code grace à la syntaxe [inline code](https://quarto.org/docs/computations/inline-code.html).
