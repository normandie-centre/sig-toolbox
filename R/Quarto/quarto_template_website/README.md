Pour un exemple complet : aller voir le quarto de Code Quantum !

Sinon vous pouvez partir de 0 et copier le fichier custom.scss à la racine de votre site puis dans le fichier _quarto.yml ajouter :

format:
  html:  
    theme: [flatly, custom.scss]
