# Shiny

Liste les dashboards créés au Cerema Normandie-Centre :
- [CH Henri Ey](https://cerema-nc.shinyapps.io/ch_henri_ey/) : Dataviz de la consommation énergétique des bâtiments du Centre Hospitalier Henri Ey. En prod.
- [Douanes](https://cerema-med.shinyapps.io/douanes_centre_val_de_loire) : un tableau de bord sur les imports/exports vers l'étranger pour la région Centre-Val de Loire. Pas en prod.
- [Flux de matières CVDL](https://dataviz.cerema.fr/flux_de_matieres_centre_val_de_loire/) : Application de restitution des résultats de l'étude des flux de matières en Centre-Val de Loire en 2014 et 2021. En prod.
- [Gare routière Saint-Sever](https://cerema-nc.shinyapps.io/pem_saint_sever/) : Visualisation des potentiels sites d'implantation de la future gare et indicateurs associés. Archivé.
- [Geofer](https://cerema-med.shinyapps.io/geofer/) : La plateforme du ferroviaire ! Tableau de bord des données ferroviaires. En prod.
- [Gisements CCF](https://dataviz.cerema.fr/gisements_ccf) : Analyse de la dureté foncière sur Communauté de Communes Coeur Côte Fleurie. En prod.
- [gtfs visualisation](https://cerema-nc.shinyapps.io/gtfs/) : Application pour visualiser des gtfs. Pour dev.
- [Haropa](https://cerema-nc.shinyapps.io/HAROPA_interne) : Visualisation de données sur l'étude ZAN pour Haropa. Archivé.
- [Instadiag](https://cerema-med.shinyapps.io/instadiag) : Calcul d'indicateurs à la commune pour faciliter les diagnostics mobilité. En prod.
- [JOP](https://cerema-nc.shinyapps.io/JOP2024_carte_access/) : Étude de l'accessibilité des sites des jeux olympiques de Paris 2024. En prod.
- [MobAuto](https://cerema-med.shinyapps.io/mobauto) : Pour faciliter le traitement MobPro MobSco afin de créer des cartes de flux. En prod.
- [PLF](https://cerema-nc.shinyapps.io/carto-plf) : Implantation d'une plateforme logistique du futur. Pas en prod.
- [Saint-Marcel](https://cerema-med.shinyapps.io/saint_marcel/): Application pour l'étude mobilités sur Saint-Marcel. Abandonné. Pas en prod.
- Scout_template : Contient un template de dashboard Scout.
- [WebGL et vecteur tuilé](https://ffz7dw-thomas-escrihuela.shinyapps.io/vecteur_tuile/) : Test de dashboard appelant une donnée vecteur tuilée créée par tippecanoe et diffusé via mapbox + WebGL. Pour dev.
- [Vulnérabilité énergétique](https://cerema-med.shinyapps.io/vulnerabilite_energetique_en_normandie/) : Statistiques sur la vulnérabilité des ménages normands. En prod.
- [ZAN Guadeloupe](https://cerema-med.shinyapps.io/zan_guadeloupe/) : Artificialisation des sols en Guadeloupe entre 2009 et 2019. En prod.
- [ZAN PETR Loire Beauce](https://cerema-med.shinyapps.io/zan_petr/) : Comparaison de MOS réalisés par TOPOS entre 2006 et 2016;. En prod.
