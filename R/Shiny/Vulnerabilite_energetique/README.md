# Vulnérabilité énergétique des ménages en Normandie

Ce répertoire est le code source de l'application [Vulnérabilité énergétique des ménages en Normandie](https://ssm-ecologie.shinyapps.io/mobilite-vulnerabilite-energie-normandie/) réalisée par le CEREMA pour le compte de la DREAL Normandie.

L’outil réalisé et les indicateurs proposés permettent de disposer d’une première approche de la problématique de la vulnérabilité énergétique des ménages liée à la mobilité.

Vous trouverez dans ce répertoire le code opensource de l'application, construite sur la technologie R-Shiny et publiée sous License MIT.
