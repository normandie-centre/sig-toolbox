
### Glossaire 

<br>
<br>
<br>

<span class="glyphicon glyphicon-play"></span> *Ménage* : au sens statistique du terme, désigne l'ensemble des occupants d'un même logement sans que ces personnes soient nécessairement unies par des liens de parenté ;

<span class="glyphicon glyphicon-play"></span> *Catégorie professionnelle* : sert à la codification du recensement et des enquêtes que l’Insee réalise auprès des ménages. Il existe 8 catégories professionnelles : « Agriculteurs exploitants », « Artisans, commerçants et chefs d'entreprise », « Cadres et professions intellectuelles supérieures », « Professions Intermédiaires », « Employés (fonction publique, administratifs d’entreprise, commerce, personnels des services aux particuliers) », « Ouvriers (ouvriers qualifiés, ouvriers non qualifiés, ouvriers agricoles) », « Retraités » et « Autres personnes sans activité professionnelle » ;

<span class="glyphicon glyphicon-play"></span> *Biactif* : Dans un couple biactif, les deux membres du couple travaillent ;

<span class="glyphicon glyphicon-play"></span> *Ménage vulnérable* : ménage ou individu subissant une dégradation énergétique liée à la mobilité. Dans cette étude, sont considérés comme ménages vulnérables les ménages biactifs ouvriers/employés résidant à plus de 30 kilomètres de leurs lieux de travail;

<span class="glyphicon glyphicon-play"></span> *Revenu médian* : le revenu médian correspond à celui pour lequel 50% des Français gagnent plus et 50% des Français gagnent moins ;

<span class="glyphicon glyphicon-play"></span> *Flux* : concernent les actifs de 15 ans ou plus ayant un emploi. Ils relient leur commune de résidence et la commune dans laquelle ils déclarent travailler.