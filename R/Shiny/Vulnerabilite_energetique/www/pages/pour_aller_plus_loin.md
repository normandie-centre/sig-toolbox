### Pour aller plus loin

<br>
<br>
<br>

L’outil réalisé et les indicateurs proposés permettent de disposer d’une première approche de la problématique de la vulnérabilité énergétique des ménages liée à la mobilité. Ce sujet est cependant très complexe et difficile à appréhender uniquement par la mobilité domicile-travail, les distances parcourues et leurs catégories socioprofessionnelles (revenus annuels du ménage).

En effet, d’autres formes de vulnérabilité existent. En matière de mobilité, d’autres motifs de déplacement peuvent nécessiter de parcourir des distances importantes pour accéder à des services ou des commerces spécialisés, voire dans certaines zones peu denses, juste pour accéder à des services et commerces de base. La vulnérabilité énergétique liée au logement est aussi bien sûr un enjeu important pour le budget des ménages. 

De plus, l’analyse de la vulnérabilité réalisée dans le cadre de cette étude se base sur les revenus médians des catégories socio-professionnelles qui peuvent masquer de grandes disparités. Une connaissance plus fine de la vulnérabilité énergétique d’un territoire nécessite donc des études plus approfondies. D’autres indicateurs proposés par l’INSEE peuvent notamment être exploités tels que le taux d’effort médian lié à la mobilité.

Des solutions existent également pour réduire cette vulnérabilité énergétique liée à la mobilité :
* Proposer des offres de mobilité moins coûteuses que la voiture individuelle (transports en commun, covoiturage, autopartage, vélo, marche… en assurant l’intermodalité) ;
* Travailler sur l’organisation spatiale du territoire et des solutions de démobilité pour réduire les distances entre logements, emplois, services…

Des ressources sont disponibles pour étudier le potentiel de ces solutions et les mettre en œuvre :
* Cellule France mobilités Normandie : https://www.francemobilites.fr/regions/normandie et https://www.expertises-territoires.fr/jcms/pl1_14598/fr/france-mobilites-normandie ;
* Outil Capamob d’aide au diagnostic de mobilité : https://capamob.cerema.fr/
* Les guides et actualités du Cerema en matière de mobilité : https://www.cerema.fr/fr/activites/mobilites
* Les solutions de financement et notamment le fonds vert pour développer le covoiturage : https://aides-territoires.beta.gouv.fr/