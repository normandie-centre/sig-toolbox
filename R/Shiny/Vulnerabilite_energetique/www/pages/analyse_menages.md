### Les ménages dans la région et leurs déplacements

<br>
<br>
<br>

La Région Normandie comprend :
* 3 325 500 habitants ;
* 1 501 936 ménages ;
* 792 528 ménages actifs.
  
On considère un ménage actif à partir du moment ou au moins une personne du ménage est active.
Les personnes résidant en Normandie mais s’étant déclaré travaillant dans les DOM-TOM ou à l’étranger sont exclus. De même, les distances domicile-travail supérieures à 150km sont également exclues.
Les distances sont calculées par la route entre hôtels de villes.

<br> 

#### Les principaux flux domicile-travail en Normandie

<br>

![flux.png](../static/flux.png)

<br>

Les flux représentés concernent les actifs. Ils relient leur EPCI de résidence et l’EPCI dans lequel ils déclarent travailler. Ces données sont issues du fichier des mobilités professionnelles de l’INSEE en 2020.
Pour des questions de lisibilité, seuls les flux supérieurs à 1000 navetteurs/jour sont ici représentés.

Les flux de plus de 2000 navetteurs/jour sont concentrés en Seine-Maritime, essentiellement dans les bassins d’emplois de Rouen et Le Havre, dans le département de l’Eure (bassin d’emplois d’Évreux) et dans le département du Calvados (bassin d’emplois de Caen). Dans la Manche et dans l’Orne, les flux sont plus faibles.

En outre, une partie des navetteurs de la Seine-Maritime et de l’Eure traverse la limite administrative de la région pour travailler en Île-de-France.

<br> 

#### Les ménages actifs résidant à plus de 30 km du lieu de travail

Pour cette analyse, on considère la distance entre la commune de résidence et la commune où la personne référence du ménage déclare travailler.

Cette première représentation permet de constater l’éloignement des bassins de résidence et d’emploi. Les distances les plus importantes correspondent aux zones périphériques des bassins d’emploi à rayonnement régionaux (Rouen, Le Havre, Caen et Evreux) et aux franges franciliennes de la Normandie fortement tournées vers la région parisienne. 

<br>

![menages_30km.png](../static/menages_30km.png)

<br>

|  | Seine-Maritime (76) | Calvados (14) | Eure (27) | Manche (50) | Orne (61) |
|--- |:-: |:-: |:-: |:-: |:-: 
| Nombre ménages | 570 684 | 317 127 | 254 430 | 229 482 | 129 193 |
| Nombre ménages actifs | 300 309 | 166 340 | 144 400 | 118 246 | 63 233 | 
| Nombre de ménages actifs à plus de 30 km du lieu de travail | 37 360 | 18 618 | 34 272 | 10 877 | 6 520 |
| Pourcentage de ménages actifs à plus de 30 km du lieu de travail | 12,4 % | 11,2 % | 23,7 % | 9,2 % | 10,3 % |

<br>

Parmi les départements Normands, on constate sur la carte comme dans le tableau ci-dessus que l’Eure se distingue particulièrement avec plus de 23 % de ménages résidant à plus de 30 km du lieu de travail contre des proportions comprises entre 9 et 12 % pour le reste de la Région.  

<br>