### Méthodologie

<br> 
<br> 
<br> 

#### Caractérisation de la vulnérabilité énergétique des ménages

Pour caractériser les niveaux de vulnérabilité énergétique liée à la mobilité des ménages, il est nécessaire de faire une analyse approfondie de différents facteurs tels que les modes de transport utilisés, la distance et la fréquence des déplacements, les infrastructures et les énergies employées, les comportements et habitudes des individus en matière de déplacement. 

Dans cette étude, il a été retenu le parti de caractériser la vulnérabilité énergétique sous l’angle de la mobilité domicile–travail, en se concentrant sur les ménages biactifs dans la catégorie socioprofessionnelle des ouvriers et des employés. Le choix de cette catégorie est motivé par le niveau de vie médian plus faible du ménage (20 290€ pour les ouvriers et 20 900€ pour les employés). 

L’identification des ménages vulnérables est alors basée sur l’importance des distances parcourues avec un seuil défini à plus de 30 km aller/jour pour chacun des membres du ménage. 

Ces critères permettent d’identifier les ménages les plus impactés par la hausse du coût de la mobilité contrainte.

<br> 
<br> 

#### Données exploitées

Les données utilisées datent de 2020 et ont été spécialement produites par l’INSEE pour les besoins de l’étude. Elles intègrent la Catégorie socioprofessionnelle (CSP) et les flux de déplacement des ménages biactifs (à partir de la base de donnée MobPro).

Pour des raisons de secret statistique, elles ont été agrégées à différentes échelles :  
* Données à la commune : nombre de ménages, avec 4 classes de distance domicile-travail effectuée en voiture par les actifs du ménage : 0-10 km, 10-20 km, 20-30 km, 30 km et + ;
* Données au pseudo-canton : nombre de ménages, avec 4 classes de distance domicile-travail effectuée en voiture par les actifs du ménage : 0-10 km, 10-20 km, 20-30 km, 30 km et +, et 2 classes CSP : ménage avec uniquement des actifs ouvriers/employés (dont le revenu médian est statistiquement plus faible), ménage avec au moins un actif non ouvrier/employé ;
* Données à l'EPCI : mêmes données qu'au pseudo-canton, en filtrant uniquement les ménages biactifs. 

<br> 
<br> 
 
#### Indicateurs représentés

L’outil réalisé propose des analyses à l’échelle du canton et de l’EPCI représentant les indicateurs suivants :  
* Nombre de ménages et pourcentage des ménages résidant à plus de 30 km de leurs lieux de travail,  
* Nombre de ménages biactifs et pourcentage des ménages biactifs résidant à plus de 30 km de leurs lieux de travail,  
* Nombre de ménages biactifs vulnérables et pourcentage de ménages biactifs vulnérables résidant à plus de 30 km de leurs lieux de travail.