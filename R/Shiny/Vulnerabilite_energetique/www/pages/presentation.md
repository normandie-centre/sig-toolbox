### Introduction

<br>
<br>
<br>

En Normandie, comme dans d’autres territoires, le coût de l’énergie pèse fortement sur le budget des ménages et lorsque les distances à parcourir sont importantes, le poids lié à la mobilité peut devenir très important. Un nombre croissant de ménages aux revenus modestes est en difficulté pour faire face à leur facture énergétique de déplacements, et se retrouve aujourd'hui exposé à une nouvelle forme de précarité.
La vulnérabilité énergétique liée à la mobilité est cependant méconnue car peu de données existent. La DREAL Normandie a donc sollicité le Cerema Normandie-Centre pour élaborer un outil de caractérisation de la vulnérabilité énergétique des ménages en fonction des déplacements.

L’enjeu est d’identifier le profil des ménages qui risquent de basculer dans la précarité, notamment les hyper mobiles qui réalisent beaucoup de kilomètres par an, et qui risquent pour certains de voir leur mode de vie transformé par la transition énergétique en cours.  
Le Cerema a donc réalisé un tableau de bord interactif permettant de :  
* Repérer les ménages qui se déplacent beaucoup ;  
* Estimer la part des ménages la plus exposée à la vulnérabilité énergétique liée à la mobilité ;  
* Affiner la connaissance sur la mobilité des ménages biactifs.  
 


