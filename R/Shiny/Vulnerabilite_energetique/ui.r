##########
##  UI  ##
##########
ui <- fluidPage(theme = shinytheme("spacelab"),

  # Initialisation du Loader
  useWaiter(),
  waiterPreloader(html = loader, fadeout = 5, color = transparent(.5)),

  # Chargement du CSS
  tags$head(tags$link(rel = "stylesheet", type = "text/css", href = "css/default.css")),
  tags$head(tags$link(rel = "stylesheet", type = "text/css", href = "css/cerema-loader.css")),
  tags$head(tags$link(rel = "stylesheet", type = "text/css", href = "css/custom_styles.css")),

  # Création de la layout
  navbarPage(
	id = "app_navbar",
	collapsible = TRUE, 
	windowTitle = "Vulnérabilité énergétique en Normandie", 
    title=tagList(
		div(actionLink("see_home", img(src="./static/logo_cerema_dreal.png", width=400, style="padding-top:15px;margin-top:-10px;margin-left:-70px;margin-right:50px;"))),
		tags$link(rel="shortcut icon", href="https://www.cerema.fr/themes/custom/uas_base/favicon.ico"),
	),

    tabPanel("Accueil", icon = icon("home", lib = "glyphicon"), fluidRow(column(8, offset=2, includeMarkdown("www/pages/presentation.md")))),
    tabPanel("Méthodologie", fluidRow(column(8, offset=2, includeMarkdown("www/pages/methodo.md")))),
    tabPanel("Analyse des ménages", fluidRow(column(8, offset=2, includeMarkdown("www/pages/analyse_menages.md")))),

	navbarMenu("Cartographies",
		tabPanel("À l'échelle du canton",
			sidebarLayout(position = "right",
				sidebarPanel(width = 5,
					div(class="sidebarpanel",
						h3(textOutput("canton_actif"), align = "center"),
						br(),
						htmlOutput("canton_description"),
						br(),
						br(),
						plotlyOutput("piechart"),
						imageOutput("graphe_dpt") 
					)
				),
				mainPanel(width = 7,
					div(class = "map-container",
						leafletOutput(outputId = "mymap", height = "100%"),
						div(class = "indic",
							selectizeInput(
								"indic_canton",
								label = "Choix de l'indicateur",
								choices = names(indic_canton),
								options = list(
									placeholder = "Sélectionner un indicateur",
									onInitialize = I('function() { this.setValue(""); }')
								)
							)
						)
					)
				)
			)
		),

		tabPanel("À l'échelle de l'EPCI",
			sidebarLayout(position = "right",
				sidebarPanel(width = 5,
					div(class="sidebarpanel",
						h3(textOutput("epci_actif"), align = "center"),
						br(),
						htmlOutput("epci_description"),
						br(),
						br(),
						plotlyOutput("piechart_epci"),
						## Bloc suivant à décommenter pour réactiver le contrôle de symbologie par l'utilisateur
						# br(),
						# br(),
						# textInput(
						# 	inputId = 'classif',
						# 	label = 'Entrez vos limites de classes séparées par des virgules pour changer la représentation',
						# 	placeholder = 'Par exemple 10,20,30,40,50',
						# 	value = '10,20,30,40,50',
						# ),
						# actionButton("default_symbo", "Revenir à la représentation par défaut"),
					)
				),
				mainPanel(width = 7,
					div(class = "map-container",
						leafletOutput(outputId = "mymap_epci", height = "100%"),
						div(class = "indic",
							selectizeInput(
								"indic_epci",
								label = "Choix de l'indicateur",
								choices = names(indic_epci),
								options = list(
									placeholder = "Sélectionner un indicateur",
									onInitialize = I('function() { this.setValue(""); }')
								)
							)
						)
					)
				)
			)
		)
	),

	tabPanel("Glossaire", fluidRow(column(8, offset=2, includeMarkdown("www/pages/glossaire.md")))),
	tabPanel("Pour aller plus loin",fluidRow(column(8, offset=2, includeMarkdown("www/pages/pour_aller_plus_loin.md")))),
  )
)