suppressMessages(library(dplyr))
suppressMessages(library(htmltools))
suppressMessages(library(leaflet))
suppressMessages(library(plotly))
suppressMessages(library(RColorBrewer))
suppressMessages(library(sf))
suppressMessages(library(shiny))
suppressMessages(library(shinythemes))
suppressMessages(library(waiter))
options("rgdal_show_exportToProj4_warnings" = "none") # mute gdal warnings


#### Waiting screen ####
loader <- tagList(
  includeHTML("www/html/cerema-loader.html"),
  h1(id="please_wait", "Chargement en cours...")
) 
map_waiter <- Waiter$new(id = "mymap", html=loader, color = transparent(.5))

### Input data
cantons <- st_read(dsn = "www/data/cantons_regroupes.geojson")
epci <- st_read(dsn = "www/data/epci.geojson")
departements <- st_read(dsn = "www/data/departements_simplified.geojson")


#### Calcul indicateurs au canton ####
cantons$nb_entre_0_et_10_km <- round(cantons$nb_men_0.10_mén.avec.au.moins.un.non.ouv.emp + cantons$nb_men_0.10_mén.avec.uniquement.ouv.emp)
cantons$nb_entre_10_et_20_km <- round(cantons$nb_men_10.20_mén.avec.au.moins.un.non.ouv.emp + cantons$nb_men_10.20_mén.avec.uniquement.ouv.emp)
cantons$nb_entre_20_et_30_km <- round(cantons$nb_men_20.30_mén.avec.au.moins.un.non.ouv.emp + cantons$nb_men_20.30_mén.avec.uniquement.ouv.emp)
cantons$nb_plus_30_km <- round(cantons$nb_men_30.et.._mén.avec.au.moins.un.non.ouv.emp + cantons$nb_men_30.et.._mén.avec.uniquement.ouv.emp)

cantons$nb_menages <- cantons$nb_entre_0_et_10_km + cantons$nb_entre_10_et_20_km + cantons$nb_entre_20_et_30_km + cantons$nb_plus_30_km 

cantons$pct_plus_30_km <- 100 * cantons$nb_plus_30_km / cantons$nb_menages
cantons$pct_plus_20_km <- 100 * (cantons$nb_plus_30_km + cantons$nb_entre_20_et_30_km) / cantons$nb_menages
cantons$pct_plus_30_km_ouv_emp <- 100 * cantons$nb_men_30.et.._mén.avec.uniquement.ouv.emp / cantons$nb_menages
cantons$pct_plus_20_km_ouv_emp <- 100 * (cantons$nb_men_30.et.._mén.avec.uniquement.ouv.emp + cantons$nb_men_20.30_mén.avec.uniquement.ouv.emp) / cantons$nb_menages

#### Calcul indicateurs à l'EPCI ####
epci$nb_entre_0_et_10_km_biactifs <- round(epci$nb_men_0.10_mén.avec.au.moins.un.non.ouv.emp_OUI + epci$nb_men_0.10_mén.avec.uniquement.ouv.emp_OUI)
epci$nb_entre_10_et_20_km_biactifs <- round(epci$nb_men_10.20_mén.avec.au.moins.un.non.ouv.emp_OUI + epci$nb_men_10.20_mén.avec.uniquement.ouv.emp_OUI)
epci$nb_entre_20_et_30_km_biactifs <- round(epci$nb_men_20.30_mén.avec.au.moins.un.non.ouv.emp_OUI + epci$nb_men_20.30_mén.avec.uniquement.ouv.emp_OUI)
epci$nb_plus_30_km_biactifs <- round(epci$nb_men_30.et.._mén.avec.au.moins.un.non.ouv.emp_OUI + epci$nb_men_30.et.._mén.avec.uniquement.ouv.emp_OUI)

epci$nb_entre_0_et_10_km <- round(epci$nb_men_0.10_mén.avec.au.moins.un.non.ouv.emp_OUI + epci$nb_men_0.10_mén.avec.uniquement.ouv.emp_OUI + epci$nb_men_0.10_mén.avec.au.moins.un.non.ouv.emp_NON + epci$nb_men_0.10_mén.avec.uniquement.ouv.emp_NON)
epci$nb_entre_10_et_20_km <- round(epci$nb_men_10.20_mén.avec.au.moins.un.non.ouv.emp_OUI + epci$nb_men_10.20_mén.avec.uniquement.ouv.emp_OUI + epci$nb_men_10.20_mén.avec.au.moins.un.non.ouv.emp_NON + epci$nb_men_10.20_mén.avec.uniquement.ouv.emp_NON)
epci$nb_entre_20_et_30_km <- round(epci$nb_men_20.30_mén.avec.au.moins.un.non.ouv.emp_OUI + epci$nb_men_20.30_mén.avec.uniquement.ouv.emp_OUI + epci$nb_men_20.30_mén.avec.au.moins.un.non.ouv.emp_NON + epci$nb_men_20.30_mén.avec.uniquement.ouv.emp_NON)
epci$nb_plus_30_km <- round(epci$nb_men_30.et.._mén.avec.au.moins.un.non.ouv.emp_OUI + epci$nb_men_30.et.._mén.avec.uniquement.ouv.emp_OUI + epci$nb_men_30.et.._mén.avec.au.moins.un.non.ouv.emp_NON + epci$nb_men_30.et.._mén.avec.uniquement.ouv.emp_NON)

epci$nb_menages_biactifs <- epci$nb_entre_0_et_10_km_biactifs + epci$nb_entre_10_et_20_km_biactifs + epci$nb_entre_20_et_30_km_biactifs + epci$nb_plus_30_km_biactifs 
epci$nb_menages <- epci$nb_entre_0_et_10_km + epci$nb_entre_10_et_20_km + epci$nb_entre_20_et_30_km + epci$nb_plus_30_km

epci$pct_plus_30_km_biactifs <- 100 * epci$nb_plus_30_km_biactifs / epci$nb_menages_biactifs
epci$pct_plus_20_km_biactifs <- 100 * (epci$nb_plus_30_km_biactifs + epci$nb_entre_20_et_30_km_biactifs) / epci$nb_menages_biactifs
epci$pct_plus_30_km_ouv_emp_biactifs <- 100 * epci$nb_men_30.et.._mén.avec.uniquement.ouv.emp_OUI / epci$nb_menages_biactifs
epci$pct_plus_20_km_ouv_emp_biactifs <- 100 * (epci$nb_men_30.et.._mén.avec.uniquement.ouv.emp_OUI + epci$nb_men_20.30_mén.avec.uniquement.ouv.emp_OUI) / epci$nb_menages_biactifs

epci$pct_plus_30_km <- 100 * epci$nb_plus_30_km / epci$nb_menages
epci$pct_plus_20_km <- 100 * (epci$nb_plus_30_km + epci$nb_entre_20_et_30_km) / epci$nb_menages
epci$pct_plus_30_km_ouv_emp <- 100 * (epci$nb_men_30.et.._mén.avec.uniquement.ouv.emp_OUI + epci$nb_men_30.et.._mén.avec.uniquement.ouv.emp_NON) / epci$nb_menages
epci$pct_plus_20_km_ouv_emp <- 100 * (epci$nb_men_30.et.._mén.avec.uniquement.ouv.emp_OUI + epci$nb_men_20.30_mén.avec.uniquement.ouv.emp_OUI + epci$nb_men_30.et.._mén.avec.uniquement.ouv.emp_NON + epci$nb_men_20.30_mén.avec.uniquement.ouv.emp_NON) / epci$nb_menages


#### Indicateurs ####
## Sont laissés en commentaires les indicateurs initialement proposés en visualisation avant sélection

indic_canton <- c(
  # "Nombre de ménages" = "nb_menages",
  # "Pourcentage de ménages à + de 20 km" = "pct_plus_20_km",
  # "Pourcentage de ménages ouvriers et employés à + de 20 km" = "pct_plus_20_km_ouv_emp",
  "Pourcentage des ménages résidant à plus de 30 km du lieu de travail de la personne de référence du ménage" = "pct_plus_30_km",
  "Pourcentage des ménages vulnérables résidant à plus de 30 km du lieu de travail de la personne de référence du ménage" = "pct_plus_30_km_ouv_emp"
)
indic_epci <- c(
  # "Nombre de ménages" = "nb_menages",
  "Pourcentage des ménages résidant à plus de 30 km du lieu de travail de la personne de référence du ménage" = "pct_plus_30_km",
  # "Pourcentage de ménages ouvriers et employés à + de 30 km" = "pct_plus_30_km_ouv_emp",
  # "Pourcentage de ménages à + de 20 km" = "pct_plus_20_km",
  # "Pourcentage de ménages ouvriers et employés à + de 20 km" = "pct_plus_20_km_ouv_emp",
  
  # "Nombre de ménages biactifs" = "nb_menages_biactifs",
  # "Pourcentage de ménages biactifs à + de 20 km" = "pct_plus_20_km_biactifs",
  # "Pourcentage de ménages biactifs ouvriers et employés à + de 20 km" = "pct_plus_20_km_ouv_emp_biactifs",
  "Pourcentage des ménages biactifs résidant à plus de 30 km de leurs lieux de travail" = "pct_plus_30_km_biactifs",
  "Pourcentage des ménages biactifs vulnérables résidant à plus de 30 km de leurs lieux de travail " = "pct_plus_30_km_ouv_emp_biactifs"
)

bins <- c(0, 10, 20, 30, 40, 100) # classes pour la symbologie
