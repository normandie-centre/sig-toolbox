server <- function(input, output, session) {


  # Message d'accueil
  observeEvent(input$waiter_shown, {    # Attend la fin du loader
    shinyalert("Avertissement", message_accueil, type = "warning", size = "m", html = TRUE)
  }, once = TRUE)


  ####  Reactive Values  ####
  fcommune <- reactive({
    artif[artif$IDCOMTXT == input$com,]
  })

  fkaru <- reactive({
    karu[karu$commune == input$com,]
  })

  findic <- reactive({
    indic[input$indic]
  })

  fslider <- reactive({
    input$slider
  })

  output$couche_slider_gauche <- renderText({
    "Orthos 2010"
  })
  output$couche_slider_droite <- renderText({
    "Orthos 2017"
  })


  ##### VOLET FRICHES ####
  output$mymap_friches <- renderLeaflet({
    leaflet() %>%
    setView(lng = -61.59882311581538, lat = 16.256289588650713, zoom = 16) %>%

    ### Fonds de cartes
    addProviderTiles(providers$Stamen.TonerLite, options = providerTileOptions(noWrap = TRUE, maxZoom = 19), group = "Stamen") %>%
    addTiles(options = WMSTileOptions(maxZoom = 19), group="OpenStreetMap") %>% 
    addTiles("https://wxs.ign.fr/ortho/geoportail/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}", options = WMSTileOptions(tileSize = 256, maxZoom = 19), group = "Orthos") %>%

    addMarkers(data = basol, icon = basol_icon, label=basol$tooltip, group = "Basol") %>%
    addMarkers(data = basias, icon = basias_icon, label=basias$tooltip, group = "Basias") %>%
    addMarkers(data = friches_ademe, label = friches_ademe$tooltip, icon = friches_ademe_icon, group = "Friches Ademe") %>%
    addPolygons(data = secteurs, label = secteurs$tooltip, stroke = TRUE, color = "black", weight = 1, fillColor = "grey", fillOpacity = 0.7, group = "Secteurs d'expérimentation") %>%
    addMarkers(data = fond_friches, icon = fond_friches_icon, label = fond_friches$tooltip, group = "Fonds friches") %>%
    addPolygons(data = karucover_dents_creuses, label = karucover_dents_creuses$tooltip, stroke = FALSE, color = "lightbrown", fillOpacity = 0.7, group = "Délaissé Karucover") %>%
    addPolygons(data = dents_creuses, label = dents_creuses$tooltip, stroke = FALSE, color = ~pal_dents_creuses(dents_creuses_en_2010_texte), fillOpacity = 0.7, group = "Dents creuses") %>%
    addPolygons(data = friches_potentielles, label = friches_potentielles$tooltip, color = "#e632ef", weight = 1, smoothFactor = 0.5, opacity = 1.0, fillOpacity = 0.3, fillColor = "#ad24ad", group = "Friches potentielles") %>%
    addAwesomeMarkers(layerId = friches_potentielles$id, data = st_centroid(friches_potentielles), icon = friches_potentielles_icon, label = friches_potentielles$tooltip, group = "Friches potentielles") %>%


    ### Ajout du groupe de Layers (via le plugin Leaflet) et du LayerControl 
    onRender(

      "function() {

        var data = {
          'overlay': {
            'Données de base': ['Basol', 'Basias', 'Friches Ademe', 'Fonds friches', 'Délaissé Karucover'],
            'Pré-identification friches': ['Friches potentielles', 'Dents creuses', \"Secteurs d'expérimentation\"]
          },
          'basemaps': ['Orthos', 'Stamen', 'OpenStreetMap']
        }

        var layers = {
          'overlay': {
            'Données de base': {},
            'Pré-identification friches': {}
          },
          'basemaps': {}
        };

        this.eachLayer(function(layer) {
            if (layer.groupname) {
              if (data['basemaps'].indexOf(layer.groupname) >= 0) {
                layers['basemaps'][layer.groupname] = layer;
              }
              else {
                for (let [k, v] of Object.entries(data['overlay'])) {
                  if (v.indexOf(layer.groupname) >= 0) {
                    layers['overlay'][k][layer.groupname] = layer;
                  }
                }
              }
            }
        });

        L.control.groupedLayers(layers['basemaps'], layers['overlay']).addTo(this);
      }"
    )
  })

  mymap_friches_proxy <- leafletProxy("mymap_friches")
  mymap_friches_proxy %>% 
    hideGroup("Délaissé Karucover") %>%
    addLegend(
    layerId = "legend_dents_creuses",
    group = "Dents creuses",
    title = "Dents creuses",
    position = "bottomright",
    pal = pal_dents_creuses,
    opacity = 0.7,
    values = dents_creuses$dents_creuses_en_2010_texte,
  )


  ## Texte du panneau friches ####
  output$friches_text <- renderUI ({
    tagList(
      h3("Méthodologie"),
      HTML(texte_friches)
    )
  })


  ## Affichage de la fiche identité de la friche ####
  observeEvent(input$mymap_friches_marker_click, {
    event <- input$mymap_friches_marker_click
    if(event$group != "Friches potentielles") return()
    isolate({mymap_friches_proxy %>% show_info_friche(id = event$id)}) # On affiche la popup associée au site
  })

  ## Boutons "Précédent/Suivant" de la fiche identité ####
  observeEvent(input$bprec, {
    if(current_id > 1) show_info_friche(id = current_id-1)
  })
  observeEvent(input$bsuiv, {
    if(current_id < nrow(friches_potentielles)) show_info_friche(id = current_id+1)
  })




  ####   VOLET ZAN    #####

  ## Affiche une popup lors du changement d'onglet à ZAN ####
  observeEvent(input$main, {
    if (input$main == "Artificialisation 2009-2019") {
      shinyalert("Données utilisées et préconisations d’usage",
        message_artificialisation,
        type = "info", size = "l", html = TRUE, showConfirmButton = TRUE
      )
    }
  }, ignoreInit = TRUE, once = TRUE)


  ## La carte de base avec ou sans slider #####
  observeEvent(fslider(), {
    if (is.null(input$mymap_zoom)) {
      lon = -61.5
      lat = 16.2
      zoom = 10
    }
    else {
      lon = input$mymap_center[1]
      lat = input$mymap_center[2]
      zoom = input$mymap_zoom
    }

    map_waiter$show()
    if (fslider()) {
      output$mymap <- renderLeaflet({
        leaflet() %>% 
        setView(lng = lon, lat = lat, zoom = zoom) %>%
        
        addPolygons(
          data = plu,
          stroke = TRUE,
          color = "black",
          fillColor = ~pal_plu(typezone),
          fillOpacity = 0.2,
          group = "PLU",
          weight = 0.2,
          popup = plu$tooltip,
          highlight = highlightOptions(
            weight = 0.5,
            color = "#666",
            fillOpacity = 0.5,
          )
        ) %>%

        addPolygons(
          data = epci,
          stroke = TRUE,
          fillOpacity = 0,
          color = "red",
          group = "EPCI",
          weight = 0.5,
          popup = epci$tooltip,
        ) %>%        

        addLegend(
          layerId = "legend_plu",
          group = "PLU",
          title = "Zonage PLU",
          position = "bottomleft",
          pal = pal_plu,
          opacity = 0.7,
          values = plu$typezone
        ) %>%
        addLayersControl(
          overlayGroups = c("Karucover", "Artificialisation", "PLU", "EPCI"),
          position = "topright",
          options = layersControlOptions(collapsed = FALSE)
        ) %>%

        clearGroup("Communes") %>%
        hideGroup("PLU") %>%
        hideGroup("EPCI") %>%

        ### Ajout du slider
        onRender(
          "function() {
            var orthos2010 = L.tileLayer('https://wxs.ign.fr/orthohisto/geoportail/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=ORTHOIMAGERY.ORTHOPHOTOS2010&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}').addTo(this);
            var orthos_hr = L.tileLayer('https://wxs.ign.fr/ortho/geoportail/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=HR.ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}').addTo(this);
            
            L.control.sideBySide(orthos2010, orthos_hr).addTo(this);
          }"
        )
      })
    }
    else {
      output$mymap <- renderLeaflet({
        leaflet() %>% 
        setView(lng = lon, lat = lat, zoom = zoom) %>%

        ### Fonds de cartes
        addProviderTiles(providers$Stamen.TonerLite, options = providerTileOptions(noWrap = TRUE), group = "Stamen") %>%
        addTiles("http://wxs.ign.fr/choisirgeoportail/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}", options = WMSTileOptions(tileSize = 256), group = "Orthos") %>%

        addPolygons(
          data = plu,
          stroke = TRUE,
          color = "black",
          fillColor = ~pal_plu(typezone),
          fillOpacity = 0.2,
          group = "PLU",
          weight = 0.2,
          popup = plu$tooltip,
          highlight = highlightOptions(
            weight = 0.5,
            color = "#666",
            fillOpacity = 0.5,
          )
        ) %>%

        addPolygons(
          data = epci,
          stroke = TRUE,
          color = "red",
          fillOpacity = 0,
          group = "EPCI",
          weight = 0.5,
          popup = epci$tooltip,
        ) %>%      

        addLegend(
          layerId = "legend_plu",
          group = "PLU",
          title = "Zonage PLU",
          position = "bottomleft",
          pal = pal_plu,
          opacity = 0.7,
          values = plu$typezone
        ) %>%

        ### LayersControl
        addLayersControl(
          baseGroups = c("Stamen", "Orthos"),
          overlayGroups = c("Karucover","Artificialisation", "PLU", "EPCI"),
          position = "topright",
          options = layersControlOptions(collapsed = FALSE)
        ) %>%

        hideGroup("PLU") %>%
        hideGroup("EPCI")

      })
    }
  })


  ## Event : chargement de l'indicateur d'artificialisation ####
  mymap_proxy <- leafletProxy("mymap")
  observe({
    ind <- artif[[findic()]]
    pal <- colorBin(
      palette = "YlOrRd",
      domain = ind,
      5
    )

    mymap_proxy %>%
    clearGroup("Communes") %>%
    hideGroup("PLU") %>%
    addPolygons(
      data = artif,
      stroke = TRUE,
      color = "white",
      fillColor = ~pal(ind),
      fillOpacity = 0.6,
      group = "Communes",
      weight = 2,
      label = artif$tooltip,
      labelOptions = labelOptions(
        style = list("font-weight" = "normal", padding = "3px 8px"),
        textsize = "15px",
        direction = "auto"
      ),
      highlight = highlightOptions(
        weight = 5,
        color = "#666",
        fillOpacity = 0.7,
      )
    ) %>%
    addLegend(
      layerId = "legend",
      group = "Communes",
      title = names(findic()),
      position = "bottomleft",
      pal = pal,
      opacity = 0.7,
      values = ind
    )
  })


  ## Event : Sélection d'une commune ####
  observe({
    req(input$com)  # nécessite l'instanciation de input$com pour exécution du bloc

    fdata <- fcommune()
    sdata <- fkaru()

    centroide <- fdata %>% st_geometry() %>% st_centroid() %>% .[[1]]

    ## Zoom sur la commune + affichage de Karucover sur celle-ci
    mymap_proxy %>%
    clearGroup("Karucover") %>%
    flyTo(lng = centroide[1], lat = centroide[2], zoom = 13) %>%
    addPolygons(
      data = sdata,
      group = "Karucover",
      stroke = TRUE,
      color = "brown",
      fillColor = ~pal_karu(sdata$dynamique),
      fillOpacity = 0.6,
      weight = 1,
      label = sdata$tooltip,
      labelOptions = labelOptions(
        style = list("font-weight" = "normal", padding = "3px 8px", "background" = "#f1f1f1"),
        textsize = "15px",
        direction = "auto"
      ),
      highlight = highlightOptions(
        weight = 2,
        color = "#666",
        fillOpacity = 0.7,
        bringToFront = TRUE
      )
    ) %>%
    addLegend(
      layerId = "legend_karu",
      group = "Karucover",
      title = "Karucover",
      position = "bottomright",
      pal = pal_karu,
      opacity = 0.7,
      values = sdata$dynamique
    )

    ## Affichage du nom de la commune dans le panneau latéral ####
    output$coucheactive <- renderText({
      fdata$IDCOMTXT
    })

    ## Affichage du camembert d'artificialisation ####
    output$piechart <- renderPlotly({
      data <- data.frame(
        group = c("À usage d'habitat", "À usage d'activité", "À usage mixte","À usage inconnu"),
        value = c(fdata$ARTHAB0919ha[1], fdata$ARTACT0919ha[1], fdata$ARTMIX0919ha[1], fdata$ARTINC0919ha[1])
      )

      fig <- plot_ly(
        data = subset(data, value != 0),
        labels = ~group,
        values = ~value,
        texttemplate="%{value}ha",
        textposition = 'inside',
        type = 'pie'
      ) %>% 
      layout(
        legend = list(orientation = 'h'),
        title = sprintf("Artificialisation 2009-2019 : %s hectares", fdata$NAFART0919ha),
        xaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE),
        yaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE),
        paper_bgcolor = "transparent"
      )
    })

    ## Affichage d'une phrase sur l'artificialisation de la commune
    output$pct_art <- renderText({
      paste0("<strong>", fdata$pct_art, "%</strong> de la commune artificialisée depuis 2009")
    })
  })
}
