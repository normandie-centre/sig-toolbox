### Volet Friches
#### Notice
L'application permet de visualiser sur le territoire de la Guadeloupe différentes sources de données pouvant potentiellement identifier des espaces en friches. Des informations sont disponibles au survol de chaque objet cartographique selon la nature de la base de données utilisée.
Un travail de croisement et d'enrichissement de ces données de base a été ensuite réalisé pour aboutir à une couche de friches potentielles. Une fiche identité pour chaque friche est disponible au clic sur l'entité de cette couche.

#### Données utilisées
5 bases de données sont utilisées : 
- Basol : base de données nationale sur les sites et sols potentiellement pollués appelant une action des pouvoirs publics, produites par le Ministère de la Transition Ecologique
- Basias : base de données de l’inventaire historique des sites industriels et activités de service, produites par le Ministère de la Transition Ecologique et le BRGM
- les friches identifiées par l'Ademe pour le projet de réhabilitations de friches photovoltaïques
- les 6 lauréats du Fonds Friches en Guadeloupe
- les espaces identifiés comme "Délaissé urbain et friches d'activité économique" dans Karucover

#### Friches potentielles
Les données Basias, Basol, Karucover "délaissé" et les parcelles comprenant + de 50% de locaux vacants depuis 5 ans constituent la base des friches potentielles. Celles-ci sont ensuite croisées avec diverses sources de données (fichiers fonciers, plu, données environnement, ...) pour les enrichir et tenter de qualifier plus précisément la friche.

### Volet Artificialisation
#### Notice et précautions d’usage
L'application permet l'affichage d'informations concernant l'artificialisation au survol des communes. Plusieurs indicateurs sont disponibles pour évaluer l'efficacité de l'artificialisation sur le territoire. Il est possible de sélectionner une commune pour obtenir une répartition visuelle des vocations de l'artificialisation, ainsi que les sites qui ont subi un changement d'artificialisation entre 2010 et 2017.

#### Données utilisées
Trois bases de données sont utilisées : 
- la [base d'artificialisation des sols](https://cerema.app.box.com/v/pnb-action7-indicateurs-ff) développée par le Cerema qui compare l'artificialisation des parcelles entre les années 2009 et 2019 à partir des fichiers fonciers et de l’identification du changement d’occupation d’une parcelle au moment de la délivrance du PC. Cette base permet de suivre annuellement l’évolution de la consommation d’espaces (au sens du CU). L’information est retransmise à l’échelle communale et non à l’échelle de la parcelle.
- La [base des évolutions de Karucover](http://www.guadeloupe.developpement-durable.gouv.fr/occupation-du-sol-a-grande-echelle-karucover-a3429.html), mode d'occupation du sol réalisé en 2010 et 2017. Elle permet une connaissance très fine du phénomène (échelle du bâti, distinction couvert et usages).
- les PLU de Guadeloupe publiés sur le [Géoportail de l'urbanisme](https://www.geoportail-urbanisme.gouv.fr/map/#tile=1&lon=-61.531206367883364&lat=16.221597009237897&zoom=10&mlon=-61.722059&mlat=15.992066)

#### Précautions d’usage : mise en perspective des résultats de l’OCSGE et des FF 

Les bases de données *fichiers fonciers* et *Karucover* ont été comparées pour tester les décalages de mesures de l’artificialisation. On constate qu’il existe de fortes disparités entre les deux bases dans les résultats obtenus sur les «surfaces nouvellement artificialisées » sur la période 2010 – 2017.
Sur certaines communes, les résultats de l’observatoire de l’artificialisation ne correspondent pas toujours aux dynamiques constatées sur le terrain.
Des éléments d’explication justifient ces résultats par le mauvais calibrage des fichiers fonciers. En Guadeloupe, de nombreuses parcelles ne correspondent pas à la réalité des découpages. L’artificialisation est donc sur-évaluée. Les fichiers fonciers commencent à être satisfaisants à partir du millésime 2015.  
A ce stade, la DEAL Guadeloupe préfère s’appuyer sur l’OCSGE pour mesurer l’artificialisation. Les résultats de l’observatoire national de l’artificialisation sont néanmoins intégrés.

Les évolutions d'artificialisation des zones de Karucover sont catégorisées comme suit : 
- *Artificialisation* : passage d'un espace NF (naturel ou forestier) en zone artificialisée
- *Désartificialisation* : passage d'une zone artificialisée en espace NF (naturel ou forestier)
- *Défrichement* : passage d'un espace NF en zone à usage agricole
- *Enfrichement* : passage d'une zone à usage agricole en espace NF