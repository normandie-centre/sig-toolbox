source("helpers.R", encoding = "UTF-8")

suppressMessages(library(dplyr))
suppressMessages(library(glue))
suppressMessages(library(leaflet))
suppressMessages(library(htmltools))
suppressMessages(library(htmlwidgets))
suppressMessages(library(plotly))
suppressMessages(library(sf))
suppressMessages(library(shiny))
suppressMessages(library(shinythemes))
suppressMessages(library(shinyalert))
suppressMessages(library(waiter))

options("rgdal_show_exportToProj4_warnings" = "none") # mute gdal warnings


#### Texte ####
message_accueil <- "Cette application permet de visualiser sur le territoire de la Guadeloupe différentes sources de données permettant potentiellement d’identifier des espaces en friches. Elle permet aussi l'affichage d'informations concernant l'artificialisation du sol au survol des communes.<br><br>
Cette application est un prototype en cours de développement. Des bugs peuvent donc apparaître. Dans ce cas, tentez un rafraîchissement de la page.<br><br>
L'actualité et la qualité des données présentées dans l'application dépendent de celles des bases originelles. Pour plus d'informations, visiter la page <b>\"Notice et précautions d'usage\"</b>"

message_artificialisation <- "Trois bases de données sont utilisées :
<ul><li>la base d'artificialisation des sols développée par le Cerema qui compare l'artificialisation des parcelles entre les années 2009 et 2019, à partir des fichiers fonciers</li>
<li>La base des évolutions de Karucover, mode d'occupation du sol réalisé en 2010 et 2017. Elle permet une connaissance très fine du phénomène (échelle du bâti, distinction couvert et usages).</li>
<li>les PLU de Guadeloupe publiés sur le Géoportail de l'urbanisme</li></ul><br>

<h4>Mise en perspective des résultats de l’OCSGE et des FF :</h4>
Les bases de données fichiers fonciers et Karucover ont été comparées pour tester les décalages de mesures de l’artificialisation. On constate qu’il existe de fortes disparités entre les deux bases dans les résultats obtenus sur les «surfaces nouvellement artificialisées » sur la période 2010 – 2017.<br>
Sur certaines communes, les résultats de l’observatoire de l’artificialisation ne correspondent pas toujours aux dynamiques constatées sur le terrain. Des éléments d’explication justifient ces résultats par le mauvais calibrage des fichiers fonciers. En Guadeloupe, de nombreuses parcelles ne correspondent pas à la réalité des découpages. L’artificialisation est donc sur-évaluée.<br>
À ce stade, la DEAL Guadeloupe préfère s’appuyer sur l’OCSGE pour mesurer l’artificialisation. Les résultats de l’observatoire national de l’artificialisation sont néanmoins intégrés."

texte_friches <- "
<p>Des secteurs d'expérimentation ont été définis dans lesquels est appliquée une méthodologie de pré-identification de friches potentielles à partir des données listées ci-dessous. Ces friches sont ensuite enrichies par croisement avec d'autres sources (fichiers fonciers, plu, ...) afin d'améliorer leur caractérisation.</p>  

<h4>Données de base</h4>
<p><ul><li>Basol : base de données nationale sur les sites et sols potentiellement pollués appelant une action des pouvoirs publics, produites par le Ministère de la Transition Ecologique</li> 
<li>Basias : base de données de l’inventaire historique des sites industriels et activités de service, produites par le Ministère de la Transition Ecologique et le BRGM</li> 
<li>les friches identifiées par l'Ademe pour le projet de réhabilitations de friches photovoltaïques</li>
<li>les 6 lauréats du Fonds Friches en Guadeloupe</li>
<li>les espaces identifiés comme \"Délaissé urbain et friches d'activité économique\" dans Karucover</li></ul><p>

<h4>Composition des friches potentielles</h4>
<p>Les friches potentielles sont constituées des sites Basias et Basol, des parcelles des fichiers fonciers comportant au moins 50% de locaux vacants depuis au moins 5 ans, des espaces agricoles non exploités de Karucover en 2010 et 2017 et d'une liste de sites potentiellement en friches fournie par les services de la DEAL.</p>
<p>Les friches sont ensuite enrichies par le calcul de plusieurs indicateurs (fonciers, environnementaux, ...) pour mieux les caractériser.</p>

<p>Une fiche identité est disponible et visible au clic d'une friche potentielle.</p>

<p>Enfin une couche de potentielles \"dents creuses\" est proposée créée à partir de la donnée Karucover.</p>"


#### Waiting screen ####
loader <- tagList(
  includeHTML("www/html/cerema-loader.html"),
  h1(id="please_wait", "Chargement en cours...")
) 
map_waiter <- Waiter$new(id = "mymap", html=loader, color = transparent(.5))


#### Input data ####
artif <- readRDS("www/data/obs_artif_conso_com_2009_2019_971.rds")
karu <- readRDS("www/data/karucover_artificialisation.rds")
plu <- readRDS("www/data/plu.rds")
epci <- readRDS("www/data/epci.rds")
basol <- readRDS("www/data/basol.rds")
basias <- readRDS("www/data/basias.rds")
fond_friches <- readRDS("www/data/fond_friches.rds")
friches_ademe <- readRDS("www/data/friches_ademe.rds")
karucover_dents_creuses <- readRDS("www/data/karucover_dents_creuses.rds")
secteurs <- readRDS("www/data/secteurs.rds")
friches_potentielles <- readRDS("www/data/friches_potentielles.rds")
dents_creuses <- readRDS("www/data/dents_creuses.rds")


#### Couleur et icones des markers de friches ####
couleur_icone <- list()
couleur_icone$basol               <- "blue"
couleur_icone$basias              <- "orange"
couleur_icone$parcelles_vacantes  <- "green"
couleur_icone$karucover           <- "red"
couleur_icone$DEAL                <- "purple"
couleur_icone$EPF                 <- "grey"

icone_friche <- list()
icone_friche$basol               <- "fa-building"
icone_friche$parcelles_vacantes  <- "fa-home"
icone_friche$basias              <- "fa-industry"
icone_friche$karucover           <- "fa-recycle"
icone_friche$DEAL                <- "fa-check"
icone_friche$EPF                 <- "fa-map-pin"


#### Calcul de l'artificialisation en hectares ####
artif$NAFART0919ha <- round(artif$NAFART0919 / 10000)
artif$ARTHAB0919ha <- round(artif$ARTHAB0919 / 10000)
artif$ARTACT0919ha <- round(artif$ARTACT0919 / 10000)
artif$ARTMIX0919ha <- round(artif$ARTMIX0919 / 10000)
artif$ARTINC0919ha <- round(artif$ARTINC0919 / 10000)
# artif$surf <- st_transform(artif, 3857) %>% st_area()
artif$surf <- st_area(artif)
artif$pct_art <- round(artif$NAFART0919 / artif$surf * 100, 1)
dents_creuses$dents_creuses_en_2010_texte <- ifelse(dents_creuses$dents_creuses_en_2010, "Depuis 2010", "Depuis 2017")

#### Infobulles ####
artif$tooltip <- sprintf(
  "<strong>%s</strong><br>
  Flux d'artificialisation 2009-2019 : %d ha<br>
  %.1f%% de la commune artificialisée depuis 2009",
  artif$IDCOMTXT, artif$NAFART0919ha, artif$pct_art
) %>% lapply(htmltools::HTML)
karu$tooltip <- sprintf(
  "<strong>Dynamique d'artificialisation dans Karucover</strong><br>
  En 2010, couverture : \"%s\" et usage : \"%s\"<br>
  En 2017, couverture : \"%s\" et usage : \"%s\"<br>
  %.1f m² en dynamique %s",
  karu$couverture_2010, karu$usage_2010, karu$couverture_2017, karu$usage_2017, karu$surface_m2, karu$dynamique
) %>% lapply(htmltools::HTML)
plu$tooltip <- sprintf(
  "<strong>Zonage PLU : %s</strong><br>
  Libellé : %s<br>
  Partition : %s",
  plu$typezone, plu$libelong, plu$partition
) %>% lapply(htmltools::HTML)
epci$tooltip <- sprintf(
  "<strong>%s</strong>",
  epci$nom_officiel
) %>% lapply(htmltools::HTML)
basol$tooltip <- sprintf(
  "<strong>%s</strong><br>
  Situation actuelle : %s<br>
  Situation technique : %s",
  basol$identification_nom_usuel, basol$environnement_utilisation_actuelle_0, basol$situation_technique
) %>% lapply(htmltools::HTML)
basias$tooltip <- sprintf(
  "<strong>%s</strong><br>
  Site en friche : %s<br>
  Site réaménagé : %s<br>",
  basias$"Raison.sociale", basias$"Site.en.friche", basias$"Site.réaménagé"
) %>% lapply(htmltools::HTML)
fond_friches$tooltip <- sprintf(
  "<strong>%s</strong><br>
  Montant de la subvention : %s<br>
  Surface en m² : %s",
  fond_friches$nom_du_projet, fond_friches$montant_de_la_subvention, fond_friches$surface_en_m2
) %>% lapply(htmltools::HTML)
friches_ademe$tooltip = "Friches Ademe identifiées pour potentielles installations photovoltaiques"
karucover_dents_creuses$tooltip <- "Catégorie [Délaissé urbain et friche d'activité économique] de Karucover"
dents_creuses$tooltip <- sprintf(
  "<strong>Dents creuses issues de Karucover</strong><br>
  Catégorie 2017 : Délaissé urbain et friche d'activité économique<br>
  Catégorie 2010 : %s<br>
  Dent creuse %s",
  dents_creuses$categorie_en_2010, tolower(dents_creuses$dents_creuses_en_2010_texte)
) %>% lapply(htmltools::HTML)
friches_potentielles$tooltip <- sprintf(
  "<strong>%s</strong>",
  friches_potentielles$nom
) %>% lapply(htmltools::HTML)
friches_potentielles$tooltip_parc <- sprintf(
  "<strong>Unité foncière de la friche</strong><br>
  Identifiant : %s<br>
  Surface : %s m²<br>
  Propriétaire : %s",
  friches_potentielles$iduf, friches_potentielles$surf_uf, friches_potentielles$nom_proprietaire
) %>% lapply(htmltools::HTML)


#### Indicateurs ####
indic <- c(
  "Flux d'artificialisation (en ha)" = "NAFART0919ha",
  "Artificialisation à usage d'habitat (en ha)" = "ARTHAB0919ha",
  "Artificialisation à usage d'activité (en ha)" = "ARTACT0919ha",
  "Artificialisation à usage mixte (en ha)" = "ARTMIX0919ha",

  "Pourcentage de la surface communale artificialisée depuis 2009" = "pct_art",

  "Variation population 2012-2017" = "POP1217",
  "Variation ménages 2012-2017" = "MEN1217",
  "Variation emplois 2012-2017" = "EMP1217",

  "M² artificialisés / variation population (2012 à 2017)" = "ARTPOP1217",
  "Evolution ménages / Ha artificialisé pour l'habitat (2012 à 2017)" = "MENHAB1217",
  "Evolution ménages+emplois / Ha artificialisé (2012 à 2017)" = "MEPART1217"
)


#### Symbologies ####
pal_karu <- colorFactor(
  palette = c("red", "orange", "green", "brown"),
  domain = c("Artificialisation", "Défrichement", "Désartificialisation", "Enfrichement")
)
pal_plu <- colorFactor(
  palette = c("#ffff00", "#ffff00", "#ff8888", "#ffabab",  "#feccbe", "#56aa02", "#56aa02", "#e60000"),
  domain = c("A", "Ah", "AU", "AUc", "AUs", "N", "Nd", "U")
)
pal_dents_creuses <- colorFactor(
  palette = c("#892804", "#DE5623"),
  domain = c("Depuis 2010", "Depuis 2017")
)


#### Icons ####
basol_icon <- makeIcon(
  iconUrl = "www/static/basol.svg",
  iconWidth = 18,
  iconHeight = 18
)
basias_icon <- makeIcon(
  iconUrl = "www/static/nuclear.svg",
  iconWidth = 18,
  iconHeight = 18
)
friches_ademe_icon <- makeIcon(
  iconUrl = "www/static/transformateur_electrique.svg",
  iconWidth = 18,
  iconHeight = 18
)
fond_friches_icon <- makeIcon(
  iconUrl = "www/static/fond_friche.svg",
  iconWidth = 18,
  iconHeight = 18
)
friches_potentielles_icon <- makeAwesomeIcon(
  library = "fa",
  iconColor = "black",
  icon = get_icon_friche(friches_potentielles),
  markerColor = get_color_friche(friches_potentielles),
)
