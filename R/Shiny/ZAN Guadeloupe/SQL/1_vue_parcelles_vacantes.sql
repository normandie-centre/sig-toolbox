drop view if exists parcelles_actvac;
create view parcelles_actvac as (
	select *, nactvacant / nlocal::float pourc_lovac, nactvac2a / nlocal::float pourc_locvac2a, nactvac5a / nlocal::float pour_locvac5a 
	from d971_ffta_2020_tup
	where tlocdomin in ('ACTIVITE', 'MIXTE')
	and nactvacant != 0
	and dcntpa > 400 -- surf > 400 m�
);