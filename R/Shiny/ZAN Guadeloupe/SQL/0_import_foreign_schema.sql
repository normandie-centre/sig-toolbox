create extension IF NOT EXISTS postgres_fdw;


----------
-- dv3f_2020 --
----------
drop server if exists dv3f_2020 cascade;
CREATE SERVER dv3f_2020
FOREIGN DATA WRAPPER postgres_fdw
OPTIONS (host '172.28.0.81', port '5432', dbname 'dv3f_2020');

CREATE USER MAPPING FOR "thomas.escrihuela"
SERVER dv3f_2020
OPTIONS (user 'thomas.escrihuela', password 'XXXXXXX');

CREATE USER MAPPING FOR "florian.grillot"
SERVER ff_2020
OPTIONS (user 'florian.grillot', password 'XXXXXXX');

IMPORT FOREIGN SCHEMA dvf_d971
FROM SERVER dv3f_2020 INTO public;

-----------------
-- dv3f_2020 annexe --
-----------------
drop server if exists dv3f_2020_annexe cascade;
CREATE SERVER dv3f_2020_annexe
FOREIGN DATA WRAPPER postgres_fdw
OPTIONS (host '172.28.0.81', port '5432', dbname 'dv3f_2020');

CREATE USER MAPPING FOR "thomas.escrihuela"
SERVER dv3f_2020_annexe
OPTIONS (user 'thomas.escrihuela', password 'XXXXXXX');

IMPORT FOREIGN SCHEMA dvf_annexe
FROM SERVER dv3f_2020 INTO public;

----------
-- ff --
----------
drop server if exists ff_2020 cascade;
CREATE SERVER ff_2020
FOREIGN DATA WRAPPER postgres_fdw
OPTIONS (host '172.28.0.81', port '5432', dbname 'fichiers_fonciers_2020');

CREATE USER MAPPING FOR "thomas.escrihuela"
SERVER ff_2020
OPTIONS (user 'thomas.escrihuela', password 'XXXXXXX');

IMPORT FOREIGN SCHEMA ff_2020_dep
FROM SERVER ff_2020 INTO public;
