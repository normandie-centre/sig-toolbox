----------------------------------------
-- Calcul des indicateurs des friches --
----------------------------------------

set search_path to guadeloupe, public;

--------------------------------------
----- Croisement infos de localisation

-- Communes
update friches_potentielles p
set commune = c.nom_officiel
from communes c
where st_within(st_pointonsurface(p.geom), c.geom);

-- EPCI
update friches_potentielles p
set epci = e.nom_officiel
from epci e
where st_within(st_pointonsurface(p.geom), e.geom);


----------------------------------
----- Croisement fichiers fonciers

-- Affectation de l'id de parcelle
with association_parcelle_friche as (  -- permet de sélectionner la parcelle intersectant la plus grande surface avec la friche
	select distinct on (pf.id) pf.id, t.idtup
	from friches_potentielles pf, tup t
	where st_intersects(pf.geom, t.geomtup)
	order by pf.id, st_area(st_intersection(pf.geom, t.geomtup)) desc
)
update friches_potentielles p 
set iduf = apf.idtup
from association_parcelle_friche apf
where apf.id = p.id;

-- Affectation des infos de parcelle
update friches_potentielles p 
set iduf = t.idtup,
l_idpar = t.idpar_l,
surf_uf = t.dcntpa,
presence_de_bati = case when t.nbat = 0 then false else true end,
surf_bati = t.slocal,
surf_arti = t.dcntarti,
surf_naf = t.dcntnaf 
from tup t
where p.iduf = t.idtup;

-- Affectation des propriétaires
with proprietaires_pour_un_comptprop as ( -- calcul le tableau (liste) des propriétaires pour une parcelle de friche
	select pf.id, pf.iduf idtup, pd.idprocpte, array_agg(distinct(ddenom)) prop, array_agg(distinct(catpro3txt)) type_prop
	from friches_potentielles pf, d971_fftp_2020_proprietaire_droit pd, tup t 
	where pf.iduf = t.idtup 
	and t.idprocpte = pd.idprocpte
	group by pf.id, pf.iduf, pd.idprocpte
)
update friches_potentielles 
set nom_proprietaire = array_to_string(prop, ' & '),
nature_du_proprietaire = array_to_string(type_prop, ' & ')
from proprietaires_pour_un_comptprop p
where iduf = p.idtup;


---------------------------------------
----- Croisement Basol/Basias

-- Affectation des sols pollués de Basol
update friches_potentielles p 
set activite = b.environnement_utilisation_actuelle_0, 
sol_pollue = true
from basol b
where b.num_basol::varchar = p.foreign_id;

-- Affectation de l'état des sites Basias
update friches_potentielles p
set activite = b."Etat occupation", 
en_friche = b."Site en friche", 
site_reamenage = b."Site réaménagé" 
from basias b
where b."Identifiant" = p.foreign_id;


--------------------
----- Croisement plu

with zonage_friche as (
	select distinct on (pf.id) pf.id, p.typezone, p.libelong 
	from friches_potentielles pf, plu p 
	where st_intersects(pf.geom, p.geom)
	order by pf.id, st_area(st_intersection(pf.geom, p.geom)) desc
)
update friches_potentielles p
set zonage_plu = z.typezone,
zonage_plu_description = z.libelong
from zonage_friche z
where z.id = p.id;


------------------
-- Croisement DV3F

with typpro as (
	select distinct(codtyppro), libtyppro
	from ann_typpro
),
mutations_friche as (
	select pf.id, pf.iduf, pf.l_idpar, pf.nom, m.l_idpar, m.libnatmut, m.libtypbien, m.anneemut, m.valeurfonc, m.codtypproa, m.codtypprov, m.nbparmut, m.geomlocmut, m.geomparmut
	from friches_potentielles pf, mutation m
	where pf.l_idpar && m.l_idpar
--	and m.filtre = '0'
	order by pf.id, m.anneemut
),
derniere_mutation_friche as (
	select distinct on (id) *
	from mutations_friche
	order by id, anneemut desc
)
update friches_potentielles pf
set annee_derniere_mutation = dmf.anneemut,
type_derniere_mutation = dmf.libnatmut,
prix_derniere_mutation = dmf.valeurfonc,
acheteur_derniere_mutation = a.libtyppro,
vendeur_derniere_mutation = v.libtyppro
from derniere_mutation_friche dmf left join typpro a on dmf.codtypproa = a.codtyppro left join typpro v on dmf.codtypprov = v.codtyppro
where dmf.id = pf.id;


------------------------------
-- Croisement occsol Karucover
-- Attention requéte qui prend 3 minutes !

-- Karu 2017
with karu_friche as (
	select distinct on (pf.id) pf.id, k.libusn5 
	from friches_potentielles pf, karucover_2017 k 
	where st_intersects(pf.geom, k.geom)
	order by pf.id, st_area(st_intersection(pf.geom, k.geom)) desc
)
update friches_potentielles pf
set occupation_sol_2017 = kf.libusn5
from karu_friche kf
where pf.id = kf.id;

-- Karu 2010
with karu_friche as (
	select distinct on (pf.id) pf.id, k.libusn5 
	from friches_potentielles pf, karucover_2010 k 
	where st_intersects(pf.geom, k.geom)
	order by pf.id, st_area(st_intersection(pf.geom, k.geom)) desc
)
update friches_potentielles pf
set occupation_sol_2010 = kf.libusn5
from karu_friche kf
where pf.id = kf.id;

-------------------
--  Croisement IRIS

update friches_potentielles pf
set taux_logvac_iris = round(i."p18_logvac"::numeric / i."p18_log"::numeric * 100, 1)
from iris i
where st_within(st_centroid(pf.geom), i.geom);


--------------------------------------
-- Croisement zonages environnementaux

update friches_potentielles pf 
set zonage_environnemental = true 
from zones_nat zn
where st_intersects(pf.geom, zn.geom);

update friches_potentielles pf 
set zone_humide = true 
from zones_humides_971 zh
where st_intersects(pf.geom, zh.geom);


with pf_znieff as (
	select distinct on (pf.id) pf.id, z."NOM" nom, z.layer
	from friches_potentielles pf, znieffs z
	where st_intersects(pf.geom, z.geom)
	order by pf.id, st_area(st_intersection(pf.geom, z.geom)) desc, st_area(z.geom)
)
update friches_potentielles pf 
set znieff = pz.nom,
znieff_type = concat('Type ', right(layer, 1))
from pf_znieff pz
where pf.id = pz.id;


---------------------------------
-- Croisement risques inondations

update friches_potentielles pf 
set risque_inondation = true 
from secteurs_inondation si
where st_intersects(pf.geom, si.geom);

with pf_ppri as (
	select distinct on (pf.id) pf.id, rp.prescripti 
	from friches_potentielles pf, reg_pprn rp
	where st_intersects(pf.geom, rp.geom)
	order by pf.id, st_area(st_intersection(pf.geom, rp.geom)) desc
)
update friches_potentielles pf 
set zonage_ppri = pp.prescripti
from pf_ppri pp
where pp.id = pf.id;


------------------------------------
-- Croisement risques technologiques
update friches_potentielles pf 
set risque_technologique = 'Oui'
from pprt_assiette pa
where st_intersects(pf.geom, pa.geom);

with pf_pprt as (
	select distinct on (pf.id) pf.id, pp.type_de_zo  
	from friches_potentielles pf, pprt_perimetre pp
	where st_intersects(pf.geom, pp.geom)
	order by pf.id, st_area(st_intersection(pf.geom, pp.geom)) desc
)
update friches_potentielles pf 
set zonage_pprt = pp.type_de_zo
from pf_pprt pp
where pp.id = pf.id;


------------------------------------
-- Croisement servitudes d'utilité publique
update friches_potentielles pf 
set presence_servitude = 'Oui' 
from sup sup 
where st_intersects(pf.geom, sup.geom);

with sup_contrainte_niveau as (
	select *, CASE 
		WHEN type_contrainte = 'Très contraignant' THEN 4
		WHEN type_contrainte = 'Contraignant' THEN 3
		WHEN type_contrainte = 'Peu contraignant' THEN 2
		WHEN type_contrainte = 'Nulle' THEN 1
		end as niveau
	from sup
),
sup_cont as (
	select distinct on (pf.id) pf.id, scn.niveau , scn.type_contrainte  
	from friches_potentielles pf, sup_contrainte_niveau scn
	where st_intersects(pf.geom, scn.geom)
	order by pf.id, niveau desc
)
update friches_potentielles pf 
set contrainte_de_la_servitude = sc.type_contrainte
from sup_cont sc
where sc.id = pf.id;


---------------------------
-- Croisement routes BDTOPO
with routes_niveau as (
	select *, CASE 
		WHEN nature in ('Route à 1 chaussée', 'Type autoroutier', 'Route à 2 chaussées') THEN 1
		WHEN nature in ('Chemin', 'Route empierrée') THEN 2
		else 3 
	end as niveau
	from troncon_de_route tdr 	
),
croisement_friches_routes as (
	select distinct on (pf.id) pf.id, rn.niveau
	from friches_potentielles pf, routes_niveau rn
	where st_intersects(st_buffer(pf.geom, 100), rn.geom)
	order by pf.id, niveau asc
)
update friches_potentielles fp
set desserte_route = case
	when cfr.niveau = 1 then 'Accès correct'
	when cfr.niveau = 2 then 'Voie dégradée'
	when cfr.niveau = 3 then 'Difficilement accessible'
	else 'Ne sait pas'
end
from croisement_friches_routes cfr
where cfr.id = fp.id;


---------------------------
-- Croisement quais BDTOPO
update friches_potentielles fp
set desserte_maritime = 'oui'
from equipement_de_transport edt
where nature in ('Port', 'Gare maritime')
and nature_detaillee not in ('Port de pêche', 'Port de plaisance')
and st_intersects(st_buffer(fp.geom, 2000), edt.geom);


-- Affichage de la table
select *
from friches_potentielles pf;
