------------------------------------------------------------------------------
-- Cr�ation de la table des IRIS avec informations communales de logements  --
-- par croisement entre IRIS GE de l'IGN et la base logements de l'INSEE    --
------------------------------------------------------------------------------

drop table if exists iris;
create table iris as (
	select ig.geom , ig.insee_com , ig.nom_com , ig."IRIS" , ig.code_iris , ig.nom_iris , ig."TYP_IRIS" , bilc.*
	from iris_ge ig, "base-ic-logement-2018" bilc 
	where bilc.iris = ig.code_iris
)