-----------------------------------
--  Karucover agricole non exploit�

drop table if exists karucover_agricole_non_exploite;
create table karucover_agricole_non_exploite as 
select k.id id_2010, k2.id id_2017, k.libusn5 usage_2010, k2.libusn5 usage_2017, k.geom geom_2010, k2.geom geom_2017
from karucover_2010 k, karucover_2017 k2 
where st_intersects(k.geom, k2.geom)
and k.libusn5 = 'Espace agricole non exploit�'
and k2.libusn5 = 'Espace agricole non exploit�'
and st_intersects(k.geom, k2.geom)
and st_area(st_intersection(k.geom, k2.geom)) / st_area(k2.geom) > 0.8
and st_area(k2.geom) > 1000;

create index on karucover_agricole_non_exploite using gist(geom_2010);
create index on karucover_agricole_non_exploite using gist(geom_2017);

---------------------------
--  Karucover dents creuses

drop table if exists karucover_dents_creuses;
create table karucover_dents_creuses as 
select *
from karucover_2017
where libusn5 = 'D�laiss� urbain et friche d''activit� �conomique'
and st_area(geom) > 400;

create index on karucover_dents_creuses using gist(geom);


alter table karucover_dents_creuses
drop column if exists categorie_en_2010,
drop column if exists dents_creuses_en_2010,
add column categorie_en_2010 varchar default null,
add column dents_creuses_en_2010 boolean default false;

-------------
-- Attention requ�te qui a pris 1h+ sur le serveur du Cerema !!
with karu_2010_union as (
	select libusn5, st_union(geom) geom
	from karucover_2010
	group by libusn5 
), 
dents_creuses_2010_et_2017 as (
	select k.id, d.libusn5
	from karu_2010_union d, karucover_dents_creuses k 
	where st_intersects(k.geom, d.geom)
	and st_area(st_intersection(k.geom, d.geom)) / st_area(k.geom) > 0.8
)
update karucover_dents_creuses k
set dents_creuses_en_2010 = case when d.libusn5 = 'D�laiss� urbain et friche d''activit� �conomique' then true else false end,
categorie_en_2010 = d.libusn5
from dents_creuses_2010_et_2017 d
where k.id = d.id;
