drop table if exists tup;
create table tup as 
select *, st_transform(geomtup, 3857) as geom
from d971_ffta_2020_tup;

drop table if exists guadeloupe.mutation;
create table guadeloupe.mutation as 
select *, st_transform(geompar, 3857) as geom
from mutation

