---------------------------------------------------
-- Cr�ation de la table des friches potentielles --
---------------------------------------------------

set search_path to guadeloupe, public;

DROP TABLE if exists guadeloupe.friches_potentielles cascade;
DROP TABLE if exists guadeloupe.dents_creuses cascade;
CREATE TABLE guadeloupe.friches_potentielles (
	id serial4 NOT NULL,
	geom geometry(multipolygon, 3857) NULL,
	foreign_id varchar NULL,
	nom varchar NULL,
	"type" varchar NULL,
	"source" varchar NULL,
	fiabilite varchar NULL,
	secteur varchar NULL,
	commune varchar NULL,
	epci varchar NULL,
	iduf varchar NULL,
	l_idpar _varchar NULL,
	surf_uf integer NULL,
	nature_du_proprietaire varchar NULL,
	nom_proprietaire varchar NULL,
	adresse varchar NULL,
	presence_de_bati bool NULL,
	surf_bati integer NULL,
	etat_bati varchar NULL,
	surf_arti integer null,
	terrain_viab integer NULL,
	surf_naf integer null,
	activite varchar null,
	nature_act_prec varchar default 'Ne sait pas',
	nature_indus varchar DEFAULT 'Ne sait pas',
	en_friche varchar null,
	site_reamenage varchar null,
	sol_pollue varchar NULL,
	terrain_cloture varchar NULL,
	annee_de_cessation integer NULL,
	/*derniere_occupation varchar NULL,il est plus dans tableau, voir avec thomas*/
	annee_derniere_mutation integer null,
	type_derniere_mutation varchar null,
	prix_derniere_mutation integer NULL,
	acheteur_derniere_mutation varchar null,
	vendeur_derniere_mutation varchar null,
	--taxe_fonciere integer NULL, idem
	--type_de_territoire varchar NULL,
	centre_bourg varchar NULL,
	trame_verte varchar default 'Ne sait pas',
	trame_bleue varchar default 'Ne sait pas',
	zonage_environnemental varchar default 'Hors zone',
	zone_humide varchar default 'Ne sait pas',
	risque_inondation bool default false,
	risque_technologique varchar default 'Non',
	vegetation_sur_le_site varchar NULL,
	desserte_route varchar NULL,
	desserte_tc varchar NULL,
	desserte_fluviale varchar NULL,
	desserte_maritime varchar NULL,
	occupation_sol_2010 varchar NULL,
	occupation_sol_2017 varchar NULL,
	zonage_plu varchar NULL,
	zonage_plu_description varchar null,
	presence_servitude varchar default 'Ne sait pas',
    contrainte_de_la_servitude varchar NULL,
	zonage_ppri varchar NULL,
	zonage_pprt varchar NULL,
	znieff varchar NULL,
	znieff_type varchar null,
	taux_logvac_iris float NULL,
	besoin_en_densification varchar NULL,
	besoin_de_redynamisation varchar NULL,
	besoin_amelioration_esthetique varchar NULL,
	besoin_espace_vert varchar NULL,
	proximite_commerce varchar NULL,
	equipement_service_proche varchar NULL,
	remise_sur_marche varchar NULL,
	occupation_lieu varchar NULL,
	monument_historique varchar NULL,
	paysage varchar NULL,
	valeur_architecturale varchar NULL,
	histoire_sociale varchar NULL,
	existence_etude varchar NULL,
	nature_projet varchar NULL,
	avancement varchar NULL,
	maitrise_douvrage varchar NULL,
	gestion_transitoire_site varchar NULL,
	besoin_territoire varchar NULL,
	enjeu_local_emploi varchar NULL,
	sentiment_habitants varchar NULL,
	volonte_elu_locaux varchar NULL,
	CONSTRAINT friches_potentielles_pkey PRIMARY KEY (id)
);
CREATE TABLE guadeloupe.dents_creuses (LIKE friches_potentielles INCLUDING all);


------------------------------------------------
-- Import des donn�es de friches potentielles --
------------------------------------------------

-- Basol
delete from friches_potentielles where "source" = 'BASOL';
insert into friches_potentielles (foreign_id, nom, secteur, "type", fiabilite, geom, "source")
select b.num_basol, b.identification_nom_usuel, s.nom, 'industrielle', 'moyenne', t.geomtup, 'BASOL'
from basol b, secteurs s, tup t 
where st_within(b.geom, s.geom)
and st_within(b.geom, t.geomtup);

-- Basias
delete from friches_potentielles where "source" = 'BASIAS';
insert into friches_potentielles (foreign_id, nom, secteur, "type", fiabilite, geom, "source")
select b."Identifiant", b."Raison sociale" , s.nom, 'industrielle', 'moyenne', t.geomtup, 'BASIAS' 
from basias b, secteurs s , tup t 
where st_within(b.geom, s.geom)
and st_within(b.geom, t.geomtup);

-- Parcelles actvac5a
delete from friches_potentielles where "source" = 'parcelles avec vacance';
insert into friches_potentielles (foreign_id, nom, secteur, "type", fiabilite, geom, "source")
select p.idtup, p.idtup , s.nom, 'urbaine', 'mauvaise', p.geom, 'parcelles avec vacance'
from parcelles_actvac_5ans_sup50pourcent p, secteurs s 
where st_intersects(p.geom, s.geom);

-- Karucover friches agricoles
delete from friches_potentielles where "source" = 'karucover agricole';
insert into friches_potentielles (foreign_id, nom, secteur, "type", fiabilite, geom, "source")
select kdc.id_2017, kdc.usage_2017, s.nom, 'agricole', 'mauvaise', kdc.geom_2017, 'karucover agricole'
from karucover_agricole_non_exploite kdc , secteurs s 
where st_intersects(kdc.geom_2017, s.geom);

-- Friches DEAL
delete from friches_potentielles where "source" = 'DEAL';
insert into friches_potentielles (foreign_id, nom, secteur, "type", fiabilite, geom, "source")
select fdr.fid, s.nom, s.nom, fdr.type, fdr.surete, t.geomtup, 'DEAL'
from friches_deal fdr , secteurs s, tup t
where st_within(fdr.geom, s.geom)
and st_within(fdr.geom, t.geomtup);

-- Friches habitat EPF
delete from friches_potentielles where "source" = 'EPF';
insert into friches_potentielles (foreign_id, nom, secteur, "type", fiabilite, geom, "source")
select epf.idpar, epf.idpar, s.nom, 'habitat', 'bonne', t.geomtup, 'EPF'
from epf_habitat epf , secteurs s, tup t
where st_within(st_centroid(epf.geom), s.geom)
and st_within(st_centroid(epf.geom), t.geomtup)
and st_area(epf.geom) >= 100
and vac_parcel = 'Vacant' 
and duree_vac != 'Moins de 2 ans';


-- Karucover délaissé friches => dents creuses
alter table dents_creuses add column dents_creuses_en_2010 boolean default false;
alter table dents_creuses add column categorie_en_2010 varchar default false;
delete from dents_creuses where "source" = 'karucover friches';
insert into dents_creuses (foreign_id, nom, secteur, geom, "source", dents_creuses_en_2010, categorie_en_2010)
select kdc.fid, kdc.libusn5 , s.nom, kdc.geom, 'karucover friches', dents_creuses_en_2010, categorie_en_2010
from karucover_dents_creuses kdc , secteurs s 
where st_intersects(kdc.geom, s.geom);

-- EPF habitat => dents creuses
delete from dents_creuses where "source" = 'EPF';
insert into dents_creuses (foreign_id, nom, secteur, geom, "source")
select epf.idpar, epf.idpar, s.nom, t.geomtup, 'EPF'
from epf_habitat epf , secteurs s, tup t
where st_within(st_centroid(epf.geom), s.geom)
and st_within(st_centroid(epf.geom), t.geomtup)
and st_area(epf.geom) >= 400
and vac_parcel = 'Dent creuse' ;

------------------------
-- Cr�ation des index --
------------------------
create index on friches_potentielles using gist(geom);
create index on friches_potentielles using btree(id);
create index on friches_potentielles using btree(nom);
create index on friches_potentielles using btree(foreign_id);
create index on dents_creuses using gist(geom);
create index on dents_creuses using btree(id);
create index on dents_creuses using btree(nom);
create index on dents_creuses using btree(foreign_id);

-- Affichage de la table
select *
from friches_potentielles pf;