set search_path to guadeloupe, public ;

DROP TABLE if exists guadeloupe.zones_nat cascade;
CREATE TABLE guadeloupe.zones_nat (
	id serial4 NOT NULL,
	geom geometry(multipolygon, 3857) NULL,
	"type" varchar NULL,
	CONSTRAINT zones_nat_pkey PRIMARY KEY (id)
);


------------------------------------------------
-- Import des donn�es de zonages naturels --
------------------------------------------------

-- Arr�t� de protection de biotope
delete from zones_nat where "type" = 'Arr�t� de protection de biotope';
insert into zones_nat ("type", geom)
select 'Arr�t� de protection de biotope', apb.geom
from zn_apb apb 
;

-- Terrains du conservatoire du littoral
delete from zones_nat where "type" = 'Terrains du conservatoire du littoral';
insert into zones_nat ("type", geom)
select 'Terrains du conservatoire du littoral', cdl.geom
from zn_cdl cdl
;

-- Parc national
delete from zones_nat where "type" = 'Parc national - coeur' or "type" = 'Parc national - adh�sion';
insert into zones_nat ("type", geom)
select 'Parc national - ' || (case code_r_enp when 'CPN' then 'coeur' when 'AAPN' then 'adh�sion' end), pn.geom
from zn_pn pn
;

-- RAMSAR
delete from zones_nat where "type" = 'RAMSAR';
insert into zones_nat ("type", geom)
select 'RAMSAR', ramsar.geom
from zn_ramsar ramsar
;

-- R�serve de biosph�re
delete from zones_nat where "type" = 'R�serve de biosph�re - zone centrale' or "type" = 'R�serve de biosph�re - zone tampon' ;
insert into zones_nat ("type", geom)
select 'R�serve de biosph�re - ' || (case nom_site when 'Archipel de Guadeloupe (zone centrale)' then 'zone centrale' 
when 'Archipel de Guadeloupe (zone tampon)' then 'zone tampon' 
when 'Archipel de Guadeloupe (zone de transition)' then 'zone de transition' end), mab.geom
from zn_mab mab
;

-- R�serve naturelle nationale
delete from zones_nat where "type" = 'R�serve naturelle nationale';
insert into zones_nat ("type", geom)
select 'R�serve naturelle nationale', rnn.geom
from zn_rnn rnn
;

