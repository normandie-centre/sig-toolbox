library(duckdb)
library(DBI)

con <- dbConnect(duckdb::duckdb(), dbdir = "www/data/pem_saint_sever.duckdb", read_only = FALSE)
dbExecute(con, "INSTALL 'spatial';")
dbExecute(con, "LOAD 'spatial';")

metropole <- st_read("www/data/pem_saint_sever.gpkg", layer = "metropole")
itineraires <- st_read("www/data/pem_saint_sever.gpkg", layer = "itineraires")
stops <- st_read("www/data/pem_saint_sever.gpkg", layer = "stops")
gares_potentielles <- st_read("www/data/pem_saint_sever.gpkg", layer = "gares_potentielles")

# g <- st_read("www/data/pem_saint_sever.gpkg", layer = "metropole")
dbWriteTable(con, "metropole", metropole, overwrite = TRUE)
dbWriteTable(con, "itineraires", itineraires, overwrite = TRUE)
dbWriteTable(con, "stops", stops, overwrite = TRUE)
dbWriteTable(con, "gares_potentielles", gares_potentielles, overwrite = TRUE)

dbDisconnect(con, shutdown=TRUE)
