##########
##  UI  ##
##########
ui <- fluidPage(theme = shinytheme("spacelab"),

	# Initialisation du Loader
	useWaiter(),
	waiterPreloader(html = loader, fadeout = 5, color = transparent(.5)),

	# Chargement du CSS
	tags$head(tags$link(rel = "stylesheet", type = "text/css", href = "css/default.css")),
	tags$head(tags$link(rel = "stylesheet", type = "text/css", href = "css/cerema-loader.css")),
	tags$head(tags$link(rel = "stylesheet", type = "text/css", href = "css/custom_styles.css")),

	# Création de la layout
	navbarPage(
		id = "app_navbar",
		collapsible = TRUE, 
		windowTitle = "EIT en Centre Val-de-Loire",
		title=tagList(
                # div(actionLink("see_home", img(src="https://www.cerema.fr/themes/custom/uas_base/favicon.ico", width=400, style="padding-top:15px;margin-top:-10px;margin-left:-70px;margin-right:50px;"))),
                tags$link(rel="shortcut icon", href="https://www.cerema.fr/themes/custom/uas_base/favicon.ico"),
        ),
		tabPanel("Échelle nationale", 
			fluidRow(
				column(2, selectizeInput("annee_nat", label = "Sélectionnez une année", choices = annees)),
				column(2, selectizeInput("impexp_nat", label = "Import ou export", choices = c("Import", "Export"))),
				column(2, selectizeInput("pays_nat", label = "Sélectionnez un pays", choices = c("Tous les pays", export_national_2021$libelle_pays %>% unique() %>% sort()), selected = "Tous les pays"))
			),
			uiOutput("laius_nat"),
			br(),
			DTOutput("table_nat"),
			plotlyOutput("graph")
		),
		tabPanel("Sur le Centre Val-de-Loire",
			br(),br(),br(),
			fluidRow(
				column(2, selectizeInput("annee_reg", label = "Sélectionnez une année", choices = annees)),
				column(2, selectizeInput("impexp_reg", label = "Import ou export", choices = c("Import", "Export"))),
				column(2, selectizeInput("pays_reg", label = "Sélectionnez un pays", choices = c("Tous les pays", export_regional_2021$libelle_pays %>% unique() %>% sort()), selected = "Tous les pays"))
			),
			br(),
			uiOutput("laius_reg"),
			br(),
			DTOutput("table_reg")
		)
		# tabPanel("Téléchargement", 
		# 	h2("National"),
		# 	br(),
		# 	fluidRow(
		# 		column(2, downloadButton('dl_export_nat_2021','Exports depuis la France en 2021')),
		# 		column(2, downloadButton('dl_import_nat_2021','Imports vers la France en 2021')),
		# 		column(2, downloadButton('dl_export_nat_2020','Exports depuis la France en 2020')),
		# 		column(2, downloadButton('dl_import_nat_2020','Imports vers la France en 2020')),
		# 		column(2, downloadButton('dl_export_nat_2019','Exports depuis la France en 2019')),
		# 		column(2, downloadButton('dl_import_nat_2019','Imports vers la France en 2019')),
		# 	),
		# 	br(),
		# 	fluidRow(
		# 		column(2, downloadButton('dl_export_nat_2018','Exports depuis la France en 2018')),
		# 		column(2, downloadButton('dl_import_nat_2018','Imports vers la France en 2018')),
		# 		column(2, downloadButton('dl_export_nat_2017','Exports depuis la France en 2017')),
		# 		column(2, downloadButton('dl_import_nat_2017','Imports vers la France en 2017')),
		# 		column(2, downloadButton('dl_export_nat_2016','Exports depuis la France en 2016')),
		# 		column(2, downloadButton('dl_import_nat_2016','Imports vers la France en 2016')),
		# 	),	
		# 	br(),
		# 	fluidRow(
		# 		column(2, downloadButton('dl_export_nat_2015','Exports depuis la France en 2015')),
		# 		column(2, downloadButton('dl_import_nat_2015','Imports vers la France en 2015')),
		# 		column(2, downloadButton('dl_export_nat_2014','Exports depuis la France en 2014')),
		# 		column(2, downloadButton('dl_import_nat_2014','Imports vers la France en 2014'))
		# 	),
		# 	br(),
		# 	h2("Région Centre Val-de-Loire"),
		# 	br(),
		# 	fluidRow(
		# 		column(2, downloadButton('dl_export_reg_2021','Exports depuis région CVDL en 2021')),
		# 		column(2, downloadButton('dl_import_reg_2021','Imports vers région CVDL en 2021')),
		# 		column(2, downloadButton('dl_export_reg_2020','Exports depuis région CVDL en 2020')),
		# 		column(2, downloadButton('dl_import_reg_2020','Imports vers région CVDL en 2020')),
		# 		column(2, downloadButton('dl_export_reg_2019','Exports depuis région CVDL en 2019')),
		# 		column(2, downloadButton('dl_import_reg_2019','Imports vers région CVDL en 2019')),
		# 	),
		# 	br(),
		# 	fluidRow(
		# 		column(2, downloadButton('dl_export_reg_2018','Exports depuis région CVDL en 2018')),
		# 		column(2, downloadButton('dl_import_reg_2018','Imports vers région CVDL en 2018')),
		# 		column(2, downloadButton('dl_export_reg_2017','Exports depuis région CVDL en 2017')),
		# 		column(2, downloadButton('dl_import_reg_2017','Imports vers région CVDL en 2017')),
		# 		column(2, downloadButton('dl_export_reg_2016','Exports depuis région CVDL en 2016')),
		# 		column(2, downloadButton('dl_import_reg_2016','Imports vers région CVDL en 2016')),
		# 	),	
		# 	br(),
		# 	fluidRow(
		# 		column(2, downloadButton('dl_export_reg_2015','Exports depuis région CVDL en 2015')),
		# 		column(2, downloadButton('dl_import_reg_2015','Imports vers région CVDL en 2015')),
		# 		column(2, downloadButton('dl_export_reg_2014','Exports depuis région CVDL en 2014')),
		# 		column(2, downloadButton('dl_import_reg_2014','Imports vers région CVDL en 2014'))
		# 	),
		# 	br(),
		# )
	)
)