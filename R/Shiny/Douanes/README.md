# Données douanes en Centre-Val de Loire

Ce répertoire est le code source de l'application [Douanes en Centre-Val de Loire](https://cerema-med.shinyapps.io/douanes_centre_val_de_loire/) réalisée par le CEREMA.

L’outil réalisé et les indicateurs proposés permettent de disposer d’une première approche des échanges transfrontaliers avec la région Centre-Val de Loire.

Vous trouverez dans ce répertoire le code opensource de l'application, construite sur la technologie R-Shiny et publiée sous License MIT.
