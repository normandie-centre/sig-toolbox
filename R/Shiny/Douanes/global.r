suppressMessages(library(arrow))
suppressMessages(library(dplyr))
suppressMessages(library(DT))
suppressMessages(library(glue))
suppressMessages(library(plotly))
suppressMessages(library(shiny))
suppressMessages(library(shinythemes))
suppressMessages(library(waiter))
options("rgdal_show_exportToProj4_warnings" = "none") # mute gdal warnings

#### Fonctions ####
prettifyNumber <- function(number, decimal = 0) {
  return(number %>% as.numeric() %>% round(decimal) %>% prettyNum(big.mark = " ", scientific = F))
}

#### Waiting screen ####
loader <- tagList(
  includeHTML("www/html/cerema-loader.html"),
  h1(id="please_wait", "Chargement en cours...")
) 
map_waiter <- Waiter$new(id = "mymap", html=loader, color = transparent(.5))

annees <- c("2021", "2020", "2019", "2018", "2017", "2016", "2015", "2014")

### Input data
export_national_2021 <- read_parquet("www/data//export_national_2021.parquet") %>% select(-"code_pays", -"code_nc8", -"code_cpf6", -"code_a129")
import_national_2021 <- read_parquet("www/data//import_national_2021.parquet") %>% select(-"code_pays", -"code_nc8", -"code_cpf6", -"code_a129")
export_regional_2021 <- read_parquet("www/data//export_regional_2021.parquet") %>% select(-"code_pays", -"code_cpf4", -"code_a129")
import_regional_2021 <- read_parquet("www/data//import_regional_2021.parquet") %>% select(-"code_pays", -"code_cpf4", -"code_a129")
export_national_2020 <- read_parquet("www/data//export_national_2020.parquet") %>% select(-"code_pays", -"code_nc8", -"code_cpf6", -"code_a129")
import_national_2020 <- read_parquet("www/data//import_national_2020.parquet") %>% select(-"code_pays", -"code_nc8", -"code_cpf6", -"code_a129")
export_regional_2020 <- read_parquet("www/data//export_regional_2020.parquet") %>% select(-"code_pays", -"code_cpf4", -"code_a129")
import_regional_2020 <- read_parquet("www/data//import_regional_2020.parquet") %>% select(-"code_pays", -"code_cpf4", -"code_a129")
export_national_2019 <- read_parquet("www/data//export_national_2019.parquet") %>% select(-"code_pays", -"code_nc8", -"code_cpf6", -"code_a129")
import_national_2019 <- read_parquet("www/data//import_national_2019.parquet") %>% select(-"code_pays", -"code_nc8", -"code_cpf6", -"code_a129")
export_regional_2019 <- read_parquet("www/data//export_regional_2019.parquet") %>% select(-"code_pays", -"code_cpf4", -"code_a129")
import_regional_2019 <- read_parquet("www/data//import_regional_2019.parquet") %>% select(-"code_pays", -"code_cpf4", -"code_a129")
export_national_2018 <- read_parquet("www/data//export_national_2018.parquet") %>% select(-"code_pays", -"code_nc8", -"code_cpf6", -"code_a129")
import_national_2018 <- read_parquet("www/data//import_national_2018.parquet") %>% select(-"code_pays", -"code_nc8", -"code_cpf6", -"code_a129")
export_regional_2018 <- read_parquet("www/data//export_regional_2018.parquet") %>% select(-"code_pays", -"code_cpf4", -"code_a129")
import_regional_2018 <- read_parquet("www/data//import_regional_2018.parquet") %>% select(-"code_pays", -"code_cpf4", -"code_a129")
export_national_2017 <- read_parquet("www/data//export_national_2017.parquet") %>% select(-"code_pays", -"code_nc8", -"code_cpf6", -"code_a129")
import_national_2017 <- read_parquet("www/data//import_national_2017.parquet") %>% select(-"code_pays", -"code_nc8", -"code_cpf6", -"code_a129")
export_regional_2017 <- read_parquet("www/data//export_regional_2017.parquet") %>% select(-"code_pays", -"code_cpf4", -"code_a129")
import_regional_2017 <- read_parquet("www/data//import_regional_2017.parquet") %>% select(-"code_pays", -"code_cpf4", -"code_a129")
export_national_2016 <- read_parquet("www/data//export_national_2016.parquet") %>% select(-"code_pays", -"code_nc8", -"code_cpf6", -"code_a129")
import_national_2016 <- read_parquet("www/data//import_national_2016.parquet") %>% select(-"code_pays", -"code_nc8", -"code_cpf6", -"code_a129")
export_regional_2016 <- read_parquet("www/data//export_regional_2016.parquet") %>% select(-"code_pays", -"code_cpf4", -"code_a129")
import_regional_2016 <- read_parquet("www/data//import_regional_2016.parquet") %>% select(-"code_pays", -"code_cpf4", -"code_a129")
export_national_2015 <- read_parquet("www/data//export_national_2015.parquet") %>% select(-"code_pays", -"code_nc8", -"code_cpf6", -"code_a129")
import_national_2015 <- read_parquet("www/data//import_national_2015.parquet") %>% select(-"code_pays", -"code_nc8", -"code_cpf6", -"code_a129")
export_regional_2015 <- read_parquet("www/data//export_regional_2015.parquet") %>% select(-"code_pays", -"code_cpf4", -"code_a129")
import_regional_2015 <- read_parquet("www/data//import_regional_2015.parquet") %>% select(-"code_pays", -"code_cpf4", -"code_a129")
export_national_2014 <- read_parquet("www/data//export_national_2014.parquet") %>% select(-"code_pays", -"code_nc8", -"code_cpf6", -"code_a129")
import_national_2014 <- read_parquet("www/data//import_national_2014.parquet") %>% select(-"code_pays", -"code_nc8", -"code_cpf6", -"code_a129")
export_regional_2014 <- read_parquet("www/data//export_regional_2014.parquet") %>% select(-"code_pays", -"code_cpf4", -"code_a129")
import_regional_2014 <- read_parquet("www/data//import_regional_2014.parquet") %>% select(-"code_pays", -"code_cpf4", -"code_a129")
