## Méthodologie

*MobAuto* est un outil d'aide au traitement et à la visualisation des mobilités professionnelles et scolaires sur la métropole.

Les données proviennent de l'INSEE et sont issues du [fichier détails 2021](https://www.insee.fr/fr/statistiques/8201899) pour MobPro et de la [base de flux de mobilité 2021](https://www.insee.fr/fr/statistiques/8201894) pour MobSco. Elles sont retraitées pour calculer à différentes échelles les flux de mobilités : 
* des flux à la commune et l'intercommunalité : voir tous les flux entrants et sortants d'une commune ou d'un EPCI
* des flux entre liste de communes : estimer les flux le long d'une ligne de transport en commun traversant plusieurs communes
* des flux entre liste de groupes de communes : pour visualiser les flux entre pôles de mobilités (gares par exemple) : flux Rouen + communes voisines vers Le Havre + communes voisines  

L'application permet de filtrer sur une commune ou un EPCI, de choisir la mobilité (professionnelle ou scolaire) ainsi que le cumul des flux origine-destination.

L'archive de téléchargement contient un tableur des données affichées ainsi qu'un fichier géographique qu'il faut glisser-déposer dans QGIS pour visualiser une carte de flux.

### Contact

Si vous avez des questions sur l'application ou si vous rencontrez un bug, n'hésitez pas à [nous contacter](mailto:thomas.escrihuela@cerema.fr,alexis.vernier@cerema.fr).
