<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis symbologyReferenceScale="-1" styleCategories="AllStyleCategories" simplifyLocal="1" simplifyMaxScale="1" readOnly="0" version="3.35.0-Master" hasScaleBasedVisibilityFlag="0" labelsEnabled="0" simplifyDrawingTol="1" maxScale="0" minScale="100000000" simplifyDrawingHints="1" simplifyAlgorithm="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
    <Private>0</Private>
  </flags>
  <temporal startField="" startExpression="" durationUnit="min" endField="" mode="0" endExpression="" durationField="" accumulate="0" limitMode="0" enabled="0" fixedDuration="0">
    <fixedRange>
      <start></start>
      <end></end>
    </fixedRange>
  </temporal>
  <elevation zoffset="0" type="IndividualFeatures" respectLayerSymbol="1" clamping="Terrain" extrusion="0" symbology="Line" extrusionEnabled="0" showMarkerSymbolInSurfacePlots="0" zscale="1" binding="Centroid">
    <data-defined-properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </data-defined-properties>
    <profileLineSymbol>
      <symbol type="line" force_rhr="0" is_animated="0" alpha="1" clip_to_extent="1" frame_rate="10" name="">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" id="{8e96d1a0-95dc-4aed-a9a1-7c38e070cc60}" class="SimpleLine" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="0" name="align_dash_pattern"/>
            <Option type="QString" value="square" name="capstyle"/>
            <Option type="QString" value="5;2" name="customdash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
            <Option type="QString" value="MM" name="customdash_unit"/>
            <Option type="QString" value="0" name="dash_pattern_offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
            <Option type="QString" value="0" name="draw_inside_polygon"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="213,180,60,255" name="line_color"/>
            <Option type="QString" value="solid" name="line_style"/>
            <Option type="QString" value="0.6" name="line_width"/>
            <Option type="QString" value="MM" name="line_width_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="0" name="trim_distance_end"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_end_unit"/>
            <Option type="QString" value="0" name="trim_distance_start"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_start_unit"/>
            <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
            <Option type="QString" value="0" name="use_custom_dash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileLineSymbol>
    <profileFillSymbol>
      <symbol type="fill" force_rhr="0" is_animated="0" alpha="1" clip_to_extent="1" frame_rate="10" name="">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" id="{e3083723-66fc-442a-bfba-d04c552340ef}" class="SimpleFill" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
            <Option type="QString" value="213,180,60,255" name="color"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="152,129,43,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="solid" name="style"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileFillSymbol>
    <profileMarkerSymbol>
      <symbol type="marker" force_rhr="0" is_animated="0" alpha="1" clip_to_extent="1" frame_rate="10" name="">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" id="{a3f2e5d9-d01e-4974-82e0-9db5a8728c98}" class="SimpleMarker" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="0" name="angle"/>
            <Option type="QString" value="square" name="cap_style"/>
            <Option type="QString" value="213,180,60,255" name="color"/>
            <Option type="QString" value="1" name="horizontal_anchor_point"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="diamond" name="name"/>
            <Option type="QString" value="0,0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="152,129,43,255" name="outline_color"/>
            <Option type="QString" value="solid" name="outline_style"/>
            <Option type="QString" value="0.2" name="outline_width"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
            <Option type="QString" value="MM" name="outline_width_unit"/>
            <Option type="QString" value="diameter" name="scale_method"/>
            <Option type="QString" value="3" name="size"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
            <Option type="QString" value="MM" name="size_unit"/>
            <Option type="QString" value="1" name="vertical_anchor_point"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </profileMarkerSymbol>
  </elevation>
  <renderer-v2 type="singleSymbol" symbollevels="0" referencescale="-1" forceraster="0" enableorderby="0">
    <symbols>
      <symbol type="line" force_rhr="0" is_animated="0" alpha="1" clip_to_extent="1" frame_rate="10" name="0">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" id="{fe49c36b-822b-470f-be83-36dcdbb9309b}" class="GeometryGenerator" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="Line" name="SymbolType"/>
            <Option type="QString" value="
              difference(
                difference(
                  make_line(
                    start_point($geometry),
                    centroid(
                      offset_curve(
                        $geometry,
                        length($geometry)/-10.0
                      )
                    ),
                    end_point($geometry)
                  ),
                  buffer(start_point($geometry), 0.01)
                ),
                buffer(end_point( $geometry), 0.01)
              )" name="geometryModifier"/>
            <Option type="QString" value="MapUnit" name="units"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <symbol type="line" force_rhr="0" is_animated="0" alpha="1" clip_to_extent="1" frame_rate="10" name="@0@0">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" id="{e16f6fe3-dcb1-4d13-b804-f37121befeb2}" class="ArrowLine" enabled="1" pass="0">
              <Option type="Map">
                <Option type="QString" value="1" name="arrow_start_width"/>
                <Option type="QString" value="MM" name="arrow_start_width_unit"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="arrow_start_width_unit_scale"/>
                <Option type="QString" value="0" name="arrow_type"/>
                <Option type="QString" value="1" name="arrow_width"/>
                <Option type="QString" value="MM" name="arrow_width_unit"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="arrow_width_unit_scale"/>
                <Option type="QString" value="1.5" name="head_length"/>
                <Option type="QString" value="MM" name="head_length_unit"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="head_length_unit_scale"/>
                <Option type="QString" value="1.5" name="head_thickness"/>
                <Option type="QString" value="MM" name="head_thickness_unit"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="head_thickness_unit_scale"/>
                <Option type="QString" value="<cumul>" name="head_type"/>
                <Option type="QString" value="1" name="is_curved"/>
                <Option type="QString" value="1" name="is_repeated"/>
                <Option type="QString" value="0" name="offset"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_unit_scale"/>
                <Option type="QString" value="0" name="ring_filter"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option type="Map" name="properties">
                    <Option type="Map" name="arrowHeadLength">
                      <Option type="bool" value="true" name="active"/>
                      <Option type="QString" value="<champ>" name="field"/>
                      <Option type="Map" name="transformer">
                        <Option type="Map" name="d">
                          <Option type="double" value="1" name="exponent"/>
                          <Option type="double" value="10" name="maxOutput"/>
                          <Option type="double" value="<max_value>" name="maxValue"/>
                          <Option type="double" value="1" name="minOutput"/>
                          <Option type="double" value="<min_value>" name="minValue"/>
                          <Option type="double" value="0" name="nullOutput"/>
                        </Option>
                        <Option type="int" value="0" name="t"/>
                      </Option>
                      <Option type="int" value="2" name="type"/>
                    </Option>
                    <Option type="Map" name="arrowHeadThickness">
                      <Option type="bool" value="true" name="active"/>
                      <Option type="QString" value="<champ>" name="field"/>
                      <Option type="Map" name="transformer">
                        <Option type="Map" name="d">
                          <Option type="double" value="1" name="exponent"/>
                          <Option type="double" value="10" name="maxOutput"/>
                          <Option type="double" value="<max_value>" name="maxValue"/>
                          <Option type="double" value="1" name="minOutput"/>
                          <Option type="double" value="<min_value>" name="minValue"/>
                          <Option type="double" value="0" name="nullOutput"/>
                        </Option>
                        <Option type="int" value="0" name="t"/>
                      </Option>
                      <Option type="int" value="2" name="type"/>
                    </Option>
                    <Option type="Map" name="arrowStartWidth">
                      <Option type="bool" value="true" name="active"/>
                      <Option type="QString" value="<champ>" name="field"/>
                      <Option type="Map" name="transformer">
                        <Option type="Map" name="d">
                          <Option type="double" value="1" name="exponent"/>
                          <Option type="double" value="5" name="maxSize"/>
                          <Option type="double" value="<max_value>" name="maxValue"/>
                          <Option type="double" value="1" name="minSize"/>
                          <Option type="double" value="<min_value>" name="minValue"/>
                          <Option type="double" value="0" name="nullSize"/>
                          <Option type="int" value="0" name="scaleType"/>
                        </Option>
                        <Option type="int" value="1" name="t"/>
                      </Option>
                      <Option type="int" value="2" name="type"/>
                    </Option>
                    <Option type="Map" name="arrowWidth">
                      <Option type="bool" value="true" name="active"/>
                      <Option type="QString" value="<champ>" name="field"/>
                      <Option type="Map" name="transformer">
                        <Option type="Map" name="d">
                          <Option type="double" value="1" name="exponent"/>
                          <Option type="double" value="10" name="maxSize"/>
                          <Option type="double" value="<max_value>" name="maxValue"/>
                          <Option type="double" value="1" name="minSize"/>
                          <Option type="double" value="<min_value>" name="minValue"/>
                          <Option type="double" value="0" name="nullSize"/>
                          <Option type="int" value="0" name="scaleType"/>
                        </Option>
                        <Option type="int" value="1" name="t"/>
                      </Option>
                      <Option type="int" value="2" name="type"/>
                    </Option>
                  </Option>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
              <symbol type="fill" force_rhr="0" is_animated="0" alpha="1" clip_to_extent="1" frame_rate="10" name="@@0@0@0">
                <data_defined_properties>
                  <Option type="Map">
                    <Option type="QString" value="" name="name"/>
                    <Option name="properties"/>
                    <Option type="QString" value="collection" name="type"/>
                  </Option>
                </data_defined_properties>
                <layer locked="0" id="{9db7bf98-6e2b-4b66-b98f-cd95979c2c81}" class="GradientFill" enabled="1" pass="0">
                  <Option type="Map">
                    <Option type="QString" value="0" name="angle"/>
                    <Option type="QString" value="0,0,255,255" name="color"/>
                    <Option type="QString" value="13,8,135,255" name="color1"/>
                    <Option type="QString" value="240,249,33,255" name="color2"/>
                    <Option type="QString" value="1" name="color_type"/>
                    <Option type="QString" value="0" name="coordinate_mode"/>
                    <Option type="QString" value="ccw" name="direction"/>
                    <Option type="QString" value="0" name="discrete"/>
                    <Option type="QString" value="255,255,255,255" name="gradient_color2"/>
                    <Option type="QString" value="0,0" name="offset"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_unit"/>
                    <Option type="QString" value="gradient" name="rampType"/>
                    <Option type="QString" value="0,0" name="reference_point1"/>
                    <Option type="QString" value="0" name="reference_point1_iscentroid"/>
                    <Option type="QString" value="1,1" name="reference_point2"/>
                    <Option type="QString" value="0" name="reference_point2_iscentroid"/>
                    <Option type="QString" value="rgb" name="spec"/>
                    <Option type="QString" value="0" name="spread"/>
                    <Option type="QString" value="0.0196078;27,6,141,255;rgb;ccw:0.0392157;38,5,145,255;rgb;ccw:0.0588235;47,5,150,255;rgb;ccw:0.0784314;56,4,154,255;rgb;ccw:0.0980392;65,4,157,255;rgb;ccw:0.117647;73,3,160,255;rgb;ccw:0.137255;81,2,163,255;rgb;ccw:0.156863;89,1,165,255;rgb;ccw:0.176471;97,0,167,255;rgb;ccw:0.196078;105,0,168,255;rgb;ccw:0.215686;113,0,168,255;rgb;ccw:0.235294;120,1,168,255;rgb;ccw:0.254902;128,4,168,255;rgb;ccw:0.27451;135,7,166,255;rgb;ccw:0.294118;142,12,164,255;rgb;ccw:0.313725;149,17,161,255;rgb;ccw:0.333333;156,23,158,255;rgb;ccw:0.352941;162,29,154,255;rgb;ccw:0.372549;168,34,150,255;rgb;ccw:0.392157;174,40,146,255;rgb;ccw:0.411765;180,46,141,255;rgb;ccw:0.431373;186,51,136,255;rgb;ccw:0.45098;191,57,132,255;rgb;ccw:0.470588;196,62,127,255;rgb;ccw:0.490196;201,68,122,255;rgb;ccw:0.509804;205,74,118,255;rgb;ccw:0.529412;210,79,113,255;rgb;ccw:0.54902;214,85,109,255;rgb;ccw:0.568627;218,91,105,255;rgb;ccw:0.588235;222,97,100,255;rgb;ccw:0.607843;226,102,96,255;rgb;ccw:0.627451;230,108,92,255;rgb;ccw:0.647059;233,114,87,255;rgb;ccw:0.666667;237,121,83,255;rgb;ccw:0.686275;240,127,79,255;rgb;ccw:0.705882;243,133,75,255;rgb;ccw:0.72549;245,140,70,255;rgb;ccw:0.745098;247,147,66,255;rgb;ccw:0.764706;249,154,62,255;rgb;ccw:0.784314;251,161,57,255;rgb;ccw:0.803922;252,168,53,255;rgb;ccw:0.823529;253,175,49,255;rgb;ccw:0.843137;254,183,45,255;rgb;ccw:0.862745;254,190,42,255;rgb;ccw:0.882353;253,198,39,255;rgb;ccw:0.901961;252,206,37,255;rgb;ccw:0.921569;251,215,36,255;rgb;ccw:0.941176;248,223,37,255;rgb;ccw:0.960784;246,232,38,255;rgb;ccw:0.980392;243,240,39,255;rgb;ccw" name="stops"/>
                    <Option type="QString" value="1" name="type"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option name="properties"/>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                </layer>
                <layer locked="0" id="{f281b5ec-9b0e-4ffa-a663-8ec4a2a82bd7}" class="SimpleFill" enabled="1" pass="0">
                  <Option type="Map">
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                    <Option type="QString" value="247,247,247,255" name="color"/>
                    <Option type="QString" value="bevel" name="joinstyle"/>
                    <Option type="QString" value="0,0" name="offset"/>
                    <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                    <Option type="QString" value="MM" name="offset_unit"/>
                    <Option type="QString" value="0,0,0,255" name="outline_color"/>
                    <Option type="QString" value="solid" name="outline_style"/>
                    <Option type="QString" value="0.36" name="outline_width"/>
                    <Option type="QString" value="MM" name="outline_width_unit"/>
                    <Option type="QString" value="no" name="style"/>
                  </Option>
                  <data_defined_properties>
                    <Option type="Map">
                      <Option type="QString" value="" name="name"/>
                      <Option name="properties"/>
                      <Option type="QString" value="collection" name="type"/>
                    </Option>
                  </data_defined_properties>
                </layer>
              </symbol>
            </layer>
          </symbol>
        </layer>
      </symbol>
    </symbols>
    <rotation/>
    <sizescale/>
    <effect type="effectStack" enabled="1">
      <effect type="dropShadow">
        <Option type="Map">
          <Option type="QString" value="13" name="blend_mode"/>
          <Option type="QString" value="2.645" name="blur_level"/>
          <Option type="QString" value="MM" name="blur_unit"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="blur_unit_scale"/>
          <Option type="QString" value="0,0,0,255" name="color"/>
          <Option type="QString" value="2" name="draw_mode"/>
          <Option type="QString" value="1" name="enabled"/>
          <Option type="QString" value="135" name="offset_angle"/>
          <Option type="QString" value="2" name="offset_distance"/>
          <Option type="QString" value="MM" name="offset_unit"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_unit_scale"/>
          <Option type="QString" value="1" name="opacity"/>
        </Option>
      </effect>
      <effect type="outerGlow">
        <Option type="Map">
          <Option type="QString" value="0" name="blend_mode"/>
          <Option type="QString" value="2.645" name="blur_level"/>
          <Option type="QString" value="MM" name="blur_unit"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="blur_unit_scale"/>
          <Option type="QString" value="69,116,40,255" name="color1"/>
          <Option type="QString" value="188,220,60,255" name="color2"/>
          <Option type="QString" value="0" name="color_type"/>
          <Option type="QString" value="ccw" name="direction"/>
          <Option type="QString" value="0" name="discrete"/>
          <Option type="QString" value="2" name="draw_mode"/>
          <Option type="QString" value="0" name="enabled"/>
          <Option type="QString" value="0.5" name="opacity"/>
          <Option type="QString" value="gradient" name="rampType"/>
          <Option type="QString" value="255,255,255,255" name="single_color"/>
          <Option type="QString" value="rgb" name="spec"/>
          <Option type="QString" value="2" name="spread"/>
          <Option type="QString" value="MM" name="spread_unit"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="spread_unit_scale"/>
        </Option>
      </effect>
      <effect type="drawSource">
        <Option type="Map">
          <Option type="QString" value="0" name="blend_mode"/>
          <Option type="QString" value="2" name="draw_mode"/>
          <Option type="QString" value="1" name="enabled"/>
          <Option type="QString" value="1" name="opacity"/>
        </Option>
      </effect>
      <effect type="innerShadow">
        <Option type="Map">
          <Option type="QString" value="13" name="blend_mode"/>
          <Option type="QString" value="2.645" name="blur_level"/>
          <Option type="QString" value="MM" name="blur_unit"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="blur_unit_scale"/>
          <Option type="QString" value="0,0,0,255" name="color"/>
          <Option type="QString" value="2" name="draw_mode"/>
          <Option type="QString" value="0" name="enabled"/>
          <Option type="QString" value="135" name="offset_angle"/>
          <Option type="QString" value="2" name="offset_distance"/>
          <Option type="QString" value="MM" name="offset_unit"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_unit_scale"/>
          <Option type="QString" value="1" name="opacity"/>
        </Option>
      </effect>
      <effect type="innerGlow">
        <Option type="Map">
          <Option type="QString" value="0" name="blend_mode"/>
          <Option type="QString" value="2.645" name="blur_level"/>
          <Option type="QString" value="MM" name="blur_unit"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="blur_unit_scale"/>
          <Option type="QString" value="69,116,40,255" name="color1"/>
          <Option type="QString" value="188,220,60,255" name="color2"/>
          <Option type="QString" value="0" name="color_type"/>
          <Option type="QString" value="ccw" name="direction"/>
          <Option type="QString" value="0" name="discrete"/>
          <Option type="QString" value="2" name="draw_mode"/>
          <Option type="QString" value="0" name="enabled"/>
          <Option type="QString" value="0.5" name="opacity"/>
          <Option type="QString" value="gradient" name="rampType"/>
          <Option type="QString" value="255,255,255,255" name="single_color"/>
          <Option type="QString" value="rgb" name="spec"/>
          <Option type="QString" value="2" name="spread"/>
          <Option type="QString" value="MM" name="spread_unit"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="spread_unit_scale"/>
        </Option>
      </effect>
    </effect>
  </renderer-v2>
  <selection mode="Default">
    <selectionColor invalid="1"/>
    <selectionSymbol>
      <symbol type="line" force_rhr="0" is_animated="0" alpha="1" clip_to_extent="1" frame_rate="10" name="">
        <data_defined_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </data_defined_properties>
        <layer locked="0" id="{1d44f12c-b228-4e6a-b9dd-e2eb56950f54}" class="SimpleLine" enabled="1" pass="0">
          <Option type="Map">
            <Option type="QString" value="0" name="align_dash_pattern"/>
            <Option type="QString" value="square" name="capstyle"/>
            <Option type="QString" value="5;2" name="customdash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
            <Option type="QString" value="MM" name="customdash_unit"/>
            <Option type="QString" value="0" name="dash_pattern_offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
            <Option type="QString" value="0" name="draw_inside_polygon"/>
            <Option type="QString" value="bevel" name="joinstyle"/>
            <Option type="QString" value="35,35,35,255" name="line_color"/>
            <Option type="QString" value="solid" name="line_style"/>
            <Option type="QString" value="0.26" name="line_width"/>
            <Option type="QString" value="MM" name="line_width_unit"/>
            <Option type="QString" value="0" name="offset"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
            <Option type="QString" value="MM" name="offset_unit"/>
            <Option type="QString" value="0" name="ring_filter"/>
            <Option type="QString" value="0" name="trim_distance_end"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_end_unit"/>
            <Option type="QString" value="0" name="trim_distance_start"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
            <Option type="QString" value="MM" name="trim_distance_start_unit"/>
            <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
            <Option type="QString" value="0" name="use_custom_dash"/>
            <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
          </Option>
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
        </layer>
      </symbol>
    </selectionSymbol>
  </selection>
  <labeling type="simple">
    <settings calloutType="simple">
      <text-style fontItalic="0" textColor="50,50,50,255" fontSizeUnit="Point" previewBkgrdColor="255,255,255,255" useSubstitutions="0" blendMode="0" fontKerning="1" forcedItalic="0" textOrientation="horizontal" fontWeight="50" fontLetterSpacing="0" legendString="Aa" fontUnderline="0" textOpacity="1" fontSizeMapUnitScale="3x:0,0,0,0,0,0" fontStrikeout="0" fontFamily="Open Sans" forcedBold="0" allowHtml="0" fontSize="10" multilineHeight="1" multilineHeightUnit="Percentage" capitalization="0" namedStyle="Regular" fieldName="round(&quot;<champ>&quot;)" isExpression="1" fontWordSpacing="0">
        <families/>
        <text-buffer bufferNoFill="1" bufferBlendMode="0" bufferSizeUnits="MM" bufferSize="1" bufferOpacity="1" bufferSizeMapUnitScale="3x:0,0,0,0,0,0" bufferColor="250,250,250,255" bufferJoinStyle="128" bufferDraw="1"/>
        <text-mask maskOpacity="1" maskedSymbolLayers="" maskEnabled="0" maskSize="0" maskType="0" maskJoinStyle="128" maskSizeMapUnitScale="3x:0,0,0,0,0,0" maskSizeUnits="MM"/>
        <background shapeRadiiUnit="Point" shapeRotation="0" shapeBlendMode="0" shapeJoinStyle="64" shapeBorderWidthMapUnitScale="3x:0,0,0,0,0,0" shapeSizeX="0" shapeSVGFile="" shapeRadiiX="0" shapeBorderWidth="0" shapeOpacity="1" shapeDraw="0" shapeSizeMapUnitScale="3x:0,0,0,0,0,0" shapeOffsetY="0" shapeRadiiY="0" shapeFillColor="255,255,255,255" shapeBorderWidthUnit="Point" shapeOffsetX="0" shapeOffsetUnit="Point" shapeBorderColor="128,128,128,255" shapeSizeUnit="Point" shapeType="0" shapeRotationType="0" shapeSizeType="0" shapeRadiiMapUnitScale="3x:0,0,0,0,0,0" shapeSizeY="0" shapeOffsetMapUnitScale="3x:0,0,0,0,0,0">
          <symbol type="marker" force_rhr="0" is_animated="0" alpha="1" clip_to_extent="1" frame_rate="10" name="markerSymbol">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" id="" class="SimpleMarker" enabled="1" pass="0">
              <Option type="Map">
                <Option type="QString" value="0" name="angle"/>
                <Option type="QString" value="square" name="cap_style"/>
                <Option type="QString" value="183,72,75,255" name="color"/>
                <Option type="QString" value="1" name="horizontal_anchor_point"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="circle" name="name"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="35,35,35,255" name="outline_color"/>
                <Option type="QString" value="solid" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="outline_width_map_unit_scale"/>
                <Option type="QString" value="MM" name="outline_width_unit"/>
                <Option type="QString" value="diameter" name="scale_method"/>
                <Option type="QString" value="2" name="size"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="size_map_unit_scale"/>
                <Option type="QString" value="MM" name="size_unit"/>
                <Option type="QString" value="1" name="vertical_anchor_point"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
          <symbol type="fill" force_rhr="0" is_animated="0" alpha="1" clip_to_extent="1" frame_rate="10" name="fillSymbol">
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
            <layer locked="0" id="" class="SimpleFill" enabled="1" pass="0">
              <Option type="Map">
                <Option type="QString" value="3x:0,0,0,0,0,0" name="border_width_map_unit_scale"/>
                <Option type="QString" value="255,255,255,255" name="color"/>
                <Option type="QString" value="bevel" name="joinstyle"/>
                <Option type="QString" value="0,0" name="offset"/>
                <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
                <Option type="QString" value="MM" name="offset_unit"/>
                <Option type="QString" value="128,128,128,255" name="outline_color"/>
                <Option type="QString" value="no" name="outline_style"/>
                <Option type="QString" value="0" name="outline_width"/>
                <Option type="QString" value="Point" name="outline_width_unit"/>
                <Option type="QString" value="solid" name="style"/>
              </Option>
              <data_defined_properties>
                <Option type="Map">
                  <Option type="QString" value="" name="name"/>
                  <Option name="properties"/>
                  <Option type="QString" value="collection" name="type"/>
                </Option>
              </data_defined_properties>
            </layer>
          </symbol>
        </background>
        <shadow shadowColor="0,0,0,255" shadowBlendMode="6" shadowDraw="0" shadowOffsetUnit="MM" shadowRadiusMapUnitScale="3x:0,0,0,0,0,0" shadowOffsetGlobal="1" shadowOffsetMapUnitScale="3x:0,0,0,0,0,0" shadowScale="100" shadowRadiusUnit="MM" shadowOffsetAngle="135" shadowRadius="1.5" shadowUnder="0" shadowOffsetDist="1" shadowOpacity="0.69999999999999996" shadowRadiusAlphaOnly="0"/>
        <dd_properties>
          <Option type="Map">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
        </dd_properties>
        <substitutions/>
      </text-style>
      <text-format rightDirectionSymbol=">" addDirectionSymbol="0" autoWrapLength="0" decimals="3" multilineAlign="0" reverseDirectionSymbol="0" useMaxLineLengthForAutoWrap="1" placeDirectionSymbol="0" wrapChar="" leftDirectionSymbol="&lt;" plussign="0" formatNumbers="0"/>
      <placement overlapHandling="PreventOverlap" repeatDistance="0" xOffset="0" placement="2" offsetType="0" lineAnchorType="0" priority="5" distMapUnitScale="3x:0,0,0,0,0,0" overrunDistance="0" overrunDistanceMapUnitScale="3x:0,0,0,0,0,0" distUnits="MM" placementFlags="10" rotationAngle="0" geometryGeneratorEnabled="0" lineAnchorPercent="0.5" layerType="LineGeometry" yOffset="0" fitInPolygonOnly="0" geometryGeneratorType="PointGeometry" centroidWhole="0" geometryGenerator="" labelOffsetMapUnitScale="3x:0,0,0,0,0,0" dist="0" quadOffset="4" offsetUnits="MM" preserveRotation="1" maxCurvedCharAngleOut="-25" repeatDistanceUnits="MM" predefinedPositionOrder="TR,TL,BR,BL,R,L,TSR,BSR" allowDegraded="0" maxCurvedCharAngleIn="25" polygonPlacementFlags="2" lineAnchorTextPoint="FollowPlacement" lineAnchorClipping="0" rotationUnit="AngleDegrees" overrunDistanceUnit="MM" centroidInside="0" repeatDistanceMapUnitScale="3x:0,0,0,0,0,0"/>
      <rendering obstacle="1" scaleVisibility="0" drawLabels="1" obstacleFactor="1" fontLimitPixelSize="0" minFeatureSize="0" fontMaxPixelSize="10000" unplacedVisibility="0" zIndex="0" limitNumLabels="0" fontMinPixelSize="3" scaleMax="0" maxNumLabels="2000" obstacleType="1" upsidedownLabels="0" mergeLines="0" labelPerPart="0" scaleMin="0"/>
      <dd_properties>
        <Option type="Map">
          <Option type="QString" value="" name="name"/>
          <Option type="Map" name="properties">
            <Option type="Map" name="Color">
              <Option type="bool" value="true" name="active"/>
              <Option type="QString" value="@symbol_color" name="expression"/>
              <Option type="int" value="3" name="type"/>
            </Option>
          </Option>
          <Option type="QString" value="collection" name="type"/>
        </Option>
      </dd_properties>
      <callout type="simple">
        <Option type="Map">
          <Option type="QString" value="pole_of_inaccessibility" name="anchorPoint"/>
          <Option type="int" value="0" name="blendMode"/>
          <Option type="Map" name="ddProperties">
            <Option type="QString" value="" name="name"/>
            <Option name="properties"/>
            <Option type="QString" value="collection" name="type"/>
          </Option>
          <Option type="bool" value="false" name="drawToAllParts"/>
          <Option type="QString" value="0" name="enabled"/>
          <Option type="QString" value="point_on_exterior" name="labelAnchorPoint"/>
          <Option type="QString" value="&lt;symbol type=&quot;line&quot; force_rhr=&quot;0&quot; is_animated=&quot;0&quot; alpha=&quot;1&quot; clip_to_extent=&quot;1&quot; frame_rate=&quot;10&quot; name=&quot;symbol&quot;>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;layer locked=&quot;0&quot; id=&quot;{f58b7e1b-9dc0-4a3a-8342-20d769c58d80}&quot; class=&quot;SimpleLine&quot; enabled=&quot;1&quot; pass=&quot;0&quot;>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;align_dash_pattern&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;square&quot; name=&quot;capstyle&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;5;2&quot; name=&quot;customdash&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;customdash_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;customdash_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;dash_pattern_offset&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;dash_pattern_offset_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;dash_pattern_offset_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;draw_inside_polygon&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;bevel&quot; name=&quot;joinstyle&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;60,60,60,255&quot; name=&quot;line_color&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;solid&quot; name=&quot;line_style&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0.3&quot; name=&quot;line_width&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;line_width_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;offset&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;offset_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;offset_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;ring_filter&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;trim_distance_end&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;trim_distance_end_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;trim_distance_end_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;trim_distance_start&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;trim_distance_start_map_unit_scale&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;MM&quot; name=&quot;trim_distance_start_unit&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;tweak_dash_pattern_on_corners&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;0&quot; name=&quot;use_custom_dash&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;3x:0,0,0,0,0,0&quot; name=&quot;width_map_unit_scale&quot;/>&lt;/Option>&lt;data_defined_properties>&lt;Option type=&quot;Map&quot;>&lt;Option type=&quot;QString&quot; value=&quot;&quot; name=&quot;name&quot;/>&lt;Option name=&quot;properties&quot;/>&lt;Option type=&quot;QString&quot; value=&quot;collection&quot; name=&quot;type&quot;/>&lt;/Option>&lt;/data_defined_properties>&lt;/layer>&lt;/symbol>" name="lineSymbol"/>
          <Option type="double" value="0" name="minLength"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="minLengthMapUnitScale"/>
          <Option type="QString" value="MM" name="minLengthUnit"/>
          <Option type="double" value="0" name="offsetFromAnchor"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromAnchorMapUnitScale"/>
          <Option type="QString" value="MM" name="offsetFromAnchorUnit"/>
          <Option type="double" value="0" name="offsetFromLabel"/>
          <Option type="QString" value="3x:0,0,0,0,0,0" name="offsetFromLabelMapUnitScale"/>
          <Option type="QString" value="MM" name="offsetFromLabelUnit"/>
        </Option>
      </callout>
    </settings>
  </labeling>
  <customproperties>
    <Option type="Map">
      <Option type="List" name="dualview/previewExpressions">
        <Option type="QString" value="&quot;Libellé origine&quot;"/>
      </Option>
      <Option type="int" value="0" name="embeddedWidgets/count"/>
      <Option name="variableNames"/>
      <Option name="variableValues"/>
    </Option>
  </customproperties>
  <blendMode>6</blendMode>
  <featureBlendMode>0</featureBlendMode>
  <layerOpacity>1</layerOpacity>
  <SingleCategoryDiagramRenderer attributeLegend="1" diagramType="Histogram">
    <DiagramCategory minScaleDenominator="0" maxScaleDenominator="1e+08" spacingUnit="MM" rotationOffset="270" sizeScale="3x:0,0,0,0,0,0" showAxis="1" height="15" barWidth="5" lineSizeScale="3x:0,0,0,0,0,0" penWidth="0" spacing="5" diagramOrientation="Up" labelPlacementMethod="XHeight" scaleDependency="Area" minimumSize="0" scaleBasedVisibility="0" backgroundAlpha="255" opacity="1" spacingUnitScale="3x:0,0,0,0,0,0" penAlpha="255" backgroundColor="#ffffff" sizeType="MM" direction="0" enabled="0" lineSizeType="MM" penColor="#000000" width="15">
      <fontProperties italic="0" description="Ubuntu,11,-1,5,50,0,0,0,0,0" bold="0" style="" strikethrough="0" underline="0"/>
      <attribute field="" colorOpacity="1" color="#000000" label=""/>
      <axisSymbol>
        <symbol type="line" force_rhr="0" is_animated="0" alpha="1" clip_to_extent="1" frame_rate="10" name="">
          <data_defined_properties>
            <Option type="Map">
              <Option type="QString" value="" name="name"/>
              <Option name="properties"/>
              <Option type="QString" value="collection" name="type"/>
            </Option>
          </data_defined_properties>
          <layer locked="0" id="{9e95a667-3800-4255-9274-19a0f0fe9ce2}" class="SimpleLine" enabled="1" pass="0">
            <Option type="Map">
              <Option type="QString" value="0" name="align_dash_pattern"/>
              <Option type="QString" value="square" name="capstyle"/>
              <Option type="QString" value="5;2" name="customdash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="customdash_map_unit_scale"/>
              <Option type="QString" value="MM" name="customdash_unit"/>
              <Option type="QString" value="0" name="dash_pattern_offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="dash_pattern_offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="dash_pattern_offset_unit"/>
              <Option type="QString" value="0" name="draw_inside_polygon"/>
              <Option type="QString" value="bevel" name="joinstyle"/>
              <Option type="QString" value="35,35,35,255" name="line_color"/>
              <Option type="QString" value="solid" name="line_style"/>
              <Option type="QString" value="0.26" name="line_width"/>
              <Option type="QString" value="MM" name="line_width_unit"/>
              <Option type="QString" value="0" name="offset"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="offset_map_unit_scale"/>
              <Option type="QString" value="MM" name="offset_unit"/>
              <Option type="QString" value="0" name="ring_filter"/>
              <Option type="QString" value="0" name="trim_distance_end"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_end_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_end_unit"/>
              <Option type="QString" value="0" name="trim_distance_start"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="trim_distance_start_map_unit_scale"/>
              <Option type="QString" value="MM" name="trim_distance_start_unit"/>
              <Option type="QString" value="0" name="tweak_dash_pattern_on_corners"/>
              <Option type="QString" value="0" name="use_custom_dash"/>
              <Option type="QString" value="3x:0,0,0,0,0,0" name="width_map_unit_scale"/>
            </Option>
            <data_defined_properties>
              <Option type="Map">
                <Option type="QString" value="" name="name"/>
                <Option name="properties"/>
                <Option type="QString" value="collection" name="type"/>
              </Option>
            </data_defined_properties>
          </layer>
        </symbol>
      </axisSymbol>
    </DiagramCategory>
  </SingleCategoryDiagramRenderer>
  <DiagramLayerSettings priority="0" linePlacementFlags="18" zIndex="0" obstacle="0" dist="0" showAll="1" placement="2">
    <properties>
      <Option type="Map">
        <Option type="QString" value="" name="name"/>
        <Option name="properties"/>
        <Option type="QString" value="collection" name="type"/>
      </Option>
    </properties>
  </DiagramLayerSettings>
  <geometryOptions removeDuplicateNodes="0" geometryPrecision="0">
    <activeChecks/>
    <checkConfiguration/>
  </geometryOptions>
  <legend type="default-vector" showLabelLegend="0"/>
  <referencedLayers/>
  <fieldConfiguration>
    <field configurationFlags="NoFlag" name="field_1">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="EPCI origine">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="Code origine">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="Libellé origine">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="EPCI destination">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="Code destination">
      <editWidget type="Range">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="Libellé destination">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
    <field configurationFlags="NoFlag" name="<champ>">
      <editWidget type="TextEdit">
        <config>
          <Option/>
        </config>
      </editWidget>
    </field>
  </fieldConfiguration>
  <aliases>
    <alias field="field_1" name="" index="0"/>
    <alias field="EPCI origine" name="" index="1"/>
    <alias field="Code origine" name="" index="2"/>
    <alias field="Libellé origine" name="" index="3"/>
    <alias field="EPCI destination" name="" index="4"/>
    <alias field="Code destination" name="" index="5"/>
    <alias field="Libellé destination" name="" index="6"/>
    <alias field="<champ>" name="" index="7"/>
  </aliases>
  <splitPolicies>
    <policy field="field_1" policy="Duplicate"/>
    <policy field="EPCI origine" policy="Duplicate"/>
    <policy field="Code origine" policy="Duplicate"/>
    <policy field="Libellé origine" policy="Duplicate"/>
    <policy field="EPCI destination" policy="Duplicate"/>
    <policy field="Code destination" policy="Duplicate"/>
    <policy field="Libellé destination" policy="Duplicate"/>
    <policy field="<champ>" policy="Duplicate"/>
  </splitPolicies>
  <defaults>
    <default field="field_1" expression="" applyOnUpdate="0"/>
    <default field="EPCI origine" expression="" applyOnUpdate="0"/>
    <default field="Code origine" expression="" applyOnUpdate="0"/>
    <default field="Libellé origine" expression="" applyOnUpdate="0"/>
    <default field="EPCI destination" expression="" applyOnUpdate="0"/>
    <default field="Code destination" expression="" applyOnUpdate="0"/>
    <default field="Libellé destination" expression="" applyOnUpdate="0"/>
    <default field="<champ>" expression="" applyOnUpdate="0"/>
  </defaults>
  <constraints>
    <constraint unique_strength="0" field="field_1" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="EPCI origine" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="Code origine" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="Libellé origine" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="EPCI destination" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="Code destination" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="Libellé destination" notnull_strength="0" constraints="0" exp_strength="0"/>
    <constraint unique_strength="0" field="<champ>" notnull_strength="0" constraints="0" exp_strength="0"/>
  </constraints>
  <constraintExpressions>
    <constraint field="field_1" exp="" desc=""/>
    <constraint field="EPCI origine" exp="" desc=""/>
    <constraint field="Code origine" exp="" desc=""/>
    <constraint field="Libellé origine" exp="" desc=""/>
    <constraint field="EPCI destination" exp="" desc=""/>
    <constraint field="Code destination" exp="" desc=""/>
    <constraint field="Libellé destination" exp="" desc=""/>
    <constraint field="<champ>" exp="" desc=""/>
  </constraintExpressions>
  <expressionfields/>
  <attributeactions>
    <defaultAction value="{00000000-0000-0000-0000-000000000000}" key="Canvas"/>
  </attributeactions>
  <attributetableconfig sortOrder="0" actionWidgetStyle="dropDown" sortExpression="">
    <columns>
      <column type="field" width="-1" name="field_1" hidden="0"/>
      <column type="field" width="-1" name="EPCI origine" hidden="0"/>
      <column type="field" width="-1" name="Code origine" hidden="0"/>
      <column type="field" width="256" name="Libellé origine" hidden="0"/>
      <column type="field" width="-1" name="EPCI destination" hidden="0"/>
      <column type="field" width="-1" name="Code destination" hidden="0"/>
      <column type="field" width="150" name="Libellé destination" hidden="0"/>
      <column type="field" width="-1" name="<champ>" hidden="0"/>
      <column type="actions" width="-1" hidden="1"/>
    </columns>
  </attributetableconfig>
  <conditionalstyles>
    <rowstyles/>
    <fieldstyles/>
  </conditionalstyles>
  <storedexpressions/>
  <editform tolerant="1"></editform>
  <editforminit/>
  <editforminitcodesource>0</editforminitcodesource>
  <editforminitfilepath></editforminitfilepath>
  <editforminitcode><![CDATA[# -*- coding: utf-8 -*-
"""
Les formulaires QGIS peuvent avoir une fonction Python qui est appelée lorsque le formulaire est
ouvert.

Utilisez cette fonction pour ajouter une logique supplémentaire à vos formulaires.

Entrez le nom de la fonction dans le champ 
"Fonction d'initialisation Python".
Voici un exemple:
"""
from qgis.PyQt.QtWidgets import QWidget

def my_form_open(dialog, layer, feature):
    geom = feature.geometry()
    control = dialog.findChild(QWidget, "MyLineEdit")
]]></editforminitcode>
  <featformsuppress>0</featformsuppress>
  <editorlayout>generatedlayout</editorlayout>
  <editable>
    <field editable="1" name="Code destination"/>
    <field editable="1" name="Code origine"/>
    <field editable="1" name="EPCI destination"/>
    <field editable="1" name="EPCI origine"/>
    <field editable="1" name="<champ>"/>
    <field editable="1" name="Libellé destination"/>
    <field editable="1" name="Libellé origine"/>
    <field editable="1" name="field_1"/>
  </editable>
  <labelOnTop>
    <field labelOnTop="0" name="Code destination"/>
    <field labelOnTop="0" name="Code origine"/>
    <field labelOnTop="0" name="EPCI destination"/>
    <field labelOnTop="0" name="EPCI origine"/>
    <field labelOnTop="0" name="<champ>"/>
    <field labelOnTop="0" name="Libellé destination"/>
    <field labelOnTop="0" name="Libellé origine"/>
    <field labelOnTop="0" name="field_1"/>
  </labelOnTop>
  <reuseLastValue>
    <field reuseLastValue="0" name="Code destination"/>
    <field reuseLastValue="0" name="Code origine"/>
    <field reuseLastValue="0" name="EPCI destination"/>
    <field reuseLastValue="0" name="EPCI origine"/>
    <field reuseLastValue="0" name="<champ>"/>
    <field reuseLastValue="0" name="Libellé destination"/>
    <field reuseLastValue="0" name="Libellé origine"/>
    <field reuseLastValue="0" name="field_1"/>
  </reuseLastValue>
  <dataDefinedFieldProperties/>
  <widgets/>
  <previewExpression>"Libellé origine"</previewExpression>
  <mapTip enabled="1"></mapTip>
  <layerGeometryType>1</layerGeometryType>
</qgis>
