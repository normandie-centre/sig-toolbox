# MobAuto

Tableau de bord pour faciliter le traitement des fichiers MobPro MobSco. Il permet de visualiser les flux de mobilités professionnelles et scolaires pour une commune ou un epci. 

Il est possible de sélectionner une commune ou un EPCI. Pour l'EPCI, la tableau affichera les flux entrants (communes -> EPCI), sortants (EPCI -> communes) ou internes (EPCI -> EPCI). Ceci permet par exemple de visualiser tous les flux communaux pour un EPCI (en filtrant par le code EPCI avec l'outil de recherche en haut à droite).

Le résultat peut être téléchargé en format csv et est adapté à un import dans QGIS pour faciliter la création de cartes de flux.

Disponible sur https://cerema-med.shinyapps.io/mobauto.

## Fonctionnement

Exécuter le script `init.R` pour installer les paquets nécessaires à l'application puis lancer l'application shiny avec `runApp()`. 

Le script *scripts/preprocess_mob.R* ne doit être lancé qu'une seule fois, à chaque nouvelle version de MobPro. Il prend en entrée les fichiers csv bruts base de flux mobilités MobPro et MobSco fournis par l'INSEE et crée 4 fichiers au format **parquet** en sortie dans `www/data` : 
* mobpro_com 
* mobsco_com 
* mobpro_epci 
* mobsco_epci

Ces 4 fichiers sont utilisés par l'application shiny afin d'afficher le tableau des flux. Tout les traitements sont effectués dans le script de preprocessing afin d'optimiser le temps de chargement des résultats dans l'application.
