server <- function(input, output, session) {

	source("helpers.R")

	####  Mise à jour de la liste des communes  ####
	updateSelectizeInput(session, 'com', choices = c(paste(communes$DEPCOM, '-', communes$NOM_DEPCOM), paste(epci$EPCI, '-', epci$NOM_EPCI)), selected = "76540 - Rouen", server = TRUE)
	updateSelectizeInput(session, 'lcom', choices = c(paste(communes$DEPCOM, '-', communes$NOM_DEPCOM), paste(epci$EPCI, '-', epci$NOM_EPCI)), selected = c("76540 - Rouen", "76681 - Sotteville-lès-Rouen", "76498 - Le Petit-Quevilly"), server = TRUE)
	updateSelectizeInput(session, 'lgcom_1', choices = c(paste(communes$DEPCOM, '-', communes$NOM_DEPCOM)), selected = c("76540 - Rouen", "76681 - Sotteville-lès-Rouen", "76498 - Le Petit-Quevilly"), server = TRUE)
	updateSelectizeInput(session, 'lgcom_2', choices = c(paste(communes$DEPCOM, '-', communes$NOM_DEPCOM)), selected = c("76217 - Dieppe", "76545 - Rouxmesnil-Bouteilles"), server = TRUE)
	updateSelectizeInput(session, 'lgcom_3', choices = c(paste(communes$DEPCOM, '-', communes$NOM_DEPCOM)), selected = c("76351 - Le Havre", "76552 - Sainte-Adresse"), server = TRUE)

  
  	#### Gestion de l'ajout de groupes de communes ####
	counter <- reactiveVal(3)
	observeEvent(input$add_group, {
		counter(counter() + 1)
		insertUI(
			selector = "#add_group",
			where = "beforeBegin",
			ui = generateSelectizeInput(counter()),
			immediate = TRUE
		)
		updateSelectizeInput(session, 
			paste0("lgcom_", counter()),
			choices = c(paste(communes$DEPCOM, '-', communes$NOM_DEPCOM)),
			server = TRUE
		)
	})

	# Observer pour supprimer un selectizeInput
	observeEvent(input$delete_button, {
		id <- gsub("removegroup_", "", input$delete_button)
		removeUI(selector = paste0("#div_", id))
		message(paste("Suppression de lgcom_", id))
		updateSelectizeInput(session, paste0("lgcom_", id), selected = character(0))	# Remettre le selectizeInput vide
	})

	#### Reactive values pour les communes saisis par l'utilisateur ####
	r_code <- reactive({
		req(input$com)
		input$com %>% strsplit(' - ') %>% .[[1]] %>% .[[1]]
	})
	r_code_liste_insee <- reactive({
		req(input$lcom)
		input$lcom %>% str_extract("\\d+")
	})
	r_communes_filtrees <- reactive({
		if (input$dev_mode == FALSE) {
			values <- grep("^lgcom_", names(input), value = TRUE) %>%
				lapply(function(id) {input[[id]] %>% gsub(".*?(\\d+).*", "\\1", .) %>% paste(collapse = ",")}) %>%
				paste(collapse = ";")
		}
		else {
			values <- input$liste_groupe_commune
		}
		values
	})
	r_liste_communes_groupees <- reactive(
		r_communes_filtrees() %>% gsub(";", ",",.) %>% strsplit(',') %>% .[[1]]
	)

	#### Dataframes de sortie ####
	r_df <- reactive({
		if (input$somme_flux_com == "Flux orientés") {
			# Choix d'une commune
			if (nchar(r_code()) == 5) get(glue(tolower(input$mob_com), "_com")) %>% filter(`Code origine` == r_code() | `Code destination` == r_code())
			# Choix d'un EPCI
			else if (nchar(r_code()) == 9) get(glue(tolower(input$mob_com), "_epci")) %>% filter(`Code origine` == r_code() | `Code destination` == r_code())
		}
		else if (input$somme_flux_com == "Flux cumulés") {
			if (nchar(r_code()) == 5) get(glue(tolower(input$mob_com), "_com")) %>% filter(`Code origine` == r_code() | `Code destination` == r_code()) %>% calcul_flux_cumules(input$mob_com)
			else if (nchar(r_code()) == 9) get(glue(tolower(input$mob_com), "_epci")) %>% filter(`Code origine` == r_code() | `Code destination` == r_code()) %>% calcul_flux_cumules(input$mob_com)
		}
	})
	r_df_liste_commune <- reactive({
	  	if (input$somme_flux_lcom == "Flux orientés") get(glue(tolower(input$mob_lcom), "_com")) %>% filter(`Code origine` %in% r_code_liste_insee() & `Code destination` %in% r_code_liste_insee())
	  	else if (input$somme_flux_lcom == "Flux cumulés") get(glue(tolower(input$mob_lcom), "_com")) %>%
			filter(`Code origine` %in% r_code_liste_insee() & `Code destination` %in% r_code_liste_insee()) %>%
			calcul_flux_cumules(input$mob_lcom)
	})
	r_df_groupe_communes <-reactive({
		if (input$somme_flux_lgcom == "Flux orientés") get(glue(tolower(input$mob_lgcom), "_com")) %>%
			filter(`Code origine` %in% r_liste_communes_groupees() & `Code destination` %in% r_liste_communes_groupees()) %>%
			df_groupe_commune(r_communes_filtrees(), .) %>%
			calcul_flux_groupes(input$mob_lgcom)
		else if (input$somme_flux_lgcom == "Flux cumulés") get(glue(tolower(input$mob_lgcom), "_com")) %>%
			filter(`Code origine` %in% r_liste_communes_groupees() & `Code destination` %in% r_liste_communes_groupees()) %>%
			df_groupe_commune(r_communes_filtrees(), .) %>%
			calcul_flux_groupes_cumules(input$mob_lgcom)
	})


	#### Valeurs pour créer le qml : nom colonne, min et max ####
	r_values <- reactive({
		list(
			champ = "Flux total",
			min_value = r_df() %>% filter(`Code origine` != `Code destination`) %>% summarise(min = min(!!sym("Flux total"))) %>% pull(min),
			max_value = r_df() %>% filter(`Code origine` != `Code destination`) %>% summarise(max = max(!!sym("Flux total"))) %>% pull(max),
			cumul = if(input$somme_flux_com == "Flux cumulés") "2" else "0"
		)
	})
	r_values_liste <- reactive({
	  	list(
			champ = "Flux total",
			min_value = r_df_liste_commune() %>% filter(`Code origine` != `Code destination`) %>% summarise(min = min(!!sym("Flux total"))) %>% pull(min),
			max_value = r_df_liste_commune() %>% filter(`Code origine` != `Code destination`) %>% summarise(max = max(!!sym("Flux total"))) %>% pull(max),
			cumul = if(input$somme_flux_lcom == "Flux cumulés") "2" else "0"
	  	)
	})
	r_values_groupe <- reactive({
	  	list(
			champ = "Flux total",
			min_value = r_df_groupe_communes() %>% filter(`Groupe origine` != `Groupe destination`) %>% summarise(min = min(!!sym("Flux total"))) %>% pull(min),
			max_value = r_df_groupe_communes() %>% filter(`Groupe origine` != `Groupe destination`) %>% summarise(max = max(!!sym("Flux total"))) %>% pull(max),
			cumul = if(input$somme_flux_lgcom == "Flux cumulés") "2" else "0"
	  	)
	})


	#### Noms de l'archive téléchargée ####
	r_nom <- reactive({
		if(input$somme_flux_com == "Flux cumulés") glue(input$mob_com, '_', r_code(), '_cumule') else glue(input$mob_com, '_', r_code())
	})
	r_nom_liste <- reactive({
	  	if(input$somme_flux_lcom == "Flux cumulés") glue(input$mob_lcom, '_liste_de_communes', '_cumule') else glue(input$mob_lcom, '_liste_de_communes')
	})
	r_nom_groupe <- reactive({
	  	if(input$somme_flux_lgcom == "Flux cumulés") glue(input$mob_lgcom, '_groupes_de_communes', '_cumule') else glue(input$mob_lgcom, '_groupes_de_communes')
	})


	#### Affichade des datatable ####
	output$table <- renderDataTable({r_df() %>% display_df(input$mob_com, input$part_modale_com)})
	output$table_liste_commune <- renderDT({r_df_liste_commune() %>% display_df(input$mob_lcom, input$part_modale_lcom)})
	output$table_liste_groupe_commune <- renderDT({r_df_groupe_communes() %>% display_df(input$mob_lgcom, input$part_modale_lgcom)})


	#### Boutons de téléchargements ####
	output$dl <- downloadHandler(
		filename = function() {
			paste0(r_nom(),".zip")
		},
		content = function(file) {
			file.copy(create_archive(r_nom(), r_df(), r_values(), template), file)
		}
	)
	output$dl_liste <- downloadHandler(
		filename = function() {
			glue(r_nom_liste(),".zip")
		},
		content = function(file) {
			file.copy(create_archive(r_nom_liste(), r_df_liste_commune(), r_values_liste(), template), file)
		}
	)
	output$dl_liste_groupe <- downloadHandler(
		filename = function() {
			glue(r_nom_groupe(),".zip")
		},
		content = function(file) {
			file.copy(create_archive(r_nom_groupe(), r_df_groupe_communes(), r_values_groupe(), template), file)
		}
	)
}