#### Présentation du site

Ce site présente les résultats chiffrés de la comptabilité des flux de matières en Centre-Val de Loire en 2014 et 2021 résultant de l'étude produite par le _Cerema_ pour la DREAL et le Conseil Régional. L'ensemble des analyses de ces résultats n'est pas présenté sur ce site mais dans les rapports d'études.

#### Utilisation du site

L’accès au site peut être interrompu à tout moment, sans préavis, notamment en cas de force majeure, de maintenance ou si le Cerema décide de suspendre ou d’arrêter la fourniture de ce service.

Le site est régi par la loi française. Les utilisateurs étrangers acceptent formellement l’application de la loi française en visitant ce site et en utilisant tout ou partie des fonctionnalités du site. La responsabilité du Cerema ne pourra en aucun cas être engagée quant au contenu et aux informations figurant sur ce site, ou aux conséquences pouvant résulter de leur utilisation ou interprétation.

#### Propriété intellectuelle

Les contenus, les textes, y compris les communiqués de presse, les vidéos, les images, les photos et les animations qui apparaissent ou sont disponibles sur le site, sont protégés par le droit de la propriété intellectuelle. Ils peuvent être la propriété du Cerema, ou des partenaires cités dans le site.

À ce titre, vous vous engagez à ne pas copier, traduire, reproduire, commercialiser, publier, exploiter et diffuser partiellement ou intégralement des contenus du site protégés par le droit de la propriété intellectuelle, sans mention du producteur desdits contenus.

La violation de l’un de ces droits de propriété intellectuelle est un délit de contrefaçon passible de deux ans d’emprisonnement et de 150.000 euros d’amende.

#### Liens

Le site peut contenir des liens vers des sites de partenaires ou de tiers. Le Cerema, ne disposant d’aucun moyen pour contrôler ces sites, n’offre aucune garantie quant au respect par ces sites des lois et règlement en vigueur.

Ce site autorise la mise en place d’un lien hypertexte pointant vers son contenu sous réserve :

- d’indiquer impérativement dans le lien la source Cerema,
- de n’utiliser les informations du site qu’à des fins personnelles, professionnelles ou associatives à l’exclusion de toute utilisation à des fins commerciales ou publicitaires.

Cette autorisation ne s’applique que pour les sites dont le contenu n’est contraire ni à la loi ni aux bonnes mœurs, et dans la mesure où ces liens ne contreviennent pas aux intérêts du Cerema et garantissent pour l’utilisateur la possibilité d’identifier l’origine et l’auteur du document.

#### Modification

Le Cerema se réserve le droit de modifier, sans préavis, les présentes conditions générales d’utilisation du site. Pour toute remarque sur le présent site, vous pouvez nous écrire à [cette adresse <i class="fa fa-envelope fa-1x"></i>](mailto:tme.dlab.dternc.cerema@cerema.fr).

#### Droit applicable

Le présent site est soumis au droit français.

#### Hébergement

Ce site est hébergé via le service shinyapps.io de RStudio.

#### Qui sommes-nous ?

Ce site a été conçu et développé par le [Cerema](https://www.cerema.fr/), établissement public relevant du ministère de la Transition écologique et de la Cohésion des territoires. Le Cerema accompagne l’État et les collectivités territoriales pour l’élaboration, le déploiement et l’évaluation de politiques publiques d’aménagement et de transport. Implanté au cœur des territoires, le Cerema bénéficie d’une connaissance historique des problématiques et contextes locaux.

Cette proximité lui permet de proposer des solutions sur mesure aux acteurs des territoires et de mettre à leur disposition des interlocuteurs concernés, engagés et disponibles.

<img src="../static/logo_cerema.png"/>

Les métiers du Cerema s’organisent autour de 6 domaines d’action complémentaires visant à accompagner les acteurs territoriaux dans la réalisation de leurs projets :

* Expertise et ingénierie territoriale

* Bâtiment

* Mobilités

* Infrastructures de transport

* Environnement et risques

* Mer et littoral

Le Cerema est aujourd’hui l’expert public de l’adaptation au changement climatique. Il développe au quotidien des savoirs scientifiques et aide à déployer des solutions techniques pour sécuriser et améliorer le cadre de vie des citoyens.

