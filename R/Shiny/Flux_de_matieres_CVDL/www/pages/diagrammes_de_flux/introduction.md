Dans les diagrammes, le rectangle central représente le territoire, et symboliquement son activité socio-économique globale.

**Sur la gauche figurent les flux entrants dans le territoire** (on y inclut les productions de biomasse et les extractions de matériaux ayant lieu sur le territoire, mais également les importations venant de l’extérieur du territoire) : ces flux traduisent les besoins du territoire en matières pour fonctionner pendant 1 an.

**Sur la droite figurent les flux sortants du territoire** (on y inclut les rejets de matières vers la nature mais également les exportations vers l’extérieur du territoire) : ces flux traduisent ce que le territoire « expulse » dans son propre environnement mais également les matières et produits qui sortent de ses limites en une année.

NB : les extractions intérieures inutilisées figurent comme flux entrants et comme flux sortants, car les matières sont bien « extraites » du territoire, mais y restent après déplacement.

**Tous les menus déroulants situés après les « bilans matières » constituent des zooms sur un flux en particulier composant le bilan matières, dont la composition est suffisamment connue pour faire l’objet de ce zoom.**

**Les analyses détaillées des flux représentés ci-dessous sont disponibles dans les livrables de l’étude.**