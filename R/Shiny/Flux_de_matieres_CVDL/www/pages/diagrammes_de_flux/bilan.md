Le bilan matières représente l’agrégation de la totalité des données collectées dans le cadre de l’étude, regroupées selon les 7 familles de flux présentées dans l’onglet « Accueil / quelles données entrent en jeu ».

Ces diagrammes comptent plus de 7 flux, car : 
* les extractions intérieures inutilisées figurent en entrée et en sortie,
* les importations/exportations, flux d’équilibrage et flux indirects sont scindés en 2 flux (un flux entrant + un flux sortant).