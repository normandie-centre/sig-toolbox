**En passant de 550 g à 490 g pour produire 1 € de valeur ajoutée, l’intensité matière s’est améliorée** de 10,9 % entre 2014 et 2021 pour la région Centre-Val de Loire.
<br>

**De même, la productivité matière s’améliore** (il s’agit de deux lectures différentes des mêmes données) : **1 kg de matière brute permet de créer davantage de valeur ajoutée en 2021 (2,05 €) qu’en 2014 (1,83 €)**.
<br>

Les indicateurs régionaux montrent une performance moindre par rapport aux indicateurs nationaux (eux aussi en amélioration sur la période, mais une amélioration moins marquée qu’en région). Il est toutefois difficile de comparer ces indicateurs entre une région seule et un territoire national comptant l’Île-de-France, avec un PIB reposant très largement sur des valeurs ajoutées dans le secteur tertiaire, peu consommatrices de matières.