#### Descriptions des indicateurs

**MI  Intensité matière** : L’Intensité matière indique la quantité de matières associée à la création d’une unité de valeur ajoutée brute : en 2021, pour le Centre-Val de Loire, il faut 540 g de matière pour créer 1€ de valeur ajoutée brute.

**MP  Productivité matière** : La Productivité matière représente la quantité de valeur ajoutée brute par tonne de matières utilisées : en 2021, pour le Centre-Val de Loire, 1 kg de matière permet la création de 1,87€ de valeur ajoutée brute.

