#### Calculs des indicateurs d'efficience

| **Indicateurs « MFA »** | **Calcul** | **Indicateur – traduction utilisée dans la méthodologie CGDD** | **Unité** | **2014** | **2021** | **Evolution 2014-2021** |
| ------------------- | ------------------- | ------------------- |  ------------------- | ------------------- | ------------------- | ------------------- |  
| MI  (Material Intensity) | =DMC/PIB | Intensité matière | kg/€ | 0,55 | 0,49 | -10,9 % |
| MP  (Material Productivity) | =PIB/DMC | Productivité matière | €/kg | 1,83 | 2,05 | 12,0 % |

