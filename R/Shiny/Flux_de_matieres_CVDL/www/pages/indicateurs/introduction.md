#### Clefs de lecture

Les indicateurs dérivés des analyses de flux de matières sont calculés **à partir des valeurs issues de la comptabilité des flux de matières**. Seules sont prises en compte des valeurs agrégées (extractions intérieures utilisées, émissions dans la nature, flux d’équilibrage en entrée, flux indirects liés aux exportations, …).

Ces valeurs agrégées sont **rapportées à la population** (résultats en tonnes/habitant), puis combinées selon différentes formules, résultant en une série d’indicateurs qui permettent d’obtenir des **valeurs plus globales, représentatives de la performance d’un territoire en termes d’impacts sur les ressources**.

Ces indicateurs sont définis dans la méthodologie du CGDD, dont sont également issus les définitions, les modes de calcul, les traductions, ainsi que le « Schéma de principe et principaux indicateurs de l’AFM territoriale » présentés dans cette partie.

![alt text](static/schema_indicateurs.png)