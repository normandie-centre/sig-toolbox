Le taux de recyclage par habitant augmente de 21,9 % entre 2014 et 2021, **une progression importante** comparée à un indicateur stationnaire au niveau national.
<br>

Toutefois, cette différence de progression tient en partie au fait que les données France sont des données 2020, année de moindre production de déchets. Au demeurant, l’indicateur reste à un niveau de moins bonne performance en région qu’au niveau national.
<br>

Le calcul du taux de recyclage prenant en compte les déchets inertes valorisés par comblement de carrières, qui représentent 71 % de la masse totale de déchets valorisés (proportion comparable au niveau national), **il convient de relativiser la performance de cet indicateur dans l’application réelle de la valorisation des matières**.