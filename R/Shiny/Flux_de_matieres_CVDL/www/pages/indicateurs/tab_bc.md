#### Calculs des indicateurs d'entrée

| **Indicateurs « MFA »** | **Calcul** | **Indicateur – traduction utilisée dans la méthodologie CGDD** | **Unité** | **2014** | **2021** | **Evolution 2014-2021** |
| ------------------- | ------------------- | ------------------- |  ------------------- | ------------------- | ------------------- | ------------------- |  
| PTB  (Physical Balance Trade) | =E-I | Balance commerciale physique | en t/hab | -1,69 | -1,59 | 5,9 % |


<br>

_Valeurs intermédiares servant aux calculs d'indicateurs_

| **Indicateurs « MFA »** | **Calcul** | **Indicateur – traduction utilisée dans la méthodologie CGDD** | **Unité** | **2014** | **2021** | **Evolution 2014-2021** |
| ------------------- | ------------------- | ------------------- |  ------------------- | ------------------- | ------------------- | ------------------- |  
| I  (Importations) |  | Importations | en t/hab | 15,1 | 16,4 | 8,6 % |
| E  (Exportations) |  | Exportations | en t/hab | 13,4 | 14,8 | 10,4 % |

