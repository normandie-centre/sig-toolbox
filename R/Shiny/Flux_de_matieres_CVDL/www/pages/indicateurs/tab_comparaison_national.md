| **Familles d’indicateurs** | **Indicateur « MFA »** | **Indicateur traduit** | **Unité** | **CVDL en 2014** | **CVDL en 2021** | **Evolution CVDL 2014-2021** | **France en 2014** | **France en 2021** | **Evolution France 2014-2021** |
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| **Indicateurs d’entrée** | DEU | Extraction intérieure utilisée | en t/hab | 13,0 | 13,1 | 0,8 % | 9,9 | 10,5 | 6,1 % |
|  |  I | Importations | en t/hab | 15,1 | 16,4 | 8,6 % | 4,6 | 4,3 | -6,5 % |
|  |  DMI | Entrée directe de matière | en t/hab | 28,1 | 29,5 | 5,0 % | 14,5 | 14,8 | 2,1 % |
|  |  TMI | Entrée totale de matière | en t/hab | 40,8  | 41,5 | 1,7 % | 22,5 | 22,2 | -1.3 % |
|  |  TMR | Mobilisation totale de matière | en t/hab |  120,1 | 128,0 | 6,6 % | 42,8 | 41,6 | -2,8 % |
|  |   |   Extraction intérieure inutilisée | en t/hab | 12,7 | 12,0 | -5,5 % | 8,0 | 7,5 | -6,3 % |
|  |   |   Flux indirects associés aux importations | en t/hab | 79,4 | 86,5 | 8,9 % | 20,3 | 19,3 | -4,9 % |
|  |  BI | Flux d’équilibrage en entrée | en t/hab | 7,2 | 6,3 | -12,5 % | 7,9 | 6,8 | -13,9 % |
| **Indicateurs de sortie** | E | Exportations | en t/hab  | 13,4 | 14,8 | 10,4 % | 3,0 | 2,8 | -6,7 % |
|  |  DPO | Émissions vers la nature | en t/hab | 8,1 | 7,3 | -9,9 % | 8,7 | 7,8 | -10,3 % |
|  |  TDO | Émissions totales vers la nature | en t/hab | 20,7 | 19,4 | -6,3 % | 16,7 | 15,3 | -8,4 % |
|  |   |   Flux indirects associés aux exportations | en t/hab | 64,3 |  70,9 | 10,3 % | 16,7 | 15,6 | -6,6 % |
|  |  BO | Flux d’équilibrage en sortie | en t/hab |  4,9 | 4,4 | -10,2 % | 5,5 | 4,9 | -10,9 % |
| **Indicateurs de consommation** | DMC | Consommation intérieure apparente de matière | en t/hab  | 14,7 | 14,7 | 0,0 % | 11,5 | 12,0 | 4,3 % |
|  | TMC | Consommation intérieure totale estimée de matières | en t/hab |  42,5 | 42,4 | -0,2 % | 23,1 | 23,1 | 0,0 % |
| **Indicateur de stock** | NAS | Addition nette au stock | en t/hab | 8,9 | 9,3 | 4,5 % | 5,2 | 6,1 | 17,3 % |
| **Indicateur de balance commerciale physique** | PTB | Balance commerciale physique | en t/hab | -1,69 | -1,59 | 5,9 % | -1,63 | -1,55 | -4,9 % |
| **Indicateurs d’efficience** | MI | Intensité matières | kg/€ | 0,55 | 0,49 | -10,9 % | 0,35 | 0,32 | -8,6 % |
|  |   MP | Productivité matière | €/kg | 1,83 |  2,05 | 12,0 % | 2,87 | 3,12 | 8,7 % |
| **Recyclage** |  | Recyclage | en t/hab |  2,15 | 2,62 | 21,9 % | 2,99 | 2,99 | 0,0 % |
