#### Calculs des indicateurs de stock

| **Indicateurs « MFA »** | **Calcul** | **Indicateur – traduction utilisée dans la méthodologie CGDD** | **Unité** | **2014** | **2021** | **Evolution 2014-2021** |
| ------------------- | ------------------- | ------------------- |  ------------------- | ------------------- | ------------------- | ------------------- |  
| NAS  (Net Addition to Stock) | =DMI+BI-DPO-E-BO | Addition nette au stock | en t/hab | 8,9 | 9,3 | 4,5 % |

