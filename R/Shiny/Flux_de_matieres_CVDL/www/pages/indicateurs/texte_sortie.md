**Les indicateurs de sortie** traduisent les **rejets** de matières dans l’environnement régional et les **sorties** de matières vers l’extérieur du territoire.
<br>

Pour **un seul habitant**, en 2021 :
* **7,3 tonnes** de matières ont été émises dans l’environnement régional.
<br>

En parallèle :
* **12,0 tonnes de matières ont été mobilisées en région mais n’ont pas été utilisées** (elles ont été seulement déplacées) : ce sont les extractions intérieures inutilisées ;
<br>

Soit une **quantité totale de 19,4 tonnes par habitant de matières constituant les « émissions totales vers la nature »**.
<br>
Par ailleurs, **14,8 tonnes** de matières sont exportées vers l’extérieur de la région.