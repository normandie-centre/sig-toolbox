#### Descriptions des indicateurs

**DMC  Consommation intérieure apparente de matière** : Ensemble des matières consommées par le système socio-économique étudié, au sens économique du terme. L’indicateur DMC est classique en économie* et représente la consommation nette intérieure d’un territoire donné.

**TMC  Consommation intérieure totale estimée de matières** : Ensemble des matières consommées par le territoire, incluant l’extraction intérieure inutilisée et les flux indirects associés aux importations. L’indicateur TMC étend la notion de consommation à l’ensemble des flux indirects pour comprendre le poids total de matières liées à la consommation ou engendrées par les activités économiques d’un territoire donné.
