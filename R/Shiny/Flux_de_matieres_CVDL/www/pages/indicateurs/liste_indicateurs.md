#### Liste des indicateurs


|     **Indicateurs**                  | **Indicateur « MFA »** | **Indicateur – nom complet**       | **Indicateur – traduction, utilisée dans la méthodologie CGDD**       |
|------------------------------------------------|------------------------|-----------------------------------|-----------------------------------------------------------------------|
| *Indicateurs d’entrée*                       |                        |                                   |                                                                       |
|                                                | DEU                    | Domestic extraction used          | Extraction intérieure utilisée                                        |
|                                                | I                      | Importations                      | Importations                                                          |
|                                                | DMI                    | Direct Material Input             | Entrée directe de matière                                             |
|                                                | TMI                    | Total Material Input              | Entrée totale de matière                                              |
|                                                | TMR                    | Total Material Requirement        | Mobilisation totale de matière                                        |
|                                                |  valeur intermédiaire  | servant au calcul d'indicateurs   | Extraction intérieure inutilisée
|                                                |  valeur intermédiaire  | servant au calcul d'indicateurs   | Flux indirects associés aux importations
|                                                | BI                     | Balancing Input                   | Flux d’équilibrage en entrée                                          |
|                                                |                        |                                   |                                                                       |
| *Indicateurs de sortie*                      |                        |                                   |                                                                       |
|                                                | E                      | Exports                           | Exportations                                                          |
|                                                | DPO                    | Domestic Processed Output         | Émissions vers la nature                                              |
|                                                | TDO                    | Total Domestic Output             | Émissions totales vers la nature                                      |
|                                                |  valeur intermédiaire  | servant au calcul d'indicateurs   | Flux indirects associés aux exportations
|                                                | BO                     | Balancing Output                  | Flux d’équilibrage en sortie                                          |
| *Indicateurs de consommation*                |                        |                                   |                                                                       |
|                                                | DMC                    | Domestic Material Consumption     | Consommation intérieure apparente de matière                          |
|                                                | TMC                    | Total Material Consumption        | Consommation intérieure totale estimée de matières                    |
|                                                |                        |                                   |                                                                       |
| *Indicateur de stock*                        | NAS                    | Net Additions to Stock            | Addition nette au stock                                               |
|                                                |                        |                                   |                                                                       |
| *Indicateur de balance commerciale physique* | PTB                    | Physical Trade Balance            | Balance commerciale physique                                          |
|                                                |                        |                                   |                                                                       |
| *Indicateurs d’efficience*                   |                        |                                   |                                                                       |
|                                                | MI                     | Material Intensity                | Intensité matières                                                    |
|                                                | MP                     | Material Productivity             | Productivité matière                                                  |
|                                                |                        |                                   |                                                                       |
| *Recyclage*                                  |  indicateur spécifique | à la méthodologie CGDD            | Recyclage                          |



#### Données utilisées

Pour les calculs, les populations Insee (Recensement de Population) suivantes ont été utilisées : 		
        
| **Insee** | **Centre-Val de Loire** | **France** |
| ------------------- | ------------------- | ------------------- | 
| Population 2014   |    2 577 435   |      64 027 958       |
| Population 2021    |      2 573 303    |      65 505 213      |
<br>
Pour les calculs des indicateurs d’efficience, les valeurs de PIB suivantes ont été utilisées (millions d’€) : 		
        
|   **Insee**    |   **Centre-Val de Loire**     |	    **France**   |
| ------------- | ------------- | ------------- | 
|    PIB 2014    |   69 472  |   2 108 550 |
|   PIB 2021   |     77 773  |   2 454 491 |
