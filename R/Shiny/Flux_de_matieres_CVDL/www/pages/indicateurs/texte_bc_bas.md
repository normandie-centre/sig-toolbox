Le niveau d’importations plus élevé que le niveau d’exportations, qui traduit une **dépendance croissante à l’extérieur pour l’approvisionnement du territoire**, demeure une constante en 2014 et en 2021, se traduisant par un indicateur négatif (cet indicateur est également négatif à l’échelle de la France).
<br>

Toutefois, en région, cette balance négative tend à diminuer, en raison d’exportations qui ont plus largement progressé (+ 10,4 % par habitant) que les importations (+ 8,6 % par habitant).