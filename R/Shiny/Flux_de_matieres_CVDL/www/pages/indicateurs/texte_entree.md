**Les indicateurs d’entrée traduisent les besoins de matières du territoire** : quelles matières doit-on produire sur le territoire et faire entrer dans le territoire, afin d’assurer son fonctionnement socio-économique pendant 1 an ?
<br>

Pour **un seul habitant**, en 2021 :
* **13,1 tonnes** de matières ont été extraites du territoire régional.
<br>

En parallèle, pour compléter cette production intérieure, il faut ajouter aux besoins de cet habitant : 
* **12,0 tonnes** de matières déplacées en région mais qui ne seront pas utilisées (extractions intérieures inutilisées) ;
* **16,4 tonnes** de matières venant de l’extérieur de la région (importations), qui ont elles-mêmes nécessité la mobilisation supplémentaire de **86,5 tonnes** de matières à l’extérieur du territoire régional (flux indirects liés aux importations).
<br>

Soit une **quantité totale de 128 tonnes de matières mobilisées pour satisfaire les besoins d’un habitant.**