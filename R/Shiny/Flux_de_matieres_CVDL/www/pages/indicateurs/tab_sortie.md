#### Calculs des indicateurs de sortie

| **Indicateur « MFA »** | **Calcul** | **Indicateur – traduction utilisée dans la méthodologie CGDD** | **Unité** | **Centre Val de Loire 2014** | **Centre Val de Loire 2021** | **Evolution Centre-Val de Loire 2014-2021** | 
| ------------------- | ------------------- | ------------------- |  ------------------- | ------------------- | ------------------- | ------------------- |  
| E  (Exportations) |   | Exportations | en t/hab | 13,4 | 14,8 | 10,4 % | 
| DPO  (Domestic Processed Ouput) |   | Émissions vers la nature | en t/hab | 8,1 | 7,3 | -9,9 % | 
| TDO  (Total Domestic Output) | = DPO + extraction intérieure inutilisée | Émissions totales vers la nature | en t/hab | 20,7 | 19,4 | -6,3 % | 

<br>

_Valeurs intermédiares servant aux calculs d'indicateurs_

| **Indicateur « MFA »** | **Calcul** | **Indicateur – traduction utilisée dans la méthodologie CGDD** | **Unité** | **Centre Val de Loire 2014** | **Centre Val de Loire 2021** | **Evolution Centre-Val de Loire 2014-2021** | 
| ------------------- | ------------------- | ------------------- |  ------------------- | ------------------- | ------------------- | ------------------- |  
| | | Extraction intérieure inutilisée | en t/hab | 12,7 | 12,0 | -5,5 % | 
| BO  (Balancing Ouput) |  | Flux d’équilibrage en sortie | en t/hab | 4,9 | 4,4 | -10,2 % | 
