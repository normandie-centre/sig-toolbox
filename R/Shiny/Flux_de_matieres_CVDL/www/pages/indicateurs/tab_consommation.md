#### Calculs des indicateurs de consommation

| **Indicateurs « MFA »** | **Calcul** | **Indicateur – traduction utilisée dans la méthodologie CGDD** | **Unité** | **2014** | **2021** | **Evolution 2014-2021** |
| ------------------- | ------------------- | ------------------- |  ------------------- | ------------------- | ------------------- | ------------------- |  
| DMC  (Domestic Material Consumption) | =DEU+I-E =DMI-E | Consommation intérieure apparente de matière | en t/hab | 14,7 | 14,7 | 0,0 % |
| TMC  (Total Material Consumption) | =DMC + extraction intérieure inutilisée + flux indirects associés aux importations - flux indirects associés aux exportations | Consommation intérieure totale estimée de matières | en t/hab | 42,5 | 42,4 | -0,2 % |
