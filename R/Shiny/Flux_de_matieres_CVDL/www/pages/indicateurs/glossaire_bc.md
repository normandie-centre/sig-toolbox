#### Descriptions des indicateurs

**PBT  Balance commerciale physique** : Pendant physique de la balance commerciale monétaire. L’évolution de la balance physique peut être comparée à celle de la balance commerciale du territoire. Cette comparaison peut notamment permettre de voir si le territoire* exporte des produits qui présentent une plus grande valeur monétaire que ceux qu’il importe, ou si c’est le cas inverse.
