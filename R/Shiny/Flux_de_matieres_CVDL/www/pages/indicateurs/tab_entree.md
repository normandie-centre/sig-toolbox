#### Calculs des indicateurs d'entrée

| **Indicateurs « MFA »** | **Calcul** | **Indicateur – traduction utilisée dans la méthodologie CGDD** | **Unité** | **2014** | **2021** | **Evolution 2014-2021** |
| ------------------- | ------------------- | ------------------- |  ------------------- | ------------------- | ------------------- | ------------------- |  
|DEU  (Domestic Extraction Used) | | Extraction intérieure utilisée | en t/hab | 13,0 | 13,1 | 0,8 % |
|DMI  (Direct Material Input) | =DEU+I | Entrée directe de matière | en t/hab | 28,1 | 29,5 | 5,0 % |
|TMI  (Total Material Input) | =DMI+extraction intérieure inutilisée  | Entrée totale de matière | en t/hab | 40,8 | 41,5 | 1,7 % |
|TMR  (Total Material Requirement) | =DMI + extraction intérieure inutilisée + flux indirects associés aux importations | Mobilisation totale de matière | en t/hab | 120,1 | 128,0 | 6,6 % |

<br>

_Valeurs intermédiares servant aux calculs d'indicateurs_

| **Indicateurs « MFA »** | **Calcul** | **Indicateur – traduction utilisée dans la méthodologie CGDD** | **Unité** | **2014** | **2021** | **Evolution 2014-2021** |
| ------------------- | ------------------- | ------------------- |  ------------------- | ------------------- | ------------------- | ------------------- |  
| I  (Importations) |  | Importations | en t/hab | 15,1 | 16,4 | 8,6 % |
|   |    |  Extraction intérieure inutilisée | en t/hab | 12,7 | 12,0 | -5,5 % |
|   |    |  Flux indirects associés aux importations | en t/hab | 79,4 | 86,5 | 8,9 % |
| BI  (Balancing Input) |  | Flux d’équilibrage en entrée |  | 7,2 | 6,3 | -12,5 % |
