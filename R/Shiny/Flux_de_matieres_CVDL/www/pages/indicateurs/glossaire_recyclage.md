#### Descriptions des indicateurs

**Recyclage** : Quantité de déchets ménagers et assimilés, déchets non dangereux, déchets inertes et déchets dangereux valorisés.
