L’augmentation de l’addition nette au stock (+4,5 % entre 2014 et 2021), est à attribuer, en région, à l’augmentation marquée des importations sur le territoire (+8,5 % par habitant). L’augmentation encore plus importante des exportations sur cette période (+10,3 % par habitant), explique que l’**augmentation d’accumulation de matière sur le territoire reste modérée**.
<br>

Cette accumulation est aussi à attribuer, dans une moindre mesure, à l’augmentation plus importante des flux d’équilibrage en entrée qu’en sortie.