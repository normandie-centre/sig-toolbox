#### Calculs des indicateurs de recyclage

| **Indicateurs « MFA »** | **Unité** | **2014** | **2021** | **Evolution 2014-2021** |
| ------------------- | ------------------- | ------------------- |  ------------------- | ------------------- | 
| Recyclage | en t/hab | 2,15 | 2,62 | 21,9 % |

