Les livrables suivants ont été produits :

- **rapport d’étude partie 1 : « Méthodologie et résultats »** (64 pages), dans lequel sont présentées les données brutes à la base des agrégations, ainsi que l’ensemble des informations relatives à la collecte de données (sources, méthodes d’estimation le cas échéant, problématiques de conversion en tonnes, disponibilité des données suivant les années et l’échelle départementale, etc.) ;

- **rapport d’étude partie 2 : « Analyses et enseignements »** (96 pages), dans lequel sont présentés un portrait succinct de la région Centre-Val de Loire, l’ensemble des diagrammes de flux analysés et commentés, l’ensemble des indicateurs dérivés analysés et commentés, les enjeux et préconisations dans une perspective d’économie circulaire, et une synthèse des enseignements de l’étude.
 
- Une annexe présentant les **focus thématiques « agriculture » et « BTP et granulats »** (44 pages),

- Une **annexe « Synthèse de l’étude »** (16 pages)

<br>

Ces livrables sont consultables sur le site <a href="https://doc.cerema.fr/" target="_blank">doc.cerema.fr</a>

<div class="button-container">
    <a href="https://doc.cerema.fr/search.aspx?SC=DEFAULT&QUERY=flux+de+mati%C3%A8res&QUERY_LABEL=#/Detail/(query:(Id:'1_OFFSET_0',Index:2,NBResults:22,PageRange:3,SearchQuery:(FacetFilter:%7B%7D,ForceSearch:!f,InitialSearch:!f,Page:0,PageRange:3,QueryGuid:'7f5ef624-0a7f-4c7d-b66a-863e7d029c3e',QueryString:'flux%20de%20mati%C3%A8res',ResultSize:10,ScenarioCode:DEFAULT,ScenarioDisplayMode:display-standard,SearchGridFieldsShownOnResultsDTO:!(),SearchLabel:'',SearchTerms:'flux%20de%20mati%C3%A8res',SortField:DateTRI_sort,SortOrder:0,TemplateParams:(Scenario:'',Scope:Default,Size:!n,Source:'',Support:'',UseCompact:!f),UseSpellChecking:!n)))" class="report-button" target="_blank">📄 Lien direct vers le rapport d’étude 1</a>
    <a href="https://doc.cerema.fr/search.aspx?SC=DEFAULT&QUERY=flux+de+mati%C3%A8res&QUERY_LABEL=#/Detail/(query:(Id:'0_OFFSET_0',Index:1,NBResults:22,PageRange:3,SearchQuery:(FacetFilter:%7B%7D,ForceSearch:!f,InitialSearch:!f,Page:0,PageRange:3,QueryGuid:'46cb324f-6d3f-4245-afa5-ff6779bfd80f',QueryString:'flux%20de%20mati%C3%A8res',ResultSize:10,ScenarioCode:DEFAULT,ScenarioDisplayMode:display-standard,SearchGridFieldsShownOnResultsDTO:!(),SearchLabel:'',SearchTerms:'flux%20de%20mati%C3%A8res',SortField:DateTRI_sort,SortOrder:0,TemplateParams:(Scenario:'',Scope:Default,Size:!n,Source:'',Support:'',UseCompact:!f),UseSpellChecking:!n)))" class="report-button" target="_blank">📄 Lien direct vers le rapport d’étude 2<br>et les deux annexes</a>
</div>