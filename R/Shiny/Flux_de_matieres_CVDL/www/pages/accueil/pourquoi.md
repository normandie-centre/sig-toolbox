Cette étude a été commandée par **la DREAL et le Conseil régional Centre-Val de Loire**, et menée par **le Cerema**, concepteur de ce site.

Il s’agit de l’**actualisation** d’une première comptabilité menée sur les données régionales 2014. L’objectif est donc de **comparer la situation 2021 à la situation 2014**, et d’analyser les évolutions.

Cette étude a vocation à contribuer à l’élaboration de la **stratégie régionale d’économie circulaire**.

En complément de l’application stricto sensu de la comptabilité de flux de matières, des recherches plus poussées ont été faites sur 4 sujets, afin d’en comprendre les enjeux en termes d’économie circulaire. Ainsi, **4 focus thématiques** complètent l'analyse : 

- un focus « agriculture » et un focus « BTP et granulats », publiés en même temps que les rapports d'étude,

- un focus « méthanisation-compostage » et un focus « filière bois », dont la publication est différée pour ne pas interférer avec le Schéma régional biomasse en cours.