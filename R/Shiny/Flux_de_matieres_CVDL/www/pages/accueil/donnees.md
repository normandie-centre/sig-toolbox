Données classées selon les 7 familles de flux : 

* **Extraction intérieure utilisée** : biomasse (agricole, sylvicole, aquatique), chasse et ramassage, minerais métalliques, minéraux non-métalliques, combustibles fossiles ;

* **Extraction intérieure inutilisée** : érosion des terres arables, terres d’excavation, résidus de récolte inutilisées, résidus de coupe des arbres, extractions inutilisées issues de l’exploitation minière, boues de dragage des ports et voies navigables, produit de la pêche rejeté en mer ;

* **Importations/exportations** : tous flux, sur la base des données des Douanes et de données de transport de marchandises, classées par catégorie de marchandises ;

* **Émissions vers la nature** : rejets dans l’air, production de déchets, rejets dans l’eau, utilisation de produits dissipatifs, pertes dissipatives ;

* **Éléments d’équilibrage** : éléments gazeux en entrée (oxygène pour les processus de combustion, oxygène pour la respiration, azote pour le procédé Haber-Bosch), éléments gazeux en sortie (vapeur pour la combustion, émissions liées à la respiration) ;

* **Addition nette au stock** : calcul sur la base des données précédentes ;

* **Flux indirects liés aux importations/exportations** : calcul sur la base des données précédentes.

L’ensemble des sources de données sont disponibles dans l’onglet « Sources de données ».