Depuis 2013, les états-membres de l’Union européenne ont une obligation de rapportage auprès d’Eurostat de leurs comptes de flux de matières, dans le cadre du [règlement européen n°691/2011 relatif aux comptes économiques européens de l’environnement](https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:2011:192:0001:0016:FR:PDF). Sur la base de la **classification Eurostat « MFA » (Material Flow Analysis)** élaborée dans ce cadre, une [méthodologie adaptée aux régions et aux départements a été développée par le Commissariat général au développement durable (CGDD)](https://www.ecologie.gouv.fr/sites/default/files/documents/EIT%20-%20comptabilite%20des%20flux%20de%20matieres.pdf) en 2014. C’est cette méthodologie qui a été utilisée dans l’étude initiale et pour la présente actualisation.

Celle-ci vise l’**exhaustivité des flux**, qu’elle quantifie en tonnes/an quelle que soit la matière, incluant matières brutes, produits semi-finis et produits finis. Les flux sont regroupés en **7 familles** : 

- les extractions intérieures utilisées,

- les extractions intérieures inutilisées,

- les importations et les exportations,

- les émissions vers la nature,

- les flux d’équilibrage,

- l’addition nette au stock,

- les flux indirects liés aux importations et aux exportations.


L’exercice de collecte des données a été fait pour le Centre-Val de Loire, mais également pour la France, afin de **comparer les évolutions entre le niveau régional et le niveau moyen à l’échelle nationale**.

Afin de compléter l’approche quantitative, des **entretiens ont été menés avec différents acteurs sur le territoire** (DREAL, Conseil régional, DRAAF, Fibois, GRDF, DREETS, Dev’Up, Chambre des métiers et de l’artisanat, CERC, Ademe), notamment pour approfondir les thématiques suivantes, définies collégialement par les parties prenantes de l’étude à l’issue de la phase de collecte de données : la méthanisation, les combustibles fossiles, le bois (matière et énergie), l’agriculture, les matériaux du BTP, les déchets, les dynamiques économiques et industrielles.
