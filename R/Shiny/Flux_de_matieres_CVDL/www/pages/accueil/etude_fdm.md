
Les études de flux de matières appartiennent à la famille des **études de métabolisme territorial**. La notion de métabolisme territorial repose sur l’idée qu’un territoire, comme un être humain, a besoin de matières et d’énergie pour fonctionner, et rejette dans la nature, ce faisant, les déchets et polluants résultant de ce fonctionnement. Étudier le métabolisme d’un territoire consiste à répertorier et quantifier les flux de matières entrantes et sortantes de ce territoire du fait de son fonctionnement socio-économique.

Ainsi, cette étude de comptabilité des flux de matières propose une **photographie de l’ensemble des matières** (matières brutes, produits semi-finis et produits finis) impliquées dans le « métabolisme territorial » du Centre-Val de Loire pendant une année, exprimées en tonnes par an.

En se conformant à une **classification particulière**, le résultat permet de répondre à de nombreuses questions : quelle quantité de matières a été produite par le territoire ? quelles matières ont été importées et exportées ? quelles matières ont été émises vers la nature ? quelle quantité moyenne de matière est consommée annuellement par un habitant ? quelles matières ont été déplacées mais non utilisées ? Etc.

<img src="../static/economie_circulaire.png"/>

Il s’agit de porter un **regard global et transversal, tant quantitatif que qualitatif, sur les matières nécessaires au territoire et produit ou rejeté par celui-ci**, dont l’analyse nous permettra notamment de comprendre les dynamiques territoriales spécifiques à la région, le degré de dépendance du territoire à l’extérieur et l’impact des activités en termes de rejets dans la nature.
