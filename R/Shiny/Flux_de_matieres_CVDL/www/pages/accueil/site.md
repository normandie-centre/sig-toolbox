<u>Les diagrammes de flux</u>

Ces diagrammes sont la **représentation du résultat de la comptabilisation** : 

- les « bilans matières » représentent les 7 familles de flux,

- les diagrammes par famille de flux détaillent les « sous-flux » qui composent la famille,

- pour certains « sous-flux », un diagramme détaille encore la composition par matière ou type de matière.


Ils ont été élaborés systématiquement pour représenter :

- la situation en Centre-Val de Loire en 2014 puis en 2021,

- la situation en France en 2014 puis 2021,

soit **4 représentations par type de diagramme**.


**Dans ces diagrammes, chaque flux montre une épaisseur proportionnelle à la valeur qu’il représente** (« diagramme de Sankey »), permettant de visualiser le poids relatif des flux sur le territoire étudié.


Les diagrammes sont donc présentés de la vision la plus globale et transversale (« bilans matières »), à la vision la plus détaillée. Ce travail d’agrégation des données repose en réalité sur une méthode inverse, qui commence par collecter, matière par matière, les données correspondantes, pour ensuite effectuer les agrégations résultant en familles de flux, puis aboutir à un bilan matière, résultat de l’agrégation de l’ensemble des données collectées (**méthode « bottom-up »**, par opposition à d’autres méthodes dites « top-down »). 

<br>

<u>Les données par famille de flux</u>

Dans cet onglet **sont représentées l’ensemble des données collectées** pour aboutir au travail d’agrégation nécessaire au « bilan matières ». Cela permet d’accéder notamment aux données intermédiaires entre 2014 et 2021 lorsqu’elles sont disponibles, et à des données détaillées dont les diagrammes de flux ne rendent pas nécessairement compte. 

Les fonctionnalités proposées permettent d’isoler une matière ou un flux dans un graphique, de zoomer ou dézoomer, ou d’exporter le graphique : ces fonctionnalités sont présentées dans l’onglet.

<br>

<u>Flux importations/exportations</u>

Dans cette partie sont présentées les **cartes de flux** qui ont pu être établies sur la base des données d’importations et d’exportations entre le Centre-Val de Loire et l’extérieur du territoire.

Certains flux ne sont pas représentés (notamment des flux d’échanges avec d’autres régions françaises par catégorie de matières) en raison du secret statistique.

Les transports de marchandises par voie aérienne n’ont pas pu être inclus car les données ne permettent pas de distinguer ce qui arrive de ce qui repart. Toutefois les tonnages de marchandises qui transitent par l’aéroport de Châteauroux nous indiquent que les quantités sont négligeables par rapport aux autres modes de transport.

<br>

<u>Les indicateurs dérivés</u>

Les indicateurs dérivés de l’analyse de flux de matières consistent :

- à **proportionner les flux comptabilisés avec le niveau de population** (indicateurs exprimés pour la plupart en tonnes/habitant), permettant ainsi la comparaison avec d’autres territoires,

- à opérer des calculs (additions, soustractions, multiplications, divisions), sur la base des flux précédemment mesurés, permettant d’aboutir à une **vision synthétique de l’impact du fonctionnement d’un territoire sur la consommation de ressources**.

