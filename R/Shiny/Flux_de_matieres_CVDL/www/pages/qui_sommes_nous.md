Le [Cerema](https://www.cerema.fr/), établissement public relevant du ministère de la Transition écologique et de la Cohésion des territoires, accompagne l’État et les collectivités territoriales pour l’élaboration, le déploiement et l’évaluation de politiques publiques d’aménagement et de transport. Implanté au cœur des territoires, le Cerema bénéficie d’une connaissance historique des problématiques et contextes locaux.

Cette proximité lui permet de proposer des solutions sur mesure aux acteurs des territoires et de mettre à leur disposition des interlocuteurs concernés, engagés et disponibles.

<img src="../static/logo_cerema.png"/>

Les métiers du Cerema s’organisent autour de 6 domaines d’action complémentaires visant à accompagner les acteurs territoriaux dans la réalisation de leurs projets :

* Expertise et ingénierie territoriale

* Bâtiment

* Mobilités

* Infrastructures de transport

* Environnement et risques

* Mer et littoral

Le Cerema est aujourd’hui l’expert public de l’adaptation au changement climatique. Il développe au quotidien des savoirs scientifiques et aide à déployer des solutions techniques pour sécuriser et améliorer le cadre de vie des citoyens.

