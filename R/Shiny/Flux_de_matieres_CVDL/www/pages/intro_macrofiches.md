Dans les graphiques présentés dans cet onglet, vous pouvez : 

- visualiser une donnée en survolant le graphique, et accéder ainsi à la donnée chiffrée correspondant à un item du graphique pour une année particulière,

- isoler une matière/un flux : en cliquant sur un item de la légende, vous pouvez ajouter ou enlever la donnée correspondante du graphique ; en double-cliquant sur un item, vous pouvez isoler cette donnée du reste des données du graphique, 

- zoomer et dézoomer sur le graphique afin de jouer sur sa taille de présentation.

L’unité M correspond à des millions de tonnes ; l’unité B correspond à des milliards de tonnes.
