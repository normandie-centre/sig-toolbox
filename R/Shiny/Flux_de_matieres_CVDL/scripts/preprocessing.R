library(arrow)
library(dplyr)
library(glue)
library(readr)
library(stringr)


############
## Fonctions
############

# Recherche de l'encodage du fichier
get_encoding <- function (file) {
    file %>% guess_encoding() %>% .[[1]] %>% .[[1]]
}

# Récupération des colonnes automatiquement avec ajout de colonnes aléatoires si champs inutilisés
get_columns <- function(file) {
    if (grepl("national", file, ignore.case = TRUE)) {
        col <- c("flux", "mois", "annee", "code_cpf6", "code_a129", "code_nc8", "code_pays", "valeur", "masse", "unite")
    }
    else if (grepl("regional", file, ignore.case = TRUE)) {
        col <- c("flux", "trimestre", "annee", "code_dpt", "code_reg", "code_a129", "code_cpf4", "code_pays", "valeur", "masse")
        nb_col_file <- file %>% readLines(n = 1) %>% str_count(";") + 1
        if (nb_col_file > length(col)) col <- c(col, paste0("temp_", seq(1, nb_col_file - length(col))))
    }
    return(col)
}

# Lecture des fichiers avec bon encodage
read <- function(file, cols = get_columns(file)) {
    read.table(file, sep = ";", header = FALSE, fileEncoding = get_encoding(file), stringsAsFactors = FALSE, col.names = cols, quote = "\"")
}


##############
#### Variables
##############
format <- "parquet"
data_folder <- "www/data/"
echelles <- c("national", "regional")
types <- c("import", "export")
annees <- seq(2014, 2021)


#############
## Constantes
#############
col_a129 <- c("code", "libelle_a129", "date_debut_validite", "date_fin_validite")
col_cpf <- c("code", "libelle_cpf", "date_debut_validite", "date_fin_validite")
col_nc8 <- c("code", "libelle_nc8", "presence_unite_supplementaire", "nature_unite", "date_debut_validite", "date_fin_validite")
col_pays <- c("code", "libelle_pays", "date_debut_validite", "date_fin_validite")
conversion_mfa_cgdd_file <- "www/data/conversion_mfa_cgdd_utf8.csv"
conversion_mfa_cn_file <- "www/data/conversion_mfa_cn.csv"
conversion_missing_nc_file <- "www/data/conversion_missing_nc.csv"
missing_nc6 <- c()
missing_mfa <- c()


############
##  MAIN  ##
############
for (annee in annees) {
    for (type in types) {
        for (echelle in echelles) {
            outfile <- glue(data_folder, type, "_", echelle , "_", annee, ".", format)
            folder <- list.files(data_folder, pattern = glue(echelle, ".*",  annee, ".*", type), full.names = TRUE, ignore.case = TRUE)

            # Listing des fichiers
            data <- list.files(folder, pattern = type, full.names = TRUE, recursive = TRUE, ignore.case = TRUE)
            libelle_a129 <- list.files(folder, pattern = "A129", full.names = TRUE, recursive = TRUE, ignore.case = TRUE)
            libelle_pays <- list.files(folder, pattern = "PAYS", full.names = TRUE, recursive = TRUE, ignore.case = TRUE)
            libelle_nc8 <- list.files(folder, pattern = "NC", full.names = TRUE, recursive = TRUE, ignore.case = TRUE)
            libelle_cpf <- list.files(folder, pattern = "CPF", full.names = TRUE, recursive = TRUE, ignore.case = TRUE)

            ## Lecture des données
            data_a129 <- read(libelle_a129, col_a129)
            data_cpf <- read(libelle_cpf, col_cpf)
            data_pays <- read(libelle_pays, col_pays)
            if (echelle == "national") data_nc8 <- read(libelle_nc8, col_nc8)

            conversion_mfa_cgdd <- read.table(conversion_mfa_cgdd_file, fileEncoding = get_encoding(conversion_mfa_cgdd_file), sep = ";", header = TRUE, quote = "\"")
            conversion_missing_nc <- read.table(conversion_missing_nc_file, fileEncoding = get_encoding(conversion_missing_nc_file), sep = ",", header = TRUE, quote = "\"")
            conversion_mfa_cn <- read.table(conversion_mfa_cn_file, fileEncoding = get_encoding(conversion_mfa_cn_file), sep = ";", header = TRUE, quote = "\"") %>% 
                filter(date_fin_validite > glue(annee, "0000") %>% as.integer()) %>%
                mutate(cn = gsub("^0", "", cn)) %>%
                group_by(mfa, cn) %>% 
                summarise(.groups = "drop")

            ## Traitement
            if (echelle == "national") {
                df <- read(data) %>%
                    filter(code_nc8 != "27160000") %>%
                    group_by(annee, code_cpf6, code_nc8, code_a129, code_pays) %>% 
                    summarise(masse_annuelle = sum(masse), .groups = "drop") %>%
                    merge(data_a129 %>% select("code", "libelle_a129"), by.x = "code_a129", by.y = "code", all.x = TRUE) %>% 
                    merge(data_cpf %>% select("code", "libelle_cpf"), by.x = "code_cpf6", by.y = "code", all.x = TRUE) %>%
                    merge(data_nc8 %>% select("code", "libelle_nc8"), by.x = "code_nc8", by.y = "code", all.x = TRUE) %>%
                    merge(data_pays %>% filter(date_fin_validite > glue(annee, "00") %>% as.integer()) %>% select("code", "libelle_pays"), by.x = "code_pays", by.y = "code", all.x = TRUE) %>%
                    merge(conversion_mfa_cn %>% select(mfa, cn), by.x = "code_nc8", by.y = "cn", all.x = TRUE) %>%
                    merge(conversion_mfa_cgdd %>% select(mfa, libelle_cgdd), by.x = "mfa", by.y = "mfa", all.x = TRUE) %>%
                    merge(conversion_missing_nc %>% select("code", "cgdd"), by.x = "code_nc8", by.y = "code", all.x = TRUE) %>%
                    mutate(libelle_cgdd = coalesce(libelle_cgdd, cgdd)) %>% select(-"cgdd")
                    
                    missing_nc6 <- c(missing_nc6, df %>% filter(is.na(libelle_cgdd) | libelle_cgdd == "") %>% pull(code_nc8) %>% unique)

                    df %>% write_parquet(outfile)
            } else if (echelle == "regional") {
                read(data) %>%
                    group_by(annee, code_cpf4, code_a129, code_pays, code_reg) %>% 
                    summarise(masse_annuelle = sum(masse), .groups = "drop") %>%
                    merge(data_a129 %>% select("code", "libelle_a129"), by.x = "code_a129", by.y = "code", all.x = TRUE) %>% 
                    merge(data_cpf %>% select("code", "libelle_cpf"), by.x = "code_cpf4", by.y = "code", all.x = TRUE) %>%
                    merge(data_pays %>% filter(date_fin_validite > glue(annee, "00") %>% as.integer()) %>% select("code", "libelle_pays"), by.x = "code_pays", by.y = "code", all.x = TRUE) %>%
                    filter(code_reg == "4" | code_reg == "04") %>%
                    write_parquet(outfile)
            }

            message("Écriture de ", outfile, " terminé.")
        }
    }
}

message("Code NC6 non trouvé : ")
print(missing_nc6 %>% unique)
