suppressMessages(library(dplyr))  # Charge le package dplyr sans afficher les messages
suppressMessages(library(readODS))  # Charge le package readODS sans afficher les messages
suppressMessages(library(zoo))  # Charge le package readODS sans afficher les messages

#############
##  Fonctions

# Fonction pour lire les données des feuilles et les traiter
read_and_process <- function(file, sheet_name) {
  read_ods(file, sheet = sheet_name, col_names = FALSE) %>%  # Lit la feuille spécifiée dans le fichier ODS sans utiliser la première ligne comme noms de colonnes
    fill_horizontal()  # Applique la fonction `fill_horizontal` pour gérer les cellules manquantes
}

# Fonction pour remplir les cellules manquantes horizontalement (de gauche à droite)
fill_horizontal <- function(df) {
  # Applique une transformation sur chaque ligne individuellement
  df[] <- t(apply(df, 1, function(row) {
    na.fill(row, fill = "left")  # Remplit les valeurs manquantes de chaque ligne en utilisant `na.fill`
  }))
  return(df)
}

# Fonction pour remplacer les valeurs NA avec les valeurs à gauche ou à droite
na.fill <- function(x, fill = "left") {
  if (fill == "left") {
    # Remplit les valeurs manquantes avec les valeurs situées à gauche dans la ligne (utilise `zoo::na.locf`)
    x[is.na(x)] <- zoo::na.locf(x, na.rm = FALSE, fromLast = FALSE)[is.na(x)]
  } else if (fill == "right") {
    # Remplit les valeurs manquantes avec les valeurs situées à droite (if `fill` est "right")
    x[is.na(x)] <- zoo::na.locf(x, na.rm = FALSE, fromLast = TRUE)[is.na(x)]
  }
  return(x)  # Renvoie la ligne après traitement
}

# Fonction pour créer des listes organisées pour chaque catégorie de données (basée sur les feuilles d'ODS)
create_list <- function(data, cellules, cols, annees) {
  lapply(names(cellules), function(category) {  # Itère sur chaque catégorie (chaque élément de `cellules`)
    result <- combine_rows(data, cellules[[category]], cols)  # Combine les lignes spécifiées pour cette catégorie
    colnames(result) <- c("Nom", annees)  # Définit les noms de colonnes avec "Nom" et les années fournies
    return(result)  # Renvoie le tableau pour cette catégorie
  })
}

# Fonction pour combiner les lignes spécifiées d'un `data.frame`
combine_rows <- function(data, rows, cols) {
  # Combine les lignes indiquées par `rows` en prenant uniquement les colonnes spécifiées par `cols`
  bind_rows(lapply(rows, function(row) data[row, cols]))  # Retourne un tableau combiné avec les lignes choisies
}

############
##  MAIN  ##
############

################
#### Fiches ####

#### Fiche 1.1 ####
fiche_1.1 <- "www/data/fiches/fiche_1/A.1.1-biom-agri-240628.ods"

fiche_1.1_fr <- read_and_process(fiche_1.1, "Production_France")
fiche_1.1_cvdl <- read_and_process(fiche_1.1, "Production_CVdL")

# Définir les lignes pour chaque catégorie
cols <- 4:13
ligne_annees <- 2
annees <- fiche_1.1_fr[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

cellules_1.1 <- list(
	"Céréales, oléagineux et protéagineux" = c(14, 20, 30),
	"Pommes de terre et tubercules" = c(36, 40),
	"Cultures légumières" = c(62, 75, 86, 90, 91, 94),
	"Cultures fruitières" = c(104, 109, 116, 122, 128, 133, 139),
	"Cultures industrielles" = c(150, 154, 160, 166),
	"Cultures fourragères" = c(171, 174, 177, 181),
	"Toutes cultures" = c(31, 41, 95, 140, 144, 146, 167, 144),
	"Cultures et résidus de récoltes" = c(168, 183)
)

# Créer les listes pour les deux feuilles
data_1.1 <- list(
	"fr" = create_list(fiche_1.1_fr, cellules_1.1, cols, annees),
	"cvdl" = create_list(fiche_1.1_cvdl, cellules_1.1, cols, annees)
)

# Nommer les éléments de la liste
names(data_1.1$fr) <- names(cellules_1.1)
names(data_1.1$cvdl) <- names(cellules_1.1)

saveRDS(data_1.1, file = "www/data/rds/data_1.1.rds")

#### Fiche 1.2 ####
fiche_1.2 <- "www/data/fiches/fiche_1/A.1.2-biom-sylv.ods"
fiche_1.2_fr <- read_and_process(fiche_1.2, "France Métropolitaine")
fiche_1.2_cvdl <- read_and_process(fiche_1.2, "Centre-Val de Loire")

# Définir les lignes pour chaque catégorie
cols <- 1:9
ligne_annees <- 2
annees <- fiche_1.2_fr[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

cellules_1.2 <- list(
	"Biomasse issue de la sylviculture" = c(5, 8, 11, 14, 16)
)

# Créer les listes pour les deux feuilles
data_1.2 <- list(
	"fr" = create_list(fiche_1.2_fr, cellules_1.2, cols, annees),
	"cvdl" = create_list(fiche_1.2_cvdl, cellules_1.2, cols, annees)
)

# Nommer les éléments de la liste
names(data_1.2$fr) <- names(cellules_1.2)
names(data_1.2$cvdl) <- names(cellules_1.2)

saveRDS(data_1.2, file = "www/data/rds/data_1.2.rds")

#### Fiche 1.3 ####
fiche_1.3 <- "www/data/fiches/fiche_1/A.1.3-biom-aqua.ods"
fiche_1.3_fr <- read_and_process(fiche_1.3, "France et Centre-Val de Loire")
cols <- 1:9
ligne_annees <- 2
annees <- fiche_1.3_fr[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

# Définir les lignes pour chaque catégorie
cellules_1.3 <- list(
	"Extraction de biomasse aquatique (en tonnes)" = c(3,4)
)

# Créer les listes pour les deux feuilles
data_1.3 <- list(
	"fr" = create_list(fiche_1.3_fr, cellules_1.3, cols, annees)
)

# Nommer les éléments de la liste
names(data_1.3$fr) <- names(cellules_1.3)

saveRDS(data_1.3, file = "www/data/rds/data_1.3.rds")

#### Fiche 1.4 ####
fiche_1.4 <- "www/data/fiches/fiche_1/A.1.4-biom-chas.ods"
fiche_1.4_fr <- read_and_process(fiche_1.4, "France Métropolitaine")
fiche_1.4_cvdl <- read_and_process(fiche_1.4, "Centre-Val de Loire")
cols <- 1:9
ligne_annees <- 2
annees <- fiche_1.4_fr[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

# Définir les lignes pour chaque catégorie
cellules_1.4 <- list(
	"Prélèvements (en tonnes)" = c(11)
)

# Créer les listes pour les deux feuilles
data_1.4 <- list(
	"fr" = create_list(fiche_1.4_fr, cellules_1.4, cols, annees),
	"cvdl" = create_list(fiche_1.4_cvdl, cellules_1.4, cols, annees)
)

# Nommer les éléments de la liste
names(data_1.4$fr) <- names(cellules_1.4)
names(data_1.4$cvdl) <- names(cellules_1.4)

saveRDS(data_1.4, file = "www/data/rds/data_1.4.rds")

#### Fiche 1.5 ####

fiche_1.5 <- "www/data/fiches/fiche_1/A.1.5\ a\ A.1.7-miner-et-combust.ods"
fiche_1.5_fr <- read_and_process(fiche_1.5, "Données France 2014 à 2021")
fiche_1.5_cvdl_minerais <- read_and_process(fiche_1.5, "Extraction Centre-Val de Loire 2014 et 2021")
fiche_1.5_cvdl_petrole <- read_and_process(fiche_1.5, "Extraction pétrole cvdl")

# Définir les lignes pour chaque catégorie
cols_fr <- 1:9
cols_cvdl_minerais <- 1:3
ligne_annees <- 4
ligne_annees_cvdl <- 7
annees_fr <- fiche_1.5_fr[ligne_annees, cols_fr[-1]] %>% trimws() %>% as.numeric()
annees_cvdl_minerais <- fiche_1.5_cvdl_minerais[ligne_annees_cvdl, cols_cvdl_minerais[-1]] %>% trimws() %>% as.numeric()

cellules_1.5_fr <- list(
	"Minerais métalliques" = c(6, 7),
	"Minéraux non-métalliques" = c(9, 10, 11, 12, 13, 14, 15, 16, 17),
	"Combustibles fossiles" = c(19, 20, 21, 22)
)
cellules_1.5_cvdl_minerais <- list(
	"Minéraux non-métalliques" = c(8, 9, 10, 11, 12, 13)
)
cellules_1.5_cvdl_petrole <- list(
	"Combustibles fossiles" = c(4)
)

# Créer les listes pour les deux feuilles
data_1.5 <- list(
	"fr" = create_list(fiche_1.5_fr, cellules_1.5_fr, cols_fr, annees_fr),
	"cvdl_minerais" = create_list(fiche_1.5_cvdl_minerais, cellules_1.5_cvdl_minerais, cols_cvdl_minerais, annees_cvdl_minerais),
	"cvdl_petrole" = create_list(fiche_1.5_cvdl_petrole, cellules_1.5_cvdl_petrole, cols_fr, annees_fr)
)

# Nommer les éléments de la liste
names(data_1.5$fr) <- names(cellules_1.5_fr)
names(data_1.5$cvdl_minerais) <- names(cellules_1.5_cvdl_minerais)
names(data_1.5$cvdl_petrole) <- names(cellules_1.5_cvdl_petrole)

saveRDS(data_1.5, file = "www/data/rds/data_1.5.rds")
#### Fiche 2 - Érosion terres arables #####
fiche_2 <- "www/data/fiches/fiche_2/extract-inter-inutil.ods"

fiche_2_erosion <- read_and_process(fiche_2, "érosion terres arables")
cellules_2_erosion_fr <- list("Terres érodées" = c(25))
cellules_2_erosion_cvdl <- list("Terres érodées" = c(26))
cols <- 1:3
ligne_annees <- 24
annees <- fiche_2_erosion[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_2_erosion <- list(
	"fr" = create_list(fiche_2_erosion, cellules_2_erosion_fr, cols, annees),
	"cvdl" = create_list(fiche_2_erosion, cellules_2_erosion_cvdl, cols, annees)
)
names(data_2_erosion$fr) <- names(cellules_2_erosion_fr)
names(data_2_erosion$cvdl) <- names(cellules_2_erosion_cvdl)
saveRDS(data_2_erosion, file = "www/data/rds/data_2_erosion.rds")

#### Fiche 2 - Excavation #####
fiche_2_excavation <- read_and_process(fiche_2, "terres d'excavation")
cellules_2_excavation_fr <- list("Terres excavées" = c(30))
cellules_2_excavation_cvdl <- list("Terres excavées" = c(28))
cols <- 1:9
ligne_annees <- 26
annees <- fiche_2_excavation[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_2_excavation <- list(
	"fr" = create_list(fiche_2_excavation, cellules_2_excavation_fr, cols, annees),
	"cvdl" = create_list(fiche_2_excavation, cellules_2_excavation_cvdl, cols, annees)
)
names(data_2_excavation$fr) <- names(cellules_2_excavation_fr)
names(data_2_excavation$cvdl) <- names(cellules_2_excavation_cvdl)
saveRDS(data_2_excavation, file = "www/data/rds/data_2_excavation.rds")

#### Fiche 2 - Résidus de récolte #####
fiche_2_residus <- read_and_process(fiche_2, "résidus récolte inut.")
cellules_2_residus_fr <- list("Résidus" = c(7))
cellules_2_residus_cvdl <- list("Résidus" = c(3))
cols <- 1:9
ligne_annees <- 2
annees <- fiche_2_residus[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_2_residus <- list(
	"fr" = create_list(fiche_2_residus, cellules_2_residus_fr, cols, annees),
	"cvdl" = create_list(fiche_2_residus, cellules_2_residus_cvdl, cols, annees)
)
names(data_2_residus$fr) <- names(cellules_2_residus_fr)
names(data_2_residus$cvdl) <- names(cellules_2_residus_cvdl)
saveRDS(data_2_residus, file = "www/data/rds/data_2_residus.rds")

#### Fiche 2 - Résidus de coupes d'arbres #####
fiche_2_arbres <- read_and_process(fiche_2, "résidus coupe arbres Centre-Val de Loire et France")
cellules_2_arbres_fr <- list("Arbres" = c(42))
cellules_2_arbres_cvdl <- list("Arbres" = c(47))
cols <- 1:3
ligne_annees <- 41
annees <- fiche_2_arbres[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_2_arbres <- list(
	"fr" = create_list(fiche_2_arbres, cellules_2_arbres_fr, cols, annees),
	"cvdl" = create_list(fiche_2_arbres, cellules_2_arbres_cvdl, cols, annees)
)
names(data_2_arbres$fr) <- names(cellules_2_arbres_fr)
names(data_2_arbres$cvdl) <- names(cellules_2_arbres_cvdl)
saveRDS(data_2_arbres, file = "www/data/rds/data_2_arbres.rds")

#### Fiche 2 - Extractions inutilisées de l'exploitation minière #####
fiche_2_mines <- read_and_process(fiche_2, "extractions inut. exploit. minière")
cellules_2_mines_fr <- list("Mines" = c(3,4,5,6,7,8,9,10,11))
cellules_2_mines_cvdl <- list("Mines" = c(28))
cols_fr <- 1:9
cols_cvdl <- 1:3
ligne_annees_fr <- 2
ligne_annees_cvdl <- 25
annees_fr <- fiche_2_mines[ligne_annees_fr, cols_fr[-1]] %>% trimws() %>% as.numeric()
annees_cvdl <- fiche_2_mines[ligne_annees_cvdl, cols_cvdl[-1]] %>% trimws() %>% as.numeric()

data_2_mines <- list(
	"fr" = create_list(fiche_2_mines, cellules_2_mines_fr, cols_fr, annees_fr),
	"cvdl" = create_list(fiche_2_mines, cellules_2_mines_cvdl, cols_cvdl, annees_cvdl)
)
names(data_2_mines$fr) <- names(cellules_2_mines_fr)
names(data_2_mines$cvdl) <- names(cellules_2_mines_cvdl)
saveRDS(data_2_mines, file = "www/data/rds/data_2_mines.rds")

#### Fiche 2 - Production pêche rejet de la mer #####
fiche_2_peche <- read_and_process(fiche_2, "prod pêche rejet mer")
cellules_2_peche_fr <- list("Pêche" = c(4))
cols <- 1:10
ligne_annees <- 2
annees <- fiche_2_peche[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_2_peche <- list(
	"fr" = create_list(fiche_2_peche, cellules_2_peche_fr, cols, annees)
)
names(data_2_peche$fr) <- names(cellules_2_peche_fr)
saveRDS(data_2_peche, file = "www/data/rds/data_2_peche.rds")

#### Fiche 2 - Boues de dragage #####
fiche_2_boues <- read_and_process(fiche_2, "boues de dragage")
cellules_2_boues_fr <- list("Boues" = c(29))
cellules_2_boues_cvdl <- list("Boues" = c(35))
cols <- 1:3
ligne_annees <- 26
annees <- fiche_2_boues[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_2_boues <- list(
	"fr" = create_list(fiche_2_boues, cellules_2_boues_fr, cols, annees),
	"cvdl" = create_list(fiche_2_boues, cellules_2_boues_cvdl, cols, annees)
)
names(data_2_boues$fr) <- names(cellules_2_boues_fr)
names(data_2_boues$cvdl) <- names(cellules_2_boues_cvdl)
saveRDS(data_2_boues, file = "www/data/rds/data_2_boues.rds")
#### Fiche 3 - Synthèse #####
fiche_3_synthese <- "www/data/fiches/fiche_3/CVL-synthese-imports-exports-pour-graphique.ods"
fiche_3_synthese_feuille <- read_and_process(fiche_3_synthese, "imports-exports-CVL-qtés-globales")
fiche_3_marchandises <- "www/data/fiches/fiche_3/CVL-TRM-pav-franc-par-type-marchandise.ods"
fiche_3_marchandises_feuille <- read_and_process(fiche_3_marchandises, "synthese imports")
fiche_3_douanes <- "www/data/fiches/fiche_3/CVL-douanes-par-pays-pour-graphq.ods"
fiche_3_douanes_feuille <- read_and_process(fiche_3_douanes, "resultats")
fiche_3_regions <- "www/data/fiches/fiche_3/CVL-imp-exp-par-region.ods"
fiche_3_regions_feuille <- read_and_process(fiche_3_regions, "resultats")

# Définir les lignes pour chaque catégorie
cols_synthese <- 1:9
ligne_annees_synthese <- 3
annees_synthese <- fiche_3_synthese_feuille[ligne_annees_synthese, cols_synthese[-1]] %>% trimws() %>% as.numeric()
cols_marchandises <- 1:10
ligne_annees_marchandises <- 3
annees_marchandises <- fiche_3_marchandises_feuille[ligne_annees_marchandises, cols_marchandises[-1]] %>% trimws() %>% as.numeric()
cols_douanes <- 1:9
ligne_annees_douanes <- 3
annees_douanes <- fiche_3_douanes_feuille[ligne_annees_douanes, cols_douanes[-1]] %>% trimws() %>% as.numeric()
cols_regions <- 1:10
ligne_annees_regions <- 2
annees_regions <- fiche_3_regions_feuille[ligne_annees_regions, cols_regions[-1]] %>% trimws() %>% as.numeric()

cellules_3_synthese <- list(
	"Transports internes région" = c(4),
	"Importations région" = c(8,9),
	"Exportations région" = c(5,6)
)
cellules_3_marchandises <- list(
	"Transports internes région" = 26:31,
	"Importations région" = 4:9,
	"Exportations région" = 15:20
)
cellules_3_douanes <- list(
	"Imports depuis UE" = 4:9,
	"Exports vers UE" = 39:44
)
cellules_3_douanes_hors_ue <- list(
	"Imports hors UE" = 84:89,
	"Exports hors UE" = 72:78
)
cellules_3_regions <- list(
	"Imports depuis régions françaises" = 3:13,
	"Exports vers régions françaises" = 16:26
)

# Créer les listes pour les deux feuilles
data_3 <- list(
	"Synthèse" = create_list(fiche_3_synthese_feuille, cellules_3_synthese, cols_synthese, annees_synthese),
	"Échanges avec les autres régions" = create_list(fiche_3_regions_feuille, cellules_3_regions, cols_regions, annees_regions),
	"Échanges avec les autres régions par type de marchandises" = create_list(fiche_3_marchandises_feuille, cellules_3_marchandises, cols_marchandises, annees_marchandises),
	"Échanges avec les pays de l'UE" = create_list(fiche_3_douanes_feuille, cellules_3_douanes, cols_douanes, annees_douanes),
	"Échanges avec les pays hors de l'UE" = create_list(fiche_3_douanes_feuille, cellules_3_douanes_hors_ue, cols_douanes, annees_douanes)
)

# Nommer les éléments de la liste
names(data_3$`Synthèse`) <- names(cellules_3_synthese)
names(data_3$`Échanges avec les autres régions`) <- names(cellules_3_regions)
names(data_3$`Échanges avec les autres régions par type de marchandises`) <- names(cellules_3_marchandises)
names(data_3$`Échanges avec les pays de l'UE`) <- names(cellules_3_douanes)
names(data_3$`Échanges avec les pays hors de l'UE`) <- names(cellules_3_douanes_hors_ue)
saveRDS(data_3, file = "www/data/rds/data_3.rds")

#### Fiche 4.1 - Rejets air #####
fiche_4.1 <- "www/data/fiches/fiche_4/F.1-rejets-air.ods"
fiche_4.1_fr <- read_and_process(fiche_4.1, "France")
fiche_4.1_cvdl <- read_and_process(fiche_4.1, "Centre-Val de Loire")
cellules_4.1 <- list("Rejets" = c(5,6,7,8,9,12,13,14,15,16,17,18,19))
cols_fr <- 2:10
cols_cvdl <- 2:11
ligne_annees <- 2
annees_fr <- fiche_4.1_fr[ligne_annees, cols_fr[-1]] %>% trimws() %>% as.numeric()
annees_cvdl <- fiche_4.1_cvdl[ligne_annees, cols_cvdl[-1]] %>% trimws() %>% as.numeric()

data_4.1 <- list(
	"fr" = create_list(fiche_4.1_fr, cellules_4.1, cols_fr, annees_fr),
	"cvdl" = create_list(fiche_4.1_cvdl, cellules_4.1, cols_cvdl, annees_cvdl)
)
names(data_4.1$fr) <- names(cellules_4.1)
names(data_4.1$cvdl) <- names(cellules_4.1)
saveRDS(data_4.1, file = "www/data/rds/data_4.1.rds")
#### Fiche 4.2 - Production de déchets #####
fiche_4.2 <- "www/data/fiches/fiche_4/F.2-prod-dechets.ods"
fiche_4.2_fr <- read_and_process(fiche_4.2, "France")
fiche_4.2_cvdl <- read_and_process(fiche_4.2, "CVDL")
# On récupère les données des 3 parties
dechets_stockes <- list(
	"cellules" = list("Déchets stockés" = 3:5),
	"cols" = 1:3,
	"ligne_annees" = 2
)
dechets_stockes$annees_fr <- fiche_4.2_fr[dechets_stockes$ligne_annees, dechets_stockes$cols[-1]] %>% trimws() %>% as.numeric()
dechets_stockes$annees_cvdl <- fiche_4.2_cvdl[dechets_stockes$ligne_annees, dechets_stockes$cols[-1]] %>% trimws() %>% as.numeric()
dma <- list(
	"cellules" = list("DMA (Déchets ménagers et assimilés)" = 8:14),
	"cols" = 2:7,
	"ligne_annees" = 7
)
dma$annees_fr <- fiche_4.2_fr[dma$ligne_annees, dma$cols[-1]] %>% trimws() %>% as.numeric()
dma$annees_cvdl <- fiche_4.2_cvdl[dma$ligne_annees, dma$cols[-1]] %>% trimws() %>% as.numeric()
dechets_valorises <- list(
	"cellules_fr" = list("Déchets valorisés" = 18:20),
	"cellules_cvdl" = list("Déchets valorisés" = 18:21),
	"cols" = 1:3,
	"ligne_annees" = 17
)
dechets_valorises$annees_fr <- fiche_4.2_fr[dechets_valorises$ligne_annees, dechets_valorises$cols[-1]] %>% trimws() %>% as.numeric()
dechets_valorises$annees_cvdl <- fiche_4.2_cvdl[dechets_valorises$ligne_annees, dechets_valorises$cols[-1]] %>% trimws() %>% as.numeric()

data_4.2 <- list(
	"fr" = list(
		"Déchets stockés" = create_list(fiche_4.2_fr, dechets_stockes[["cellules"]], dechets_stockes[["cols"]], dechets_stockes[["annees_fr"]])[[1]],
		"DMA (Déchets ménagers et assimilés)" = create_list(fiche_4.2_fr, dma[["cellules"]], dma[["cols"]], dma[["annees_fr"]])[[1]],
		"Déchets valorisés" = create_list(fiche_4.2_fr, dechets_valorises[["cellules_fr"]], dechets_valorises[["cols"]], dechets_valorises[["annees_fr"]])[[1]]
	),
	"cvdl" = list(
		"Déchets stockés" = create_list(fiche_4.2_cvdl, dechets_stockes[["cellules"]], dechets_stockes[["cols"]], dechets_stockes[["annees_cvdl"]])[[1]],
		"DMA (Déchets ménagers et assimilés)" = create_list(fiche_4.2_cvdl, dma[["cellules"]], dma[["cols"]], dma[["annees_cvdl"]])[[1]],
		"Déchets valorisés" = create_list(fiche_4.2_cvdl, dechets_valorises[["cellules_cvdl"]], dechets_valorises[["cols"]], dechets_valorises[["annees_cvdl"]])[[1]]
	)
)
saveRDS(data_4.2, file = "www/data/rds/data_4.2.rds")

#### Fiche 4.3 - Rejets dans l'eau #####
fiche_4.3 <- "www/data/fiches/fiche_4/F.3-rejets-eau.ods"
fiche_4.3_fr <- read_and_process(fiche_4.3, "France")
fiche_4.3_cvdl <- read_and_process(fiche_4.3, "Centre-Val de Loire")
cellules_4.3 <- list("Rejets" = c(3,4,5,6))
cols <- 1:8
ligne_annees <- 1
annees <- fiche_4.3_fr[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_4.3 <- list(
	"fr" = create_list(fiche_4.3_fr, cellules_4.3, cols, annees),
	"cvdl" = create_list(fiche_4.3_cvdl, cellules_4.3, cols, annees)
)
names(data_4.3$fr) <- names(cellules_4.3)
names(data_4.3$cvdl) <- names(cellules_4.3)
saveRDS(data_4.3, file = "www/data/rds/data_4.3.rds")
#### Fiche 4.4 - Produits dissipatifs #####
fiche_4.4 <- "www/data/fiches/fiche_4/F.4-produits-dissipatifs.ods"
## Boues d'épuration
fiche_4.4_epuration <- read_and_process(fiche_4.4, "Boues d'épuration")
cellules_4.4_fr <- list("Rejets" = c(17))
cellules_4.4_cvdl <- list("Rejets" = c(15))
cols_fr <- 2:9
cols_cvdl <- 2:8
ligne_annees_fr <- 16
ligne_annees_cvdl <- 14
annees_fr <- fiche_4.4_epuration[ligne_annees_fr, cols_fr[-1]] %>% trimws() %>% as.numeric()
annees_cvdl <- fiche_4.4_epuration[ligne_annees_cvdl, cols_cvdl[-1]] %>% trimws() %>% as.numeric()

data_4.4_epuration <- list(
	"fr" = create_list(fiche_4.4_epuration, cellules_4.4_fr, cols_fr, annees_fr),
	"cvdl" = create_list(fiche_4.4_epuration, cellules_4.4_cvdl, cols_cvdl, annees_cvdl)
)
names(data_4.4_epuration$fr) <- names(cellules_4.4_fr)
names(data_4.4_epuration$cvdl) <- names(cellules_4.4_fr)
saveRDS(data_4.4_epuration, file = "www/data/rds/data_4.4_epuration.rds")
## Engrais organiques
fiche_4.4_engrais_organiques <- read_and_process(fiche_4.4, "Engrais organique")
cellules_4.4_fr <- list("Rejets" = c(30))
cellules_4.4_cvdl <- list("Rejets" = c(21,22,23,24))
cols <- 2:4
ligne_annees <- 19
annees <- fiche_4.4_engrais_organiques[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_4.4_engrais_organiques <- list(
	"fr" = create_list(fiche_4.4_engrais_organiques, cellules_4.4_fr, cols, annees),
	"cvdl" = create_list(fiche_4.4_engrais_organiques, cellules_4.4_cvdl, cols, annees)
)
names(data_4.4_engrais_organiques$fr) <- names(cellules_4.4_fr)
names(data_4.4_engrais_organiques$cvdl) <- names(cellules_4.4_cvdl)
saveRDS(data_4.4_engrais_organiques, file = "www/data/rds/data_4.4_engrais_organiques.rds")
## Engrais minéral
fiche_4.4_engrais_mineral <- read_and_process(fiche_4.4, "Engrais minéral")
cellules_4.4_fr <- list("Rejets" = c(9))
cellules_4.4_cvdl <- list("Rejets" = c(8))
cols <- 2:10
ligne_annees <- 7
annees <- fiche_4.4_engrais_mineral[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_4.4_engrais_mineral <- list(
	"fr" = create_list(fiche_4.4_engrais_mineral, cellules_4.4_fr, cols, annees),
	"cvdl" = create_list(fiche_4.4_engrais_mineral, cellules_4.4_cvdl, cols, annees)
)
names(data_4.4_engrais_mineral$fr) <- names(cellules_4.4_fr)
names(data_4.4_engrais_mineral$cvdl) <- names(cellules_4.4_fr)
saveRDS(data_4.4_engrais_mineral, file = "www/data/rds/data_4.4_engrais_mineral.rds")
## Compost
fiche_4.4_compost <- read_and_process(fiche_4.4, "Compost")
cellules_4.4_fr <- list("Rejets" = c(20))
cellules_4.4_cvdl <- list("Rejets" = c(17))
cols_fr<- 1:4
cols_cvdl<- 1:5
ligne_annees_fr <- 18
ligne_annees_cvdl <- 15
annees_fr <- fiche_4.4_compost[ligne_annees_fr, cols_fr[-1]] %>% trimws() %>% as.numeric()
annees_cvdl <- fiche_4.4_compost[ligne_annees_cvdl, cols_cvdl[-1]] %>% trimws() %>% as.numeric()

data_4.4_compost <- list(
	"fr" = create_list(fiche_4.4_compost, cellules_4.4_fr, cols_fr, annees_fr),
	"cvdl" = create_list(fiche_4.4_compost, cellules_4.4_cvdl, cols_cvdl, annees_cvdl)
)
names(data_4.4_compost$fr) <- names(cellules_4.4_fr)
names(data_4.4_compost$cvdl) <- names(cellules_4.4_cvdl)
saveRDS(data_4.4_compost, file = "www/data/rds/data_4.4_compost.rds")
## Pesticides
fiche_4.4_pesticides <- read_and_process(fiche_4.4, "Pesticides")
cellules_4.4_fr <- list("Rejets" = c(10))
cellules_4.4_cvdl <- list("Rejets" = c(9))
cols <- 2:10
ligne_annees <- 8
annees <- fiche_4.4_pesticides[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_4.4_pesticides <- list(
	"fr" = create_list(fiche_4.4_pesticides, cellules_4.4_fr, cols, annees),
	"cvdl" = create_list(fiche_4.4_pesticides, cellules_4.4_cvdl, cols, annees)
)
names(data_4.4_pesticides$fr) <- names(cellules_4.4_fr)
names(data_4.4_pesticides$cvdl) <- names(cellules_4.4_cvdl)
saveRDS(data_4.4_pesticides, file = "www/data/rds/data_4.4_pesticides.rds")

#### Fiche 5 ####
fiche_5 <- "www/data/fiches/fiche_5/G.1 et 2-elements-equil.ods"
fiche_5_feuille <- read_and_process(fiche_5, "Résultats éléments d'équilibrage Centre-Val de Loire et France")
cellules_5_fr <- list(
	"Éléments d'équilibrage en entrée" = c(25,26,27),
	"Éléments d'équilibrage en sortie" = c(28,29)
)
cellules_5_cvdl <- list(
	"Éléments d'équilibrage en entrée" = c(19,20,21),
	"Éléments d'équilibrage en sortie" = c(22,23)
)
cols <- 2:4
ligne_annees <- 18
annees <- fiche_5_feuille[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_5 <- list(
	"fr" = create_list(fiche_5_feuille, cellules_5_fr, cols, annees),
	"cvdl" = create_list(fiche_5_feuille, cellules_5_cvdl, cols, annees)
)
names(data_5$fr) <- names(cellules_5_fr)
names(data_5$cvdl) <- names(cellules_5_fr)
saveRDS(data_5, file = "www/data/rds/data_5.rds")

#### Fiche 6 ####
fiche_6 <- "www/data/fiches/fiche_6/addition-nette-stock-240628.ods"
fiche_6_feuille <- read_and_process(fiche_6, "Fiche 6 addition nette au stock")
## Histogrammes des flux entrées - sorties
cellules_6_fr <- list(
	"Flux en entrée" = 35:37,
	"Flux en sortie" = 39:41
)
cellules_6_cvdl <- list(
	"Flux en entrée" = 23:25,
	"Flux en sortie" = 27:29
)
cols <- 1:3
ligne_annees <- 22
annees <- fiche_6_feuille[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_6 <- list(
	"fr" = create_list(fiche_6_feuille, cellules_6_fr, cols, annees),
	"cvdl" = create_list(fiche_6_feuille, cellules_6_cvdl, cols, annees)
)
names(data_6$fr) <- names(cellules_6_fr)
names(data_6$cvdl) <- names(cellules_6_fr)
saveRDS(data_6, file = "www/data/rds/data_6.rds")

## Scatterplot des additions nettes au stock
cellules_6_add_fr <- list("Additions nettes au stock" = c(44))
cellules_6_add_cvdl <- list("Additions nettes au stock" = c(32))

data_6_add <- list(
	"fr" = create_list(fiche_6_feuille, cellules_6_add_fr, cols, annees),
	"cvdl" = create_list(fiche_6_feuille, cellules_6_add_cvdl, cols, annees)
)
names(data_6_add$fr) <- names(cellules_6_add_fr)
names(data_6_add$cvdl) <- names(cellules_6_add_fr)
saveRDS(data_6_add, file = "www/data/rds/data_6_add.rds")

#### Fiche 7 ####
fiche_7 <- "www/data/fiches/fiche_7/calcul-flux-indirects.ods"
fiche_7_fr <- read_and_process(fiche_7, "France")
fiche_7_cvdl <- read_and_process(fiche_7, "Centre-Val de Loire")
cellules_7 <- list(
	"Importations" = 34:43,
	"Exportations" = 46:55
)
cols <- 1:3
ligne_annees <- 33
annees <- fiche_7_fr[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_7 <- list(
	"fr" = create_list(fiche_7_fr, cellules_7, cols, annees),
	"cvdl" = create_list(fiche_7_cvdl, cellules_7, cols, annees)
)
names(data_7$fr) <- names(cellules_7)
names(data_7$cvdl) <- names(cellules_7)
saveRDS(data_7, file = "www/data/rds/data_7.rds")


#####################
#### Indicateurs ####

#### Indicateurs d'entrée ####
indicateurs_fichier <- "www/data/fiches/indicateurs.ods"
ind_entree <- read_and_process(indicateurs_fichier, "Indicateurs d'entrée")

tab_mfa_entree <- ind_entree[1:6,1:7]
tab_interm_entree <- ind_entree[9:12,1:7]
saveRDS(tab_mfa_entree, file = "www/data/rds/data_tab_mfa_entree.rds")
saveRDS(tab_interm_entree, file = "www/data/rds/data_tab_interm_entree.rds")

cellules_entree <- list(
	"DEU > Extraction\nintérieure utilisée" = c(24),
	"DMI > Entrée\ndirecte de matière" = c(24,28),
	"TMI > Entrée\ntotale de matière" = c(24,28,29),
	"TMR > Mobilisation\ntotale de matière" = c(24,28,29,30)
)
cols <- 3:5
ligne_annees <- 23
annees <- ind_entree[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_ind_entree <- list(
	"cvdl" = create_list(ind_entree, cellules_entree, cols, annees)
)
names(data_ind_entree$cvdl) <- names(cellules_entree)
saveRDS(data_ind_entree, file = "www/data/rds/data_ind_entree.rds")

#### Indicateurs de sortie ####
ind_sortie <- read_and_process(indicateurs_fichier, "Indicateurs de sortie")

tab_mfa_sortie <- ind_sortie[1:6,1:7]
tab_interm_sortie <- ind_entree[9:10,1:7]
saveRDS(tab_mfa_sortie, file = "www/data/rds/data_tab_mfa_sortie.rds")
saveRDS(tab_interm_sortie, file = "www/data/rds/data_tab_interm_sortie.rds")

cellules_sortie <- list(
	"DPO > Émissions\nvers la nature" = c(25),
	"TDO > Émissions totales\nvers la nature" = c(25,28)
)
cols <- 3:5
ligne_annees <- 23
annees <- ind_sortie[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_ind_sortie <- list(
	"cvdl" = create_list(ind_sortie, cellules_sortie, cols, annees)
)
names(data_ind_sortie$cvdl) <- names(cellules_sortie)
saveRDS(data_ind_sortie, file = "www/data/rds/data_ind_sortie.rds")

#### Indicateurs de consommation ####
ind_consommation <- read_and_process(indicateurs_fichier, "Indicateurs de consommation")
cellules_consommation <- list(
	"DMC > Consommation\napparente de matière" = c(13),
	"TMC > Consommation totale\nestimée de matière" = c(13,15)
)
cols <- 1:3
ligne_annees <- 12
annees <- ind_consommation[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_consommation <- list(
	"cvdl" = create_list(ind_consommation, cellules_consommation, cols, annees)
)
names(data_consommation$cvdl) <- names(cellules_consommation)
saveRDS(data_consommation, file = "www/data/rds/data_consommation.rds")

#### Indicateurs de stock ####
ind_stock <- read_and_process(indicateurs_fichier, "Indicateur de stock")
cellules_stock <- list("NAS" = c(10))
cols <- 3:5
ligne_annees <- 9
annees <- ind_stock[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_stock <- list(
	"cvdl" = create_list(ind_stock, cellules_stock, cols, annees)
)
names(data_stock$cvdl) <- names(cellules_stock)
saveRDS(data_stock, file = "www/data/rds/data_stock.rds")

#### Indicateurs de balance commerciale ####
ind_bc <- read_and_process(indicateurs_fichier, "Indicateur de balance commerciale physique")
data_kpi_bc <- c(
	"2014" = -1.69,
	"2021" = -1.59
)
cellules_bc <- list(
	"I (Importations)" = c(13),
	"E (Exportations)" = c(14)
)
cols <- 3:5
ligne_annees <- 12
annees <- ind_bc[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_bc <- list(
	"cvdl" = create_list(ind_bc, cellules_bc, cols, annees)
)
names(data_bc$cvdl) <- names(cellules_bc)
saveRDS(data_bc, file = "www/data/rds/data_bc.rds")
saveRDS(data_kpi_bc, file = "www/data/rds/data_kpi_bc.rds")

#### Indicateurs d'efficience ####
ind_efficience <- read_and_process(indicateurs_fichier, "Indicateurs d'efficience")
cellules_efficience <- list(
	"MI (Material Intensity)" = c(12),
	"MP (Material Productivity)" = c(13)
)
cols <- 3:5
ligne_annees <- 11
annees <- ind_efficience[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_efficience <- list(
	"cvdl" = create_list(ind_efficience, cellules_efficience, cols, annees)
)
names(data_efficience$cvdl) <- names(cellules_efficience)
saveRDS(data_efficience, file = "www/data/rds/data_efficience.rds")

#### Indicateurs de recyclage ####
ind_recyclage <- read_and_process(indicateurs_fichier, "Recyclage")
cellules_recyclage <- list(
	"Recyclage" = c(11)
)
cols <- 1:3
ligne_annees <- 10
annees <- ind_recyclage[ligne_annees, cols[-1]] %>% trimws() %>% as.numeric()

data_recyclage <- list(
	"cvdl" = create_list(ind_recyclage, cellules_recyclage, cols, annees)
)
names(data_recyclage$cvdl) <- names(cellules_recyclage)
saveRDS(data_recyclage, file = "www/data/rds/data_recyclage.rds")

#### Indicateurs de comparaison nationale ####
ind_comparaison_national <- read_and_process(indicateurs_fichier, "Comparaison avec niveau national")
ind_comparaison_national[sapply(ind_comparaison_national, is.na)] <- ""
saveRDS(ind_comparaison_national, file = "www/data/rds/data_comparaison_national.rds")