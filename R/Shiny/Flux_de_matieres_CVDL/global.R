suppressMessages(library(arrow))
suppressMessages(library(dplyr))
suppressMessages(library(DT))
suppressMessages(library(glue))
suppressMessages(library(markdown))
suppressMessages(library(plotly))
suppressMessages(library(purrr))
suppressMessages(library(readODS))
suppressMessages(library(shiny))
suppressMessages(library(shinyBS))
suppressMessages(library(shinythemes))
suppressMessages(library(tidyr))
suppressMessages(library(waiter))
options("rgdal_show_exportToProj4_warnings" = "none") # mute gdal warnings

source("helpers.R")

#### Waiting screen ####
loader <- tagList(
	includeHTML("www/html/cerema-loader.html"),
	h1(id="please_wait", "Chargement en cours...")
) 

## Lecture des tableurs
### Fiches
data_1.1 <- readRDS("www/data/rds/data_1.1.rds")
data_1.2 <- readRDS("www/data/rds/data_1.2.rds")
data_1.3 <- readRDS("www/data/rds/data_1.3.rds")
data_1.4 <- readRDS("www/data/rds/data_1.4.rds")
data_1.5 <- readRDS("www/data/rds/data_1.5.rds")

data_2_erosion <- readRDS("www/data/rds/data_2_erosion.rds")
data_2_excavation <- readRDS("www/data/rds/data_2_excavation.rds")
data_2_residus <- readRDS("www/data/rds/data_2_residus.rds")
data_2_arbres <- readRDS("www/data/rds/data_2_arbres.rds")
data_2_mines <- readRDS("www/data/rds/data_2_mines.rds")
data_2_peche <- readRDS("www/data/rds/data_2_peche.rds")
data_2_boues <- readRDS("www/data/rds/data_2_boues.rds")

data_3 <- readRDS("www/data/rds/data_3.rds")

data_4.1 <- readRDS("www/data/rds/data_4.1.rds")
data_4.2 <- readRDS("www/data/rds/data_4.2.rds")
data_4.3 <- readRDS("www/data/rds/data_4.3.rds")
data_4.4_epuration <- readRDS("www/data/rds/data_4.4_epuration.rds")
data_4.4_engrais_organiques <- readRDS("www/data/rds/data_4.4_engrais_organiques.rds")
data_4.4_engrais_mineral <- readRDS("www/data/rds/data_4.4_engrais_mineral.rds")
data_4.4_compost <- readRDS("www/data/rds/data_4.4_compost.rds")
data_4.4_pesticides <- readRDS("www/data/rds/data_4.4_pesticides.rds")

data_5 <- readRDS("www/data/rds/data_5.rds")

data_6 <- readRDS("www/data/rds/data_6.rds")
data_6_add <- readRDS("www/data/rds/data_6_add.rds")

data_7 <- readRDS("www/data/rds/data_7.rds")

### Indicateurs
data_ind_entree <- readRDS("www/data/rds/data_ind_entree.rds")
data_tab_mfa_entree <- readRDS("www/data/rds/data_tab_mfa_entree.rds")
data_tab_interm_entree <- readRDS("www/data/rds/data_tab_interm_entree.rds")
data_ind_sortie <- readRDS("www/data/rds/data_ind_sortie.rds")
data_tab_mfa_sortie <- readRDS("www/data/rds/data_tab_mfa_sortie.rds")
data_tab_interm_sortie <- readRDS("www/data/rds/data_tab_interm_sortie.rds")
data_consommation <- readRDS("www/data/rds/data_consommation.rds")
data_stock <- readRDS("www/data/rds/data_stock.rds")
data_bc <- readRDS("www/data/rds/data_bc.rds")
data_kpi_bc <- readRDS("www/data/rds/data_kpi_bc.rds")
data_efficience <- readRDS("www/data/rds/data_efficience.rds")
data_recyclage <- readRDS("www/data/rds/data_recyclage.rds")
data_comparaison_national <- readRDS("www/data/rds/data_comparaison_national.rds")


