### Flux de matières en Centre-Val de Loire

Ce répertoire contient le code source de l'application [Flux de matières en Centre-Val de Loire](https://cerema-med.shinyapps.io/flux_de_matieres_centre_val_de_loire/) réalisée par le CEREMA pour le compte de la région Centre-Val de Loire. La plateforme réalisée synthétise les travaux de traitements de données des flux de matières transitant depuis ou vers la région.
