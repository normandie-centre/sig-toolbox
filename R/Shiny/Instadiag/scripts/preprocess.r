suppressMessages(library(dplyr))
suppressMessages(library(sf))
suppressMessages(library(tictoc))

#### Import data ####
gpkg_file <- "./www/data/instadiag.gpkg"
communes <- st_read(gpkg_file, layer='d27_communes')
locaux <- st_read(gpkg_file, layer='d27_fftp_2021_pb0010_local')
sirene <- st_read(gpkg_file, layer='d27_geosirene')

#### Pré-traitements ####
tic("\n\n\n------------------------------------------\nPré-traitements")

## Traitements des logements
locaux <- locaux %>%  mutate(
	nb_hab = case_when(
		dteloc == '1' & dnatlc %in% c('L', 'P') ~ stoth / 45,		# si le local est une maison
		dteloc == '2' & dnatlc %in% c('L', 'P') ~ stoth / 32.5,		# si le local est un appartement
		.default = 0
		)
) 
locaux %>%
    group_by(idcom) %>% 
    summarize(population_communale_calculee = sum(nb_hab, na.rm=TRUE)) %>% st_drop_geometry() %>%
	merge(locaux, by = "idcom", all.y = TRUE) %>%
	merge(communes %>% st_drop_geometry(), by.x = "idcom", by.y = "insee_com", all.x = TRUE) %>%
	mutate(
		population_communale_legale = population,
		coeff = population_communale_legale / population_communale_calculee,
		nombre_habitant_logement = coeff * nb_hab
	) %>% 
    select("idlocal", "idcom", "population_communale_calculee", "population_communale_legale", "coeff", "nombre_habitant_logement", "geom") %>% 
    st_as_sf() %>% 
	st_write(gpkg_file, "d27_logements", delete_layer = TRUE)


## Traitement de Sirene
sirene %>% 
  	## Aménité : on attribue une aménité selon le code NAF
  	mutate(
		amenite = case_when(
			activitePrincipaleEtablissement %in% c('86.23Z','86.21Z','86.22A','86.22B','86.22C','86.10Z') ~ 'sante',
			activitePrincipaleEtablissement %in% c('56.30Z' , '56.10A','56.10B','56.10C') ~ 'restauration',
			activitePrincipaleEtablissement %in% c('10.71C' , '10.71D','10.71B' , '10.13B' , '47.21Z','47.11A','47.11B','47.11C','47.11D','47.11E','47.11F','47.19A','47.19B','47.71Z','47.72A','47.72B') ~'achats',
			activitePrincipaleEtablissement %in% c('90.01Z','90.04Z','93.21Z','93.29Z','91.01Z','91.02Z','91.03Z','91.04Z','92.00Z','93.11Z','93.13Z') ~ 'loisirs',
			.default = 'NR'
		),

		## nombre de salariés estimés (estimation basse) selon les tranches    
		effectifs = case_when(
			trancheEffectifsEtablissement == 'NN' ~ 1,
			trancheEffectifsEtablissement == '00' ~ 1,
			trancheEffectifsEtablissement == '01' ~ 2,
			trancheEffectifsEtablissement == '02' ~ 4,
			trancheEffectifsEtablissement == '03' ~ 7,
			trancheEffectifsEtablissement == '11' ~ 11,
			trancheEffectifsEtablissement == '12' ~ 21,
			trancheEffectifsEtablissement == '21' ~ 51,
			trancheEffectifsEtablissement == '22' ~ 101,
			trancheEffectifsEtablissement == '31' ~ 201,
			trancheEffectifsEtablissement == '32' ~ 251,
			trancheEffectifsEtablissement == '41' ~ 501,
			trancheEffectifsEtablissement == '42' ~ 1001,
			trancheEffectifsEtablissement == '51' ~ 2001,
			trancheEffectifsEtablissement == '52' ~ 5001,
			trancheEffectifsEtablissement == '53' ~ 10001,
			.default = 0
		)
	) %>%
	select(id, siret, amenite, effectifs) %>%
	st_write(gpkg_file, "d27_sirene", delete_layer = TRUE)

toc()