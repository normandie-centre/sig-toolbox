library(shiny)
library(leaflet)
library(sf)
library(dplyr)
library(markdown)

### Input data
chu <- st_read("www/data/henri_ey.geojson")


##########
##  UI  ##
##########
ui <- fluidPage(

  # Chargement du CSS
  tags$head(tags$link(rel = "stylesheet", type = "text/css", href = "css/default.css")),

  # Création de la layout
  navbarPage("Centre Henri Ey - Cartographie dynamique", id = "main",
    tabPanel("Map",

      div(class = "outer",
        ## La cartographie
        leafletOutput(
          outputId = "mymap",
          height = "100%",
          width = "100%"
        ),

        ## Le panneau latéral des résultats
        absolutePanel(
          id = "controls",
          class = "panel panel-default",
          fixed = TRUE,
          draggable = TRUE,
          top = 60, left = "auto", right = 20, bottom = "auto",
          width = 510, height = "auto",

          ## Combobox du bâti
          h2("Choix du bâtiment"),
          selectizeInput(
            "bati",
            label = NULL,
            choices = sort(chu$nom),
            options = list(
              placeholder = 'Sélectionner un bâtiment du CH Henri Ey',
              onInitialize = I('function() { this.setValue(""); }')
            )
          ),

          ## Résultats de l'identification
          hr(style = "border-top: 1px solid #000000;"),
          br(),
          h3(textOutput("nom_bat"), align = "center"),
          br(),
          htmlOutput("id_bat"),
          htmlOutput("adresse_bat"),
          htmlOutput("conso_bat"),
          br(),
          uiOutput("photo_bat"),
          br(),
          uiOutput("fiche_bat"),
          br(),
        )
      )
    ),
    tabPanel("Méthodologie", includeMarkdown("www/pages/methodo.md"))
  )
)


##############
##  Server  ##
##############
server <- function(input, output, session) {

  ##################
  ## Reactive Values
  ## on définit rv$bat qui écoute à la fois le clic bati carte et la combobox pour l'évènement "affichage des infos du bati"
  ## rv$bati_combobox sert uniquement au flyTo 
  rv <- reactiveValues()
  observeEvent(input$mymap_marker_click, {
    rv$bat <- chu[chu$fid == input$mymap_marker_click$id,]
  })
  observeEvent(input$bati, {
    rv$bat <- chu[chu$nom == input$bati,]
    rv$bati_combobox <- chu[chu$nom == input$bati,]
  })


  ## La carto
  output$mymap <- renderLeaflet({
    leaflet() %>%
      setView(lng = 1.35, lat = 48.27, zoom = 10) %>%

    ### Fonds de cartes
	addProviderTiles(providers$CartoDB.Positron, group = "Fond clair") %>%
	addTiles("https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Dark_Gray_Base/MapServer/tile/{z}/{y}/{x}", group = "Fond sombre") %>%
	addTiles("https://data.geopf.fr/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}",options = WMSTileOptions(tileSize = 256),group = "Photographies aériennes") %>%
	addTiles("https://data.geopf.fr/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=PCI%20vecteur&TILEMATRIXSET=PM&FORMAT=image/png&LAYER=CADASTRALPARCELS.PARCELLAIRE_EXPRESS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}",options = WMSTileOptions(tileSize = 256),group = "Parcelles") %>%
	addTiles(group = 'OpenStreetMap') %>%

	### Bâtiments
	addMarkers(
		layerId = ~fid,
		group = "CH Henri Ey",
		data = chu,
		label = chu$nom,
		clusterOptions = markerClusterOptions(),
		labelOptions = labelOptions(
			style = list("font-weight" = "normal", padding = "3px 8px", "background" = "#f1f1f1"),
			textsize = "15px",
			direction = "auto"
		)
	) %>%

    ### LayersControl
    addLayersControl(
		baseGroups = c("Fond clair", "Fond sombre", "Photographies aériennes", "OpenStreetMap",'Parcelles'),
		overlayGroups = c("CH Henri Ey"),
		position = "topleft"
    )
  })

  ## Proxy map
  mymap_proxy <- leafletProxy("mymap")

  ## Zoom to à la sélection de la combobox
  observe({
    fdata <- rv$bati_combobox
    mymap_proxy %>%
      clearMarkers() %>%
      flyTo(lng = st_coordinates(fdata)[1], lat = st_coordinates(fdata)[2], zoom = 17)
  })

  ## Affichage des caractéristiques du batiment 
  observeEvent(rv$bat, {

    bat <- rv$bat
    lien_fiche <- paste0("fiches/", bat$identifiant, ".pdf")

    # Infos basiques
    output$nom_bat <- renderText(bat$nom)
    output$id_bat <- renderText(paste0("<strong>Identifiant :</strong> ", bat$identifiant))
    output$adresse_bat <- renderText(paste0("<strong>Adresse :</strong> ", bat$adresse))
    output$conso_bat <- renderText(paste0("<strong>Consommation énergétique moyenne :</strong> ", bat$conso_moy, " kWh/m²"))

    ## Gestion des photos
    # affiche une photo si batiment simple, plein de photos si groupement de batiments
    if (grepl("Groupement", bat$nom)) {image_width = 150;image_height = 112}
    else {image_width = "100%";image_height = "100%"}
    output$photo_bat <- renderUI({
      image_output_list <- lapply(strsplit(bat$photos, ',')[[1]], function(i) {
        id = strsplit(i, '@')[[1]][1]  # pour les groupements, champ photos de la forme '01.34@Les blés d'Or,01.35@Lgt OASIS,...'
        nom = strsplit(i, '@')[[1]][2]

        lien_photo_thumb <- paste0("photos/", id, ".jpg")
        lien_fiche_thumb <- paste0("fiches/", id, ".pdf")

        # Hyperlien d'image vers fiche pdf
        a(img(src = lien_photo_thumb, title=paste0(id, " : ", coalesce(nom,bat$nom)), width=image_width, height=image_height, alt=id), target='_blank',  href = lien_fiche_thumb)
      })
      do.call(tagList, image_output_list)
    })

    # Lien vers la fiche batiment
    output$fiche_bat <- renderUI({
      a(paste0("Consommation énergétique de '",bat$nom, "'"), target="_blank", href=lien_fiche)
    })

  }, ignoreInit = TRUE)

}

shinyApp(ui, server)