### À propos

Ce démonstrateur est développé par le Cerema pour tenter de cartographier l'artificialisation du PETR Loire-Beauce entre 2006 et 2016. Ce prototype est en cours de développement et n'a pas vocation à être diffusé publiquement. La responsabilité du Cerema ne pourra en aucun cas être engagée quant au contenu et aux informations figurant sur ce site, ou aux conséquences pouvant résulter de leur utilisation ou interprétation.

Les données présentées dans ce démonstrateur, en phase beta, ont pour principal but d’illustrer le travail en cours de compréhension de l'artificialisation du PETR. Les données représentées ici sont issues d'un croisement réalisé entre les modes d'occupation des sols 2006 et 2016 de TOPOS pour tenter d'identifier les zones ayant changé d'usage entre ces deux années.

La catégorisation des espaces est issue d'après la nomenclature de niveau 1 proposée par TOPOS :
* les espaces agricoles
* les espaces naturels
* les espaces aménagés

Il est ici considéré comme **Artificialisé** tout passage d'une zone agricole ou naturelle en 2006 à un espace aménagé en 2016. Inversement, il est considéré comme **Désartificialisé** le passage d'un espace aménagé à un zone agricole ou naturelle.

L'*artificialisation brute* d'une commune correspond à la somme des surfaces artificialisées entre 2006 et 2016, la *désartificialisation* à la somme des surfaces désartificialisées, et l'*artificialisation nette* à la différence des ces deux totaux.

Ces résultats communaux sont comparés avec les valeurs annoncées par le Cerema entre 2006 et 2016, correspondant à de l'artificialisation brute. Le même travail est réalisé également à l'échelle de l'EPCI.