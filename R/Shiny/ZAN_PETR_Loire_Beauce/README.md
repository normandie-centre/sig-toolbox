# ZAN PETR Loire-Beauce

Le [tableau de bord](https://cerema-nc.shinyapps.io/zan_petr) développé permet de comparer les valeurs d'artificialisation annoncées par le Cerema et celles calculées par l'agence d'urbanisme d'Orléans, TOPOS, entre les années 2006 et 2016.

Le workflow classique est un traitement des données sous QGIS puis export en GeoJSON en 4326 dans le dossier `www/data/`. Les données exportées sont composées de la couche des communes, de la couche des EPCI et de la couche des changements 2006-2016 calculés à partir des MOS de TOPOS.

Le [script de pré-traitement](./scripts/preprocessing.R) doit être lancé en premier pour :
* convertir les geojson en rds au format sf et supprimer les fichiers initiaux geojson (et qmd si existe)
* ajouter une colonne de surface aux données de changements
* calculer les sommes d'artificialisation et de désartificialisation aux mailles communale et EPCI

Le pré-traitement tend à réduire le coût des calculs côté serveur de l'application pour améliorer sa fluidité.

Une fois les données prêtes, le tableau de bord peut être lancé classiquement avec un `runApp()`.