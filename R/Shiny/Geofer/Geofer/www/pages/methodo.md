
### Objectif<br><br>

Geofer est un outil développé par le Cerema Normandie-Centre, avec pour objectif d'afficher des données quantitatives sur le potentiel voyageurs et marchandises du réseau ferré français.
Il s'enrichit progressivement de couches de données, sur le modèle de Géoportail.
La mise en service n'est pas terminée, l'outil est en version bêta.


### Fonctionnement<br><br>


L'utilisateur peut choisir les couches de données qu'il souhaite visualiser : plan, vue aérienne, réseaux ferrés et routiers, entreprises, établissements scolaires, etc. Il peut cliquer sur un point (gare voyageurs ou site marchandises/ITE) pour faire apparaître un encart contenant la vue aérienne du site et les principales données relatives. Ces données sont par ailleurs téléchargeables au format de tableur sur l'onglet "téléchargement" de la plateforme.

Il est ainsi possible de connaître pour les gares ouvertes du réseau ferré national, mais aussi certaines gares fermées, les éléments suivants :

- Nombre de salariés, scolaires et habitants à moins de 15 minutes à pied, 30 minutes à pied (= 10 minutes à vélo, en première approche) ou 10 minutes en voiture de la gare.
- Nombre de chambres d'hôtels et emplacements de camping dans les communes accessibles en moins de 30 minutes en pied de la gare (Note : cette donnée n'est pas accessible à l'adresse précise, mais à l'échelle de la commune)
Pour les ITE, les éléments suivants
- Desserte de la gare : nombre d'arrêts quotidiens dans cette gare
- Fréquentation de la gare en nombre de voyageurs par jour : montées + descentes, autocars inclus, voyageurs en correspondance inclus.
- Trafics routiers sur les axes principaux en moyenne journalière lissée sur l'année.
Etc.


**Données utilisées :**<br><br>

- Isochrones : module isochrone de l'IGN
- Salariés : fichier Sirene 2020 
- Scolaires : fichier issu d' lopendata du ministère de l'Education Nationale, 2014 - 2019
- lits touristiques et places de camping : Fichier communal du tourisme, INSEE 2021
- Localisation, desserte et fréquentation des gares : base des gares proposée par la SNCF en open data, 2014 - 2020
- Trafics routiers : service statistique du ministère chargé des transports
- Fret : base de données ITE (Installations Terminales Embranchées) du Cerema, les données s'étendent entre 2015 et 2021 suivant les ITE renseignées
- Infrastructure ferroviaire et passages à niveau : open data SNCF + Cerema
- Plans : open street map, IGN
- Passage à niveau : Open data SNCF, https://ressources.data.sncf.com/explore/dataset/liste-des-passages-a-niveau/information/
- Réseau ferroviaire : Basé sur un fichier Réseau Ferré de France (2011) remis à jour régulièrement par le Cerema

Les aménités sont issues d'une classification des établissements du fichier Sirene réalisée par le Cerema.Voici la description, pour chaque activité, des codes NAF sélectionnés.

##### Détails des établissements de loisirs :<br><br>

- Arts du spectacle vivant (90.01Z)
- Gestion de salles de spectacles (90.04z)
- Activités des parcs d'attractions et parcs à thèmes (93.21z)
- Autres activités récréatives et de loisirs (93.29z)
- Gestion des bibliothèques et des archives (91.01z)
- Gestion des musées (91.02z)
- Gestion des sites et monuments historiques et des attractions touristiques similaires (91.03z)
- Gestion des jardins botaniques et zoologiques et des réserves naturelles (91.04z)
- Organisation de jeux de hasard et d'argent (92.00z)

###### Détail des services de santé :<br><br>

- Pratique dentaire (86.23z)
- Activité des médecins généralistes (86.21z)
- Activités de radiodiagnostic et de radiothérapie (86.22a)
- Activités chirurgicales (86.22b)
- Autres activités des médecins spécialistes (86.22c)
- Activités hospitalières (86.10z)

##### Détail des commerces :<br><br>

- Boulangerie et boulangerie-pâtisserie (10.71c)
- Pâtisserie (10.71d)
- Cuisson de produits de boulangerie (10.71b)
- Charcuterie (10.13b)
- Commerce de détail de fruits et légumes en magasin spécialisé (47.21z)
- Commerce de détail de produits surgelés (47.11a)
- Commerce d'alimentation générale (47.11b)
- Supérettes (47.11c)
- Supermarchés (47.11d)
- Magasins multi-commerces (47.11e)
- Hypermarchés (47.11f)
- Commerce de détail non alimentaire (47.19a)
- Autres commerces de détail en magasin non spécialisé (47.19b)
- Commerce de détail d'habillement en magasin spécialisé (47.71z)
- Commerce de détail de la chaussure (47.72a)
- Commerce de détail de maroquinerie et d'articles de voyage (47.72b) 

##### Détail de la restauration :<br><br>

- Débits de boisson (56.30z)
- Restauration traditionnelle (56.10a)
- Cafétérias et autres libres-services (56.10b)
- Restauration de type rapide (56.10c)

##### Détails des établissements sportifs :<br><br>

- Gestion d'installations sportives (93.11z)
- Activités des centres de culture physique (93.13z)

**Classification des passages à niveau :**<br><br>
- 10 	PN public pour voitures sans barrières protection assurée par un agent
- 11 	PN public pour voitures avec barrières gardé sans passage piétons accolé manoeuvré à pied d'œuvre
- 12 	PN public pour voitures avec barrières gardé sans passage piétons accolé manoeuvré à distance
- 13 	PN public pour voitures avec barrières gardé sans passage piétons accolé manoeuvré à pied d'oeuvre et distance
- 14 	PN public pour voitures avec barrières gardé avec passage piétons accolé manoeuvré à pied d'œuvre
- 15 	PN public pour voitures avec barrières gardé avec passage piétons accolé manoeuvré à distance
- 16 	PN public pour voitures avec barrières gardé avec passage piétons accolé manoeuvré à pied d'oeuvre distance
- 17 	PN public pour voitures avec barrières ou 1/2 barrières non gardé à SAL 2 et SAL 2B
- 18 	PN public pour voitures avec barrières ou 1/2 barrières non gardé à SAL 2 + ilot séparateur
- 19 	PN public pour voitures avec barrières ou 1/2 barrières non gardé à SAL 4
- 20 	PN public pour voitures sans barrières sans SAL
- 21 	PN public pour voitures sans barrières avec SAL 0
- 31 	PN public isolé pour piétons sans portillons
- 32 	PN public isolé pour piétons avec portillons
- 41 	PN privé pour voitures sans barrières
- 42 	PN privé pour voitures avec barrières sans passage piétons accolé
- 43 	PN privé pour voitures avec barrières avec passage piétons accolé public
- 44 	PN privé pour voitures avec barrières avec passage piétons accolé privé
- 45 	PN privé isolé pour piétons sans portillons
- 46 	PN privé isolé pour piétons avec portillons
- 0 	PN secondaire : Un PN peut être localisé sur plusieurs lignes et possède une identification différente sur chacune des lignes où il est localisé. Une des identifications, appelée PN père, porte la classe du PN (classe 10 à classe 46). Les autres identifications, appelées PN secondaires, portent la classe 00. 

**Légende de la hiérarchisation du réseau ferroviaire !**
