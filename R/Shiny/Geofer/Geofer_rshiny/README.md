## Geofer

🚅 La plateforme du Ferroviaire !  

Cette plateforme utilise la licence payante de shinyapps du Cerema Med pour son hébergement.

L'URL pour visualiser sur Shinyapps : https://cerema-med.shinyapps.io/geofer

#### Déploiement sur Shinyapps

Lancer le script `deploy.R` qui contient les credentials du compte shinyapps du Cerema Normandie-Centre.

##### Mise à jour de l'application

Les données sont assez lourdes et demandent d'être régulièrement réactualisées (chaque année). Les scripts avec les explications sont en train d'être réalisés.

