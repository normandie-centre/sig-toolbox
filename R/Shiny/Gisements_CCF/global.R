# Installer les packages nécessaires si ce n'est pas déjà fait
suppressMessages(library(arrow))
suppressMessages(library(COGiter))
suppressMessages(library(dplyr))
suppressMessages(library(ggplot2))
suppressMessages(library(glue))
suppressMessages(library(leafgl))
suppressMessages(library(leaflet))
suppressMessages(library(markdown))
suppressMessages(library(plotly))
suppressMessages(library(RColorBrewer))
suppressMessages(library(rjson))
suppressMessages(library(sf))
suppressMessages(library(sfarrow))
suppressMessages(library(shiny))
suppressMessages(library(shinyBS))
suppressMessages(library(shinymanager))
suppressMessages(library(viridis))
suppressMessages(library(waiter))

source("helpers.R", encoding = "UTF-8")


# Récupération des identifiants
user <- fromJSON(file="user.json")
credentials <- data.frame(
	user = user$login,
	password = user$password
)

#### Waiting screen ####
loader <- tagList(
  includeHTML("www/html/cerema-loader.html")
)
map_waiter <- Waiter$new(id = "gisement_map", html=loader, color = transparent(.5))

# Charger les données
communes_ccf_contours <- st_read("./data/communes_ccf_contours.geojson")
communes_ccf <- communes_ccf_contours %>% left_join(communes, by = c('INSEE_COM' = 'DEPCOM'), keep = TRUE) %>% left_join(epci) %>% filter(EPCI == '241400415') %>% st_as_sf(crs = 2154) %>% st_transform(4326)
contour_carte <- st_read("./data/contour_carte.geojson")

# gisement <- read_parquet('data/gisements_pour_alexis.parquet') 
# gisement <- st_read_parquet('./data/gisements_500m_2024-09-23.geoparquet')
gisement <- st_read_parquet('./data/gisements_500m_2024-11-15.geoparquet')
gisement <- gisement %>% rename(id = gisement_id)  %>%  
	inner_join(communes_ccf %>% st_drop_geometry %>% rename(idcom = DEPCOM)) %>% 
	st_as_sf(crs = st_crs(2154))  %>% st_transform(4326)  %>% st_as_sf() %>% st_make_valid() %>% 
  	mutate(`Type de gisement` = case_when(
		type == 'type 1' ~ 'Dent creuse',
		type == 'type 2' ~ 'BIMBY')
	) %>% 
  	rename(
		'Risque naturel' = 'note_ppr',
		'Patrimoine remarquable' = 'note_spr', 
		'Trame verte et bleue' = 'note_tvb'
	) %>% 
  	mutate(validation_communes = case_when(
		validation_communes == 'invalide' ~ '3. Invalidé',
		validation_communes == 'rugosite' ~ '2. Difficile',
		validation_communes == 'valide' ~ '1. Validé',
		validation_communes == 'coup_parti' ~ '4. Coup parti',
		.default = '5. Sans information')
	) %>% 
	mutate(label = paste0(label, ', ',`Type de gisement`)) %>% 
  	rename(
		`Terrain ayant \nmuté récemment` = `Terrain.ayant.muté.récemment` ,
		`Propriétaire habitant \nloin du territoire` = `Propriétaire.habitant.loin.du.territoire`,
		`Autorisation d'urbanisme \ndéposée récemment` = `Autorisation.d.urbanisme.déposée.récemment`,
		`Terrain difficilement \naccessible` = `Terrain.difficilement.accessible`
	)

gisement_carte <- gisement %>% filter(!(NOM == 'Villers-sur-Mer' & validation_communes == '5. Sans information')) # on enlève les gisements de Villers sans info sur la carte

names(gisement) <- gsub("\\.", " ", names(gisement)) 
gisement$somme_services <- rowSums(select(gisement %>% st_drop_geometry, starts_with("Service_")), na.rm = TRUE)

# Vérifier que la colonne score_durete existe
if (!"score_durete" %in% colnames(gisement)) {
	stop("La colonne 'score_durete' est manquante dans les données.")
}

zone_interet <- st_read('./data/2024-09-23_C139_zone_interet_34.geojson') %>% 
	mutate(libelle_type_zone = case_when(
		type_zone == 1 ~'Secteur de veille',
		type_zone == 2 ~'Secteur d\'intervention foncière',
		type_zone == 3 ~'Secteur de projet',
		type_zone == 4 ~'Secteur à préserver',
		type_zone == 99 ~'Autre')
	) %>% 
	st_make_valid %>% 
	mutate(id = rownames(.))

#Palette de zone d'intérêt
# Créer une fonction pour obtenir la couleur basée sur le score_durete
breaks_score_durete <- c(0,2,10,15,20,30)

# Création d'une palette allant du vert au rouge en passant par le jaune vif
pal_durete <- colorBin(
	palette = c("#fedcdb",'#ffaaa9',"#ff5659", "#ff0707", '#ad0100'),  # Palette personnalisée
	domain = gisement$score_durete,  # Domaine des valeurs à représenter
	bins = breaks_score_durete,      # Les "breaks" définis manuellement
	right = FALSE                    # Inclure ou non la limite droite
)

breaks_score_valeur_env <- c(0, 3, 6, 9, 110)
pal_score_valeur_env <- colorBin(
	palette = c('#bcdd8c',"#8ec640", "#61b567", '#448e60'),       # Palette de couleurs
	domain = gisement$score_valeur_env,  # Domaine des valeurs à représenter
	bins = breaks_score_valeur_env,           # Les "breaks" définis manuellement
	right = FALSE            # Inclure le bord droit dans les intervalles
)

# Définir une palette catégorique pour 'type_zone' (si c'est une variable factorielle)
pal_zone_interet <- colorFactor(
	palette = brewer.pal(5, "Set1"),  # Palette catégorique avec 5 couleurs
	domain = zone_interet$libelle_type_zone
)

## Palette pour la validation
# Valeurs possibles dans le champ 'Validation'
valeurs_validation <- factor(gisement$Validation, levels = c("Validé", "Difficile", "Invalidé", "Coup parti", "Sans information"))

# Couleurs personnalisées pour chaque valeur
couleurs_validation <- c(
	"Validé" = "#60B467",
	"Difficile" = "#FDEB7D", 
	"Invalidé" = "#F49D54", 
	"Coup parti" = "#7E97CE",
	"Sans information" = "pink"
)

# Création de la palette avec colorFactor
pal_validation <- colorFactor(
	palette = couleurs_validation,
	domain = valeurs_validation
)

### Bubbleplot
# gisement <- gisement %>% mutate(across(starts_with("Service_"), as.numeric))%>% 
#   mutate_if(is.factor, ~as.numeric(as.character(.)))

## Graphe Bubble plot par commune, validation et type
gisement_bubbleplot_par_commune_par_validation_par_type <- gisement %>% 
	group_by(NOM_DEPCOM,validation_communes,`Type de gisement`) %>% 
	summarize(
		score_durete = mean(score_durete, na.rm = TRUE),
		score_environnement = mean(score_valeur_env, na.rm = TRUE),
		surface = sum(surface, na.rm = TRUE),
		nombre_equipement = sum(somme_services, na.rm = TRUE)
	) %>% ungroup %>% st_drop_geometry

## Graphe Bubble plot par commune
gisement_bubbleplot_par_commune <- gisement %>% 
	group_by(NOM_DEPCOM) %>% 
	summarize(
		score_durete = mean(score_durete, na.rm = TRUE),
		score_environnement = mean(score_valeur_env, na.rm = TRUE),
		surface = sum(surface, na.rm = TRUE),
		nombre_equipement = sum(somme_services, na.rm = TRUE)
	) %>% ungroup %>% st_drop_geometry

# Créer le bubble plot sans traces
fig <- plot_ly(
	gisement_bubbleplot_par_commune, 
	x = ~score_durete, 
	y = ~score_environnement, 
	size = ~surface, 
	color = ~nombre_equipement,
	text = ~NOM_DEPCOM,
	type = 'scatter', 
	mode = 'markers',
	marker = list(sizemode = 'diameter', opacity = 0.5, line = list(width = 1))
) %>%
layout(
	title = "Bubble Plot",
	xaxis = list(title = "Score Dureté"),
	yaxis = list(title = "Score Environnement"),
	showlegend = TRUE
)
							