
# Analyse des gisements fonciers

Les gisements repérés, une fois passés par un premier tri grâce aux entretiens menés avec chaque commune, ont été analysés selon une triple grille de lecture :

1. **La dureté foncière** : C’est-à-dire la mesure de la difficulté à mobiliser un gisement pour des raisons de propriété, d’accès ou de forme de parcelle.
2. **La valeur environnementale des gisements** : Plus leur valeur environnementale est importante, plus ces gisements seront à préserver plutôt qu’à destiner à de la construction.
3. **Le potentiel stratégique** : La proximité d’un gisement par rapport aux équipements de transport ou à d’autres équipements publics.

Un système de scoring a ainsi été mis en place pour chacun de ces trois axes.

---

# Dureté foncière

Plus le score est élevé, plus la dureté est importante, autrement dit, plus il sera difficile de mobiliser le gisement pour de la densification.

| Indicateur                                | Source                  | Détails de la notation                                                                                                                                                        | Pondération |
|-------------------------------------------|-------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------|
| **Typologie du propriétaire**             | Fichiers fonciers        | Commune ou EPCI <br>Autre acteur public (État, département…)  <br>Acteur « para-public » ou de l’aménagement : organisme de logement social, entreprise de réseau, aménageur…<br>Personne physique ou morale privée         |  ++<br>+<br>-<br><br>--          |
| **Nombre de propriétaires**               | Fichiers fonciers        | Propriétaire unique<br>2 propriétaires (indivision ou pas)<br>3 propriétaires ou plus (indivision ou pas)                                                                 |  ++<br>+<br>-          |
| **Localisation du propriétaire**          | Fichiers fonciers        | Même commune<br>Même département <br>Même région<br>France métropolitaine<br>Outre-mer, étranger ou inconnu                                                        |  +++<br>++<br>+<br>-<br>--          |
| **Date de la dernière mutation**          | Fichiers fonciers        | 2023 ou 2024 <br>2021 ou 2022<br>2019 ou 2020<br>Antérieur à 2019                                                                                               |  --<br>-<br>+<br>++          |
| **Pente**                                 | IGN carte des pentes     | \< 10%<br>Entre 10 et 20%<br>Entre 20 et 30%<br>\> 30%                                                                                                         |  ++<br>+<br>-<br>--          |
| **Accès au gisement**                     | Calcul SIG               | Accès direct<br>Accès indirect possible<br>Accès indirect impossible                                                                                            |  ++<br>+<br>-          |
| **Gisement en cours de transformation** (autorisations d'urbanisme récentes)   | Sitadel                  | Autorisation déposée en 2023 ou 2024 pour une nouvelle construction ou un agrandissement, et dont la géolocalisation est suffisamment fiable                    |  ---          |

# Valeur environnementale

Plus leur valeur environnementale est importante, plus ces gisements seront à préserver plutôt qu’à destiner à de la construction.

| Indicateur                          | Source                      | Valeurs observées                                                                                             |
|-------------------------------------|-----------------------------|---------------------------------------------------------------------------------------------------------------|
| **Trame verte et bleue**            | Etude TVB de la 4CF          | Nombre de sous-trames (boisée, aquatique, noire…) intersectées sur le gisement                                 |
| **Plan de prévention des risques naturels(\*)** | PPRI et PPRMT de la DDTM14  | Zone rouge ou inconstructible équivalent<br>Zone bleue ou orange (aménagement possible mais contraint)         |
| **Sites patrimoniaux remarquables** | 4CF                          | Au moins 50% de la parcelle est couverte par un SPR de type jardin remarquable, espace boisé classé…            |

\* Ont été analysés :
- Le PPRMT Trouville, Villerville, Cricquebœuf
- le PPRi de la Basse Vallée de la Touques
- Le PPRMT Falaises des Vaches Noires
- Le PPRMT du Mont Canisy et de son versant nord

# Potentiel stratégique

Les équipements dans un rayon de 500m autour du gisement ont été comptés à partir de la base permanente des équipements (BPE). L'indice d'équipement est la somme de ces équipements, sans pondération. Voici la liste des équipements répertoriés dans cette base de données : 

| Secteur                 | Détails                                                                                                                                                                                                                                                                      |
|-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Commerces               | Commerces alimentaires<br>Commerces spécialisés non-alimentaires<br>Grandes surfaces                                                                                                                                                                                         |
| Enseignement            | Enseignement supérieur non-universitaire<br>Formation continue<br>Enseignement du second degré - second cycle<br>Enseignement supérieur universitaire<br>Enseignement du premier degré<br>Enseignement du second degré - premier cycle<br>Autres services de l'éducation         |
| Santé et action sociale | Fonctions médicales et paramédicales (à titre libéral)<br>Autres établissements et services à caractère sanitaire<br>Etablissements et services de santé<br>Action sociale pour handicapés<br>Autres services d'action sociale<br>Action sociale pour personnes âgées<br>Action sociale pour enfants en bas-âge |
| Services pour les particuliers | Artisanat du bâtiment<br>Autres services<br>Services automobiles<br>Services généraux<br>Services publics                                                                                                                                                             |
| Sports, loisirs et culture | Equipements sportifs<br>Equipements culturels et socioculturels<br>Equipements de loisirs                                                                                                                                                                                   |
| Tourisme                | Tourisme                                                                                                                                                                                                                                                                      |
| Transports et déplacements | Infrastructures de transports                                                                                                                                                                                                                                                |

