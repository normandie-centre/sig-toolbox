# Diagnostic foncier de la Communauté de Communes Cœur Côte Fleurie

Cette plateforme vous propose une exploration complète des résultats du diagnostic foncier approfondi mené sur l’ensemble du territoire par le **Cerema** entre mars et septembre 2024.

La **Communauté de Communes Cœur Côte Fleurie (CC-CCF)** souhaite engager une procédure de révision-extension du premier **PLUi** (Plan Local d'Urbanisme intercommunal), visant à mettre à jour ce projet au regard de l’évolution des enjeux du territoire, notamment en matière de **Zéro Artificialisation Nette (ZAN)**.

<img src="./illustration_accueil_md.png"/>


La collectivité s’est ainsi rapprochée du Cerema afin de réaliser, en amont, une étude sur le potentiel foncier en tâche urbaine. L'objectif est d’évaluer et cartographier les espaces disponibles pour le développement urbain.

## Méthodologie

Cette démarche repose sur une méthodologie incluant :

- **La collecte et l’analyse des données géospatiales, foncières et environnementales.**
- **L’identification des parcelles vacantes, sous-occupées et des secteurs stratégiques** (de projet, veille, etc.).
- **Une évaluation des contraintes et opportunités**, prenant en compte les aspects environnementaux, réglementaires ainsi qu’une analyse de la propriété.
- **Une phase de consultation locale**, permettant de confronter les résultats aux besoins et enjeux des communes.

## Outils interactifs

À travers une **carte interactive** et des **graphiques détaillés**, vous pouvez visualiser les marges de manœuvre du territoire pour de nouveaux projets d’habitat, d’activités ou d’équipements.

Cet outil a été conçu pour soutenir une réflexion collective en faveur d'un aménagement du territoire adapté aux enjeux actuels et futurs de la CC-CCF