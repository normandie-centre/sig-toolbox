## Envie d'aller plus loin ?

Des fiches détaillées par commune ont été créées dans lesquelles sont précisés les résultats de l'étude réalisée par le Cerema.
Vous pouvez les visualiser ci-dessous ou télécharger l'intégralité des fiches via le lien suivant : https://cerema.box.com/v/ccf-etude-fonciere.

