## Lecture du graphique :

Chaque bulle représente l'ensemble des gisements *d'une seule commune*. 

Le graphique se lit de la manière suivante :
- L'axe des abscisses représente la **moyenne de la dureté foncière** des gisements de la commune
- L'axe des ordonnées représente la **moyenne de la valeur environnementale** des gisements de la commune
- La taille de la bulle représente la **surface totale** des gisements sur la commune
- La couleur indique **l'intensité de la présence d'équipement** autour des gisements de la commune.