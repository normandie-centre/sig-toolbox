Application Shiny pour l'étude de la dureté foncière sur la communauté de communes Cœur Côte Fleurie accessible via https://cerema-med.shinyapps.io/gisements_ccf/.

#### Mise à jour du dashboard

On veut garder une historisation des versions de gisements.

Pour mettre à jour l'application avec une nouvelle donnée de gisements, il faut lancer le script `preprocess.R` en modifiant dedans le chemin vers la nouvelle donnée et celui vers la donnée de sortie. Ensuite il faut mettre à jour le `global.R` avec le chemin vers la donnée de sortie comme précédemment. Enfin pour démarrer le shiny, lancer `runapp()`.