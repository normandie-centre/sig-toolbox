library(arrow)
library(COGiter)
library(DBI)
library(jsonlite)
library(rjson)
library(RPostgres)
library(rsconnect)
library(sfarrow)
library(sf)
library(tictoc)
library(tidyverse)

rsconnect_ovh <- fromJSON(file="rsconnect_ovh.json")

# gisement <- read_parquet('data/gisements_pour_alexis.parquet')
gisement <- st_read('data/gisements_pour_dashboard_vdef.geojson')
gisement <- gisement %>% 
	st_as_sf(crs = st_crs(2154)) %>%
	st_transform(4326) %>%
	st_as_sf() %>%
	st_make_valid() %>%
	filter(st_geometry_type(.) %in% c("POLYGON", "MULTIPOLYGON")) %>% 
	filter(!is.na(score_durete)) %>%
	st_as_sf() %>% 
	mutate(label = paste0(catpro2txt, ' (',surface,'m²)')) %>% 
	rename(
		`Type de propriétaire` = note_type_proprio,
		`Terrain ayant muté récemment` = note_anneemut,
		`Propriétaire habitant loin du territoire` = note_locprop,
		`Autorisation d'urbanisme déposée récemment` = note_pc,
		`Terrain en pente` = note_pente,
		`Terrain difficilement accessible` = note_acces,
		`Propriétaires nombreux` = note_nb_prop,
		gisement_id = id
	) %>% 
  	st_transform(2154)

drv <- dbDriver("Postgres")
con_insee <- dbConnect(
	drv,
	dbname = "r_insee",
	host = rsconnect_ovh$host,
	port = rsconnect_ovh$port,
	user = rsconnect_ovh$user,
	password = rsconnect_ovh$password
)
con_bdtopo <- dbConnect(
	drv,
	dbname = "r_bdtopo_2023",
	host = rsconnect_ovh$host,
	port = rsconnect_ovh$port,
	user = rsconnect_ovh$user,
	password = rsconnect_ovh$password
)
## Chargement de la BPE
bpe_14 <- st_read(con_insee, query = 'select * from bpe_2023.bpe_2023_ensemble_xy where dep = \'14\' ') 

## Chargement des routes de la BDTopo, seulement les autoroutes, nationales et départementales
route <- st_read(con_bdtopo, query = "select * from bdtopo_d014_2023.route_numerotee_ou_nommee where type_route in ('Autoroute','Nationale','Départementale','Route européenne')")

# Création des tampons
gisement_500m <- gisement %>% st_buffer(dist = 500)
# gisement_750m <- gisement %>% st_buffer(dist = 750)
# gisement_1000m <- gisement %>% st_buffer(dist = 1000)

## Calcul sur les tampons pour calculer les aménités indiqués dans la BDTopo (27 champs)
calcul_amenites_polygone <- function(polygone) {
	amenites <- bpe_14 %>%
		st_join(polygone, join = st_within) %>%  # Joindre les tampons
		group_by(lib_sdom,gisement_id) %>%  # Groupement par valeur de lib_sdom et identifiant du tampon
		summarise(count = n(), .groups = 'drop') %>% 
		st_drop_geometry() %>% 
		pivot_wider(names_from = 'lib_sdom', values_from =   'count',names_prefix = 'Service_')

	resultat_polygone <- polygone %>% 
		select(gisement_id) %>%
		left_join(amenites)

	# route_filter <- route %>% st_join(gisement_500m, join = st_intersects) %>% filter(!is.na(gisement_id))
	# resultat_polygone <- polygone %>% left_join(route_filter)
	resultat_polygone <- resultat_polygone %>%
		mutate(intersection_route = st_intersects(gisement_500m, route, sparse = FALSE) %>% 
		rowSums() > 0) %>%
		mutate(across(where(is.numeric), ~ replace_na(., 0)))

	return(resultat_polygone)

}

c <- calcul_amenites_polygone(gisement_500m) 
gisement <- gisement %>% left_join(c %>% st_drop_geometry)
st_write_parquet(gisement,'./data/gisements_500m_2024-11-15.geoparquet')
