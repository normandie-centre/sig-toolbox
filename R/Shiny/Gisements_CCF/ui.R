# Personnalisation des messages
set_labels(
  language = "fr",
  "Please authenticate" = "<h1>Bienvenue sur l'application Gisements CCF</h2><br><small style='text-align: justify; display: block;'>Accédez à des données précises et des analyses détaillées sur les gisements. Connectez-vous pour explorer les informations stratégiques et optimiser vos décisions.</small>"
)

ui <- secure_app(bootstrapPage(

	# Chargement des scripts et CSS
	tags$head(
		# tags$link(rel = "stylesheet", type = "text/css", href = "css/custom_styles.css"),
		tags$link(rel = "stylesheet", type = "text/css", href = "css/default.css"),
		tags$link(rel = "stylesheet", type = "text/css", href = "css/cerema-loader.css"),
		tags$title("Carte des gisements fonciers de la CC Cœur Côte Fleurie"),  # Titre dans l'onglet du navigateur
		tags$link(rel = "icon", type = "image/png", href = "./static/illustration_accueil_md.png")  # Chemin vers le favicon
	),

	# Initialisation du Loader
	useWaiter(),
	autoWaiter(html = loader, fadeout = 5, color = transparent(.5)),
	waiterPreloader(html = loader, fadeout = 5, color = transparent(.5)),
	
	div(class = "main-container",
		div(class = "content-wrapper",
			navbarPage(
				windowTitle = "Carte des gisements fonciers de la CC Cœur Côte Fleurie",
				title = div(width = '500px', img(src = "./static/logo_total.png", height = "50px", style = "margin-left: -50px;")),
				
				# Onglet Accueil
				tabPanel("Accueil", class = "markdown-content", 
					fluidRow(class = "markdown-content", column(8, offset=2, includeMarkdown("www/pages/accueil.md"))),
				),
				
				# Onglet Résultats
				tabPanel("Cartographie",
					fluidRow(
						column(4,
							bsCollapse(id = 'collapse',
								bsCollapsePanel('Dureté foncière', style = "danger",
									htmlOutput('texte_initial'),
									plotlyOutput("thermometer_durete"),
									plotlyOutput("pie_chart", height = "300px", width = "100%")
								),
								bsCollapsePanel('Score environnemental', style = "danger",
									htmlOutput('texte_initial_2'),
									plotlyOutput('thermometer_score_environnemental'),
									plotlyOutput("pie_chart_score_environnemental")
								),
								bsCollapsePanel("Proximité des services", style = "danger",
									htmlOutput('texte_initial_3'),
									plotlyOutput("plotly_services", height = "300px", width = "100%")
								)
							)
						),
						column(8,class = "content-column", 
							div(
								sliderInput("slider_surface", label = "Surface (en m²)", min = 300, max = 35000, value = c(300, 35000), step = 1000),
								sliderInput("slider_durete", label = "Score de dureté foncière", min = 0, max = 27, value = c(0, 27)),
								sliderInput("slider_environnement", label = "Score environnemental", min = 0, max = 106, value = c(0, 106)),
							style = "display: flex; gap: 10vw;"),
							leafletOutput("gisement_map")
						)
					)
				),
			
				# Onglet Statistiques
				tabPanel("Statistiques",
					fluidRow(
						column(6, sidebarPanel(plotlyOutput("KPI_hectare"), width = "100%")),
						column(6, plotlyOutput("pie_chart_surface_total_validation"))
					),
					fluidRow(
						column(6, sidebarPanel(plotOutput("Barplot_ha_par_commune"), width = "100%")),
						column(6,plotlyOutput("pie_chart_surface_total_type"))
					),
					fluidRow(
						sidebarPanel(class = "sidebar-markdown", includeMarkdown("www/pages/lecture_bubbleplot.md")),
						column(8, plotlyOutput("bubbleplot_par_commune"))
					), 
					br(),br(),br(),br(),br(),br(),
					fluidRow(
						column(4,
							sidebarPanel(
								selectInput("validation_input", "Validation:",
											choices = setNames(
												unique(gisement$validation_communes) %>% sort(),
												unique(gisement$validation_communes) %>% sort() %>% substring(3)
											),
											selected = unique(gisement$validation_communes) %>% sort() %>% .[[1]]
								),
								selectInput("type_input", "Type:",
											choices = unique(gisement$`Type de gisement`) %>% sort(),
											selected = unique(gisement$`Type de gisement`) %>% sort() %>% .[[1]]
								),
								width = 12
							)
						),
						column(8, plotlyOutput("bubbleplot_par_commune_par_validation_par_type"))
					)
					# includeMarkdown('./www/pages/bas_de_page.md')
				),
			
				# Onglet Méthodologie
				tabPanel("Méthodologie", class = "markdown-content",
					fluidRow(class = "markdown-content", column(8, offset=2, includeMarkdown("www/pages/gisements.md"))),
				),
			
				# Onglet Pour aller plus loin
				tabPanel("Pour aller plus loin", class = "markdown-content",
					fluidRow(class = "markdown-content", column(8, offset=2, includeMarkdown("www/pages/pour_aller_plus_loin.md"))),
					br(),
					fluidRow(column(8, offset=2,
						selectInput("pdf_select", "Sélectionnez une fiche :", choices = communes_ccf$NOM_DEPCOM %>% unique() %>% sort()),
						uiOutput("pdf_viewer")
					))
				),
			)
		),

		tags$footer(class = "footer",
			div(class = "footer-content",
				div(style = "margin:auto;", 
					"Powered by ©Cerema",
					div(
						tags$a(href = "https://www.linkedin.com/company/cerema", icon("linkedin"), target = "_blank"),
						tags$a(href = "https://twitter.com/CeremaCom", icon("twitter"), target = "_blank"),
						tags$a(href = "https://www.cerema.fr", icon("link"), target = "_blank"),
					)
				)
			)
		)
	)
), language = "fr", theme = bslib::bs_theme(bootswatch = "united"))
