# Server - Partie serveur
server <- function(input, output, session) {
  
	# Ajout de l'authentification
	res_auth <- secure_server(
    	check_credentials = check_credentials(credentials) # Vérifie les identifiants
	)

	#### Reactive values  ####
	filtered_gisements <- reactive({
		req(res_auth$user)
		result <- gisement_carte %>%
			filter(
				surface >= input$slider_surface[1],
				surface <= input$slider_surface[2],
				score_durete >= input$slider_durete[1],
				score_durete <= input$slider_durete[2],
				score_valeur_env >= input$slider_environnement[1],
				score_valeur_env <= input$slider_environnement[2]
			)
		
		# Vérifier si le résultat est vide
		if (nrow(result) == 0) {
			showNotification("Aucun gisement ne correspond aux filtres actuels, ré-affichage de tous les gisements", type = "error")
			result <- gisement_carte
		}
		result %>% st_as_sf()
  	})

	# Créer une carte leaflet avec les polygones
	output$gisement_map <- renderLeaflet({
		leaflet() %>%
		addLayersControl(
			baseGroups = c("Fond clair", "Fond sombre", "Photographies aériennes", "OpenStreetMap",'Parcelles'),
			overlayGroups = c('Zones d\'intérêt', 'Dureté des gisements', 'Score environnemental des gisements', 'Validation des gisements par les communes'),
			position = "topleft"
		) %>%
		setView(lng = 0.097, lat = 49.348, zoom = 12) %>%
		# addProviderTiles(providers$CartoDB.DarkMatter, group = "Fond très sombre") %>%
		addProviderTiles(providers$CartoDB.Positron, group = "Fond clair") %>%
		addTiles("https://services.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Dark_Gray_Base/MapServer/tile/{z}/{y}/{x}", group = "Fond sombre") %>%
		addTiles("https://data.geopf.fr/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}",options = WMSTileOptions(tileSize = 256),group = "Photographies aériennes") %>%
		addTiles("https://data.geopf.fr/wmts?REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=PCI%20vecteur&TILEMATRIXSET=PM&FORMAT=image/png&LAYER=CADASTRALPARCELS.PARCELLAIRE_EXPRESS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}",options = WMSTileOptions(tileSize = 256),group = "Parcelles") %>%
		addTiles(group = 'OpenStreetMap') %>% 
		addPolygons(
			data = contour_carte,
			fillOpacity = 0.2,
			color = 'lightblue'
		) %>% 
		addPolygons(
			data = communes_ccf_contours,
			fillOpacity = 0,
			color = '#f8e6f3'
		) %>% 
		addPolygons(
			data = gisement_carte,
			color = 'lightgrey',
			fillColor = ~pal_durete(score_durete),  # Appliquer les couleurs en fonction du score_durete
			fillOpacity = 0.5,  
			weight = 1,
			label = ~label,
			layerId = ~id,
			group = 'Dureté des gisements'
		) %>% 
		addPolygons(
			data = gisement_carte,
			color = 'lightgrey',
			fillColor = ~pal_score_valeur_env(score_valeur_env),  # Appliquer les couleurs en fonction du score_durete
			fillOpacity = 0.6,  
			weight = 1,
			label = ~label,
			layerId = ~id,
			group = 'Score environnemental des gisements'
		) %>% 
		addPolygons(
			data = zone_interet %>% st_as_sf,
			color = ~pal_zone_interet(libelle_type_zone),
			fillColor = NULL,
			# fillColor = ~pal_zone_interet(libelle_type_zone),  #
			fillOpacity = 0,  
			dashArray = "10, 10",
			label = ~libelle,
			weight = 2,
			layerId = ~id,
			group = 'Zones d\'intérêt'
		) %>% 
		addPolygons(
			data = gisement_carte,
			color = 'lightgrey',
			fillColor = ~pal_validation(validation_communes %>% substring(4)),  # Appliquer les couleurs en fonction du score environnemental
			fillOpacity = 0.5,  
			weight = 1,
			label = ~label,
			layerId = ~id,
			group = 'Validation des gisements par les communes'
		) %>%
		addLegend(
			group = 'Validation des gisements par les communes',
			pal = pal_validation, 
			values = factor(gisement$validation_communes %>% substring(4), levels = c("Validé", "Difficile", "Invalidé", "Coup parti", "Sans information")),
			title = "Validation des gisements par les communes",
			position = "bottomright"
		) %>%  
		addLegend(
			group = 'Zones d\'intérêt',
			pal = pal_zone_interet, 
			values = zone_interet$libelle_type_zone,
			title = "Zones d'intérêt",
			position = "bottomright"
		) %>%  
		addLegend(
			group = 'Dureté des gisements',
			pal = pal_durete, 
			values = gisement$score_durete,
			title = "Score de dureté",
			position = "bottomright"
		) %>% 
		addLegend(
			group = 'Score environnemental des gisements',
			pal = pal_score_valeur_env, 
			values = gisement$score_valeur_env,
			title = "Score environnemental",
			position = "bottomright"
		) %>% 
		hideGroup(group = c('Score environnemental des gisements', 'Dureté des gisements'))
	})

	# Réagir aux clics sur les polygones pour afficher les informations correspondantes
	click_counter <- reactiveVal(0)

	## Initialisation des collapsePanel
	observe({if (is.null(input$gisement_map_shape_click)) {output$texte_initial <- renderPrint(HTML('<span class="glyphicon glyphicon-search"></span> <i>Cliquez sur un gisement pour obtenir sa description.</i>'))}else{output$texte_initial <- renderPrint(HTML(""))}})
	observe({if (is.null(input$gisement_map_shape_click)) {output$texte_initial_2 <- renderPrint(HTML('<span class="glyphicon glyphicon-search"></span> <i>Cliquez sur un gisement pour obtenir sa description.</i>'))}else{output$texte_initial_2 <- renderPrint(HTML(""))}})
	observe({if (is.null(input$gisement_map_shape_click)) {output$texte_initial_3 <- renderPrint(HTML('<span class="glyphicon glyphicon-search"></span> <i>Cliquez sur un gisement pour obtenir sa description.</i>'))}else{output$texte_initial_3 <- renderPrint(HTML(""))}})

	observeEvent(input$gisement_map_shape_click, {
		# print(input$gisement_map_shape_click)
		req(input$gisement_map_shape_click$group == "Zones d'intérêt")
		c_zone_interet <- zone_interet %>% filter(id == input$gisement_map_shape_click$id)
		content <-  div(HTML(paste0(h5(c_zone_interet$libelle_type_zone),h6(c_zone_interet$description))))
		show_modal(contenu = content)
	})


	observeEvent(input$gisement_map_shape_click, {
		req(input$gisement_map_shape_click$group %in% c("Dureté des gisements",'Validation des gisements par les communes','Score environnemental des gisements'))
		click <- input$gisement_map_shape_click
		
		click_counter(click_counter() + 1)

		# Trouver le polygone correspondant en fonction de l'ID cliqué
		selected_id <- click$id
		# print(click)
		req(!is.null(click$id))
		if (!is.null(click) & click_counter()<2) {updateCollapse(session, id = 'collapse',open = "Dureté foncière")}
		selected_gisement <- gisement %>% filter(id == click$id)
			
		# Extraire les informations à afficher (par exemple, la dureté)
		output$selected_gisement <- renderText({
			paste('Surface : ',selected_gisement$surface,' m²',', type propriétaire : ',selected_gisement$catpro2txt)
		})

		output$thermometer_durete <- renderPlotly({

			# Définition des couleurs du thermomètre
			therm_color <- case_when(
				selected_gisement$score_durete < 2  ~ "#fedcdb",
				selected_gisement$score_durete < 10 ~ "#ffaaa9",
				selected_gisement$score_durete < 15 ~ "#ff5659",
				selected_gisement$score_durete < 20 ~ "#ff0707",
				TRUE                                ~ "#ad0100"
			)

			plot_ly(
				type = "indicator",
				mode = "gauge+number",
				value = selected_gisement$score_durete,  # Valeur de la dureté
				title = list(text = "Dureté foncière de la parcelle", font = list(size = 18)),
				gauge = list(
					axis = list(range = list(0, 25),  visible = FALSE),
					bar = list(color = 'lightgrey', line = list(color = "black", width = 2)),  # Couleur de l'indicateur
					steps = list(
						list(range = c(0, 2), color = "#fedcdb"),
						list(range = c(2, 10), color = "#ffaaa9"),
						list(range = c(10, 15), color = "#ff5659"),
						list(range = c(15, 20), color = "#ff0707"),
						list(range = c(20, 25), color = "#ad0100")
					),
					threshold = list(
						line = list(color = "black", width = 4),
						thickness = 0.75,
						value = selected_gisement$score_durete  # Position de la dureté actuelle
					)
				),
				number = list(
						font = list(color = "white", size = 0, family = "Arial")  # Couleur et taille du chiffre
				)
			) %>%
			layout(
				margin = list(l = 20, r = 20, b = 40, t = 50),  # Ajuster les marges
				paper_bgcolor = "white",
				font = list(color = "black", size = 14),
				
				# Ajouter une annotation pour mettre en valeur le chiffre
				annotations = list(
					list(
						x = 0.5,  # Position X
						y = 0.5,  # Position Y
						text = paste0("<b style='font-size:48px;'>", selected_gisement$score_durete, "</b>"),  # Texte
						showarrow = FALSE,  # Pas de flèche
						font = list(color = "black", size = 48),  # Couleur et taille du texte
						bgcolor = therm_color,  # Fond coloré basé sur la valeur
						bordercolor = "black",  # Bordure noire autour du texte
						borderwidth = 4,  # Largeur de la bordure
						xanchor = "center",  # Ancrer le texte au centre
						yanchor = "middle"
					)
				)
			)
		})
		
	
		output$pie_chart <- renderPlotly({
				
			# Sélectionner uniquement les colonnes que vous souhaitez inclure dans le camembert
			selected_columns <- c(
				"Propriétaire habitant \nloin du territoire", 
				"Autorisation d'urbanisme \ndéposée récemment", 
				"Terrain en pente", 
				# "note_ppr",  # Garde ce nom s'il n'a pas été renommé
				"Terrain difficilement \naccessible", 
				"Type de propriétaire", 
				"Propriétaires nombreux", 
				"Terrain ayant \nmuté récemment"
			)
				
			df <- gisement %>% filter(id == selected_gisement$id) %>% select(selected_columns)
			# Extraire les noms de colonnes et les valeurs associées
			labels <- colnames(df)
			values <- as.numeric(df[1, ])
			
			# Filtrer pour ne garder que les valeurs non nulles
			valid_indices <- values != 0
			labels <- labels[valid_indices]
			values <- values[valid_indices]
			
			# Créer un graphique en camembert pour la dureté foncière avec Plotly
			plot_ly(labels = labels, 
				values = values, 
				type = "pie",
				textinfo = "percent",
				hoverinfo = "label+percent",
				marker = list(colors = RColorBrewer::brewer.pal(length(labels), "Set3"))
			) %>% 
			plotly::layout(title = "Causes de la dureté foncière",
				showlegend = TRUE,
				autosize = TRUE, # Ajuster la taille de la police pour petits écrans
				legend = list(
					x = 1, 
					y = 0,
					bgcolor = 'rgba(0, 0, 0, 0)',
					xanchor = "left",
					# yanchor = "bottom",
					orientation = "v",  # Orientation verticale
					traceorder = "normal",  # Ordre normal des éléments de la légende
					
					margin = list(l = 0, r = 0, b = 0, t = 0),  # Augmenter la marge en haut
					width = 800,  # Largeur du graphique
					height = 800 ,
					font = list(size = 12)
				)
			) %>%
			config(responsive = TRUE)
		})
			
		output$thermometer_score_environnemental <- renderPlotly({
			couleur_palette <- c('#bcdd8c',"#8ec640", "#61b567", '#448e60')
			therm_color <- case_when(
				selected_gisement$score_valeur_env < 3   ~ couleur_palette[1],
				selected_gisement$score_valeur_env < 6   ~ couleur_palette[2],
				selected_gisement$score_valeur_env < 8   ~ couleur_palette[3],
				selected_gisement$score_valeur_env < 110 ~ couleur_palette[4],
				TRUE                                     ~ couleur_palette[5]
			)
				
			plot_ly(
				type = "indicator",
				mode = "gauge+number",
				value = log(selected_gisement$score_valeur_env) ,  # Valeur de la dureté
				title = list(text = "Score environnemental de la parcelle", font = list(size = 18)),
				gauge = list(
					axis = list(range = list(0, log(110)), visible = FALSE),
					bar = list(color = 'lightgrey', line = list(color = "black", width = 2) ),  # Couleur de l'indicateur
					steps = list(
						list(range = c(0, log(3)), color = couleur_palette[1]),
						list(range = c(log(3),log(6)), color = couleur_palette[2]),
						list(range = c(log(6),log(9)), color = couleur_palette[3]),
						list(range = c(log(9), log(110)), color = couleur_palette[4])
					),
					threshold = list(
						line = list(color = "black", width = 4),
						thickness = 0.75,
						value = log(selected_gisement$score_valeur_env)   # Position de la dureté actuelle)
					)
				),
				number = list(
					font = list(color = "white", size = 0, family = "Arial")  # Couleur et taille du chiffre
				)
			) %>%
			layout(
				margin = list(l = 20, r = 20, b = 40, t = 50),  # Ajuster les marges
				paper_bgcolor = "white",
				font = list(color = "black", size = 14),
				
				# Ajouter une annotation pour mettre en valeur le chiffre
				annotations = list(
					list(
						x = 0.5,  # Position X
						y = 0.5,  # Position Y
						text = paste0("<b style='font-size:48px;'>", selected_gisement$score_valeur_env, "</b>"),  # Texte
						showarrow = FALSE,  # Pas de flèche
						font = list(color = "black", size = 48),  # Couleur et taille du texte
						bgcolor = therm_color,  # Fond coloré basé sur la valeur
						bordercolor = "black",  # Bordure noire autour du texte
						borderwidth = 4,  # Largeur de la bordure
						xanchor = "center",  # Ancrer le texte au centre
						yanchor = "middle"
					)
				)
			)
		})
			
		output$pie_chart_score_environnemental <- renderPlotly({
			
			# Sélectionner uniquement les colonnes que vous souhaitez inclure dans le camembert
			selected_columns <- c(
				"Risque naturel", 
				"Patrimoine remarquable", 
				"Trame verte et bleue"
			)
			
			df <- gisement %>% filter(id == selected_gisement$id) %>% select(selected_columns)
			# Extraire les noms de colonnes et les valeurs associées
			labels <- colnames(df)
			values <- as.numeric(df[1, ])
			
			# Filtrer pour ne garder que les valeurs non nulles
			valid_indices <- values != 0
			labels <- labels[valid_indices]
			values <- values[valid_indices]
			
			# Créer un graphique en camembert pour la dureté foncière avec Plotly
			plot_ly(
				labels = labels, 
				values = values, 
				type = "pie",
				textinfo = "percent",
				text = paste("Score de", values),
				hoverinfo = "label+text+percent",
				marker = list(colors = RColorBrewer::brewer.pal(length(labels), "Set3"))
			) %>% 
			layout(title = "Causes du score environnemental",
				showlegend = TRUE,
				autosize = TRUE,
					legend = list(
					x = 1, 
					y = 0,
					bgcolor = 'rgba(0, 0, 0, 0)',
					xanchor = "left",
					# yanchor = "bottom",
					orientation = "v",  # Orientation verticale
					traceorder = "normal",  # Ordre normal des éléments de la légende
					
					margin = list(l = 0, r = 0, b = 0, t = 0),  # Augmenter la marge en haut
					width = 800,  # Largeur du graphique
					height = 800 ,
					font = list(size = 12)
				)
			) %>%
			config(responsive = TRUE)
		})
		
		
		# Créer l'histogramme des services proches du gisement avec Plotly
		output$plotly_services <- renderPlotly({
			df <- gisement %>% filter(id == selected_gisement$id)
			
			service_columns <- grep("^Service_", names(df), value = TRUE)
			service_values <- df[service_columns]
			
			# Supprimer le mot "Service_" au début des champs
			names(service_values) <- gsub("Service_", "", names(service_values))
			
			# Créer des labels avec des sauts de ligne après le troisième espace
			short_names <- sapply(names(service_values), insert_newline_after_nth_space, n = 3)
			
			# Convertir en un data frame pour Plotly
			plot_data <- data.frame(
				Service = names(service_values),
				Value = as.numeric(service_values)
			)
		
			# Filtrer les services avec des valeurs non nulles
			plot_data <- plot_data %>% filter(Value > 0)
			
			plot_ly(
				data = plot_data,
				y = ~Service,
				x = ~Value,
				type = 'bar',
				orientation = 'h',
				text = ~Value,
				textposition = 'inside'
			) %>%
			layout(
				title = "Services Accessibles à moins de 500m",
				xaxis = list(title = ""),
				yaxis = list(
					title = "",
					ticktext = short_names,
					tickmode = "linear", # Ajuster l'angle des labels
					tickvals = 1:length(short_names),
					automargin = TRUE # Ajuster automatiquement les marges pour le texte
				)
			)
		})

		leafletProxy("gisement_map") %>%
		clearGroup(group = 'Gisement sélectionné') %>%  # On enlève tous les polygones du groupe
		addPolygons(
			data = selected_gisement,
			fillColor = "yellow",  # Couleur de surbrillance pour le polygone cliqué
			weight = 3,
			opacity = 1,
			color = "black",
			label = selected_gisement$label,
			layerId = selected_id,
			group = 'Gisement sélectionné'
		)
	})
  
	observe({
		leafletProxy("gisement_map") %>%
		clearGroup(c('Dureté des gisements', 'Score environnemental des gisements', 'Validation des gisements par les communes')) %>%
		addPolygons(
			data = filtered_gisements(),
			color = 'lightgrey',
			fillColor = ~pal_durete(score_durete),  # Appliquer les couleurs en fonction du score_durete
			fillOpacity = 0.5,  
			weight = 1,
			label = ~label,
			layerId = ~id,
			group = 'Dureté des gisements'
		) %>% 
		addPolygons(
			data = filtered_gisements(),
			color = 'lightgrey',
			fillColor = ~pal_score_valeur_env(score_valeur_env),  # Appliquer les couleurs en fonction du score_durete
			fillOpacity = 0.6,  
			weight = 1,
			label = ~label,
			layerId = ~id,
			group = 'Score environnemental des gisements'
		) %>% 
		addPolygons(
			data = filtered_gisements(),
			color = 'lightgrey',
			fillColor = ~pal_validation(validation_communes %>% substring(4)),  # Appliquer les couleurs en fonction du score environnemental
			fillOpacity = 0.5,  
			weight = 1,
			label = ~label,
			layerId = ~id,
			group = 'Validation des gisements par les communes'
		)
	})

	##### Onglet statistiques

	# Agréger les surfaces par validation
	df_agg_validation <- gisement %>%
		st_drop_geometry %>% 
		mutate(validation_communes_label = substring(validation_communes,3,length(validation_communes))) %>% 
		group_by(validation_communes_label) %>%
		summarize(total_surface = sum(surface)) %>% 
		ungroup()
  
	df_agg_validation <- df_agg_validation %>% arrange(validation_communes_label)
	
	output$pie_chart_surface_total_validation <- renderPlotly({

		couleur_validation <- c('#7E97CE', '#FDEB7D', '#F49D54','pink','#60B467')

		plot_ly(
			data = df_agg_validation,
			labels = ~validation_communes_label,   # Utilisation du champ 'validation' comme labels
			values = ~total_surface,  # Les valeurs des surfaces totales
			type = 'pie',  # Type de graphique
			text = paste0(round(df_agg_validation$total_surface/10000,0),' ha'),
			hoverinfo = "label+text+percent",
			insidetextfont = list(color = 'white',opacity = '1'),
			insidetextorientation = 'radial',  # Positionnement du texte à l'intérieur du cercle
			marker = list(colors = couleur_validation, opacity = 0.1)  
		) %>% style(opacity = 0.8) %>% 
		layout(
			title = list(text = "Répartition des surfaces par validation"),
			showlegend = TRUE,  # Afficher la légende
			legend = list(
				title = list(
					text = '',
					orientation = 'z',  # Orientation horizontale de la légende
					x = 0.5, y = -0.2,  # Positionnement de la légende en dessous
					xanchor = 'center', yanchor = 'top', color = 'white',
					opacity = 100
				)
			)
		)
	})
  
	### Agréger les surfaces par Type (bimby ou dent creuse)
	df_agg_type <- gisement %>%
		st_drop_geometry %>% 
		group_by(`Type de gisement`) %>%
		summarize(total_surface = sum(surface))
  
	# Créer le pie chart avec plotly
	output$pie_chart_surface_total_type <- renderPlotly({
		plot_ly(
			data = df_agg_type,
			labels = ~`Type de gisement`,   # Utilisation du champ 'validation' comme labels
			values = ~total_surface,  # Les valeurs des surfaces totales
			type = 'pie',  # Type de graphique
			text = paste0(round(df_agg_type$total_surface/10000,0),' ha'),
			hoverinfo = "label+text+percent",
			insidetextfont = list(color = 'white',opacity = '1'),
			insidetextorientation = 'radial',  # Positionnement du texte à l'intérieur du cercle
			marker = list(colors = c('#60B467', '#F49D54'))  # Tu peux personnaliser les couleurs ici
		) %>%
		layout(
			title = list(text = "Répartition des surfaces par type de gisement"),
			showlegend = TRUE,  # Afficher la légende
			legend = list(
				title = list(
					text = '',
					orientation = 'z',  # Orientation horizontale de la légende
					x = 0.5, y = -0.2,  # Positionnement de la légende en dessous
					xanchor = 'center', yanchor = 'top', color = 'white',
					opacity = 100)
			)
		) %>%
		style(opacity = 0.8)
	})
  
  	# Créer le bubble plot sans traces
	output$bubbleplot_par_commune <-renderPlotly({

  		gisement_bubbleplot_par_commune <- gisement_bubbleplot_par_commune %>%
			mutate(`Indice de l'équipement` = nombre_equipement/100, label = paste0(NOM_DEPCOM,' (',round(surface/10000,0),' ha)'))
  
		plot_ly(gisement_bubbleplot_par_commune, 
			x = ~score_durete, 
			y = ~score_environnement, 
			size = ~surface, 
			color = ~`Indice de l'équipement`,
			text = ~label,
			type = 'scatter', 
			mode = 'markers',
			hoverinfo = 'text',
			marker = list(sizemode = 'diameter', opacity = 0.5, line = list(width = 1))
		) %>%
		layout(
			title = list(
				text = "Répartition par commune des gisements \nen fonction de la dureté foncière et du score environnemental",
				x = 0.5,        # Centrer horizontalement
				xanchor = "center" # Ancrer le titre horizontalement au centre
			),
			xaxis = list(title = "Score Dureté"),
			yaxis = list(title = "Score Environnement"),
			showlegend = TRUE
		) %>%
		colorbar(title = "Indice de l'équipement") 
	})

	observeEvent(input$validation_input,{
		observeEvent(input$type_input,{
			req(!is.null(input$validation_input))
			req(!is.null(input$type_input))
			
			data <- gisement_bubbleplot_par_commune_par_validation_par_type %>% 
				filter(validation_communes == input$validation_input & `Type de gisement` == input$type_input) %>% 
				mutate(`Indice équipement` = nombre_equipement/100,label = paste0(NOM_DEPCOM,' (',round(surface/10000,0),' ha)'))
			
			output$bubbleplot_par_commune_par_validation_par_type <- renderPlotly({
				plot_ly(
					data, 
					x = ~score_durete, 
					y = ~score_environnement, 
					size = ~surface, 
					color = ~`Indice équipement`,
					text = ~label,
					type = 'scatter', 
					hoverinfo = 'text',
					mode = 'markers',
					marker = list(sizemode = 'diameter', opacity = 0.5, line = list(width = 1))
				) %>%
				layout(title = "",
					xaxis = list(title = "Score Dureté"),
					yaxis = list(title = "Score Environnement"),
					showlegend = TRUE
				) %>%
				colorbar(title = "Indice de l'équipement") 
			})
		})
	}) 
		
	output$KPI_hectare <- renderPlotly({
		surface_totale_disponible <- gisement$surface[gisement$validation_communes %in% c('1. Validé','2. Difficile')] %>% sum
		creation_KPI_hectares(surface_totale_disponible/10000)
	}) 

	output$Barplot_ha_par_commune <- renderPlot({ 
		repartition_gisement_disponible_commune(gisement_input = gisement)
	})

	output$pdf_viewer <- renderUI({
		selected_pdf <- glue("pdf/{input$pdf_select}.pdf#navpanes=0")
		tags$iframe(style = "width:100%; height:800px;", src = selected_pdf)
	})

}





