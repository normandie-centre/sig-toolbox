
# Pourquoi une cartographie ?<br><br>

La cartographie répond à l’objectif de représenter de manière intuitive le recensement des travaux et des aménagements de mise en accessibilité des sites, accueillant les épreuves olympiques et paralympiques, et de leurs abords. Certains sites d’entraînement ont également été inclus dans le recensement car des données fiables étaient disponibles, en particulier pour ceux supervisés par la Solidéo.  


# Points de vigilance<br><br>

- Le recensement des sites et des travaux effectués ne saurait être exhaustif face à la spécificité du sujet et au nombre important de sites potentiels.
- Le recensement des travaux et des aménagements de mise en accessibilité des établissements ne prend pas en compte l’accessibilité initiale du site. Certains établissements étaient déjà accessibles avant 2018 et n’ont donc pas fait l’objet de travaux spécifiques.
- Ce recensement n’est pas une description de l’accessibilité de l’établissement : la réalisation ou non de travaux ne présage pas de la conformité de l’établissement aux réglementations en vigueur. 
- Les établissements neufs sont supposés être conformes aux réglementations accessibilité en vigueur lors du dépôt de permis de construire.
- L’année 2018 est prise comme référence pour le début des travaux en lien avec l’accessibilité (concomitance avec la création de la Solidéo). C’est-à-dire que seules les réalisations datant de 2018 ou après sont répertoriées sur cette carte.
