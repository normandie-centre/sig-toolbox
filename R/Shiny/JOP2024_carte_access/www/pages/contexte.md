
# Présentation générale de l’étude<br><br>

Dans le cadre des Jeux Olympiques et Paralympiques (JOP) de Paris 2024, l’État a lancé un programme d’évaluation le 9 février 2021. Ce programme est composé de 13 études visant à évaluer l’impact de l’organisation des JOP sur une série de politiques publiques.  
Plus particulièrement, une étude sur l’impact des JOP sur l’accessibilité a été lancée. Elle est portée par la DIJOP (délégation interministérielle aux JOP) et la DGALN (direction générale de l’aménagement, le logement et la nature). Cette étude a pour but de mesurer « l’évolution de l’accessibilité des sites olympiques et de leur environnement immédiat aux personnes en situation de handicap ».  
L’accessibilité universelle des sites et des équipements est un sujet complexe et large qui nécessite d’étudier l’intégralité de la chaîne de déplacement (logement, espaces extérieurs, transport, bâtiment recevant du public). Chaque site et équipement, et plus généralement leur environnement immédiat, doit répondre aux normes et aux réglementations en vigueur, pour les quatre familles de handicap et plus largement pour les personnes à mobilité réduite ou en situation de handicap (parents avec poussette, enfants, personnes portant de lourdes charges, personnes âgées, étrangères, distraites…).  Le Cerema a été choisi pour réaliser cette évaluation qui se déroulera jusqu’en 2026.  


>**L’étude d’impact devra répondre à 3 questions :**  
>- Quelles sont les réalisations (travaux et aménagements) qui ont été mises en œuvre pour rendre accessibles les sites et les équipements accueillant les JOP ? Et quelles sont celles qui restent en phase héritage ?  
>- Ces réalisations sont-elles innovantes/performantes, qualitatives, reproductibles et exemplaires ?  
>- Les JOP de Paris 2024 ont-ils fait progresser l’accessibilité ? Si oui, dans quelle mesure cette progression est-elle plus qualitative et plus rapide ?  



---