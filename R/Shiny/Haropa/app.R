suppressMessages(library(dplyr))
suppressMessages(library(geojsonsf))
# suppressMessages(library(googlesheets4))
suppressMessages(library(leaflet))
suppressMessages(library(htmlwidgets))
# suppressMessages(library(rgdal))
options("rgdal_show_exportToProj4_warnings" = "none")
suppressMessages(library(sf))
suppressMessages(library(shiny))


#### Paramètres ####
# gs4_auth(cache=".secrets", email="adl.dternc@cerema.fr")
# sheet_id = "https://docs.google.com/spreadsheets/d/1pQKGzettkRgjO0EXSWqHNT2GiEatq97tGrQLwdu9wPU/"


#### Functions ####
prettifyNumber <- function(number, decimal) {
  return(number %>% as.numeric() %>% round(decimal) %>% prettyNum(big.mark = " ", scientific = F))
}

loadData <- function() {
  read_sheet(sheet_id)
}

saveData <- function(data) {
  data <- data %>% as.list() %>% data.frame()
  sheet_append(sheet_id, data)
}

deleteRow <- function(line) {
  range_delete(sheet_id, range = as.character(line))
}

getLastState <- function(id) {
  return(loadData() %>% filter(fid == id) %>% .$vrai_artif)
}


#### Input data ####
# ff_in_haropa <- readOGR(
#   dsn = "www/data/ff_in_haropa.geojson",
#   use_iconv = TRUE,
#   encoding = "UTF-8"
# ) %>% st_as_sf()
# ff <- readOGR(
#   dsn = "www/data/conso_espaces.geojson",
#   use_iconv = TRUE,
#   encoding = "UTF-8"
# ) %>% st_as_sf()
# domaine <- readOGR(
#   dsn = "www/data/domaine_haropa.geojson",
#   use_iconv = TRUE,
#   encoding = "UTF-8"
# ) %>% st_as_sf()

ff_in_haropa <- geojson_sf("www/data/ff_in_haropa.geojson")
ff <- geojson_sf("www/data/conso_espaces.geojson")
domaine <- geojson_sf("www/data/domaine_haropa.geojson")
ff$area <- st_area(ff)
ff$conso <- paste(ff$n0, "=>", ff$n1)
ff$vrai_artif <- NULL


pal_haropa <- colorFactor(
  palette = c("brown", "blue", "orange"),
  domain = c("Domaine du port du Havre", "Domaine du port de Rouen", "Domaine du port de Paris")
)
pal_conso <- colorFactor(
  palette = c("#30123b", "#455bcd", "#3e9cfe", "#18d7cb", "#48f882", "#a4fc3c", "#e2dc38", "#fea331", "#ef5911", "#c22403", "#7a0403"),
  domain = c("2009 => 2011", "2011 => 2012", "2012 => 2013", "2013 => 2014", "2014 => 2015", "2015 => 2016", "2016 => 2017", "2017 => 2018", "2018 => 2019", "2019 => 2020", "2020 => 2021")
)

ff_in_haropa$popup <- sprintf(
  "TUP : %s<br><br>
  crgnumdtxt : %s<br>
  dcntarti : %s<br>
  dcntnaf : %s<br><br>
  <b>NAF</b><br>
  TERRES : %s<br>
  PRES : %s<br>
  VERGERS : %s<br>
  VIGNES : %s<br>
  EAUX : %s<br>
  BOIS : %s<br>
  LANDES : %s<br><br>
  <b>Artificialisé</b><br>
  CARRIERES : %s<br>
  JARDINS : %s<br>
  TERRAINS A BATIR : %s<br>
  TERRAINS D AGREMENT : %s<br>
  CHEMIN DE FER : %s<br>
  SOL : %s<br>",
  ff_in_haropa$idtup, ff_in_haropa$cgrnumdtxt, ff_in_haropa$dcntarti, ff_in_haropa$dcntnaf, ff_in_haropa$dcnt01,ff_in_haropa$dcnt02,ff_in_haropa$dcnt03,ff_in_haropa$dcnt04,ff_in_haropa$dcnt08,ff_in_haropa$dcnt05,ff_in_haropa$dcnt06,ff_in_haropa$dcnt07,ff_in_haropa$dcnt09,ff_in_haropa$dcnt10,ff_in_haropa$dcnt11,ff_in_haropa$dcnt12,ff_in_haropa$dcnt13
) %>% lapply(htmltools::HTML)

ff$popup <- sprintf(
  "
    <h5>Artificialisation %s => %s</h5>
    Surface de la TUP : %s m² soit %s ha<br>
    Surface artificialisée : <b>%s m² soit %s ha</b>
    <ul><li>à usage d'habitat : %s m²<br></li>
    <li>à usage d'activité : %s m²<br></li>
    <li>à usage mixte: %s m²<br></li>
    <li>à usage non connu: %s m²<br></li></ul>
  ",
  ff$n0, ff$n1, ff$area %>% prettifyNumber(0), round(ff$area/10000) %>% prettifyNumber(0), round(ff$naf_arti) %>% prettifyNumber(0), round(ff$naf_arti/10000) %>% prettifyNumber(0), round(ff$arti_hab) %>% prettifyNumber(0), round(ff$arti_act) %>% prettifyNumber(0), round(ff$arti_mix) %>% prettifyNumber(0), round(ff$arti_nc) %>% prettifyNumber(0)
) %>% lapply(htmltools::HTML)


##########
##  UI  ##
##########
ui <- fluidPage(

  # Chargement du CSS
  tags$head(tags$link(rel = "stylesheet", type = "text/css", href = "css/default.css")),
  tags$head(tags$script(src = "https://cdn.rawgit.com/digidem/leaflet-side-by-side/gh-pages/leaflet-side-by-side.min.js")),   # Ajout de la librairie js de slider leaflet

  # Création de la layout
  navbarPage("Haropa - Objectif ZAN", id = "main",
    tabPanel("Carte",
      div(class = "outer",
        leafletOutput(
          outputId = "mymap",
          height = "100%",
          width = "100%"
        ),
        # absolutePanel(
        #   id = "controls",
        #   class = "panel panel-default",
        #   fixed = TRUE,
        #   draggable = TRUE,
        #   top = 300, left = "auto", right = 20, bottom = "auto",
        #   width = 150, height = "auto",
        #   h5(textOutput("ff_actif")),
        #   # br(),
        #   radioButtons(
        #     "vrai_pos",
        #     "Vrai positif ?",
        #     choices = c("Oui", "Non"),
        #     selected = character(0),
        #   ),
        #   textOutput("radio"),
        # ),
      ),
    ),
  )
)


##############
##  Server  ##
##############
server <- function(input, output, session) {

  output$mymap <- renderLeaflet({
    leaflet() %>%
    addProviderTiles(providers$Stadia.AlidadeSmoothDark, group = "Dark") %>%
    addProviderTiles(providers$CartoDB.Positron, group = "Positron") %>%
    # addProviderTiles(providers$GeoportailFrance.orthos, group = "Orthophoto") %>%
    # addProviderTiles(providers$GeoportailFrance.parcels, group = "Parcelles") %>%
    addTiles(group = 'OSM') %>%
    
    setView(lng = 1, lat = 49, zoom = 9) %>%

    addPolygons(
      data = domaine,
      group = "Domaine Haropa",
      stroke = TRUE,
      weight = 2,
      # color = ~pal_haropa(nom),
      fillOpacity = 0.2,
      color = ~pal_haropa(nom),
      label = domaine$nom
    ) %>%

    addPolygons(
      layerId = ~fid,
      data = ff,
      group = "Artificialisation",
      stroke = TRUE,
      weight = 1,
      color = "black",
      fillOpacity = 0.5,
      fillColor = ~pal_conso(conso),
      label = paste0(round(ff$naf_arti/10000), " ha entre ", ff$n0, " et ", ff$n1),
      popup = ff$popup
    ) %>%

    addPolygons(
      data = ff_in_haropa,
      group = "Fichiers Fonciers 2021",
      stroke = TRUE,
      weight = 0.5,
      color = "black",
      fillOpacity = 1,
      fillColor = "grey",
      label = paste0("Type de sol : ", ff_in_haropa$cgrnumdtxt),
      popup = ff_in_haropa$popup
    ) %>%

    addLegend(
      layerId = "legend_ff",
      group = "Artificialisation",
      title = "Artificialisation",
      position = "bottomright",
      pal = pal_conso,
      opacity = 0.7,
      values = ff$conso
    ) %>%

    addLegend(
      # layerId = "legend_ff",
      group = "Domaine Haropa",
      title = "Domaine Haropa",
      position = "bottomright",
      pal = pal_haropa,
      opacity = 0.7,
      values = domaine$nom
    ) %>%

    ### LayersControl
    addLayersControl(
      overlayGroups = c("Domaine Haropa", "Fichiers Fonciers 2021", "Artificialisation"),
      baseGroups = c("Positron","Dark", "OSM"),
      position = "topright"
    ) %>%

    hideGroup(c("Fichiers Fonciers 2021", "Artificialisation"))

  })

  ## On récupère l'état actuel et on met le slider
  observe({
    req(input$mymap_shape_click$group == "Artificialisation")
    this_ff <- ff %>% filter(fid == input$mymap_shape_click$id)

    output$ff_actif <- renderText(paste("ID :", this_ff$fid))
    updateRadioButtons(session, "vrai_pos", choices  = c("Oui","Non"), selected = getLastState(this_ff$fid))
  }) %>% bindEvent(input$mymap_shape_click)

  # On écrit dans le googlesheet lors du changement du radiobutton
  observe({
    req(input$vrai_pos)
    this_ff <- ff %>% filter(fid == input$mymap_shape_click$id)

    if(any(loadData()$fid == this_ff$fid)) deleteRow(which(loadData()$fid == this_ff$fid)+1)    # on supprime l'enregistrement s'il existe (+1 pour le header)
    df <- data.frame(c(this_ff$fid), c(input$vrai_pos))
    saveData(df)
  }) %>% bindEvent(input$vrai_pos)
}

shinyApp(ui, server)