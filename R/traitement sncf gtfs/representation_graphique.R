# Charger les packages nécessaires
library(ggplot2)
library(dplyr)
library(lubridate)
library(plotly)

# Fonction pour créer le graphique avec filtrage par code_uic et plage de dates
plot_cumulative_by_code_uic_and_date <- function(code_uic) {
  # Convertir les dates en objets Date

  # Filtrer les données pour le code_uic spécifié et dans la plage de dates
  data <- nb_desserte_serie %>%mutate(date = as.Date(ymd(date))) %>% 
                                        mutate(type_train_refactor = 
                                                 case_when(type_train %in% c('Tramtrain','TramTrain') ~ 'TramTrain',
                                                           type_train %in% c('TGV INOUI','TGV','OUIGO','Lyria','Eurostar','ICE') ~ 'TAGV',
                                                           type_train %in% c('Noctilien','INTERCITES DE NUIT','Corail Lunéa') ~ 'Train de nuit',
                                                           type_train %in% c('Car à réservation','C','Car TER') ~ 'Car',
                                                           type_train %in% c('Corail Intercité','Intercités Eco','INTERCITES') ~ 'Intercités',
                                                           type_train %in% c('Train','Navette') ~ 'Train indéterminé',
                                               .default = type_train))
  
  start_date <- min(data$date)
  end_date <- max(data$date)
  
  filtered_data <- data %>%
    filter(code_uic == code_uic,
           date >= start_date,
           date <= end_date) %>%
    group_by(date, type_train_refactor) %>%
    summarize(compte = sum(compte), .groups = 'drop') %>%
    arrange(date)
  
  # Si les données sont manquantes pour certaines dates, ajouter des zéros
  complete_data <- filtered_data   # %>%
    # tidyr::complete(date = seq.Date(min(date), max(date), by = "day"),
    #                 type_train = unique(filtered_data$type_train),
    #                 fill = list(compte = 0)) %>%
    # arrange(date, type_train)
  
  complete_data <- complete_data %>%
    mutate(date_factor = factor(format(date, "%Y-%m-%d")))
  
  # Créer le graphique empilé
  fig <- complete_data %>%
    plot_ly(x = ~date_factor, y = ~compte, color = ~type_train_refactor, colors = "Set1", type = 'scatter', mode = 'lines', stackgroup='one') %>%
    layout(
      title = paste("Courbes Empilées pour Code UIC:", code_uic),
      xaxis = list(title = "Date", tickangle = 45),
      yaxis = list(title = "Nombre de Trains"),
      showlegend = TRUE
    )
  
  return(fig)
}

plot_avg_train_by_code_uic_and_day_in_week <- function(code_uic_input,day_input) # annee en format aaaa, day en anglais (monday, tuesday...)
  {
  # Convertir les dates en objets Date
  
  
  # Filtrer les données pour le code_uic spécifié 
  data <- nb_desserte_serie %>% mutate(annee = substr(date,1,4)) %>% 
    mutate(type_train_refactor = 
             case_when(type_train %in% c('Tramtrain','TramTrain') ~ 'TramTrain',
                       type_train %in% c('TGV INOUI','TGV','OUIGO','Lyria','Eurostar','ICE') ~ 'TAGV',
                       type_train %in% c('Noctilien','INTERCITES DE NUIT','Corail Lunéa') ~ 'Train de nuit',
                       type_train %in% c('Car à réservation','C','Car TER') ~ 'Car',
                       type_train %in% c('Corail Intercité','Intercités Eco','INTERCITES') ~ 'Intercités',
                       type_train %in% c('Train','Navette') ~ 'Train indéterminé',
           type_train %in% c('Train TER') ~ 'Train TER',           
           .default = type_train))
  
  filtered_data <- data %>%
    filter(code_uic == code_uic_input & jour == day_input)  %>%
    group_by(annee, type_train_refactor) %>%
    summarize(compte = mean(compte), .groups = 'drop') %>%
    arrange(annee)
  
  # Créer le graphique empilé
  fig <- filtered_data %>%
    plot_ly(x = ~annee, y = ~compte, color = ~type_train_refactor, colors = "Set1", type = 'scatter', mode = 'lines', stackgroup='one') %>%
    layout(
      title = paste0("Dessertes pour la gare ", code_uic, ", le jour ",day),
      xaxis = list(title = "Année", tickangle = 45),
      yaxis = list(title = "Nombre de Trains"),
      showlegend = TRUE
    )
  
  return(fig)
}
