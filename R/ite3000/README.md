# ITE3000

Ce répertoire est le code du script permettant de mettre en forme les réponses du questionnaire 46331 pour l'enquête ITE3000.
Le fichier source, c'est à dire le questionnaire de sortie de limesurvey, doit être stockée dans /data/results-survey46331.csv.
Les fichiers de sortie est en format geojson, stocké dans le dossier /prod/

Deux fichiers de sortie sont produits : l'un avec les champs correspondant aux questions, l'autre avec des champs abrégés.