function [heure_deb, heure_fin] = plage_Hsoleil_sup(jour, coordonnees, pas, lim_inf) % lim_inf = 15°
	heure_ok = [];  % la liste va grandir en fonction de l'importance de la plage horaire
	for heur = 2:22 % boucle pour les heures
		for minu = 0:pas:45  % pour les minutes
			% calcul de la position du soleil, dont la hauteur
			[~, hauteur, ~]=calcul_position_soleil(jour, [heur minu], coordonnees);
			if hauteur>=lim_inf
				heure_ok = [heure_ok ; heur minu] ; % ajout de l'heure qd H est ok
			end
		end
	end
	% export des informations souhaitees :
 	heure_deb = heure_ok(1,:);			heure_fin = heure_ok(end,:);