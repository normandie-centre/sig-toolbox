SELECT fsr.*,
    fsr.srvname AS foreign_server_name,
    fsr.srvtype AS foreign_server_type,
    fsr.srvversion AS foreign_server_version,
    fsr.srvoptions AS foreign_server_options,
    fsr.srvowner AS foreign_server_owner,
    fsr.srvfdw AS foreign_server_fdw
FROM
    pg_foreign_server fsr
ORDER BY
    fsr.srvname;
	
SELECT *
FROM information_schema.foreign_servers;

select 
    srvname as name, 
    srvowner::regrole as owner, 
    fdwname as wrapper, 
    srvoptions as options
from pg_foreign_server
	join pg_foreign_data_wrapper w on w.oid = srvfdw
;

SELECT
    w.fdwname as wrapper, 		
    fsr.srvowner::regrole as owner, 
    current_database() AS database_name,	
	fsr.srvname AS foreign_server_name,
    --fsr.srvtype AS foreign_server_type,
    --fsr.srvversion AS foreign_server_version,
    --fsr.srvoptions AS foreign_server_options,
	regexp_replace( (fsr.srvoptions)[1], '.*dbname=(.*?)(,|$).*', '\1') as wrapped_database,
    --fsr.srvowner AS foreign_server_owner,
    --fsr.srvfdw AS foreign_server_fdw,	
	--ftr.ftserver,
    --ftr.ftrelid AS foreign_table_id,
    ns.nspname AS schema_name_in_foreign_server,
    pc.relname AS foreign_table_name	
	--,ftr.*, fsr.*, ns.*, pc.*, w.*
FROM
    pg_foreign_server fsr
	join pg_foreign_data_wrapper w on w.oid = fsr.srvfdw
	join pg_foreign_table ftr on fsr.oid = ftr.ftserver
    JOIN pg_class pc ON ftr.ftrelid = pc.oid
    JOIN pg_namespace ns ON pc.relnamespace = ns.oid
ORDER BY
    fsr.srvname, ns.nspname, pc.relname;
-- pour mémoire comment j'ai créé le foreign serveur :
CREATE EXTENSION IF NOT EXISTS postgres_fdw;

-- On crée le "foreign server" qui est une connexion
CREATE SERVER f_rdi
    FOREIGN DATA WRAPPER postgres_fdw
    OPTIONS (dbname 'rdi');

ALTER SERVER f_rdi
    OWNER TO postgres;

-- on map un utilisateur	
CREATE USER MAPPING FOR "postgres" SERVER f_rdi
OPTIONS ("user" 'postgres', password 'petitétronfumant');

-- on créé le schéma qui va accueillir
create schema f_enjeux;

-- puis on import les tables qui nous interessent, elles seront dispo dans foreign tables (et pas tables attention !)
IMPORT FOREIGN SCHEMA enjeux
FROM SERVER f_rdi INTO f_enjeux;
