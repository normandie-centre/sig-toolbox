// fichier main.js
// pour lancer l'application npm start dans le terminal en étant bien dans pg linker et pas juste dans sig toolbox !

const express = require("express");
const app = express();
const session = require("express-session");
const bodyParser = require("body-parser");

const { DatabaseManager } = require("./ClassManager");
const { getDataDBTree, getDataWorkDB } = require("./getDataDB");

const port = process.env.SERVERPORT || 3000;

app.use(express.static("public"));
// la façon dont on décode ce qu'on passe du client (form, fetch etc...)
app.use(bodyParser.urlencoded({ extended: true }));

var expiryDate = new Date(Date.now() + 2 * 60 * 60 * 1000); // 1 hour
app.use(
	session({
		secret: "ChoubiDou76!", // Changez cela avec une clé secrète sécurisée
		resave: false,
		saveUninitialized: true,
		cookie: { secure: false },
		expires: expiryDate,
	})
);
// ACCUEIL
app.get("/", (req, res) => {
	res.sendFile(__dirname + "/index.html");
});

// SUBMIT FORM LOGIN
app.post("/login", async (req, res) => {
	const {
		distant_host,
		distant_port,
		distant_default_database,
		distant_username,
		distant_password,
		work_host,
		work_port,
		work_default_database,
		work_username,
		work_password,
	} = req.body;
	console.log(req.body);

	try {
		const localServerManager = new DatabaseManager(
			distant_host,
			distant_port,
			distant_default_database,
			distant_username,
			distant_password
		);
		// Effectuez d'autres vérifications si nécessaire
		req.session.distant_host = req.body.distant_host;
		req.session.distant_port = req.body.distant_port;
		req.session.distant_default_database = req.body.distant_default_database;
		req.session.distant_username = req.body.distant_username;
		req.session.distant_password = req.body.distant_password;
		await localServerManager.connect();

		const workServerManager = new DatabaseManager(
			work_host,
			work_port,
			work_default_database,
			work_username,
			work_password
		);
		req.session.work_host = req.body.work_host;
		req.session.work_port = req.body.work_port;
		req.session.work_default_database = req.body.work_default_database;
		req.session.work_username = req.body.work_username;
		req.session.work_password = req.body.work_password;
		await workServerManager.connect();

		// Si tout est OK, redirigez vers la page linker.html
		res.redirect("/linker.html");

		// N'oubliez pas de déconnecter le serveur après utilisation
		await localServerManager.disconnect();
		await workServerManager.disconnect();
	} catch (error) {
		console.error("Erreur de connexion (accueil) à PostgreSQL:", error);
		// Affichez une alerte avec le message d'erreur
		res.send(
			`<script>alert("Erreur de connexion (accueil) à PostgreSQL : ${error}. Veuillez vérifier vos informations d\'identification."); window.location.href = "/";</script>`
		);
	}
});

// RENVOYER LE JSON DU DBTREE
app.get("/getDBTree", async (req, res) => {
	try {
		const dbTree = await getDataDBTree(
			req.session.distant_host,
			req.session.distant_port,
			req.session.distant_default_database,
			req.session.distant_username,
			req.session.distant_password
		);
		res.json(dbTree);
	} catch (error) {
		console.error(error);
		res.status(500).json({
			error: "Une erreur s'est produite lors de la récupération des données.",
		});
	}
});

// RENVOYER LE JSON DES WORK DB (work c'est pour dire là où on veut travailler)
app.get("/getWorkDB", async (req, res) => {
	try {
		const dbTree = await getDataWorkDB(
			req.session.work_host,
			req.session.work_port,
			req.session.work_default_database,
			req.session.work_username,
			req.session.work_password
		);
		//console.log(dbTree)
		res.json(dbTree);
	} catch (error) {
		console.error(error);
		res.status(500).json({
			error: "Une erreur s'est produite lors de la récupération des données.",
		});
	}
});

// Définissez un chemin pour récupérer les données de session
app.get("/getSessionData", function (req, res) {
	// Récupérez les données de session de req.session
	const sessionData = {
		distant_host: req.session.distant_host,
		distant_port: req.session.distant_port,
		distant_username: req.session.distant_username,
		distant_password: req.session.distant_password,
		work_host: req.session.work_host,
		work_port: req.session.work_port,
		work_username: req.session.work_username,
		work_password: req.session.work_password,
	};
	res.json(sessionData);
});

app.post("/setForeignData", async function (req, res) {
    try {
        //const sessionData = req.body.sessionData;
        const workDB = req.body.workDB;
		const db = req.body.db;
		const s = req.body.s;
        const t = req.body.t;

        //console.log('coucou work in progress', db, s, t);
		//console.log(req.session.distant_host);

		const connexion = new DatabaseManager(
			req.session.work_host,
			req.session.work_port,
			workDB,
			req.session.work_username,
			req.session.work_password
		);
		await connexion.connect();
		const msg = await connexion.setForeignData(
			db,
			s,
			t,
			req.session.distant_host,
			req.session.distant_port,
			req.session.distant_username,
			req.session.distant_password
			);
		await connexion.disconnect();
        res.json({ msg: msg }); // Envoi du message en tant que réponse JSON
        // Faites quelque chose avec param1 et param2
    } catch (error) {
		const msg = `Erreur : ${error.message}`;
		res.json({ msg: msg}); // Envoi du message en tant que réponse JSON
		/*
        res.status(500).json({
            error: error.message, // Afficher le message d'erreur complet
        });
		*/
    }
});

app.listen(port, () => {
	console.log(`Serveur en cours d'exécution sur le port ${port}`);
});
