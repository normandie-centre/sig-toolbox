const { Client } = require("pg");
const dotenv = require("dotenv");
dotenv.config();

// La classe principale pour communiquer avec notre base de données !
class DatabaseManager {
	constructor(host, port, databaseName, username, password) {
		//console.log(host, port, databaseName, username, password);
		this.host = host;
		this.port = port;
		this.databaseName = databaseName;
		this.username = username;
		this.password = password;
		/*
		this.client = new Client({
			host: host,
			port: port,
			database: databaseName,
			user: username,
			password: password			
		});
		*/
		// Construire l'URL de connexion
		// Paramètres qui fonctionnent pour localhost		
		
		const connectionString = `postgres://${username}:${password}@${host}:${port}/${databaseName}`;

		// Initialiser le client avec l'URL de connexion
		this.client = new Client({
			connectionString: connectionString,
			
		});
		
		// paramètres qui ne marchent pour le nouveau serveur...
		/*
		const connectionString = `postgres://${username}:${password}@${host}:${port}/${databaseName}?sslmode=require`;
		// Initialiser le client avec l'URL de connexion
		this.client = new Client({
			connectionString: connectionString,
			ssl: {
				rejectUnauthorized: false // Désactive la vérification du certificat
			}
		});
		*/
	}

	async connect() {
		try {
			await this.client.connect();
			console.log("Connecté à PostgreSQL");
		} catch (error) {
			console.error("Erreur de connexion SERVER/BDD à PostgreSQL:", error);
			throw error;
		}
	}

	async disconnect() {
		try {
			await this.client.end();
			console.log("Déconnecté de PostgreSQL");
		} catch (error) {
			console.error("Erreur de déconnexion SERVER/BDD de PostgreSQL:", error);
			throw error;
		}
	}

	async listDatabases() {
		const query =
			"SELECT datname FROM pg_database WHERE datname NOT IN ('template0', 'template1') order by datname";

		try {
			const result = await this.client.query(query);
			return result.rows.map((row) => row.datname);
		} catch (error) {
			console.error(
				"Erreur lors de la récupération de la liste des bases de données:",
				error
			);
			throw error;
		}
	}

	async listSchemas() {
		const query = `SELECT schema_name FROM information_schema.schemata
						where schema_name not in ('information_schema','pg_catalog','pg_toast')
						`;
		try {
			const result = await this.client.query(query);
			return result.rows.map((row) => row.schema_name);
		} catch (error) {
			console.error("Error listing schemas:", error);
			throw error;
		}
	}

	async listTables(schemaName) {
		const query = ` SELECT table_name, table_type
					FROM information_schema.tables
					WHERE table_schema = $1
					order by 2,1
					`;

		try {
			const result = await this.client.query(query, [schemaName]);
			return result.rows.map((row) => ({
				tableName: row.table_name,
				tableType: row.table_type
			})
			);
		} catch (error) {
			console.error("Error listing tables:", error);
			throw error;
		}
	}

	async setForeignData(db, s, t = null, distant_host, distant_port, distant_username, distant_password) {
		// A voir si il ne faut pas vérifier qu'il n'y a pas de ; dans ces variables pour l'injection de sql ? ? ?
		// et comment récupérer les messages de postgres ? Notamment si ça se passe pas bien
		let limitTo = "";
		if (typeof t != 'undefined' && t !== null && t != 'undefined') {
			limitTo = `limit to (${t})`;
		}

		const query1 = ` CREATE EXTENSION IF NOT EXISTS postgres_fdw;`;
		const query2 = ` CREATE SERVER IF NOT EXISTS fdw_${db}_server 
		 					FOREIGN DATA WRAPPER postgres_fdw 
		 					OPTIONS (host '${distant_host}', port '${distant_port}', dbname '${db}');`;
		const query3 = ` ALTER SERVER fdw_${db}_server OWNER TO "${this.username}";`;
		const query4 = ` CREATE USER MAPPING IF NOT EXISTS FOR "${this.username}" SERVER fdw_${db}_server 
						OPTIONS ("user" '${distant_username}', password '${distant_password}');`;
		const query5 = ` CREATE SCHEMA IF NOT EXISTS fdw_${db}_${s};`;
		const query6 = `IMPORT FOREIGN SCHEMA ${s} 
		 				${limitTo} 
						FROM SERVER fdw_${db}_server INTO fdw_${db}_${s};`;

		// console.log(query1);
		// console.log(query2);
		// console.log(query3);
		// console.log(query4);
		// console.log(query5);
		// console.log(query6);
		try {
			await this.client.query(query1);
			await this.client.query(query2);
			await this.client.query(query3);
			await this.client.query(query4);
			await this.client.query(query5);
			await this.client.query(query6);
			//console.log(db,s, limitTo, distant_host, distant_port, distant_username, distant_password, this.username)
			const msg = `Success : Le serveur distant fdw_${db}_server a été créé et les données (${s} ${limitTo}) ont été importées dans les foreign tables du schéma fdw_${db}_${s}.`;
			return (msg);

		} catch (error) {
			console.error("setForeignData a échoué :", error);
			throw error;
		}
	}
}

module.exports = { DatabaseManager };
