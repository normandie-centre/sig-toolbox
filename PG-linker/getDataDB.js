// fichier dataGenerator.js

const { DatabaseManager } = require("./ClassManager");

// A voir si ça pourrait pas être des méthodes directement dans ClassManager
async function getDataDBTree(host, port, database, username, password) {
    const serverManager = new DatabaseManager(host, port, database, username, password);
    await serverManager.connect();

    const databases = await serverManager.listDatabases();
    const dbTree = [];

    for (const databaseName of databases) {
        //console.info(typeof password);

        const databaseManager = new DatabaseManager(host, port, databaseName, username, password);
        await databaseManager.connect();

        const schemas = await databaseManager.listSchemas();

        const databaseObject = {
            database: databaseName,
            schemas: [],
        };

        for (const schemaName of schemas) {
            const tables = await databaseManager.listTables(schemaName);
            //console.log(tables);
            const schemaObject = {
                schema: schemaName,
                tables: tables,
            };
            databaseObject.schemas.push(schemaObject);
        }

        dbTree.push(databaseObject);

        await databaseManager.disconnect();
    }

    await serverManager.disconnect();
    return dbTree;
}

// A voir pour cette fonction, on pourrait l'avoir déjà les classes de bases
async function getDataWorkDB(host, port, database, username, password) {
    const serverManager = new DatabaseManager(host, port, database, username, password);
    await serverManager.connect();

    const databases = await serverManager.listDatabases();
   
    await serverManager.disconnect();
    //console.log(databases);
    return databases;
}

module.exports = { getDataDBTree, getDataWorkDB};
