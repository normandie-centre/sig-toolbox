# Carte interactive des IPS de la Métropole Rouen Normandie
<p align="center">
    <a href="https://github.com/aatomium/ips_mrn/graphs/contributors" alt="Contributors">
        <img src="https://img.shields.io/github/contributors/aatomium/ips_mrn" /></a>
    <a href="https://github.com/aatomium/ips_mrn/pulse" alt="Activity">
        <img src="https://img.shields.io/github/commit-activity/m/aatomium/ips_mrn" /></a>

Une cartographie interactive, en 2D et 3D, des IPS (Indice de Position Sociale) des établissements scolaires de la Métropole Rouen Normandie.

Accessible sur https://ips-mrn.onrender.com.


## Installation 

Pour installer les packages nécéssaires, 
```
pip install -r requirements.txt
```

## Utilisation

Pour lancer le serveur : 
```
python3 main.py
```

## Remerciements

Merci à Thomas Escrihuela et à toute l'équipe du CEREMA pour leur aide ;)