########################################################################################################################
# Nettoyage de données Avifaune                                            29 juillet 2022 - Cerema NC / DLAB / MEL / SB
########################################################################################################################
# Développé sur
#   PyCharm 2022.1.4 (Community Edition)
#   Build #PC-221.6008.17, built on July 20, 2022
#   Runtime version: 11.0.15+10-b2043.56 amd64
#   VM: OpenJDK 64-Bit Server VM by JetBrains s.r.o.
#   Windows 10 10.0   |   Memory: 2048M   |   Cores: 16
########################################################################################################################
# Etapes :
# récupérer le nom du fichier shp ou csv à traiter
# sélectionner les champs à conserver et y ajoutant les 3 champs prévus
# exporter, sous la forme du format d'origine, la données avec les champs voulus
########################################################################################################################

# _______________________________________________________________________________________________________________________
# BIBLIOTHEQUES ........................................................................................................
# -----------------------------------------------------------------------------------------------------------------------
from math      import floor
from time      import time
from datetime  import datetime
from tkinter   import filedialog as fd
from tkinter   import ttk
from tkinter   import *
import tkinter.messagebox
import geopandas as gpd   # --------------> /!\ a nécessité l'installation directement depuis "environnement" d'anaconda
import pandas    as pd
import tkinter.font as font
import ctypes

# initialisation du chonomètre :
start = time()
# ¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
# pour la transformation en .exe, execution dans "CMD.exe Prompt 0.1.1 d'Anaconda des commandes suivantes : ¤¤¤¤¤¤¤¤¤¤¤¤
# ¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
# pip install pyinstaller ¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
# cd C:\Users\sebastien.bouland\PycharmProjects\avifaune ¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
# pyinstaller --onefile avifaune_final_20220729.py ¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
# ¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
# Lorsque vous exécutez cette commande, le processus de conversion du fichier Python en exe se déclenche et créer ¤¤¤¤¤¤
# 3 répertoires dans le répertoir du .py. Le répertoire avec le nom dist contient votre fichier exe. ¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
# ¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤

# _______________________________________________________________________________________________________________________
## PARAMETRAGE FENETRES ................................................................................................
# -----------------------------------------------------------------------------------------------------------------------
fen_larg = 680
fen_haut = 430
user32 = ctypes.windll.user32
posX_fen = floor((user32.GetSystemMetrics(0) - fen_larg) / 2)
posY_fen = floor((user32.GetSystemMetrics(1) - fen_haut) / 2)
screen = str(fen_larg) + 'x' + str(fen_haut) + '+' + str(posX_fen) + '+' + str(posY_fen)

# _______________________________________________________________________________________________________________________
# Variables globales ...................................................................................................
# -----------------------------------------------------------------------------------------------------------------------
name_file = ""
select_list = []
select_list_brute = []
datt = datetime.today().strftime('%Y%m%d%H%M%S')

# _______________________________________________________________________________________________________________________
## FONCTIONS ...........................................................................................................
# -----------------------------------------------------------------------------------------------------------------------
def select_fichier():
    global name_file, frame_file, txt_file
    filetypes = [('CSV / SHP', '.csv .CSV .shp .SHP')]
    name_file = fd.askopenfilename(
        parent=frame_file,
        title='Sélectionner le fichier à nettoyer : ',
        initialdir='/',
        filetypes=filetypes)
    txt_file.set(name_file)

def just_name(chemin_file):
    try:
        index = chemin_file.rindex('/') + 1
    except ValueError:
        index = chemin_file.rindex("\\") + 1
    return chemin_file[index:]

def quitter_fenetre_selection():
    global fenetre_selection, name_file
    if len(name_file) == 0:
        print("Fichier non sélectionné !!! ")
    else :
        print("Fichier sélectionné ! ")
    fenetre_selection.destroy()

def liste_champs():
    global champsvar, name_file, gdf2
    extension = name_file[-3:]
    # print("Récupération des noms des champs du fichier " + name_file + ",\nayant pour extension : " + extension)
    if extension.lower() == "csv":  # cas des CSV .......................................................
        df = pd.read_csv(name_file, delimiter=",", encoding='utf-8')
        champsvar = df.columns.values.tolist()
    else:  # cas des SHP .......................................................
        #gdf2 = gpd.GeoDataFrame.from_file(name_file)
        gdf2 = gpd.GeoDataFrame.from_file(name_file)
        champsvar = gdf2.columns.tolist()

def selected_item():
    global select_list, maliste
    for i in maliste.curselection():
        select_list.append(maliste.get(i))
        # print(maliste.get(i))

def csv_modif_export(name_csv, list_chp, name_export):
    df = pd.read_csv(name_csv, delimiter=",")

    champs = df.columns.values.tolist()
    nb_chps = len(champs)

    if nb_chps == 1:
        df = pd.read_csv(name_csv, delimiter=";")

    # petit nettoyage des noms des champs
    nb_select = len(list_chp)
    for iii in range(nb_select):
        list_chp[iii] = list_chp[iii].replace("'", "")

    data_ok = pd.DataFrame()
    data_ok = df.reindex(columns=list_chp
                                 + ['Balise', 'Ref_espece', 'Type'])

    # préparation du nom du fichier
    #write = r'' + name_csv[:-4] + "_" + datt + ".csv"
    write = r'' + name_export

    # écriture du fichier
    data_ok.to_csv(write, index=False, header=True, encoding='utf-8')

def shp_modif_export(list_chp, gdf2, name_export):
    # ouverture du fichier dans un geodataframe
    #gdf2 = gpd.GeoDataFrame.from_file(name_shp)
    #gdf2 = gpd.GeoDataFrame.from_file(name_shp)

    # on conserve la géomatrie
    if not 'geometry' in list_chp:
        list_chp.append('geometry')

    gdf_final2 = gdf2.loc[:, list_chp]

    # ajout des 3 colonnes
    gdf_final2['Balise'] = ""
    gdf_final2['Ref_espece'] = ""
    gdf_final2['Type'] = ""

    # préparation du nom du fichier
    # name_export = name_shp[:-4] + "_" + datt + ".shp"

    # écriture du fichier
    gdf_final2.to_file(name_export)

def traitement_suivant_extension(name_file, liste_chp, name_export):
    extension = name_file[-3:]
    if extension == 'CSV' or extension == 'csv':
        csv_modif_export(name_file, liste_chp, name_export)
    elif extension == 'SHP' or extension == 'shp':
        shp_modif_export(liste_chp, gdf2, name_export)

def quitter_fenetre_chmp():
    global select_list, fenetre_chmp, name_file, name_export
    traitement_suivant_extension(name_file, select_list, name_export)
    fenetre_chmp.destroy()

# Fenêtres .............................................................................................................
def fenetre_select_file():
    global screen, name_file, frame_file, fenetre_selection, txt_file
    fenetre_selection = Tk()
    fenetre_selection.geometry(screen)
    fenetre_selection.title('Allègement des données avifaune (outil interne)')
    frame_file = LabelFrame(fenetre_selection, text="", padx=2, pady=2, borderwidth=2)
    frame_file.pack(fill="both", expand=1)
    frame_file.config(width=fen_larg, height=5)
    txt_file = StringVar()
    Label(frame_file, text="\n\n").pack(padx=10, pady=10)
    bb0 = ttk.Button(frame_file,
               text='\n     1- Sélectionner le fichier à traiter ...      \n',
               command=select_fichier).pack()
    Label(frame_file, text="").pack(padx=10, pady=10)
    txtfile = Label(frame_file, textvariable=txt_file)
    txtfile.config(bg="white")
    txtfile.pack(padx=10, pady=10)
    name_file = txt_file.get()

    Label(frame_file, text="\nVeuillez attendre l'affichage du chemin du fichier sélectionné avant de poursuivre\n").pack()

    f = font.Font(size=35)
    Label(frame_file, text="\n\n").pack(padx=10, pady=10)
    bb1 = ttk.Button(frame_file,
               text='\n     2- Aller à la prochaine étape :  la sélection des champs ...      \n',
               command=quitter_fenetre_selection).pack()
    fenetre_selection.mainloop()

def fenetre_select_chmp():
    global screen, select_list, fenetre_chmp, select_list_brute, maliste
    fenetre_chmp = Tk()
    fenetre_chmp.geometry(screen)
    fenetre_chmp.title('Allègement des données avifaune (outil interne)')
    frame_chmp = LabelFrame(fenetre_chmp, text=" Sélection des champs à conserver ", padx=10, pady=10,
                            borderwidth=2)
    frame_chmp.pack(fill="both", expand=1)
    frame_chmp.config(width=fen_larg, height=5)

    scrollbar = Scrollbar(frame_chmp)
    scrollbar.pack(side='right', fill='both')

    maliste = Listbox(frame_chmp, selectmode="multiple")
    maliste.pack(expand=1, fill='both')

    for each_item in range(len(champsvar)):
        l = maliste.insert(END, champsvar[each_item])
        maliste.itemconfig(each_item, bg="LightYellow" if each_item % 2 == 0 else "white")

    maliste.bind("<<ListboxSelect>>")
    maliste.config(yscrollcommand=scrollbar.set)
    scrollbar.config(command=maliste.yview)

    btn1 = Button(frame_chmp,
                  text='1- Valider ma sélection ',
                  command=selected_item)
    for l in select_list:
        print(l)
    btn2 = Button(frame_chmp,
                  text='2- Lancer le traitement ',
                  command=quitter_fenetre_chmp)

    btn1.pack(side='left')
    btn2.pack(side='right')
    maliste.pack()

    fenetre_chmp.mainloop()

########################################################################################################################
### PROGRAMME PRINCIPALE ###############################################################################################
########################################################################################################################

#   1- sélection du fichier à traiter
fenetre_select_file()
print("Fichier : " + name_file)
name_export = name_file[:-4] + "_" + datt + name_file[-4:]
print("Export :  " + name_export)

#   2- listage des champs du fichier à considérer
liste_champs()

#   3- sélection des champs à conserver et traitement
fenetre_select_chmp()

#      fin du chonomètre :
end = time()

#   4- petit message box "merci" ;)

msgg = "Fichier traité : \n\"" + just_name(name_file)   + "\"\n\n\n" + \
       "Fichier créé   : \n\"" + just_name(name_export) + "\"\n\n\n" + \
       "Traitement terminé en {:.2f}".format(end-start) + " sec\n\n" +\
       "\nMerci d'avoir utilisé cet outil" +\
       "\n© CeremaNC / DLAB / MEL / SB"

tkinter.messagebox.showinfo(title="Allègement des données avifaune",
                            message=msgg)