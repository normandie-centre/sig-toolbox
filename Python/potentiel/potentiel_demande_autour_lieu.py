import requests
import pandas as pd
import plotly.express as px
import plotly.io as pio
pio.renderers.default = "notebook"
from urllib.request import urlopen, HTTPError
import pprint
import json
from geojson import Polygon



##Token de l'API FF. Il est nécessaire de la renouveler tous les 30 jours en se rendant sur ce site : https://consultdf.cerema.fr/consultdf/services/apidf/token


token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjY2NTE2ODk2LCJpYXQiOjE2NjM5MjQ4OTYsImp0aSI6IjJlYWI1MjdhZGVmZTQwMjliNjM4MjBiZTQ5MGQxNDc3IiwidXNlcl9uYW1lIjoiYWxleGlzLW0udmVybmllciIsImF1ZCI6ImFwaWRmIn0.7KM5gbY0FXl8nPrzyYa9L4Ha7vFml6CQlRqUwPp7O7k"

def apidf(code_insee = None, token=None):
    

    BASE_URL_API = "https://apidf-preprod.cerema.fr"

    url_endpoint = BASE_URL_API + f"/ff/locaux/?code_insee={code_insee}"
    HEADERS = {
        "Content-Type": "application/json",
    }
    if token:
        HEADERS["Authorization"] = "Token " + token
    response = requests.get(
        url_endpoint,
        headers=HEADERS,
    )  
    if response.status_code == 200:
      return response.json()
    return response.status_code



url_api_sirene_base = "http://api.cquest.org/sirene?"
# Dictionnaire des tranches d'effectifs
dict_tranche_effectif = {
    "N" : [0, 0],
    "NN" : [0, 0],
    "00" : [0, 0],
    "01" : [1, 2],
    "02" : [3, 5],
    "03" : [6, 9],
    "11" : [10, 19],
    "12" : [20, 49],
    "21" : [50, 99],
    "22" : [100, 199],
    "31" : [200, 249],
    "32" : [250, 499],
    "41" : [500, 999],
    "42" : [1000, 1999],
    "51" : [2000, 4999],
    "52" : [5000, 9999],
    "53" : [10000, 10000]
}

dict_uai_educnat ={
    "101" : "ECOLE MATERNELLE",
    "102" : "ECOLE MATERNELLE ANNEXE D INSPE",
    "103" : "ECOLE MATERNELLE D APPLICATION",
    "151" : "ECOLE DE NIVEAU ELEMENTAIRE",
    "153" : "ECOLE ELEMENTAIRE D APPLICATION",
    "160" : "ECOLE DE PLEIN AIR",
    "162" : "ECOLE DE NIVEAU ELEMENTAIRE SPECIALISEE",
    "169" : "ECOLE REGIONALE DU PREMIER DEGRE",
    "170" : "ECOLE SANS EFFECTIFS PERMANENTS",
    "300" : "LYCEE ENSEIGNT GENERAL ET TECHNOLOGIQUE",
    "301" : "LYCEE D ENSEIGNEMENT TECHNOLOGIQUE",
    "302" : "LYCEE D ENSEIGNEMENT GENERAL",
    "306" : "LYCEE POLYVALENT",
    "307" : "LYCEE ENS GENERAL TECHNO PROF AGRICOLE",
    "310" : "LYCEE CLIMATIQUE",
    "312" : "ECOLE SECONDAIRE SPECIALISEE (2 D CYCLE)",
    "315" : "LYCEE EXPERIMENTAL",
    "320" : "LYCEE PROFESSIONNEL",
    "332" : "ECOLE PROFESSIONNELLE SPECIALISEE",
    "334" : "SECTION D ENSEIGNEMENT PROFESSIONNEL",
    "335" : "SECTION ENSEIGT GENERAL ET TECHNOLOGIQUE",
    "340" : "COLLEGE",
    "342" : "GROUPEMENT D OBSERVATION DISPERSE",
    "344" : "CETAD (TOM)",
    "345" : "CENTRE DE JEUNES ADOLESCENTS",
    "349" : "ETABLISSEMENT DE REINSERTION SCOLAIRE",
    "350" : "COLLEGE CLIMATIQUE",
    "352" : "COLLEGE SPECIALISE",
    "370" : "ETAB REGIONAL/LYCEE ENSEIGNEMENT ADAPTE",
    "380" : "MAISON FAMILIALE RURALE EDUCATION ORIENT",
    "390" : "SECTION ENSEIGNT GEN. ET PROF. ADAPTE",
}

class lieu:
    """Définit un lieu sur lequel on va observer les différentes caractéristiques permettant d'identifier les potentiels. On va donc regarder le nombre de salariés, la population, les scolaires, etc.
    
    Il est aussi intéressant d'observer si le lieu est accessible : proximité d'une gare ou d'un arrêt de transport en commun à proximité.

    """
    def __init__(self, latitude = None, longitude = None,*args, **kwargs):
        self.latitude = latitude
        self.longitude = longitude

##############Cette partie s'attache à calculer les effectifs de salariés à proximité d'un lieu#########
#########
#########


    def apidf(code_insee= None, token=None):
        token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjY2NTE2ODk2LCJpYXQiOjE2NjM5MjQ4OTYsImp0aSI6IjJlYWI1MjdhZGVmZTQwMjliNjM4MjBiZTQ5MGQxNDc3IiwidXNlcl9uYW1lIjoiYWxleGlzLW0udmVybmllciIsImF1ZCI6ImFwaWRmIn0.7KM5gbY0FXl8nPrzyYa9L4Ha7vFml6CQlRqUwPp7O7k"
        BASE_URL_API = "https://apidf-preprod.cerema.fr"
        url_endpoint = BASE_URL_API + f"/ff/locaux/?code_insee={code_insee}"

        HEADERS = {
            "Content-Type": "application/json",
        }
        if token:
            HEADERS["Authorization"] = "Token " + token
        response = requests.get(
            url_endpoint,
            headers=HEADERS,
        )  
        if response.status_code == 200:
            return response.json()
        return response.status_code

        ## 



    def habitant_dans_isochrone(self, temps= None, mode = None):
        """Cette méthode prend toutes les communes situées dans une isochrone dont les caractéristiques sont définies par "temps" et "mode". 
        Les locaux sont ensuite géolocalisés à la parcelle,
        Puis, seuls les locaux situés dans l'isochrone sont retenus, (à voir si on fait pas une méthode "géolocalisation depuis une parcelle", sacha)
        Puis à chaque local est attribué un nombre d'habitants estimé.
        L'algorithme calcule enfin le total d'habitants sur l'isochrone"""

       
##############Cette partie s'attache à calculer les effectifs d'habitant à proximité d'un lieu#########
#########
#########

    def effectif_sirene_tampon(self, distance = None):
        ## point est un tuple (latitude,longitude)

        url_endpoint = f"{url_api_sirene_base}lat={self.latitude}&lon={self.longitude}&dist={distance}"

        sirene_json = requests.get(url_endpoint).json()
        etablissement_sirene_json = sirene_json['features']
        ## On calcule les effectifs minimmaux et maximaux sur un tampon autour du  point en fonction d'une certaine distance
        
        eff_min = sum([dict_tranche_effectif[ligne['properties']['trancheeffectifsetablissement']][0] for ligne in etablissement_sirene_json if 'trancheeffectifsetablissement' in ligne['properties']])

        eff_max = sum([dict_tranche_effectif[ligne['properties']['trancheeffectifsetablissement']][1] for ligne in etablissement_sirene_json if 'trancheeffectifsetablissement' in ligne['properties']])
        
        return(eff_min,eff_max)

    def sirene_dans_bbox(self, xmin = None, ymin = None, xmax = None, ymax = None):
        """sélectionne dans l'API Sirene les entreprises situées dans la bbox ainsi définie."""

        url_api_sirene_base = "http://api.cquest.org/sirene?bbox="
        url_endpoint = f"{url_api_sirene_base}{xmin},{ymin},{xmax},{ymax}"
        sirene_dans_bbox_json = requests.get(url_endpoint).json()
        
        eff_min = sum([dict_tranche_effectif[ligne['properties']['trancheeffectifsetablissement']][0] for ligne in sirene_dans_bbox_json if 'trancheeffectifsetablissement' in ligne['properties']])

        eff_max = sum([dict_tranche_effectif[ligne['properties']['trancheeffectifsetablissement']][1] for ligne in sirene_dans_bbox_json if 'trancheeffectifsetablissement' in ligne['properties']])


        return(eff_min,eff_max)

    def isochrone(self, temps = None, mode = None):
        """Cette méthode permet de calculer des isochrones avec deux modes différents : pedestrian (pour piéton) et car (pour voiture)."""

        url_ign_base = "https://wxs.ign.fr/essentiels/geoportail/isochrone/rest/1.0.0/isochrone?"
        url_endpoint = f"{url_ign_base}point={self.longitude},{self.latitude}&resource=bdtopo-pgr&profile={mode}&costType=time&costValue={temps}&diretion=departure"
        isochrone_json = requests.get(url_endpoint).json()
        geom = isochrone_json['geometry']['coordinates'][0]

        ###### Générer un geojson ######

        geom_geojson = Polygon([geom])


        print(geom_geojson)
        return(geom)
        

    def effectif_sirene_dans_bbox_encadrant_un_isochrone(self, temps = None, mode = None):
        """sélectionne dans l'API Sirene les entreprises situées dans la bbox entourant un isochrone"""
        geom = self.isochrone(temps,mode)
        xmin = min([coord[0] for coord in geom])
        ymin = min([coord[1] for coord in geom])
        xmax = max([coord[0] for coord in geom])
        ymax = max([coord[1] for coord in geom])
        print(xmin,ymin,xmax,ymax)

        print(self.sirene_dans_bbox(xmin,ymin,xmax,ymax))
        # eff_max_bbox_contenant_isochrone = self.sirene_dans_bbox(xmin,ymin,xmax,ymax)[1]

        # return(eff_min_bbox_contenant_isochrone,eff_max_bbox_contenant_isochrone)

    def effectifs_sirene_isochrone(self, temps = None, mode = None):

        """Cette méthode permet de calculer les effectifs salariés sur un isochrone avec un temps et un mode donné.
        Comme on ne peut pas charger un polygone dans l'API Sirene, la procédure consiste alors à :
        - charger l'isochrone, avec la méthode "isochrone",
        - sélectionner la box qui contient l'isochrone,
        - récupérer l'ensemble des établissements recensés dans l'API Sirene sur cette box,
        - sélectionner les établissements étant dans l'isochrone,
        - compter les effectifs salariés ainsi sélectionnés.
        """

        isochrone_json = self.isochrone(temps,mode)     

    def scolaire_dans_isochrone(self, temps = None, mode = None):
        """Cette méthode va calculer l'ensemble des scolaires du second degré (collégiens et lycéens) sur un isochrone défini."""

        url_educ_nat_base = 'https://data.education.gouv.fr/api/records/1.0/search/?dataset=fr-en-annuaire-education'

        #Définition de l'isochrone sur lequel le calcul va être réalisé
        polygone = self.isochrone(temps,mode)
        liste_geom_en_forme = [f"({coord[1]},{coord[0]})" for coord in polygone]
        polygone_en_forme = ""
        for geom in liste_geom_en_forme:
            polygone_en_forme = polygone_en_forme+"," + geom
            
        #Construction de la requête
        url_endpoint = f"{url_educ_nat_base}&rows=1000&geofilter.polygon={polygone_en_forme[1:]}"
        educnat_json = requests.get(url_endpoint).json()        

        #On filtre sur les établissements du secondaire. Il s'agit des établissements dont le code nature uai est supérieur à 300. On sélectionne également que les établissements dont le nombre d'élèves est indiqué.
        
        nb_eleves = sum([record['fields']['nombre_d_eleves'] for record in educnat_json['records'] if 'nombre_d_eleves' in record['fields']  and int(record['fields']['code_nature'])>300])

        return(nb_eleves)

def geojson(geom = None, type_geometrie = None):
        """Cette fonction transforme un geom de type [[latitude,longitude],[latitude,longitude]...] en format geojson.
        Le type de geometrie peut être Point, Polygon ou Line"""

        out ={
  "type": "FeatureCollection",
  "geometry": {
    "type": type_geometrie,
    "coordinates": [[coord[0],coord(0)] for coord in geom]
  }
}

        return(out)



lieu_test = lieu(latitude = '49.44062', longitude = '1.09975')
print(lieu_test.scolaire_dans_isochrone(900,'pedestrian'))

geojson(lieu_test.isochrone(900,'pedestrian'))

#test = lieu_test.effectif_sirene_tampon(700)
#test_2 = lieu_test.isochrone(600,'pedestrian')
#test_bbox = lieu_test.effectif_sirene_dans_bbox_encadrant_un_isochrone(temps = 600, mode = 'pedestrian' )
#print(test_bbox)


      

