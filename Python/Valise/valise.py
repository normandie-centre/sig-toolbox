#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Création d'une gui nommée Valise du Cerema

La Valise du Cerema est une interface qui permet de filtrer des documents listés dans un fichier structuré.
Le filtrage de documents se fait par thématique, section ou bien par mots-clefs.

Usage :
-------
    python valise.py

"""

__author__ = "Thomas Escrihuela"
__contact__ = "Thomas.Escrihuela@cerema.fr"
__version__ = "1.5"
__date__ = "2020/06"

import os
import sys
import tkinter as tk
import tkinter.ttk as ttk
from tkinter import filedialog
from tkinter import messagebox
from tkinter.font import Font
from tkinter import tix
from pyexcel_ods import get_data
import webbrowser
import unicodedata
import re
import pprint

# color = 'SystemButtonFace' # default color
color = "sandy brown"

extensions = dict(
    fichier = ("pdf"),
    media = ("mp4","flv","m4v","jpg", "jpeg", "png","vob","wmv"),
    web = ("url","html")
)

themes = dict(
    accessibilite = ('A','Accessibilité','Informations relatives aux déplacements des personnes à mobilité réduite'),
    amenagement = ('B','Aménagement espace public','Ensemble des éléments conceptuels des espaces publics'),
    gouvernance = ('C','Gouvernance et management','Organisation, gestion et planification des déplacements dans les territoires'),
    mobilite_alternative = ('D','Mobilité alternative','Optimisation de la voiture et nouvelle mobilité'),
    territoires_peu_denses = ('E','Mobilité dans les territoires peu denses','Traitement des déplacements dans les territoires ruraux'),
    modes_actifs = ('F','Modes actifs','Moyens de développer et intégrer le vélo et la marche'),
    voirie = ('G','Partage de la voirie','Gestion de la cohabitation des modes de déplacement et renforcement de la fonctionnalité des espaces publics'),
    planification = ('H','Planification','Anticipation et intégration des nouvelles mobilités dans les territoires'),
    securite_routiere = ('I','Sécurité routière','Accidentologie et traitement d\'espaces publics plus sécurisés'),
    stationnement = ('J','Stationnement','Développement d\'une stratégie de stationnement'),
)

sections = dict(
    generalite = ('1','Généralité','Eléments de cadrage de la thématique','yellow'),
    reglementation = ('2','Réglementation','Textes réglementaires et législatifs liés à la thématique','purple'),
    technique = ('3','Technique','Références techniques liées à la thématique','green'),
    methodologie = ('4','Méthodologie','Références et repères méthodologiques liés à la thématique','black'),
    etude = ('5','Etude de cas','Présentation de cas concrets','cyan'),
    infos_locales = ('6','Infos locales','Informations ou réalisations relatives au territoire ornais','maroon'),
    communication = ('7','Communication','Eléments communicants liés à la thématique','blue'),
    formation = ('8','Formation','Supports de présentation préétablis','red'),
    phototheque = ('9','Médiathèque','Vidéo ou photographie illustrant des références de la thématique','grey'),
)

mots_clefs_predefinis = (
    "accessibilité",
    "aire piétonne",
    "alter-modalité",
    "aménagement",
    "apaisement",
    "commerce",
    "données",
    "économie",
    "électro-mobilité",
    "espace public",
    "gouvernance",
    "habitat-bâti",
    "handicap",
    "implication citoyenne",
    "mobilité",
    "modes actifs",
    "multi-modalité",
    "optimisation automobile",
    "partage",
    "paysage-espace vert",
    "piéton",
    "plan mobilité",
    "planification",
    "rural",
    "santé",
    "sécurité routière",
    "signalétique",
    "stationnement",
    "stratégie",
    "transport collectif",
    "urbain",
    "urbanisme",
    "vélo",
    "vieillissement",
    "zone 30",
    "zone rencontre"
)


# ************************
# Scrollable Frame Class
# ************************
class Scrollable(tk.Frame):
    """
       Make a frame scrollable with scrollbar on the right.
       After adding or removing widgets to the scrollable frame, 
       call the update() method to refresh the scrollable area.

       source : https://stackoverflow.com/questions/3085696/adding-a-scrollbar-to-a-group-of-widgets-in-tkinter
    """

    def __init__(self, frame, width=16):

        self.scrollbar = tk.Scrollbar(frame, width=width)
        self.scrollbar.grid(row=0,column=1,sticky='nse')

        self.canvas = tk.Canvas(frame, yscrollcommand=self.scrollbar.set, bg=color, bd=-2)
        self.canvas.grid(row=0,column=0,sticky='nsew')

        self.scrollbar.config(command=self.canvas.yview)

        self.canvas.bind('<Configure>', self.__fill_canvas)

        frame.grid_rowconfigure(0, weight=1)
        frame.grid_columnconfigure(0, weight=1)

        # base class initialization
        tk.Frame.__init__(self, frame, bg=color)         

        # assign this obj (the inner frame) to the windows item of the canvas
        self.windows_item = self.canvas.create_window(0,0, window=self, anchor=tk.NW)


    def __fill_canvas(self, event):
        "Enlarge the windows item to the canvas width"

        canvas_width = event.width
        self.canvas.itemconfig(self.windows_item, width = canvas_width)        

    def update(self):
        "Update the canvas and the scrollregion"

        self.update_idletasks()
        self.canvas.config(scrollregion=self.canvas.bbox(self.windows_item))




################################
#### Classe de l'interface #####
################################

class Interface(tk.Tk):
    """La classe principale du programme. Crée l'interface.
    
    Attributs : tous les widgets de l'interface
    Méthodes :
    - __init__ : le constructeur de la classe
    - create_tab : crée les onglets
    - initialize : initialise l'interface avec les boutons
    - create_checkbutton : crée un bouton à cocher
    - find_doc : prend en entrée un répertoire contenant les documents et le fichier *valise.ods* 
    - filter_doc : lance le filtrage des documents en fonction des éléments cochés
    - filter_keywords : lance le filtrage des documents en fonction du mot-clef
    - create_result : crée une ligne de résultat cliquable
    - open : ouvre le document résultat (pdf, url, media, ...)
    - string_sans_accent : Renvoie une chaîne de caractère sans accent
    - remise_a_zero : réinitialise le panneau résultats
    - delete_result : supprime une ligne de résultat
    - toggle_checkbox : décoche tous les boutons sections
    - toggle_fullscreen : passage en plein écran via la touche F11
    - end_fullscreen : fin du plein écran via la touche Echap

    """
    
    def __init__(self):
        """Le constructeur de l'application."""
        tix.Tk.__init__(self)
        # tk.Tk.__init__(self)
        
        self.state('zoomed')
        self.option_add("*Font", "TkDefaultFont")

        # Attributs
        self.source_file = "base_de_donnees.ods"
        self.logo_cerema = tk.PhotoImage(file = resource_path(r".\static\logo_cerema.png"))
        self.logo_orne = tk.PhotoImage(file = resource_path(r".\static\logo_orne.png"))
        self.video = resource_path(r".\static\La_Valise.mp4")


        self.thematique_checkbutton= dict()
        self.section_checkbutton= dict()
        self.valise= dict()
        self.is_ods = False
        self.rep = ""

        self.create_tab()
        self.initialize()

        self.bind("<F11>", self.toggle_fullscreen)
        self.bind("<Escape>", self.end_fullscreen)


    def create_tab(self):
        """Crée les onglets."""
        notebook = ttk.Notebook(self)

        self.f1 = tk.Frame(notebook,bg=color)
        self.f2 = tk.Frame(notebook,bg=color)
        
        notebook.add(self.f1, text="Valise")
        notebook.add(self.f2, text="Notice")
        
        notebook.grid(sticky="nsew")


    def initialize(self):
        """Initialise la fenêtre de l'application."""

        
        ######################################
        ### Création de la notice
        ######################################

        myfont = Font(family='Helvetica',size=11)

        self.frame1_notice = tk.Frame(self.f2,bg=color,bd=2,relief="groove")
        self.frame2_notice = tk.Frame(self.f2,bg=color,bd=2,relief="groove")
        text1 = tk.Text(self.frame1_notice,wrap=tk.WORD, bg=color,bd=0)
        text2 = tk.Text(self.frame2_notice,wrap=tk.WORD,bg=color, bd=0)
        lien_video = tk.Label(self.frame2_notice,text="Vidéo de démonstration", cursor="hand2", fg="blue",bg=color)
        lien_video.configure(font=('Helvetica 11 italic underline'))
        lien_video.bind("<Button-1>", lambda e: self.open(self.video))

        text1.configure(font=myfont)
        text2.configure(font=myfont)

        self.frame1_notice.grid(row=0,column=0,sticky='nsew')
        self.frame2_notice.grid(row=0,column=1,sticky='nsew')
        text1.grid(row=0,column=0,padx=15, sticky='nsew')
        text2.grid(row=0,column=0,padx=15,sticky='nsew')
        lien_video.grid(row=1,column=0, sticky='nsew')


        notice1 = """
        La Valise
        
        Principe

        La Valise est une interface permettant une recherche de documents pluridiscipinaire (urbanisme, accessibilité, sécurité routière, aménagement de l’espace public..) simple et intuitive. La Valise propose une approche globale pour accompagner les agents dans leurs missions vers les territoires urbains mais également les territoires ruraux. Son objectif est de disposer d’un outil interne unique de référence déclinant les orientations en matière de politiques de déplacement, de promotion des modes actifs et d’aménagement de l’espace public. La Valise est également un outil de communication et de formation sur lequel les agents de la DDT61 pourront s’appuyer pour sensibiliser et acculturer les collectivités ornaises et, au besoin, d’autres partenaires.

        Fonctionnement

        La Valise permet la recherche et le filtrage de productions dans une banque documentaire, par thématique ou mots-clefs. La Valise a besoin pour fonctionner d'un répertoire source avec :
            - des dossiers contenant les documents rangés dans une arborescence fixée;
            - un fichier base_de_donnees.ods.

        Arborescence

        Le répertoire source associe tous les fichiers de la banque documentaire (fiches, vidéos, liens web, ...) dans une arborescence de dossiers composée de 10 thématiques et de 9 rubriques. Les documents doivent se trouver dans un sous-répertoire thématique/rubrique pour pouvoir être analysés (par exemple : F_Modes actifs\F3_Technique\F309_2013_CEREMA_voie_verte.pdf).

        Base de données

        La Valise s'appuie sur le fichier base_de_donnees.ods pour lister les documents et leurs informations. La première feuille est la valise en elle-même, elle est composée de colonnes listant le nom du fichier, ses mots-clefs associés (séparés par une virgule), sa thématique et sa section. L'ordre des colonnes est important et ne doit pas être modifié. La deuxième feuille liste les thématiques et rubriques disponibles, l'ajout d'un nouveau document dans la valise (feuille 1) forcera le choix d'une thématique et d'une rubrique prédéfinie.

        Il est important de spécifier dans la base de données les extensions des documents (.pdf pour les fiches, .url pour les liens web, ...)."""

        notice2 = """
        Utlisation de la Valise

        Avant toute utilisation de la valise, il est nécessaire de charger le répertoire source contenant la base de données, via le bouton Sélection du répertoire source. Un bouton Remise à zéro permet de réinitialiser l'affichage en supprimant les résultats et les recherches.


        Recherche par thématiques et rubriques

        Le premier panneau permet la recherche de documents par thématiques et rubriques. Des informations sont disponibles au survol de chaque item. Il est possible de cocher plusieurs thématiques et plusieurs sections pour effectuer une recherche multi-critères. Le bouton Lancer la recherche permet de faire apparaître les documents candidats dans deux cadres résultats : les fiches (pdf) et les médias (url, vidéo et photo). Il est possible d'ouvrir chaque fichier en cliquant sur le résultat.

        Recherche par mots-clefs

        La Valise permet de rechercher les documents par mots-clefs selon ceux définis dans la base de données.
        
        La première recherche permet de rentrer un ou plusieurs mots. Les documents retournés sont ceux qui contiendront tous ces termes comme mots-clefs dans la base de données. Il est ainsi possible de rechercher "velo" pour obtenir tous les documents liés au vélo, ou "velo, Signalétique" pour affiner la recherche à un sujet en particulier. Les abréviations et morceaux de mots sont acceptés ("urba" pour urbanisme, "zone" pour obtenir les documents traitant de "zone 30" et "zone piétonne", ...).

        Enfin un filtrage par mots-clefs prédéfinis est implémenté pour une recherche plus rapide.

        Le tutoriel ci-après sous format vidéo montre un exemple recherche avec la Valise :"""
        
        text1.insert(tk.INSERT, notice1)
        text2.insert(tk.INSERT, notice2)

        self.highlight(text1,2,8,'end','Helvetica 18 bold')
        self.highlight(text1,4,8,'end','Helvetica 14')
        self.highlight(text1,8,8,'end','Helvetica 14')
        self.highlight(text1,10,180,186,'Helvetica 11 bold')
        self.highlight(text1,12,25,44,'Helvetica 11 bold')
        self.highlight(text1,14,8,'end','Helvetica 12 italic')
        self.highlight(text1,16,312,372,'Helvetica 11 italic')
        self.highlight(text1,18,8,'end','Helvetica 12 italic')
        self.highlight(text1,20,42,61,'Helvetica 11 bold')

        self.highlight(text2,2,8,'end','Helvetica 18 bold')
        self.highlight(text2,4,89,95,'Helvetica 11 bold')
        self.highlight(text2,4,140,172,'Helvetica 11 italic')
        self.highlight(text2,4,182,195,'Helvetica 11 italic')
        self.highlight(text2,7,8,'end','Helvetica 12 italic')
        self.highlight(text2,9,275,294,'Helvetica 11 italic')
        self.highlight(text2,11,8,'end','Helvetica 12 italic')
        
        text1.configure(state='disabled')
        text2.configure(state='disabled')


        ######################################
        ### Création des widgets de l'onglet 1
        ######################################

        # Création des 3 labels labelframe
        self.lf_filtre = tk.LabelFrame(self.f1, text="Filtre par thématique",bg=color)       
        self.lf_mots_clefs = tk.LabelFrame(self.f1, text="Recherche par mots-clefs",bg=color)       
        self.lf_resultats = tk.LabelFrame(self.f1, text="Résultats",bg=color)       
        self.lf_filtre.grid(column=0,row=0,padx=20,pady=(20,10),sticky='nsew')
        self.lf_mots_clefs.grid(column=0,row=1,padx=20,pady=(10,20),sticky='nsew')
        self.lf_resultats.grid(column=1,row=0,rowspan=2,padx=20,pady=20,sticky='nsew')


        #########################
        ### LabelFrame Thématique

        # Création des 2 panneaux dans le labelframe thématique
        Frame1 = tk.Frame(self.lf_filtre,bg=color)
        Frame2 = tk.Frame(self.lf_filtre,bg=color)
        Frame1.grid(row=0,column=0,padx=15,pady=10,sticky='nw')
        Frame2.grid(row=0,column=1,padx=15,pady=10,sticky='nw')

        # Panneau 1 : thématiques
        tk.Label(Frame1, text="Thématiques",bg=color).grid(pady=20)
        for k,v in themes.items():
            self.thematique_checkbutton[k] = dict()
            self.thematique_checkbutton[k]["label"] = v[1]
            self.thematique_checkbutton[k]["infobulle"] = v[2]
            self.thematique_checkbutton[k]["var"] = tk.BooleanVar()
            thematique = v[0] + " - " + self.thematique_checkbutton[k]["label"]
            self.create_checkbutton(Frame1,thematique,self.thematique_checkbutton[k]["infobulle"],self.thematique_checkbutton[k]["var"]).grid(column=0,sticky='w')

        # Panneau 2 : sections
        tk.Label(Frame2, text="Rubriques",bg=color).grid(pady=20)
        for k,v in sections.items():
            ligne = tk.Frame(Frame2,bg=color)
            ligne.grid(column=0,sticky='w')
            tk.Canvas(ligne,bg=v[3],width=10,height=10,bd=-2).grid(row=0,column=0,sticky='w',padx=5,pady=5)
            self.section_checkbutton[k] = dict()
            self.section_checkbutton[k]["label"] = v[1]
            self.section_checkbutton[k]["infobulle"] = v[2]
            self.section_checkbutton[k]["var"] = tk.BooleanVar()
            section =  v[0] + " - " + self.section_checkbutton[k]["label"]
            self.create_checkbutton(ligne,section,self.section_checkbutton[k]["infobulle"],self.section_checkbutton[k]["var"]).grid(row=0,column=1,sticky='w')
        
        # Bouton Tout cocher
        self.check_all = tk.BooleanVar()
        self.all_checkbutton = tk.Checkbutton(Frame2,bg=color,activebackground=color,text="Tout cocher",variable=self.check_all,command=self.toggle_checkbutton)
        self.all_checkbutton.grid(column=0,sticky='w')

        # Création du bouton d'import de répertoire
        import_button = tk.Button(self.lf_filtre, text="Sélection du répertoire source",command=self.find_doc)
        import_button.grid(row=1,column=0,padx=10,pady=10,sticky='w')

        # Création du bouton de lancement du filtrage de documents
        go = tk.Button(self.lf_filtre,text="Lancer la recherche",command=self.filter_doc)
        go.grid(row=1,column=1,padx=10,pady=10,sticky='w')


        #########################
        ### LabelFrame Rechercher

        # Création du bouton de recherche par mots-clefs de l'utilisateur
        mots_clefs_frame = tk.Frame(self.lf_mots_clefs,bg=color)
        mots_clefs_label = tk.Label(mots_clefs_frame,text="Entrez un mot-clef :",bg=color)
        self.mots_clefs_entry = tk.Entry(mots_clefs_frame)
        mots_clefs_rechercher = tk.Button(mots_clefs_frame,text="Rechercher",command=self.filter_keywords)
        mots_clefs_frame.grid(sticky='w')
        mots_clefs_label.grid(row=0,column=0,sticky='w',padx=5)
        self.mots_clefs_entry.grid(row=0,column=1,sticky='w',padx=5)
        mots_clefs_rechercher.grid(row=0,column=2,sticky='w',padx=5)
        self.mots_clefs_entry.bind('<Return>',self.filter_keywords)

        # Création du bouton de recherche par mots-clefs prédéfinis
        list_mots_clefs_frame = tk.Frame(self.lf_mots_clefs,bg=color)
        recherche_mots_clefs_label = tk.Label(list_mots_clefs_frame,text="Recherchez avec un mot-clef prédéfini :",bg=color)
        self.mots_clefs_combobox = ttk.Combobox(list_mots_clefs_frame,values=mots_clefs_predefinis,state='readonly')
        liste_mots_clefs_rechercher = tk.Button(list_mots_clefs_frame,text="Rechercher",command=self.search_keywords)
        list_mots_clefs_frame.grid(pady=(0,10),sticky='w')
        recherche_mots_clefs_label.grid(row=1,column=0,sticky='w',padx=5)
        self.mots_clefs_combobox.grid(row=1,column=2,sticky='w',padx=5)
        liste_mots_clefs_rechercher.grid(row=1,column=3,sticky='w',padx=5)
        self.mots_clefs_combobox.bind('<Return>',self.filter_keywords)


        #########################
        ### LabelFrame Résultats

        # Création des 2 cadres des résultats
        self.cadre_resultats_fichier = tk.LabelFrame(self.lf_resultats,text="Fiches",bg=color)       
        self.cadre_resultats_media = tk.LabelFrame(self.lf_resultats,text="Sites Webs, Photos & Vidéos",bg=color)

        self.cadre_resultats_fichier.grid(row=0,column=0,padx=10,pady=10,sticky='nw')
        self.cadre_resultats_media.grid(row=1,column=0,padx=10,sticky='nw')

        # Création des panneaux avec listes déroulantes
        self.cadre_resultats_fichier_frame = tk.Frame(self.cadre_resultats_fichier,bg=color)
        self.cadre_resultats_media_frame = tk.Frame(self.cadre_resultats_media,bg=color)
        
        self.scrollable_cadre_resultats_fichier_frame = Scrollable(self.cadre_resultats_fichier_frame)
        self.scrollable_cadre_resultats_media_frame = Scrollable(self.cadre_resultats_media_frame)

        
        # Création du bouton de remise à zéro
        raz = tk.Button(self.lf_resultats,text="Remise à zéro",command=self.remise_a_zero)
        raz.grid(row=2,column=0,padx=10,pady=10,sticky='s')


        ######################
        ### Boutons de l'appli

        # Création logo Cerema
        logo_frame = tk.Frame(self,bg=color)
        logo_frame.grid(row=1,column=0,sticky='sw',padx=10,pady=10)
        cerema = tk.Label(logo_frame,image=self.logo_cerema)
        orne = tk.Label(logo_frame,image=self.logo_orne)
        cerema.grid(row=1,column=0,sticky='sw',padx=10,pady=10)
        orne.grid(row=1,column=1,sticky='sw',padx=10,pady=10)

        # Bouton quitter
        bouton_quitter = tk.Button(self, text="Quitter", command=self.quit)
        bouton_quitter.grid(row=1,column=0,padx=10,pady=10,sticky='se')


        ####################################
        ### Gestion du placement des widgets

        # change la taille auto des lignes et colonnes
        self.grid_rowconfigure(0,weight=1)
        self.grid_columnconfigure(0,weight=1)
        self.f1.grid_rowconfigure((0,1),weight=1)
        self.f1.grid_columnconfigure((0,1),weight=1)
        self.f2.grid_rowconfigure(0,weight=1)
        self.f2.grid_columnconfigure((0,1),weight=1)
        self.frame1_notice.grid_rowconfigure(0, weight=1)
        self.frame1_notice.grid_columnconfigure(0, weight=1)
        self.frame2_notice.grid_rowconfigure((0,1), weight=1)
        self.frame2_notice.grid_columnconfigure(0, weight=1)
        self.lf_filtre.grid_rowconfigure((0,1),weight=1)
        self.lf_filtre.grid_columnconfigure((0,1),weight=1)
        self.lf_mots_clefs.grid_rowconfigure((0,1),weight=1)
        self.lf_mots_clefs.grid_columnconfigure(0,weight=1)
        self.lf_resultats.grid_rowconfigure(0,weight=1)
        self.lf_resultats.grid_columnconfigure(0,weight=1)
        self.cadre_resultats_fichier.grid_rowconfigure(0, weight=1)
        self.cadre_resultats_fichier.grid_columnconfigure(0, weight=1)
        self.cadre_resultats_media.grid_rowconfigure(0, weight=1)
        self.cadre_resultats_media.grid_columnconfigure(0, weight=1)

        
        self.resizable(True,True)


    def create_checkbutton(self,frame,label,message,var):
        """Crée un bouton case à cocher."""
        but = tk.Checkbutton(frame,bg=color,activebackground=color,text=label,variable=var)
        b = tix.Balloon(frame,initwait=500)
        b.bind_widget(but, msg=message)
        for sub in b.subwidgets_all():
            sub.config(bg='grey85')
        
        return but


    def find_doc(self):
        """Lecture du fichier valise."""
        self.valise = dict()
        self.rep = filedialog.askdirectory()
        self.is_ods = False
        for file in os.listdir(self.rep):
            if file.endswith(self.source_file):
                self.is_ods = True
                ods = get_data(os.path.join(self.rep,file))
                for line in ods["valise"][1:]: # On ne prend en compte que la feuille "valise" et on ne lit pas la première ligne considérée comme un header
                    if len(line) > 0 :
                        if line[0] not in self.valise:  
                            self.valise[line[0]] = []
                        self.valise[line[0]].append(dict(motsclefs = [self.string_sans_accent(w.strip().lower()) for w in line[1].split(',')], thematique = line[2], section = line[3]))
        if not self.check_rep():
            return False

        return True


    def filter_doc(self):
        """Filtre les documents à partir des boutons."""
        if not self.check_rep():
            return False
        self.delete_result()

        # Récupère les cases cochées par l'utilisateur
        active_thema, active_sect = [], []
        [active_thema.append(v["label"]) for v in self.thematique_checkbutton.values() if v["var"].get()]
        [active_sect.append(v["label"]) for v in self.section_checkbutton.values() if v["var"].get()]
        
        # Lecture de la valise et création des résultats
        bool = False
        for doc,doc_list in self.valise.items():
            for doc_dict in doc_list:
                if doc_dict["thematique"] in active_thema and (doc_dict["section"] in active_sect or doc_dict["section"] == "Toutes"):
                    bool = True
                    self.create_result(doc)
                    break

        if not bool:
            messagebox.showwarning(message="Aucun document ne correspond à la recherche",title='Pas de document trouvé')
            
        return True


    def filter_keywords(self,event=None):
        """Filtre les documents à partir des mots-clefs."""
        if not self.check_rep():
            return False
        self.delete_result()

        # Lecture de la valise et création des résultats
        entree = [ self.string_sans_accent(word.strip().lower()) for word in self.mots_clefs_entry.get().split(',')]
        
        ### Recherche par mot entier
        # [self.create_result(doc) for doc,doc_list in self.valise.items() if all(word in doc_list[0]["motsclefs"] for word in entree)] or messagebox.showwarning(message="Aucun document ne correspond à la recherche",title='Pas de document trouvé')

        ### Recherche par sous-chaîne
        [self.create_result(doc) for doc,doc_list in self.valise.items() if all(len(list(filter(re.compile(".*" + word + ".*").match,doc_list[0]["motsclefs"]))) > 0 for word in entree)] or messagebox.showwarning(message="Aucun document ne correspond à la recherche",title='Pas de document trouvé')

        return True


    def search_keywords(self):
        """Filtre les documents à partir des mots-clefs prédéfinis dans la liste déroulante."""
        if not self.check_rep():
            return False
        self.delete_result()

        # Lecture de la valise et création des résultats
        [self.create_result(doc) for doc,doc_list in self.valise.items() if self.string_sans_accent(self.mots_clefs_combobox.get()) in doc_list[0]["motsclefs"]] or messagebox.showwarning(message="Aucun document ne correspond à la recherche",title='Pas de document trouvé')

        return True


    def create_result(self,doc):
        """Crée une ligne de résultat cliquable."""

        # Création du chemin complet vers le fichier
        the = [v for k,v in themes.items() if self.valise[doc][0]["thematique"] in v[1]][0] or messagebox.showwarning(message="La thématique du document [" + doc + "] ne correspond à aucune thématique prédéfinie",title='Mauvaise thématique dans la base de données')
        sec = [v for k,v in sections.items() if self.valise[doc][0]["section"] in v[1]][0] or messagebox.showwarning(message="La section du document [" + doc + "] ne correspond à aucune section prédéfinie",title='Mauvaise section dans la base de données')
        the_folder,sec_folder = the[0] + "_" + the[1] , the[0] + sec[0] + "_" + sec[1]         # A_Accessibilité , A1_Généralité
        filepath = os.path.join(self.rep,the_folder,sec_folder,doc)

        # Détection du type de fichier
        if any([doc.lower().endswith(k) for k in extensions["fichier"]]):
            self.cadre_resultats_fichier.grid(sticky='news')
            self.cadre_resultats_fichier_frame.grid(sticky='news')
            result_frame = tk.Frame(self.scrollable_cadre_resultats_fichier_frame,bg=color)
        elif any([doc.lower().endswith(k) for k in extensions["media"]]) or any([doc.lower().endswith(k) for k in extensions["web"]]):
            self.cadre_resultats_media.grid(sticky='news')
            self.cadre_resultats_media_frame.grid(sticky='news')
            result_frame = tk.Frame(self.scrollable_cadre_resultats_media_frame,bg=color)
        else:
            messagebox.showerror(message="{} non pris en compte (ni un fichier pdf, mp4, jpg, ni une url http)".format(doc),title="Impossible d'ouvrir le fichier")
            return False

        
        tk.Canvas(result_frame,bg=sec[3],width=10,height=10,bd=-2).grid(row=0,column=0,sticky='w',padx=5,pady=5)
        
        resultat = tk.Label(result_frame, text=doc, cursor="hand2",bg=color)
        resultat.bind("<Button-1>", lambda e: self.open(filepath))
        resultat.grid(row=0,column=1,sticky='nsew')
        result_frame.grid(column=0,sticky='nsew')

        self.scrollable_cadre_resultats_fichier_frame.update()
        self.scrollable_cadre_resultats_media_frame.update()

        return True


    def open(self,ressource):
        """Ouvre un document (pdf, url, vidéos, images, ...)."""
        
        file = os.path.join(self.rep,ressource)
        if os.path.isfile(file):
            webbrowser.open_new(file)
        else:
            messagebox.showwarning(message="Fichier pdf {} non trouvé".format(file),title='Fichier inexistant')

        return True


    def check_rep(self):
        """Vérifie si le fichier de base de données a bien été chargée"""
        if self.is_ods == False:
            messagebox.showerror(message="Pas de répertoire sélectionné, veuillez d'abord sélectionner un répertoire contenant le fichier [" + self.source_file + "]",title='Répertoire non valable')
            return False
        else:
            return True



    def string_sans_accent(self,word):
        """Renvoie une chaîne de caractère sans accent"""
        return unicodedata.normalize('NFKD', word).encode('ASCII', 'ignore').decode('UTF-8')


    def highlight(self,text,line_number,bchar,echar,font):
        """Change de font pour le texte."""
        tag ="{}_{}".format(line_number,bchar)
        text.tag_add(tag, "{}.{}".format(line_number,bchar), "{}.{}".format(line_number,echar))
        text.tag_config(tag, font=(font))
        
        return True


    def remise_a_zero(self):
        """Réinitialise le panneau Résultats."""
        self.delete_result()
        
        [checkbox[1]["var"].set(False) for checkbox in self.thematique_checkbutton.items()]
        [checkbox[1]["var"].set(False) for checkbox in self.section_checkbutton.items()]
        self.check_all.set(False)
        self.mots_clefs_entry.delete(0,'end')
        self.mots_clefs_combobox.set('')

        return True


    def delete_result(self):
        """Supprime tous les résultats."""
        # Détruit tous les résultats

        for wid in self.scrollable_cadre_resultats_fichier_frame.grid_slaves():
            wid.destroy()
        self.cadre_resultats_fichier.grid_remove()
        for wid in self.scrollable_cadre_resultats_media_frame.grid_slaves():
            wid.destroy()
        self.cadre_resultats_media.grid_remove()

        return True


    def toggle_checkbutton(self):
        """Coche ou décoche tous les boutons sections"""
        [checkbox[1]["var"].set(self.check_all.get()) for checkbox in self.section_checkbutton.items()]
        
        return True


    def toggle_fullscreen(self, event=None):
        """Passage en plein écran."""
        self.state = not self.state
        self.attributes("-fullscreen", self.state)

        return True


    def end_fullscreen(self, event=None):
        """Sortir du mode plein écran."""
        self.state = False
        self.attributes("-fullscreen", False)

        return True


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller 
    source : https://stackoverflow.com/questions/7674790/bundling-data-files-with-pyinstaller-onefile"""
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


#################
####  MAIN  #####
#################

if __name__ == "__main__":
    app = Interface()
    app.iconbitmap(resource_path(r".\static\cerema.ico"))
    app.title("La Valise du Cerema")
    app.configure(bg=color)

    app.mainloop()
    app.quit()
