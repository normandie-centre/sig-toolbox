# Valise du Cerema

## Objectif

Le script **valise.py** peut être lancé par python pour créer une interface graphique nommée "Valise" qui permet de filtrer des documents à partir d'un répertoire donné.

Le répertoire doit contenir un fichier _base_de_donnees.ods_ contenant les liens entre fiches pdf, vidéos mp4, sites web et mots-clefs, thématiques et sections.

Le filtrage peut se faire soit selon la thématique, soit par mots-clefs.

## Compilation en exécutable

Le script peut être également compilé en exécutable. 

Pour ceci, il faut avoir python3 et le module *pyinstaller* et lancer la commande suivante :

`pyinstaller.exe --clean --onefile --add-data 'static\logo_cerema.png;static' --add-data 'static\logo_orne.png;static' --add-data 'static\La_Valise.mp4;static' --add-data 'static\cerema.ico;static' --noconsole -i .\static\cerema.ico valise.py`

Il faut au préalable avoir modifié le hook de tkinter en ajoutant ceci en début de script afin de créer le bundle vers le module *tix* (par exemple `C:\Users\thomas.escrihuela\AppData\Local\Programs\Python\Python38-32\Lib\site-packages\PyInstaller\hooks\hook-_tkinter.py`) :
```
from PyInstaller.utils.hooks import collect_submodules
hiddenimports = collect_submodules('package')

datas = [
(sys.prefix+r'\tcl\tix8.4.3', r'/tcl/tix8.4.3'),
(sys.prefix+r'\tcl\tix8.4.3\bitmaps', r'/tcl/tix8.4.3/bitmaps'),
(sys.prefix+r'\tcl\tix8.4.3\pref', r'/tcl/tix8.4.3/pref')
]
```

Le dossier *dist* est alors créé et contiendra l'exécutable **valise.exe**.

## Lancer l'éxecutable

Pour une utilisation immédiate, l'éxecutable du dépôt peut directement être lancé (éxecutable dans **dist/**).
