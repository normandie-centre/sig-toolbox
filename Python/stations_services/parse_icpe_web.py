from selenium import webdriver
from selenium.webdriver.common.proxy import Proxy, ProxyType
from bs4 import BeautifulSoup
from pprint import pprint
import time
import csv


# Paramètres
input_file = "./IC_1435_sample.csv"
input_file_delimiter = ";"
output_file = "./stations_services_sample.csv"
nb_reprises_sur_erreur = 5


# Constantes
rubrique_ic = "1435"
url_template = 'https://www.georisques.gouv.fr/risques/installations/donnees/details/'
cols = ["Nom", "X", "Y", "Rubrique IC", "Alinéa", "Date autorisation", "Etat activité", "Régime autorisé", "Activité", "Volume", "Unité"] # colonnes du csv en sortie
stations_services = []


# Gestion du proxy dans le driver chrome
prox = Proxy()
prox.proxy_type = ProxyType.MANUAL
prox.http_proxy = "http://pfrie-std.proxy.e2.rie.gouv.fr:8080"
prox.socks_proxy = "http://pfrie-std.proxy.e2.rie.gouv.fr:8080"
prox.ssl_proxy = "http://pfrie-std.proxy.e2.rie.gouv.fr:8080"

capabilities = webdriver.DesiredCapabilities.CHROME
prox.add_to_capabilities(capabilities)
browser = webdriver.Chrome(desired_capabilities=capabilities)


# Compte le nombre de lignes dans le fichier en entrée
nb_lines = len(open(input_file, 'r', encoding='utf-8').readlines())


# Construction des stations services en parsant georisques
with open(input_file, 'r', encoding='utf-8') as f:
    for i, line in enumerate(f):
        time.sleep(1) # be a responsible citizen ;)

        ## On ne prend pas en compte la ligne d'en-tête et les lignes vides
        if not line[0].isdigit():
            continue  

        ## Parsing de georisques
        numero_inspection = line.split(input_file_delimiter)[0]
        url = url_template + numero_inspection
        print("{}/{} : {}".format(i, nb_lines, url))
        try:
            browser.get(url)
            soup = BeautifulSoup(browser.page_source, features="html.parser")
        except Exception as e:
            print(e)


        ## Reprise sur erreur
        c = 1
        while None in [soup.find("div", {"id": "titreEtablissementFiche"}), soup.find_all("p", {"class": "detailAttributFiche"}), soup.find("td", text=rubrique_ic)] and c < nb_reprises_sur_erreur:
            print("Essai {}...".format(c))
            time.sleep(3)
            try:
                browser.get(url)
                soup = BeautifulSoup(browser.page_source, features="html.parser")
            except Exception as e:
                print(e)
            c += 1


        ## Get name 
        icpe = {}
        nom = soup.find("div", {"id": "titreEtablissementFiche"}).find("h2").text.strip()        
        icpe["Nom"] = nom

        ## Get X/Y
        for line in soup.find_all("p", {"class": "detailAttributFiche"}):
            if "X : " in line.text:
                icpe["X"] = str(line).split(":")[1].split("<")[0].strip()
            if "Y : " in line.text:
                icpe["Y"] = str(line).split(":")[1].split("<")[0].strip()


        ## Get 1435 line
        line = soup.find("td", text=rubrique_ic).find_next_siblings("td")
        icpe["Rubrique IC"] = rubrique_ic
        icpe["Alinéa"] = line[0].text
        icpe["Date autorisation"] = line[1].text
        icpe["Etat activité"] = line[2].text
        icpe["Régime autorisé"] = line[3].text
        icpe["Activité"] = line[4].text
        icpe["Volume"] = line[5].text
        icpe["Unité"] = line[6].text

        stations_services.append(icpe)

pprint(stations_services)


# Write csv output
with open(output_file, 'w', newline='', encoding = 'utf-8') as f:
    wr = csv.DictWriter(f, fieldnames = cols) 
    wr.writeheader()
    wr.writerows(stations_services)