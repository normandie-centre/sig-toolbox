# -*- coding: utf-8 -*-

"""
    Recherche de code SIRET

    Ce script prend en entrée un csv contenant des entreprises au format 
        'id | raison sociale | ville'
    Il recherche ensuite sur google cette entreprise via la raison sociale et la ville et ouvre le lien contenant le nom de site ${site} dans la limite des ${nombre_resultats} premiers résultats. Ce site est scrappé pour identifier le numéro de SIRET de l'entreprise.
    Ensuite certaines informations concernant l'entreprise sont récupérées avec l'API SIREN.

    Attention :
        Le script est adapté pour rechercher le numéro de SIRET de l'entreprise sur le site web "societe.com".

    Exemple :
        $ python search_siret_on_google.py

    Sortie :
        Le script crée le csv ${output_file} avec des champs contenant le numéro de SIRET et les  informations récupérées par l'API.
        Les logs sont renvoyés en console et dans un fichier de log.
"""


__authors__ = ("Alexis Vernier", "Thomas Escrihuela")
__contact__ = ("Alexis.Vernier@cerema.fr", "Thomas.Escrihuela@cerema.fr")
__date__ = "08/2021"
__version__ = "1.1"


from bs4 import BeautifulSoup
import csv
from googlesearch import search
import logging
from selenium import webdriver
import requests
from webdriver_manager.chrome import ChromeDriverManager


#############
## Paramètres
input_file = "ite_sans_siret.csv"
output_file = "ite_avec_siret.csv"
output_cols = ["ID", "Raison sociale", "Commune", "Site scrappé", "N° SIRET", "Tranche effectifs", "Année effectifs", "Établissement siège", "Libellé voie", "Code postal", "Libellé commune", "Date début", "État administratif", "Activité principale"]
site = "societe.com"
nombre_resultats = 10
log_level = logging.INFO


##############
##  Constantes
api_siret_endpoint = "https://entreprise.data.gouv.fr/api/sirene/v3/etablissements/"
proxy = 'https://pfrie-std.proxy.e2.rie.gouv.fr:8080'
options = webdriver.ChromeOptions()
options.add_argument('log-level=3')     # Passage en LOG_FATAL pour éviter d'être floodé par les handshake SSL échoués


#########
# Logging
logger = logging.getLogger('search_siret_on_google')
logger.setLevel(log_level)
formatter = logging.Formatter('%(levelname)s - %(message)s')
# File
log_file = __file__.replace(".py", ".log")
file_handler = logging.FileHandler(log_file, encoding = "utf8", mode = 'w')
file_handler.setLevel(log_level)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)
# Console
console_handler = logging.StreamHandler()
console_handler.setLevel(log_level)
console_handler.setFormatter(formatter)
logger.addHandler(console_handler)


#################
##  Fonctions  ##
#################
def get_siret(page_web, driver, logger):
    """ Récupération du n° de SIRET par deux tentatives, sinon siret = 'inconnu'. """
    logger.info(f"Scrapping de {page_web}")

    driver.get(page_web)            
    soup = BeautifulSoup(driver.page_source, features="html.parser")
    try:
        siret = soup.find_all("span", {"class": "copyNumber__copy"})[1].text
    except:
        logger.warning(f"[{id}] Pas de 'copyNumber__copy' dans la page. On essaie avec le n° de SIRET...")
        try:
            siret =  soup.find("td", text="N° de SIRET").find_next_siblings("td")[0].text
        except:
            logger.warning(f"[{id}] Pas de n° de SIRET trouvé dans la page. Valeur 'inconnu' affectée au siret.")
            siret = "inconnu"
    logger.info(f"N° SIRET : {siret}")

    return siret


def get_entreprise_info(siret, logger):
    """ Récupération des informations de l'entreprise par son SIRET avec l'API SIREN. """
    logger.info(f"Récupération des infos de l'entreprise [{siret}]")

    entreprise = {}
    proxy_dict = {'https' : proxy, 'http': proxy}
    req = api_siret_endpoint + siret
    logger.debug(f"Requête : {req}")

    try:
        r = requests.get(req, proxies=proxy_dict)
        r.raise_for_status()
        r = r.json()
    except requests.exceptions.HTTPError as err:
        logger.error(err)
        return None

    entreprise['tranche_effectifs'] = r['etablissement']['tranche_effectifs']
    entreprise['annee_effectifs'] = r['etablissement']['annee_effectifs']
    entreprise['etablissement_siege'] = r['etablissement']['etablissement_siege']
    entreprise['libelle_voie'] = r['etablissement']['libelle_voie']
    entreprise['code_postal'] = r['etablissement']['code_postal']
    entreprise['libelle_commune'] = r['etablissement']['libelle_commune']
    entreprise['date_debut'] = r['etablissement']['date_debut']
    entreprise['etat_administratif'] = r['etablissement']['etat_administratif']
    entreprise['activite_principale'] = r['etablissement']['activite_principale']

    logger.debug(f"Tranche effectifs : {entreprise['tranche_effectifs']}")
    logger.debug(f"Année effectifs : {entreprise['annee_effectifs']}")
    logger.debug(f"Établissement siège effectifs : {entreprise['etablissement_siege']}")
    logger.debug(f"Libellé voie : {entreprise['libelle_voie']}")
    logger.debug(f"Code postal : {entreprise['code_postal']}")
    logger.debug(f"Libellé commune : {entreprise['libelle_commune']}")
    logger.debug(f"Date début : {entreprise['date_debut']}")
    logger.debug(f"État administratif : {entreprise['etat_administratif']}")
    logger.debug(f"Activité principale : {entreprise['activite_principale']}")

    return entreprise


##############
###  MAIN  ###
##############
if __name__ == "__main__":
    ite_avec_siret = []

    # Lecture du fichier en entrée
    f = open(input_file, 'r', encoding='utf8')
    ite_sans_siret = f.readlines()
    f.close()

    browser = webdriver.Chrome(ChromeDriverManager().install(), options=options)

    for ite in ite_sans_siret[1:]:     # on ne prend pas en compte le header
        line = ite.replace('"', '').strip()
        id = line.split(',')[0]
        raison_sociale = line.split(',')[1]
        commune = line.split(',')[2]

        logger.info('-' * 80)
        logger.info(f"Traitement de l'ITE [{id}] {raison_sociale} à {commune}")
        
        req = raison_sociale + ' ' + commune + ' siret'
        logger.debug(f"Requête envoyée à Google : {req}")

        for urlpage in search(req, num_results = nombre_resultats, proxy = proxy):
            # Si on ne tombe pas sur le bon site, on passe au suivant
            if site not in urlpage:
                logger.warning(f"{urlpage} ne contient pas le site désiré [{site}]. On essaie le site suivant...")
                continue

            # Scrapping du site & appel à l'API SIREN
            siret = get_siret(urlpage, browser, logger)
            infos = get_entreprise_info(siret, logger)

            try:
                ite_avec_siret.append([id, raison_sociale, commune, urlpage, siret, infos['tranche_effectifs'], infos['annee_effectifs'], infos['etablissement_siege'], infos['libelle_voie'], infos['code_postal'], infos['libelle_commune'], infos['date_debut'], infos['etat_administratif'], infos['activite_principale']])
            except:
                logger.error(f"Une erreur n'a pas permis de récupérer les informations de l'entreprise [{siret}].")
                ite_avec_siret.append([id, raison_sociale, commune, urlpage, siret, '', '', '', '', '', '', '', '', ''])

            break   # pour stopper la boucle une fois qu'on est tombé sur le bon site

    browser.close()

    # Écriture du fichier en sortie
    with open(output_file, 'w', newline='', encoding = 'utf-8') as f:
        wr = csv.writer(f) 
        wr.writerow(output_cols)
        wr.writerows(ite_avec_siret)
    
    logger.info(f"Écriture de {len(ite_avec_siret)} lignes dans '{output_file}'.\n Fichier de log : {log_file}")
