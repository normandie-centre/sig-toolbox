"""
app.py - Application Dash pour la transformation de données NetCDF en csv pour IceTool

Description:
------------
Ce script implémente une application web interactive basée sur Dash. L'application permet de :
- Sélectionner un lieu en France sur une carte (via latitude et longitude)
- Télécharger un fichier CSV généré à partir des données traitées
- Téléverser une archive compressée contenant des fichiers NetCDF
- Traiter les fichiers NetCDF pour extraire les données météorologiques (RH, T2M et SSRD)

Utilisation:
------------
- Exécutez ce script pour lancer le serveur web local Dash : `python app.py`
- Accédez à l'application via `http://127.0.0.1:8050/icetool/` dans un navigateur web.
- Sélectionnez un lieu et téléversez vos données NetCDF pour lancer le traitement.

Dépendances:
------------
- Dash : pour la création de l'application web
- Dash Leaflet : pour la gestion des cartes interactives
- Flask : pour la gestion du serveur
- Pandas : pour la manipulation des fichiers CSV
- NetCDF4 : pour la lecture des fichiers NetCDF
- shutil, base64, os : pour la gestion des fichiers téléversés et des archives

Auteurs:
--------
- Thomas Escrihuela <thomas.escrihuela@cerema.fr>
- Teodolina Lopez <teodolina.lopez@cerema.fr>
- Erwan Vatan <erwan.vatan@cerema.fr>

Date:
-----
- 03/09/24
"""


##############
## Dépendances
import os
import base64
from shutil import rmtree

from dash import dash, html, Input, Output, State, dcc
import dash_bootstrap_components as dbc
import dash_leaflet as dl

from fill_weatherdata import write_weatherdata


##########
## Serveur
location = "/icetool/"
app = dash.Dash(__name__, title="Traitement des NetCDF pour IceTool", requests_pathname_prefix=location, external_stylesheets=[dbc.themes.MINTY, dbc.icons.BOOTSTRAP])
app.config.suppress_callback_exceptions = True


#############
## Constantes
INITIAL_COORDS = [49.4444, 1.1045]
UPLOAD_DIRECTORY = './upload'
WEATHERDATA_FILE = './WeatherData.csv'
WEATHERDATA_TEMPLATE = './WeatherData.csv.template'


#########
## Layout
app.layout = dbc.Container(fluid=True, style={"margin": "10px"}, children=[

    ## Header avec une image et un titre
    dbc.Row(dbc.Col(html.Header(html.Div([
        html.Img(src=f'{location}assets/icetool.png', width='500px'),
        html.H1('Traitement de données NetCDF pour IceTool'),
    ])), width={"size":8, "offset":2})),

    ## Avertissement
    dbc.Row(dbc.Col(dbc.Alert("⚠️ Cette interface a pour objectif de facililter la transformation de données NetCDF pour IceTool. Le traitement fonctionne uniquement avec des données NetCDF d'humidité relative (RH) ou température / radiation solaire (T2m / SSRD). Cette application est en cours de développement et peut rencontrer des bugs.🐞", color="warning"), width={"size":8, "offset":2})),

    ## Le contenu
    dbc.Row([

        ## Choix du lieu
        dbc.Col(html.Div([
            html.H3('🌐 Choisissez un lieu'),
                html.Label('Latitude :'),
                dcc.Input(id='latitude', type='text', value=INITIAL_COORDS[0]),
                html.Br(),
                html.Br(),
                html.Label('Longitude :'),
                dcc.Input(id='longitude', type='text', value=INITIAL_COORDS[1]),
            dl.Map([
                dl.TileLayer(),
                dl.Marker(position=INITIAL_COORDS, id='marker')
            ], id='map', center=INITIAL_COORDS, zoom=18),
            html.Br(),
        ]), width=6),

        ## Téléversement d'une archive
        dbc.Col(children=html.Div([
            html.H3('📁 Téléversez une archive'),
            dbc.Alert("Sélectionnez une archive au format zip ou tar contenant des données NetCDF d'extension .nc", color="info"),
            dcc.Upload(
                id='upload-data',
                children=dbc.Button('Upload archive (zip/tar)', outline=True, color="secondary", className="d-grid gap-2 col-5 mx-auto"),
                multiple=False,
                accept='.zip,.tar'
            ),
        ]), width=3),

        ## Affichage des résultats
        dbc.Col(html.Div([            
            html.H3('⚙️ Résultats'),
            dcc.Store(id='stored-file-path'),  # Ajout de dcc.Store pour stocker le chemin du fichier
            html.Div(id='output-data-upload', style={'display':'none'}),
            dcc.Loading(
                id="loading",
                children=html.Div(id='output-go'),
                type="circle",
            )
        ]), width=3)
    ]),

    ## Le footer
    dbc.Row(dbc.Col(html.Footer(html.Img(src=f'{location}assets/cerema_cropped.png'))))

], id='main')


############
## Callbacks

# Callback pour centrer la carte et le marqueur lorsque les inputs sont modifiés
@app.callback(
    [Output('map', 'center'), Output('marker', 'position')],
    [Input('latitude', 'value'), Input('longitude', 'value')]
)
def update_map_center(lat, lon):
    """
    Callback pour centrer la carte et le marqueur lorsque les inputs sont modifiés.

    Parameters
    ----------
    lat : str
        Valeur de latitude saisie par l'utilisateur.
    lon : str
        Valeur de longitude saisie par l'utilisateur.

    Returns
    -------
    center : list
        Nouvelle position du centre de la carte [lat, lon].
    position : list
        Nouvelle position du marqueur [lat, lon].
    """
    if lat and lon:
        try:
            # Convertir les valeurs en float pour les utiliser comme positions
            lat = float(lat)
            lon = float(lon)

            return [lat, lon], [lat, lon]
        except ValueError:
            # Si les valeurs ne sont pas valides, ne rien faire
            return dash.no_update, dash.no_update
    return dash.no_update, dash.no_update

# Callback pour téléverser un fichier
@app.callback(
    Output('output-data-upload', 'children'),
    Output('stored-file-path', 'data'),
    Input('upload-data', 'contents'),
    State('upload-data', 'filename'),
    prevent_initial_call=True  # Empêche l'appel initial non désiré
)
def save_uploaded_file(contents, filename):
    """
    Callback pour enregistrer le fichier uploadé sur le disque.

    Parameters
    ----------
    contents : str
        Contenu du fichier uploadé, sous forme d'une chaîne de caractères
        codée en base64.
    filename : str
        Nom du fichier uploadé.

    Returns
    -------
    output_children : html.Div or html.H5
        Rien si succès, message à afficher en cas d'erreur.
    file_path : str or None
        Chemin du fichier sauvegardé sur le disque, ou None si le fichier
        n'a pas été sauvegardé.
    """
    if contents is not None:

        ## Suppression du répertoire d'upload
        if os.path.exists(UPLOAD_DIRECTORY):
            rmtree(UPLOAD_DIRECTORY)
        os.makedirs(UPLOAD_DIRECTORY)

        # Décodage du contenu du fichier uploadé
        content_type, content_string = contents.split(',')
        decoded = base64.b64decode(content_string)

        # Vérification de l'extension du fichier
        if filename.endswith('.zip') or filename.endswith('.tar'):
            # Enregistrement du fichier sur le disque
            file_path = os.path.join(UPLOAD_DIRECTORY, filename)
            with open(file_path, 'wb') as f:
                f.write(decoded)

            return html.Div(style={'display': 'none'}), file_path
        else:
            return html.H5("Type de fichier non supporté."), None

        
# Callback pour lancer le traitement
@app.callback(
    Output('output-go', 'children'),
    Input('stored-file-path', 'data'),
    State('latitude', 'value'),
    State('longitude', 'value'),
    prevent_initial_call=True
)
def run(file, lat, lon):
    """
    Callback pour lancer le traitement du fichier uploadé.

    Parameters
    ----------
    file : str
        Chemin du fichier uploadé.
    lat : str
        Valeur de latitude saisie par l'utilisateur.
    lon : str
        Valeur de longitude saisie par l'utilisateur.

    Returns
    -------
    output_children : html.Div or html.H5
        Message et bouton de téléchargement du fichier si succès, message à afficher en cas d'erreur.
    """
    if os.path.exists(file):
        # Lancement du traitement
        if write_weatherdata(file, UPLOAD_DIRECTORY, WEATHERDATA_FILE, WEATHERDATA_TEMPLATE, [float(lon), float(lat)]):
            return html.Div([
                dbc.Alert("Écriture du fichier WeatherData.csv effectuée avec succès !", color="success"),
                dbc.Button("Télécharger le WeatherData", color="success", className="d-grid gap-2 col-6 mx-auto", id="download-button", outline=True),
                dcc.Download(id='download')
            ])
        else:
            return html.H5("Erreur lors du traitement de l'archive.")
    else:
        return html.H5(f"Le fichier {file} n'a pas été trouvé !")

# Callback pour le téléchargement du fichier de résultat
@app.callback(
    Output("download", "data"),
    Input("download-button", "n_clicks"),
    prevent_initial_call=True,
)
def func(n_clicks):
    """
    Callback pour le téléchargement du fichier WeatherData.csv.

    Parameters
    ----------
    n_clicks : int
        Nombre de clics sur le bouton de téléchargement.

    Returns
    -------
    dcc.send_file(WEATHERDATA_FILE) : send_file
        Fichier WeatherData.csv à télécharger.
    """
    return dcc.send_file(WEATHERDATA_FILE)


#######
## Main
if __name__ == '__main__':
    app.run_server(debug=True, port=8056)
