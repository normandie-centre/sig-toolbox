# -*- coding: utf-8 -*-
"""
Created on Thu Mar 21 11:35:12 2019

    Lecture du fichier C3S pour l'humidité des sols
    
@author: Teo
"""
###############################################################################
import numpy as np
from netcdf4icetool import read_C3S_RH
###############################################################################

#### Déclaration vecteurs ####
date = []

#### Déclaration des coordonnées [lon, lat] ####
# poi_coord = [0, 0]
poi_coord = [1.10459, 49.44441]
readfile = './Donnees/Humidite_relative_annee_2022.nc'
writepath = 'RH'


val_poi = np.array([])


#### Lecture fichiers - compléter les chemins de lectures et d'écritures####
#### Ajouter nom des fichiers####
readpath = ''
writefilename1 = '.txt'

lat, lon, dates_tmp, relathum = read_C3S_RH(readpath, readfile)
date = date + dates_tmp                                                            

###### Création des points d'extractions ######   
n = abs(lon - poi_coord[0]).argmin()
m = abs(lat -poi_coord[1]).argmin()
val_poi = relathum[:, m, n]
    
#### Création fichier texte ####
textfile = open(writepath + writefilename1,"w")
textfile.write("%s\n" % ('year\tmonth\tday\thour\tRH'))
data_str = ''
date_str = ''
for i, _ in enumerate(val_poi):
    date_str = (str(date[i][0]) + '\t' + str(date[i][1]) + '\t' + str(date[i][2]) + '\t' + str(date[i][3] + 1) + '\t')
    data_str += (date_str + str(val_poi[i]) + '\n')
data_str = data_str[:-2] 
textfile.write(data_str)
textfile.close()