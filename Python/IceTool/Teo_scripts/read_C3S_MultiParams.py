# -*- coding: utf-8 -*-
"""    
    Ce script prend en entrée un fichier NetCDF comprenant T2m et netsolarad et le transforme en .txt avec une ligne par journée.

@author: Teodolina, Thomas
"""

import netCDF4 as nc
import numpy as np
from datetime import datetime, timedelta


def read_C3S_MultiParam(fichier):
    dates, T2m, netsolarad = [], [], []

    f = nc.Dataset(fichier, 'r') #lecture du fichier

    # glo_attr = f.ncattrs() #attributs globaux
    attr_list = f.variables #attributs des variables. Toujours vérifier dans la fenêtre de commande 
                       #avec la ligne de commande : ncdump -h nom du fichier.
    # var_list = f.variables.keys()
    
    lat = np.array(attr_list['latitude']) #créer une matrice avec les valeurs de long
    lon = np.array(attr_list['longitude']) #créer une matrice aves les valeurs de lat
    temps = np.array(attr_list['time']) #créer une matrice avec les valeurs de temps
    
    # wolsolwater1_tmp= np.array(attr_list['swvl1'])                   
    # missing_value1 = attr_list['swvl1']._FillValue
    # wolsolwater1_tmp[wolsolwater1_tmp == missing_value1] = np.NaN
    
    # wolsolwater2_tmp= np.array(attr_list['swvl2'])                         
    # missing_value2 = attr_list['swvl2']._FillValue 
    # wolsolwater2_tmp[wolsolwater2_tmp == missing_value2] = np.NaN

    # wolsolwater3_tmp= np.array(attr_list['swvl3'])                         
    # missing_value3 = attr_list['swvl3']._FillValue
    # wolsolwater3_tmp[wolsolwater3_tmp == missing_value3] = np.NaN

    # wolsolwater4_tmp= np.array(attr_list['swvl4'])                         
    # missing_value4 = attr_list['swvl4']._FillValue
    # wolsolwater4_tmp[wolsolwater4_tmp == missing_value4] = np.NaN

    # soilT1_tmp= np.array(attr_list['stl1'])                         
    # missing_value5 = attr_list['stl1']._FillValue
    # soilT1_tmp[soilT1_tmp == missing_value5] = np.NaN

    # soilT2_tmp= np.array(attr_list['stl2'])                         
    # missing_value6 = attr_list['stl2']._FillValue
    # soilT2_tmp[soilT2_tmp == missing_value6] = np.NaN

    # soilT3_tmp= np.array(attr_list['stl3'])                         
    # missing_value7 = attr_list['stl3']._FillValue
    # soilT3_tmp[soilT3_tmp == missing_value7] = np.NaN

    # soilT4_tmp= np.array(attr_list['stl4'])                         
    # missing_value8 = attr_list['stl4']._FillValue
    # soilT4_tmp[soilT4_tmp == missing_value8] = np.NaN
    
    T2m_tmp = np.array(attr_list['t2m'])
    missing_value9 = attr_list['t2m']._FillValue                            
    T2m_tmp[T2m_tmp == missing_value9] = np.NaN
    T2m = T2m_tmp - 273.5 #conversion en °C
    
    # d2m_tmp= np.array(attr_list['d2m'])                         
    # missing_value10 = attr_list['d2m']._FillValue
    # d2m_tmp[d2m_tmp == missing_value10] = np.NaN
    
    # horizontalWind_tmp= np.array(attr_list['u10'])                         
    # missing_value11 = attr_list['u10']._FillValue 
    # horizontalWind_tmp[horizontalWind_tmp == missing_value11] = np.NaN

    # verticalWind_tmp= np.array(attr_list['v10'])                         
    # missing_value12 = attr_list['v10']._FillValue 
    # verticalWind_tmp[verticalWind_tmp == missing_value12] = np.NaN

    # fluxlat_tmp = np.array(attr_list['slhf'])
    # missing_value13 = attr_list['slhf']._FillValue                            
    # fluxlat_tmp[fluxlat_tmp == missing_value13] = np.nan
    # lheatflux = fluxlat_tmp/XXXXXXXX #exprimer en W m-2
    
    netsolarad_tmp = np.array(attr_list['ssrd'])
    missing_value14 = attr_list['ssrd']._FillValue                            
    netsolarad_tmp[netsolarad_tmp == missing_value14] = np.nan
    netsolarad = netsolarad_tmp/3600 #exprimer en W m-2 et divisé 
                                         # par 3600 s pour une accumulation sur 1h
    
    # sensibheatflux_tmp = np.array(attr_list['sshf'])                         
    # missing_value15 = attr_list['sshf']._FillValue
    # sensibheatflux_tmp[sensibheatflux_tmp == missing_value15]
    # sensibheatflux = sensibheatflux_tmp/XXXXXXXXXX #exprimer en W m-2
    
    # evap_tmp = np.array(attr_list['e'])
    # missing_value4 = attr_list['e']._FillValue                            
    # evap_tmp[evap_tmp == missing_value4] = np.nan
    # evap = (evap_tmp * 1000)/XXXXXXXXXXX #exprimer en mm
    
    # pontEvap_tmp = np.array(attr_list['pev'])                         
    # missing_value17 = attr_list['pev']._FillValue
    # pontEvap_tmp[PontEvap_tmp == missing_value17] = np.NaN
    
    # tp_tmp = np.array(attr_list['tp'])
    # missing_value18 = attr_list['tp']._FillValue                            
    # tp_tmp[tp_tmp == missing_value18] = np.NaN
    # tp = (tp_tmp * 1000) #exprimer en mm
    
    #Permet de convertir les valeurs de temps en date
    base = datetime(1900, 1, 1,0,0,0)
    for i in temps:
        tmp = base + timedelta(hours=i.item())
        dates.append([tmp.year,tmp.month,tmp.day,tmp.hour,0,0])

    return lat, lon, dates, T2m, netsolarad
