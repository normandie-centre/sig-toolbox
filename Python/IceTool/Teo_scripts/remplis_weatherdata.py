import pandas as pd

df = pd.read_csv("WeatherData.csv", delimiter = ";")
# print(df)
# exit()

rh = open("RH.txt","r")
rh_text = rh.readlines()
rh.close()
ssrd_t2m = open("ssrd_and_t2m.txt","r")
ssrd_t2m_text = ssrd_t2m.readlines()
ssrd_t2m.close()

for i, line in enumerate(rh_text):
    if i == 0:
        continue
    df.loc[i-1, "Dry Bulb Temperature [DegC]"] = ssrd_t2m_text[i].strip().split('\t')[-2]
    df.loc[i-1, "Global Horizontal Radiation [Wh/m2]"] = ssrd_t2m_text[i].strip().split('\t')[-1]
    df.loc[i-1, "Relative Humidity"] = rh_text[i].strip().split('\t')[-1]

df.to_csv("results.csv", index = False, sep = ",")
