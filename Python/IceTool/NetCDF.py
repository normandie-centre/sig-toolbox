"""
NetCDF.py - Classe de traitement des fichiers NetCDF pour extraire des données météorologiques

Description:
------------
Ce module définit une classe `NetCDF` pour lire, extraire et traiter les fichiers NetCDF contenant des données météorologiques. Il permet d'extraire les variables d'intérêt (telles que la température et l'humidité relative) à partir d'un fichier NetCDF pour un point d'intérêt géographique donné (latitude, longitude). Les données traitées sont ensuite stockées dans un dataframe pandas, et peuvent être exportées dans un fichier CSV.

Fonctionnalités principales:
----------------------------
1. Lecture et extraction des données d'un fichier NetCDF.
2. Détermination du type de fichier (T2m ou RH) et des variables contenues.
3. Conversion des données temporelles en dates exploitables.
4. Extraction des données pour des points spécifiques (coordonnées géographiques).
5. Traitement des variables spécifiques (conversion de la température en degrés Celsius, etc.).
6. Export des données extraites dans un fichier CSV.

Classes:
--------
- `NetCDF`: Représente un fichier NetCDF et contient des méthodes pour extraire et manipuler les données.

Méthodes:
---------
1. `__init__(self, nc_file, poi_coord)`: Initialise un objet NetCDF avec le fichier et les coordonnées du point d'intérêt.
2. `read_nc(self)`: Lit le fichier NetCDF et renvoie ses attributs sous forme de dictionnaire.
3. `get_type(self)`: Détermine le type de fichier NetCDF (T2m ou RH).
4. `get_variables(self)`: Récupère les variables du fichier NetCDF.
5. `compute_vars(self)`: Construit un dataframe à partir des données du point d'intérêt en convertissant les données temporelles et en traitant les variables.
6. `asdataframe(self)`: Retourne le dataframe contenant les données extraites du point d'intérêt.
7. `to_csv(self, output_file, sep=',', index=False, header=True)`: Exporte le dataframe dans un fichier CSV.

Utilisation:
------------
1. Initialisez un objet `NetCDF` avec le chemin vers le fichier NetCDF et les coordonnées du point d'intérêt (latitude, longitude).
2. Utilisez la méthode `asdataframe()` pour obtenir les données sous forme de dataframe pandas.
3. Utilisez la méthode `to_csv()` pour exporter les données dans un fichier CSV.

Dépendances:
------------
- datetime: pour la gestion des données temporelles.
- netCDF4: pour lire les fichiers NetCDF.
- numpy: pour les opérations numériques.
- pandas: pour la manipulation des dataframes et l'exportation en CSV.
- dataclasses: pour la gestion des classes de données.

Exemple d'utilisation:
----------------------
from NetCDF import NetCDF

# Initialisation
poi_coord = [1.10459, 49.44441]
nc_file = './Donnees/Humidite_relative_annee_2022.nc'
output_file = 'output.txt'

# Traitement
ncdf = NetCDF(nc_file, poi_coord)
ncdf.to_csv(output_file, sep='\t', index=False, header=True)

Auteurs:
--------
- Thomas Escrihuela <thomas.escrihuela@cerema.fr>
- Teodolina Lopez <teodolina.lopez@cerema.fr>
- Erwan Vatan <erwan.vatan@cerema.fr>

Date:
-----
- 03/09/24
"""

##############
## Dépendances
from datetime import datetime, timedelta
import netCDF4 as nc
import numpy as np
import pandas as pd
from dataclasses import dataclass


##############
##  Constantes
NC_VARIABLES = {
    'RH': [{'name': 'Relative Humidity', 'value': 'r'}],
    'T2m': [{'name': 'Dry Bulb Temperature [DegC]', 'value': 't2m'}, {'name': 'Global Horizontal Radiation [Wh/m2]', 'value': 'ssrd'}]
}


###########
##  Classes
@dataclass
class NetCDF():
    def __init__(self, nc_file, poi_coord):
        """
        Initialisation de la classe.

        Parameters
        ----------
        nc_file : str
            Chemin du fichier NetCDF à traiter.
        poi_coord : list of float
            Coordonnées du point d'intérêt [longitude, latitude].

        Attributs
        ---------
        nc_file : str
            Chemin du fichier NetCDF à traiter.
        poi_coord : list of float
            Coordonnées du point d'intérêt [longitude, latitude].
        attr_list : netCDF4.Dataset
            Attributs du fichier NetCDF.
        type : str
            Type du fichier NetCDF (T2m ou RH).
        variables : list of dict
            Dictionnaire des variables du fichier NetCDF :
                name : nom de la variable
                value : nom de la variable dans le fichier NetCDF
        var : pandas.DataFrame
            Dataframe contenant les données du point d'intérêt.
        """
        self.nc_file = nc_file
        self.poi_coord = poi_coord

        ## Lecture du fichier
        self.attr_list = self.read_nc()
        self.type = self.get_type()
        self.variables = self.get_variables()
        self.var = self.compute_vars()

    ## Fonctions
    def read_nc(self):
        """
        Lit le fichier NetCDF et renvoie un dictionnaire des attributs du fichier.

        Returns
        -------
        dict
            Attributs du fichier NetCDF.
        """
        nc_data = nc.Dataset(self.nc_file, 'r')
        attr_list = nc_data.variables

        return attr_list

    def get_type(self):
        """
        Retourne le type du fichier NetCDF (T2m ou RH).

        Returns
        -------
        str
            Type du fichier NetCDF (T2m ou RH), ou None si le fichier n'est pas reconnu.
        """
        if 't2m' in self.attr_list:
            type = 'T2m'
        elif 'r' in self.attr_list:
            type = 'RH'
        else:
            type = None

        return type

    def get_variables(self):
        """
        Retourne la liste des variables du fichier NetCDF.

        Returns
        -------
        list of dict
            Liste des variables du fichier NetCDF, où chaque dictionnaire contient
            les clés 'name' et 'value' correspondant au nom et à la valeur de la
            variable respective.
        """
        if self.type == 'RH':
            variables = NC_VARIABLES['RH']
        elif self.type == 'T2m':
            variables = NC_VARIABLES['T2m']
        else:
            variables = None

        return variables

    def compute_vars(self):
        ## Conversion des temps en date et création du dataframe
        """
        Conversion des temps en date et création du dataframe.

        Retourne un dataframe contenant les données du point d'intérêt, où l'index est la date, et les colonnes sont les variables demandées.
        
        Parameters
        ----------
        None
        
        Returns
        -------
        pandas.DataFrame
            Dataframe contenant les données du point d'intérêt.
        """
        dates = [datetime(1970, 1, 1) + timedelta(seconds=int(t)) for t in self.attr_list['valid_time']]
        df = pd.DataFrame(index=dates)
        df['month'] = [d.month for d in dates]
        df['day'] = [d.day for d in dates]
        df['hour'] = [d.hour + 1 for d in dates]

        ## Création des points d'extractions  
        lat = np.array(self.attr_list['latitude']) 
        lon = np.array(self.attr_list['longitude'])
        index_lat_min = abs(lat - self.poi_coord[1]).argmin()
        index_lon_min = abs(lon - self.poi_coord[0]).argmin()

        ## Extraction des données et traitement des variables
        for variable in self.variables:
            values, missing_value = self.attr_list[variable["value"]], self.attr_list[variable["value"]]._FillValue
            df[variable["name"]] = values[:, index_lat_min, index_lon_min]
            df.loc[df[variable["name"]] == missing_value, variable["name"]] = np.nan    # on remplace les valeurs manquantes par NaN

            ## Traitement des variables spécifiques
            if variable["name"] == 'Dry Bulb Temperature [DegC]':
                df[variable["name"]] -= 273.15
            elif variable["name"] == 'Global Horizontal Radiation [Wh/m2]':
                df[variable["name"]] = df[variable["name"]].diff().fillna(0)    # on soustrait les valeurs successives et on met 0 pour la première ligne
                df.loc[df[variable["name"]] < 0, variable["name"]] = 0
                df[variable["name"]] /= 3600

        return df

    def asdataframe(self):
        """
        Retourne le dataframe `var` contenant les données du point d'intérêt.

        Returns
        -------
        pandas.DataFrame
            Dataframe contenant les données du point d'intérêt.
        """
        return self.var

    def to_csv(self, output_file, sep=',', index=False, header=True):
        """
        Exporte le dataframe `var` dans un fichier CSV.

        Parameters
        ----------
        output_file : str
            Chemin du fichier CSV à écrire.
        sep : str, optional
            Séparateur de colonnes. Par défaut `,`.
        index : bool, optional
            Si True, exporte l'index du dataframe. Par défaut False.
        header : bool, optional
            Si True, exporte le header du dataframe. Par défaut True.
        """
        self.var.to_csv(output_file, sep=sep, index=index, header=header)


############
##  MAIN  ##
############
if __name__ == '__main__':
    poi_coord = [1.10459, 49.44441]
    # nc_file = './Donnees/Juillet_2022.nc'
    nc_file = './Donnees/Humidite_relative_annee_2022.nc'
    output_file = 'output.txt'

    ncdf = NetCDF(nc_file, poi_coord)
    with open(output_file, 'w') as textfile:
        ncdf.to_csv(output_file, sep='\t', index=False, header=True)