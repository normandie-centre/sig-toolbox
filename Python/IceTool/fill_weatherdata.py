"""
app.py - Script pour le traitement de fichiers NetCDF et la génération de WeatherData.csv

Description:
------------
Ce script permet d'extraire, traiter et fusionner des fichiers NetCDF pour générer un fichier CSV contenant des données météorologiques (`WeatherData.csv`). Il fonctionne en plusieurs étapes :
1. Extraction d'une archive contenant des fichiers NetCDF au format ZIP ou TAR.
2. Fusion des fichiers NetCDF avec un fichier de modèle de données météorologiques, selon des coordonnées spécifiques (latitude, longitude).
3. Concatenation des fichiers NetCDF mensuels (T2m) et annuels (RH) pour produire un fichier CSV final.

Fonctionnalités principales:
----------------------------
1. Extraction d'archives compressées au format ZIP ou TAR.
2. Lecture et traitement des fichiers NetCDF selon leur type (T2m ou RH).
3. Fusion des fichiers NetCDF avec les données existantes en utilisant des colonnes de jointure communes.
4. Génération d'un fichier CSV `WeatherData.csv` à partir des données traitées.

Utilisation:
------------
- Pour une exécution de ce script, définissez les chemins vers l'archive, le fichier de sortie CSV et le dossier temporaire pour le traitement des fichiers directement dans le main.
- Assurez-vous que les fichiers NetCDF se trouvent dans une archive compressée au format ZIP ou TAR.
- Exécutez le script depuis un terminal en spécifiant les chemins vers l'archive, le fichier de sortie CSV et le dossier temporaire pour le traitement des fichiers.
- Le fichier `WeatherData.csv` sera généré dans le répertoire de travail.

Dépendances:
------------
- pandas : pour la manipulation des fichiers CSV et la fusion des dataframes
- zipfile, tarfile : pour l'extraction des fichiers ZIP et TAR
- os : pour la gestion des chemins et des fichiers sur le système
- NetCDF : classe pour la lecture et la transformation des fichiers NetCDF
- shutil : pour la manipulation des répertoires temporaires

Exemple d'utilisation:
----------------------
$ python fill_weatherdata.py

Auteurs:
--------
- Thomas Escrihuela <thomas.escrihuela@cerema.fr>
- Teodolina Lopez <teodolina.lopez@cerema.fr>
- Erwan Vatan <erwan.vatan@cerema.fr>

Date:
-----
- 03/09/24
"""

##############
## Dépendances
import os
import zipfile
import tarfile
from NetCDF import NetCDF
import pandas as pd


#############
##  Fonctions
def extract_archive(archive_path, extract_to):
    """
    Décompresse une archive ZIP ou TAR.

    Parameters
    ----------
    archive_path : str
        Chemin de l'archive à décompresser.
    extract_to : str
        Chemin du dossier où placer les fichiers décompressés.
    """

    def extract_zip(zip_path, extract_to):
        """
        Décompresse un fichier ZIP.

        Parameters
        ----------
        zip_path : str
            Chemin du fichier ZIP à décompresser.
        extract_to : str
            Chemin du dossier où placer les fichiers décompressés.
        """
        with zipfile.ZipFile(zip_path, 'r') as zip_ref:
            zip_ref.extractall(extract_to)
        print(f"Décompressé : {zip_path} vers {extract_to}")

    def extract_tar(tar_path, extract_to):
        """
        Décompresse un fichier TAR ou TAR.GZ.

        Parameters
        ----------
        tar_path : str
            Chemin du fichier TAR à décompresser.
        extract_to : str
            Chemin du dossier où placer les fichiers décompressés.
        """
        with tarfile.open(tar_path, 'r:*') as tar_ref:
            tar_ref.extractall(extract_to)
        print(f"Décompressé : {tar_path} vers {extract_to}")
        # Si le dossier n'existe pas, on le crée
        if not os.path.exists(extract_to):
            os.makedirs(extract_to)

    if zipfile.is_zipfile(archive_path):
        extract_zip(archive_path, extract_to)
    elif tarfile.is_tarfile(archive_path):
        extract_tar(archive_path, extract_to)
    else:
        print(f"Format d'archive non supporté : {archive_path}")


def merge_df(wd_df, nc_df, join_cols=['month', 'day', 'hour']):
    """Fusionne deux df, en ne gardant les valeurs que du second df.

    Parameters
    ----------
    wd_df : pandas.DataFrame
        Le premier df qui contient les valeurs WeatherData.
    nc_df : pandas.DataFrame
        Le seond df qui contient les valers des données NetCDF.
    join_cols : list of str, optional
        Les colonnes de jointure. Par défaut ['month', 'day', 'hour'].

    Returns
    -------
    pandas.DataFrame
        Le df fusionné, avec seulement les valeurs de nc_df.
    """
    df = pd.merge(wd_df, nc_df, on=join_cols, how='left', suffixes=('_remove', ''))
    df.drop([col for col in df.columns if 'remove' in col], axis=1, inplace=True)

    return df

def write_weatherdata(archive, tmp_dir, weatherdata_file, weatherdata_template, poi_coord):
    ## Extraction de l'archive
    """
    Extraction d'une archive, lecture des fichiers NetCDF dedans, et écriture d'un fichier WeatherData.csv.

    Parameters
    ----------
    archive : str
        Chemin de l'archive à extraire.
    tmp_dir : str
        Chemin du dossier où extraire l'archive.
    weatherdata_file : str
        Chemin du fichier WeatherData.csv à écrire.
    weatherdata_template : str
        Chemin du fichier WeatherData.csv.template servant de base pour le WeatherData.csv à écrire.
    poi_coord : list of float
        Coordonnées du point d'intérêt [longitude, latitude].

    Returns
    -------
    bool
        True si succès, False sinon.
    """
    extract_archive(archive, tmp_dir)

    ## Traitement des fichiers
    list_t2m_df = []    # liste des dataframes des fichiers T2m (un par mois)
    wd = pd.read_csv(weatherdata_template, sep=';')
    for root, dirs, files in os.walk(tmp_dir):
        for file in files:
            if file.endswith('.nc'):
                nc_file = os.path.join(root, file)
                print(f'Traitement du fichier : {nc_file}')
                ncdf = NetCDF(nc_file, poi_coord)
                
                if ncdf.type == 'T2m':
                    list_t2m_df.append(ncdf.asdataframe())
                elif ncdf.type == 'RH':
                    wd = merge_df(wd, ncdf.asdataframe())   # fusion du fichier RH (un par an)

    t2m_df = pd.concat(list_t2m_df, ignore_index=True)  # concatenation des fichiers T2m
    wd_final = merge_df(wd, t2m_df)
    wd_final.to_csv(weatherdata_file, sep=';', index=False, header=True)

    return True


############
##  MAIN  ##
############
if __name__ == "__main__":
    poi_coord = [1.10459, 49.44441]
    weatherdata_file = './WeatherData.csv'
    weatherdata_template = './WeatherData.csv.template'
    archive = './archive.zip'
    tmp_dir = './tmp'

    write_weatherdata(archive, tmp_dir, weatherdata_file, weatherdata_template, poi_coord)
