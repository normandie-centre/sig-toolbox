"""
Librairie de gestion d'import et de traitement des données géographiques pour le tableau de bord PLF.
"""

import json
import dash_leaflet as dl
import dash_html_components as html


def get_color(json, feature):
    """Définit la couleur de l'entité."""

    # Si la donnée contient une clef "color", couleur unique pour toutes les entités
    if 'color' in json.keys():
        return json["color"]
    # Si la donnée contient une clef "colorscale", couleur en fonction d'un attribut (aplat de couleurs)
    elif 'colorscale' in json.keys():
        return json["colorscale"][feature['properties'][json["field"]]]
    else:
        return 'black'


def get_geom(feature):
    """
    Récupère la géométrie de l'entité.
    Type accepté : MultiPolygon, Polygon, LineString, Point.
    """

    if feature["geometry"]["type"] == 'MultiPolygon':
        return [list(reversed(r)) for p in feature['geometry']['coordinates'] for q in p for r in q]
    elif feature["geometry"]["type"] == 'Polygon':
        return [list(reversed(q)) for p in feature['geometry']['coordinates'] for q in p]
    elif feature["geometry"]["type"] == 'LineString':
        return [list(reversed(p)) for p in feature['geometry']['coordinates']]
    elif feature["geometry"]["type"] == 'Point':
        return list(reversed(feature['geometry']['coordinates']))
    else:
        return None


def compute_geojson(gjson, label1 = None, label2 = None, champ_svg = None):
    """
    Retourne un tableau d'entités géographiques avec infobulles et popup à partir d'un geojson.
    Si label1 et label2 sont définis, création d'une popup.
    Si champ_svg est défini et que la couche est de type ponctuel, représentation en svg des points en fonction des valeurs du champs <champ_svg> (les svg doivent se trouver dans le dossier static/.
    """

    geojson = json.load(open(gjson["path"],encoding='utf8'))


    if 'Polygon' in geojson["features"][0]["geometry"]["type"]:
        data = [
            dl.Polygon(
                positions=get_geom(feat),
                children=[
                    dl.Tooltip(gjson["prefix"] + str(feat['properties'][label1]) + " : " + str(feat['properties'][label2])) if label1 and label2 else '',
                    dl.Popup([html.P(k + " : " + str(v)) for k,v in feat["properties"].items()],maxHeight=300) if label1 else '',
                ],
                color=get_color(gjson,feat), weight=0.2, fillOpacity=gjson["opacity"], stroke=True
            ) for feat in geojson['features']
        ]
    elif 'Line' in geojson["features"][0]["geometry"]["type"]:
        data = [
            dl.Polyline(
                positions=get_geom(feat),
                children=[
                    dl.Tooltip(gjson["prefix"] + str(feat['properties'][label1]) + " : " + str(feat['properties'][label2])) if label1 and label2 else '',
                    dl.Popup([html.P(k + " : " + str(v)) for k,v in feat["properties"].items()],maxHeight=300),
                ],
                color=get_color(gjson,feat),opacity=gjson["opacity"], weight=gjson["weight"]
            ) for feat in geojson['features']
        ]
    elif 'Point' in geojson["features"][0]["geometry"]["type"]:
        if champ_svg:
            data = [
                dl.Marker(
                    position=get_geom(feat),
                    icon={
                        "iconUrl" : '/static/' + feat["properties"][champ_svg] + '.svg',
                        "iconSize": [20, 20],
                    },
                    children=[
                        dl.Tooltip(gjson["prefix"] + str(feat['properties'][label1]) + " : " + str(feat['properties'][label2])) if label1 and label2 else '',
                        dl.Popup([html.P(k + " : " + str(v)) for k,v in feat["properties"].items()],maxHeight=300)
                    ], 
                    opacity=gjson["opacity"]
                ) for feat in geojson['features']
            ]
        else:
            data = [
                dl.CircleMarker(
                    center=get_geom(feat),
                    radius=5,
                    children=[
                        dl.Tooltip(gjson["prefix"] + str(feat['properties'][label1]) + " : " + str(feat['properties'][label2])) if label1 and label2 else '',
                        dl.Popup([html.P(k + " : " + str(v)) for k,v in feat["properties"].items()],maxHeight=300)
                    ], 
                    fillOpacity=gjson["opacity"], color='black', weight=0.3, fillColor=get_color(gjson,feat)
                ) for feat in geojson['features']
            ]

    return data
