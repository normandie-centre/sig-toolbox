## Données de base

Les données de base sont les EPCI, Installations Classées, Zones d'activité et d'intérêt et Friches. Seules sont prises celles inclues dans la zone d'étude. 

### EPCI

Les établissements publics de coopération intercommunale (EPCI) sont des regroupements de communes ayant pour objet l'élaboration de « projets communs de développement au sein de périmètres de solidarité ». Ils sont soumis à des règles communes, homogènes et comparables à celles de collectivités locales. Les communautés urbaines, communautés d'agglomération, communautés de communes, syndicats d'agglomération nouvelle, syndicats de communes et les syndicats mixtes sont des EPCI.

**Emprise :** complète  
**Fraîcheur des données :** 2020   
**Source :** IGN


### Aires Logistiques

__Par entrepôt ou plate-forme logistique (EPL),__ on entend tout espace dédié à l’entreposage, ainsi qu’aux opérations logistiques telles que la préparation de commandes, le conditionnement, la réception ou l’expédition de marchandises, etc. Si la grande majorité des EPL sont des bâtiments fermés, certains peuvent être à air libre. C’est le cas, par exemple, des entrepôts de véhicules ou de matériaux de construction. Enfin, un EPL peut aussi être situé dans un établissement dont l’activité principale n’est pas la logistique : par exemple, un entrepôt qui alimente une usine située sur le même site.

__Une aire logistique, dite dense,__ est un espace dans lequel chaque EPL de plus de 5 000 m² qui le compose est distant de moins de deux kilomètres d’un autre EPL. Chaque aire logistique est composée d’au moins trois EPL de plus de 5 000 m².

#### Variables

Ces informations sont disponibles au clic :  
  \- *Identifiant aire logistique dense (e1)* : Correspond à l'identifiant de l'aire logistique dans le fichier aires_logistiques_denses.shp  
  \- *Région d'implantation* :	Code officiel de la région dans laquelle est implantée l'aire logistique  
  \- *Numéro aires dans région* : Numéro de l'aire logistique tel qu'il apparait sur les cartes et les tableaux du document pdf "Atlas des entrepôts et des aires logistiques en France en 2015"  
  \- *Communes concernées par l'aire logistique* :	Liste des communes sur lesquelles l'aires logistique est implantée.  
  \- *Nombre d'EPL de plus de  5 000 m2* :	Nombre d'EPL de plus de  5 000 m2 présents dans l'aire logistique en 2015  
  \- *Surface totale* : Surface totale d'entreposage des EPL de plus de 5000 m2 en 2015  
  \- *P_Transport_et_entreposage* : Répartition des EPL selon l'activité des entreprises exploitanes (en %) en 2015 : Secteur Transport et entreposage  
  \- *P_commerce* : Répartition des EPL selon l'activité des entreprises exploitanes (en %) en 2015 : Secteur Commerce  
  \- *P_industrie* : Répartition des EPL selon l'activité des entreprises exploitanes (en %) en 2015: Secteur industrie manufacturière  
  \- *P_autres* : Répartition des EPL selon l'activité des entreprises exploitanes (en %) en 2015 : Autres secteurs d'activité  
  \- *Eff_com_entreposage* : Effectifs salariés des communes accueillant des aires logsitiques dans les professions de l'entreposage et de la manutention au 31/12/2014  
  \- *EFF_EPL_5000* : Effectifs salariés de l'aire logistique dans les EPL de plus de 5 000 m2 dans des professions de l'entreposage et de la manutention au 31/12/2014  
  \- *Poids de l'entreposage* : Poids des professions de l'entreposage et la manutention dans les effectifs salariés totaus dans les commaunes accueillant l'aire logistique  
  \- *Chargement* : Poids des communes accueillant l'aire logistique dans le nombre de chargements de poids lourds de la région (en ‰), moyenne 2011-2015  
  \- *Déchargement* : Poids des communes accueillant l'aire logistique dans le nombre de déchargements de poids lourds de la région (en ‰), moyenne 2011-2015  

Des données statistiques des aires logistiques par région sont disponibles [dans ce rapport](/static/datalab-14-atlas-entrepots-aires-logistiques.pdf).
**Emprise :** complète  
**Fraîcheur des données :** 2015  
**Source :** SOeS, Cerema [site](https://www.statistiques.developpement-durable.gouv.fr/atlas-des-entrepots-et-des-aires-logistiques-en-france-en-2015)


### ICPE : Installations Classées

Sont représentées les ICPE issues de [Géorisques](https://www.georisques.gouv.fr/donnees/bases-de-donnees/installations-industrielles) comportant les libellés NAF suivants :  
\- Commerce de détail de carburants en magasin spécialisé  
\- Raffinage du pétrole  
\- Extraction de pétrole brut  
\- Affrètement et organisation des transports  
\- Transports routiers de fret de proximité  
\- Transports routiers de fret interurbains  
\- Transport ferroviaire interurbain de voyageurs  
\- Transports urbains et suburbains de voyageurs  
\- Messagerie, fret express  

**Emprise :** complète  
**Fraîcheur des données :** 05/20


### ITE : Installations terminales embranchées

Les Installations Terminale Embranchées sont des voies ferrées privatives desservant un chargeur.
Le fichier, issu de la base ITE300 produite par le Cerema, recense ces installations sur toute la France par une enquête téléphonique individuelle auprès des chargeurs :
\- La date indiquée est celle d'administration du questionnaire  
\- Réceptionne des marchandises : Indique si le chargeur reçoit des marchandises _tous modes de transport_  
\- Expédie des marchandises : Indique le chargeur expédie des marchandises _tous modes de transport_  
\- Marchandises recues tous modes de transport (NST) : Type de marchandise reçue selon la nomenclature NST _tous modes de transport_  
\- Marchandises expédiées tous modes de transport (NST) : Type de marchandise reçue selon la nomenclature NST _tous modes de transport_  
\- Utilisation de l'ITE : Indique si l'ITE est utilisée ou non.  

Si l'ITE n'est plus utilisée :  
\- Etat ITE : Indique l'état physique de l'ITE selon 4 valeurs :Neuf, Bon état, Mauvais état et inutilisable.  
* Neuf : l'ITE est neuve, même si elle n'est pas utilisée
* Bon état : utilisable immédiatement en l'état
* Mauvais état : réutilisable moyennant quelques frais de remise à niveau
* Inutilisable : L'iTE n'est pas réutilisable (dépose de l'aiguille, dépose des voies par exemple)
* Si l'ITE est utilisée, le champ n'est pas complété  
   
\- Quelle est l'année d'arrêt des trains : Indique la date de l'arrêt de circulations ferroviaires sur l'ITE  
\- Projet de réutiliser le train : Indique si l'interlocuteur avait des projets de réutiliser l'ITE à court ou moyen terme.  

Si l'ITE est toujours utilisée :  
\- Marchandises en réception par trains (NST) : Type de marchandise reçue selon la nomenclature NST _par mode ferroviaire_  
\- Marchandises en expédition par trains (NST) : Type de marchandise expédié selon la nomenclature NST _par mode ferroviaire_  
\- Evolution ferroviaire depuis 2010 : L'interlocuteur indique comment le trafic ferroviaire a évolué depuis 2010 sur l'ITE.  
\- Evolution ferroviaire dans le futur : L'interlocuteur indique quelle est son estimation d'évolution du trafic ferroviaire dans les 5 prochaines années.   
\- Nombre de voies sur site : Indique le nombre de voies utilisables sur l'ITE.  
\- Longueur de la voie la plus longue : Indique la longueur de la plus longue voie sur l'ITE.  
  

**Emprise :** complète  
**Fraîcheur des données :** 2015-2019   
**Source :** Cerema

### Quais

Sont représentés les quais issus de la BD Topo IGN. Plusieurs ouvrages sont concernés par cette base, tous ne permettent pas de réaliser des opérations logistiques :

\- Muraille en maçonnerie ou enrochement, élevée le long d’un cours d’eau pour retenir les berges, pour empêcher les débordements. 
\- Rivage d’un port aménagé pour l’accostage des bateaux. 
\- Ouvrage en maçonnerie s’opposant à l’écoulement des eaux marines ou fluviales.

**Emprise :** complète  
**Source :** IGN

### Bretelle d'accès au reseau routier

Sont représentés par les bretelles les Voies d'entrée ou de sortie permettant la communication entre 2 routes qui sont le plus souvent deniveaux différents : 
\- Bretelles de liaison ou d’échangeur 
\- Bretelle d'accès à une 'Aire de repos ou de service'.

**Emprise :** complète  
**Source :** IGN

### ZAI : Zones d'activité et d'intérêt

Sont considérées les zones d'activité et d'intérêt de la BDTopo de l'IGN de catégorie "Industriel et commercial"

**Emprise :** complète  
**Fraîcheur des données :** 10/2019


### Friches

Ces données sont issues d'une campagne de recensement des friches par l'EPF de Normandie. Pour en savoir plus, lire la [synthèse de l'EPF](/static/EPF_friches_synthese.pdf).

**Emprise :** Haute-Normandie  
**Fraîcheur des données :** 2015 

### Tâches Urbaines
