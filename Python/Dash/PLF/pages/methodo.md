## Méthodologie

### Objectif

Ce tableau de bord a pour but de visualiser les avancées cartographiques du projet de **Plaforme Logistique du Futur**. Développé par le Cerema, il permet la visualisation des principales données géographiques utilisées dans l'analyse d'un site propice à l'accueil d'une future plateforme logistique à rayonnement international.

La zone d'étude est constituée de l'axe Seine et son environnement + le littoral normand jusqu'à Cherbourg. Une analyse territoriale multi-scalaire sur un territoire aussi vaste nécessite de préalablement qualifier le potentiel logistique à une échelle plus fine. Le Cerema propose ainsi la création de Zones d'Activités Logistiques comme maillon d'études de calcul du potentiel logistique.

Toutes les informations des bases de données sont accessibles en cliquant sur les entités.

### Construction des Zones d'Activités Logistiques (ZAL)

Les Zones d'Activité Logistiques sont calculées à partir des données de base suivantes :  
  \- des Aires Logistiques denses du Cerema (voir définition dans l'onglet **Données**)  
  \- des Zones d'Activité industrielles et commerciales de la BDTOPO de l'IGN.

Les ZAL correspondent à l'enveloppe concave formée par une aire logistique et ses zones d'activité l'intersectant. Ces zones constituent la plus petite entité sur lesquelles seront calculés les différents indicateurs de potentialité logistique. Elles sont au nombre de **40** sur le périmètre d'étude.

Pour prendre en compte l'environnement proche des ZAL, des tampons de 500m, 1km et 2km sont calculés et représentés et permettent d'analyser la desserte logistique de chaque site (proche d'une ITE, d'une bretelle d'autoroute, ...).

### Calcul des indicateurs

<WIP>


