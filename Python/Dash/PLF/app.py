#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Création d'un tableau de bord Python avec la librairie Dash pour le projet d'implantation d'une Plateforme Logistique du Futur.

Ce tableau de bord permet une visualisation cartographique des indicateurs calculés pour identifier un territoire propice au développement d'une zone logistique à rayonnement national, ainsi que les informations sur la méthodologie mise en place et les données récupérées.


Usage :
-------
    python app.py

"""

__author__ = ("Thomas Escrihuela", "Alexis Vernier", "Yohan Urie")
__contact__ = ("Thomas.Escrihuela@cerema.fr", "Alexis.Vernier@cerema.fr", "Yohan.Urie@cerema.fr")
__version__ = "1.1"
__date__ = "2020/08"


############
# Librairies
import os
import json
import flask
import dash
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output, State
import dash_leaflet as dl
import dash_leaflet.express as dlx
from modules.geodata import get_color, get_geom, compute_geojson     


######################################
# Paramètres des données géographiques
input_data = {
    'zal': {
        'path': './data/ZAL.geojson',
        'prefix': '[ZAL]',
        'color': 'brown',
        'opacity': 0.8
    },
    'tache_urbaine': {
        'path': './data/TacheUrbaine.geojson',
        'color': 'grey',
        'opacity': 0.2,
    },
    'macro_zal': {
        'path': './data/macroZAL.geojson',
        'prefix': '[macroZAL]',
        'color': '#2b83ba',
        'opacity': 0.3
    },
    'arretsTC': {
        'path': './data/arretsTC.geojson',
        'color': 'blue',
        'opacity': 0.5
    },
    'zal_tampons': {
        'path': './data/ZAL_tampons.geojson',
        'field' : 'tampon',
        'colorscale': {
            500 : '#d7191c',
            1000 : '#fec980',
            2000 : '#2b83ba'
        },
        'opacity': 0.3
    },
    'icpe': {
        'path': './data/ICPE.geojson',
        'prefix': '[icpe] ',
        'opacity': 0.8
    },
    'bretelle_autoroute': {
        'path': './data/bretelle_autoroute.geojson',
        'prefix': '[bretelle_autoroute] ',
        'opacity': 1,
        'weight': 1.5,
        'color': '#f59636'
        
    },  
    'quai': {
        'path': './data/quai.geojson',
        'prefix': '[quai] ',
        'opacity': 1,
        'weight': 1.5,
        'color': 'brown'
        
    },    
    'ite': {
        'path': './data/ITE.geojson',
        'prefix': '[ite] ',
        'opacity': 0.5,
        'field' : 'Etat ITE',
        'colorscale': {
            'Neuf' : '#2b83ba',
            'Bon' : '#abdda4',
            'Mauvais' : '#fdae61',
            'Inutilisable' : '#d7191c',
            '': '#cb0df5',
            None: '#cb0df5'
        },
    },
    'zai': {
        'path': './data/ZAI.geojson',
        'color': 'green',
        'prefix': '[zai] ',
        'opacity': 0.4
    },
    'epci': {
        'path': './data/EPCI.geojson',
        'color': 'red',
        'prefix': '[EPCI] ',
        'opacity': 0
    },
    'seine': {
        'path': './data/seine.geojson',
        'color': 'blue',
        'opacity': 0.8,
        'weight': 0.7
    },
    'aires_logistiques': {
        'path': './data/aires_logistiques_denses.geojson',
        'color' : 'blue',
        'prefix': '[Aires logistiques] ',
        'opacity': 0.5
    }
}

fond = {
    "Positron": "https://cartodb-basemaps-c.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png",
    "Ortho": "https://wxs.ign.fr/choisirgeoportail/geoportail/wmts?&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=ORTHOIMAGERY.ORTHOPHOTOS&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}",
    "Scan-express": "https://wxs.ign.fr/choisirgeoportail/geoportail/wmts?&REQUEST=GetTile&SERVICE=WMTS&VERSION=1.0.0&STYLE=normal&TILEMATRIXSET=PM&FORMAT=image/jpeg&LAYER=GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD&TILEMATRIX={z}&TILEROW={y}&TILECOL={x}",
}


#####################
# Création du serveur
server = flask.Flask(__name__)
app = dash.Dash(__name__, server=server, title='Parc Logistique du Futur')
app.config.suppress_callback_exceptions = True


####################
# Import des geojson
zal_json = compute_geojson(input_data["zal"],"Lib_ZAL","TypEnvUrb")
tache_urbaine_json = compute_geojson(input_data["tache_urbaine"])
macrozal_json = compute_geojson(input_data["macro_zal"],"Libellé MacroZAL ","Nombre ZAL")
arretsTC_json = compute_geojson(input_data["arretsTC"],"name")
zal_tampons_json = compute_geojson(input_data["zal_tampons"])[::-1]       ## reverse de la liste pour que les faibles tampons soient au-dessus des autres
epci_json = compute_geojson(input_data["epci"])
seine_json = compute_geojson(input_data["seine"])
icpe_json = compute_geojson(input_data["icpe"],"Libellé NAF","Nom entreprise", "svg")
bretelle_autoroute_json = compute_geojson(input_data["bretelle_autoroute"])
quai_json = compute_geojson(input_data["quai"],"Nom du lieu","Nature détaillée")
ite_json = compute_geojson(input_data["ite"],"Raison sociale", "Etat ITE")
zai_json = compute_geojson(input_data["zai"],"Nature de la ZAI","Nom du lieu")
aires_logistiques_json = compute_geojson(input_data["aires_logistiques"],"Identifiant aire logistique dense (e1)","Communes concernées par l'aire logistique")


##########
### Layout
app.layout = html.Div(
    id='page',
    children=[
        dcc.Location(id='url', refresh=False),
        html.Div([
            html.H1('PARC LOGISTIQUE DU FUTUR - Approche SIG',className='logo'),
            # Menu
            html.Ul([
                html.Li(id='carto',
                    className='current_page_item',
                    children=html.P('Cartographie')),
                html.Li(id='methodo',
                    children=html.P('Méthodologie')),
                html.Li(id='data',
                    children=html.P('Données')),
            ],className='menu')
        ],className='header'),
        html.Div(id='page-content', className='content'),   # sera surchargé par les pages de l'application
    ]
)

##############
### Page Carto
carto_layout = html.Div([
    # Carte
    dl.Map(id='map', center=[49.3, 0], zoom=8, children=[
        # dl.LocateControl(options={'locateOptions': {'enableHighAccuracy': True}}),
        dl.LayerGroup(id='epci',children=epci_json),
        dl.LayerGroup(id='seine',children=seine_json),
        dl.LayersControl(
            [dl.BaseLayer(dl.TileLayer(id='map_tile',url=fond[key],opacity=0.8), name=key, checked= key=="Positron") for key in fond]
            +
            [
                dl.Overlay(dl.LayerGroup(id='aires_logistiques',children=aires_logistiques_json), name="Aires logistiques", checked=False),
                dl.Overlay(dl.LayerGroup(id='zai',children=zai_json), name="ZAI", checked=False),
                dl.Overlay(dl.LayerGroup(id='tache_urbaine',children=tache_urbaine_json), name="Tâches Urbaines", checked=False),
                dl.Overlay(dl.LayerGroup(id='zal_tampons',children=zal_tampons_json), name="Tampons ZAL", checked=False),
                dl.Overlay(dl.LayerGroup(id='macro_zal',children=macrozal_json), name="Macro ZAL", checked=True),
                dl.Overlay(dl.LayerGroup(id='zal',children=zal_json), name="ZAL", checked=True),
                dl.Overlay(dl.LayerGroup(id='icpe',children=icpe_json), name="ICPE", checked=True),
                dl.Overlay(dl.LayerGroup(id='bretelle_autoroute',children=bretelle_autoroute_json), name="Bretelle autoroutière", checked=True),
                dl.Overlay(dl.LayerGroup(id='quai',children=quai_json), name="Quai", checked=True),
                dl.Overlay(dl.LayerGroup(id='arretTC',children=arretsTC_json), name="Arrêts TC", checked=False),
                dl.Overlay(dl.LayerGroup(id='ite',children=ite_json), name="ITE", checked=True),
            ]
        )
    ], style = { 'width': '100%', 'height': '100%', 'position': 'absolute', 'opacity': '0.9'})
])

##############
### Page Infos
infos_page = open('./pages/methodo.md', 'r', encoding='utf8') 
infos_layout = html.Div([
    dcc.Markdown(infos_page.read())
], className='methodo')

##############
### Page Data
data_page = open('./pages/data.md', 'r', encoding='utf8') 
data_layout = html.Div([
    dcc.Markdown(data_page.read())
], className='data')


###################
# Routes & Callback

# Callback du Menu
@app.callback([Output('page-content', 'children'), Output('carto','className'), Output('methodo','className'),Output('data','className')],
[Input('carto', 'n_clicks'),Input('methodo', 'n_clicks'),Input('data', 'n_clicks')])
def showPage(carto,methodo,data):
    """"Change de page lors du clic sur les items du menu."""
    ctx = dash.callback_context 
    if not ctx.triggered:
        return carto_layout, 'current_page_item', '', ''
    else:
        item_name = ctx.triggered[0]['prop_id'].split('.')[0]
        if 'carto' in item_name:
            return carto_layout, 'current_page_item', '', ''
        elif 'methodo' in item_name:
            return infos_layout, '', 'current_page_item', ''
        elif 'data' in item_name:
            return data_layout, '', '', 'current_page_item'
        else:
            raise 'Erreur'


######
# Main
if __name__ == '__main__':
    app.run_server(debug=True)

