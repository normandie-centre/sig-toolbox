# Parc Logistique du Futur

## Objectif

Ce tableau de bord a pour but de montrer les avancées de l'approche SIG du projet de Plateforme Logistique du Futur. L'application contient la méthodologie ainsi qu'une interface cartographique avec les données et indicateurs utilisés pour déterminer le territoire le plus propice à accueillir une plateforme logistique d'envergure nationale.

## Visualisation

Le dashboard est accessible sur https://carto-plf.herokuapp.com/

*Attention :* Appli non scalable, peut rencontrer des dysfonctionnements.

:warning _Work in Progress_

## Déploiement

### Déploiement en local

Le script **app.py** peut être lancé par Python pour déployer le tableau de bord interactif en local après avoir au préalable installé les librairies nécessaires à son fonctionnement :

```
$ cd PLF/
$ virtualenv -p /usr/bin/python3 venv
$ source venv/bin/activate
$ pip install -r requirements.txt
$ python app.py
Dash is running on http://127.0.0.1:8050/
```

### Déploiement sur Internet

Le tableau de bord est déployé sur la plateforme Heroku. Il faut au préalable avoir [installé et configuré heroku](https://devcenter.heroku.com/articles/git#prerequisites-install-git-and-the-heroku-cli) pour associer le contenu du dossier **PLF/** à la plateforme heroku _carto-plf_ . Une remote *heroku* est notamment ajoutée au dépôt.
À chaque modification du tableau de bord, un commit + push permet de mettre à jour l'interface web :

```
$ git commit -a -m 'modification du tableau de bord'
$ git push heroku master
```

Le tableau de bord est visible sur https://carto-plf.herokuapp.com/

