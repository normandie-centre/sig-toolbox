import os 

from dash import dash, html, Input, Output, State, dcc
import flask
import geojson
import requests

from data import indic, liste_dpts, departements


##########
## Serveur
location = "/urbansimul/"
server = flask.Flask(__name__)
app = dash.Dash(__name__, server=server, title="Récupérer les données d'Urbansimul", url_base_pathname=location)
app.config.suppress_callback_exceptions = True


#############
## Constantes
api_us_url = 'https://urbansimul-app.cerema.fr/map/api/exportData/prepare'
headers = {
    'Content-Type': 'application/json',
    'Cookie': os.environ['US_COOKIE'],
}


#########
## Layout
app.layout = html.Div(children=[
    html.Header(html.Img(src='assets/us.png')),
    html.Div(children=[
        html.H1('Récupération de données depuis Urbansimul'),
        html.Label('Sélectionnez une donnée disponible : '),
        dcc.Dropdown(list(indic.keys()), id='cb_data', placeholder='Sélection de la donnée'),
        html.Br(),
        html.Label('Sélectionnez un département : '),
        dcc.Dropdown(liste_dpts, id='cb_dpts', placeholder='Sélection du département'),
        html.Button('Télécharger la donnée', id='button', n_clicks=0),
        dcc.Loading(
            id="loading-2",
            children=[dcc.Download(id='dl_link')],
            type="circle",
        ),
    ]),
    html.Div(style={'margin':'100px'}),
    html.Footer(html.Img(src='assets/cerema.png')),
], id = 'main')


###########
## Callback
@app.callback(
    Output('dl_link', 'data'),
    Input('button', 'n_clicks'),
    State('cb_data', 'value'), 
    State('cb_dpts', 'value'),
    prevent_initial_call=True
)
def update_output(n_clicks, cb_data_value, cb_dpts_value):

    numero_dpt = cb_dpts_value.split('-')[0].strip()
    nom = f"{cb_data_value} du {cb_dpts_value}"

    # On rajoute la géométrie du département séléctionné pour intersection lors de l'appel d'UrbanSimul
    geom_dpt = [dpt for dpt in departements if dpt["properties"]["INSEE_DEP"] == numero_dpt][0]["geometry"]
    payload = indic[cb_data_value]
    payload["geometry"] = geom_dpt

    rep = requests.post(api_us_url, data=geojson.dumps(payload), headers=headers).json()
    try:
        url_callback = rep["url"]
        print(url_callback)
    except:
        print(rep["message"])
        exit()

    r = requests.get(url_callback, headers=headers, stream=True).json()
    return dict(content=geojson.dumps(r), filename=f"{nom}.geojson")


#######
## Main
if __name__ == '__main__':
    app.run_server(debug=True, port=8051)
