import fiona
from os.path import dirname

indic = {
    "Bâti" : {
        "sourceId":"bati",
        "mapboxFilter":["all",["in",["get","typ_bati"],["literal",["dur","01","léger","02"]]]]
    },
    "Bâti apparu depuis 2010" : {
        "sourceId":"bati",
        "mapboxFilter":["all",[">=",["to-number",["get","annee_construction"]],2010],["in",["get","typ_bati"],["literal",["dur","01","léger","02"]]]]
    },
    "Dispositifs d'aménagement" : {
        "sourceId":"amenagement",
        "mapboxFilter":["all",["in",["get","typ_zone"],["literal",["info_zac","info_02_zac","prescri_45_ZAC","info_afu","info_39_afup","info_pae","info_13_pae","info_pcvd","lotissement","Perimetre_Amenagement_SELA","prescri_OAP","prescri_18_OAP","prescri_1801_OAP","prescri_1802_OAP","prescri_1806_OAP","prescri_1807_OAP","avap","sup_ac4_zppaup_avap","sup_ac4bis_pvap","prescri_log_mix_sociale","info_pae","info_13_pae","info_pup","info_30_pup","info_tamajore","info_32_sect_ta_majore","info_oin","info_34_oin","info_ort","info_sursis_statuer","prescri_dens_max","info_12_sursis_statuer","prescri_12_ER_PAPAG","info_zad","info_05_zad","egf_collectivité","egf_maitrise","epf_maitrise","egf_la_Roche","info_0401_preem_urbain_renforce","anru","qpv","zus"]]],["==",["geometry-type"],"Polygon"]]
    },
    "Enjeux" : {
        "sourceId":"contraintes",
        "mapboxFilter":["all",["in",["get","niv_cont"],["literal",[1]]],["in",["get","typ_cont"],["literal",["R","P","B","E","U","XU"]]],["==",["geometry-type"],"Polygon"]]
    },
    "Friches" : {
        "sourceId":"cartofriche",
        "mapboxFilter":None
    },
    'Gisements fonciers potentiels' : {
        "sourceId":"uf_dispo",
        "mapboxFilter":["all",[">=",["to-number",["get","surf_ufzon"]],10],["in",["get","nonbati"],["literal",["oui"]]],["in",["get","dispo"],["literal",["mobilisable terrain nu","reserve fonciere nue","mobilisable activite terrain nu","reserve fonciere activite nue","engagé"]]],["==",["geometry-type"],"Polygon"]]
    },
    'Logements vacants' : {
        "sourceId":"log_vac",
        "mapboxFilter":["all",["in",["get","typo_vac"],["literal",["1","8","12","2","13","3"]]],[">=",["to-number",["get","dureevacance"],None],["to-number",3,None]]]
    },
    "PLU" : {
        "sourceId":"plu",
        "mapboxFilter":["all",["\u0021",["in",["get","zone_harm"],["literal",[]]]],["\u0021",["in",["get","zone_harm"],["literal",[]]]],["==",["geometry-type"],"Polygon"]]
    },
    "UF" : {
        "sourceId":"uf",
        "mapboxFilter":["all",["==",["geometry-type"],"Polygon"]]
    },
    "UF avec vacance résiduelle" : {
        "sourceId":"uf_vacant",
        "mapboxFilter":["all",[">=",["to-number",["get","pourc_stoth_vac"],None],["to-number",70,None]],[">=",["to-number",["get","dureevacancemax"],None],["to-number",3,None]],["==",["geometry-type"],"Polygon"]]
    },
    "Zonages d'activités économiques" : {
        "sourceId":"plu",
        "mapboxFilter":["all",["in",["get","zone_harm"],["literal",["UE","AUEC","AUES"]]],["in",["get","zone_harm"],["literal",["UE","AUEC","AUES"]]],["==",["geometry-type"],"Polygon"]]
    },
    "Zones d'influence autour des arrêts de TC" : {
        "sourceId":"desserte_tc",
        "mapboxFilter":None
    }
}
[d.update({"format": "geojson", "operator": "intersects"}) for d in indic.values()]

departements = fiona.open(f"{dirname(__file__)}/departements.geojson")
liste_dpts = [f"{dpt['properties']['INSEE_DEP']} - {dpt['properties']['NOM']}" for dpt in departements]