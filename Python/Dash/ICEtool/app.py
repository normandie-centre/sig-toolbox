import os 
import base64

from dash import dash, html, Input, Output, State, dcc
import flask

from netcdf import read, list_variables


##########
## Serveur
# location = "/netcdf/"
server = flask.Flask(__name__)
# app = dash.Dash(__name__, server=server, title="Lire des données NetCDF", url_base_pathname=location)
app = dash.Dash(__name__, server=server, title="Lire des données NetCDF")
app.config.suppress_callback_exceptions = True


#########
## Layout
app.layout = html.Div(children=[
    html.Header(html.Img(src='assets/ICEtool.png')),
    html.Div(children=[
        html.H1('Simplification de lecture des NetCDF pour IceTool'),
        dcc.Upload(
            id = 'upload-netcdf', 
            children = html.Button('Télécharger un ou plusieurs NetCDF'),
            multiple = True
        ),
        dcc.Dropdown(
            options=[{'label': k, 'value': k} for k in available_countries],
            value="",
            multi=True,
            placeholder = "Sélectionnez un ou plusieurs indicateurs"
        )
        html.Textarea(id = 'text')
        # html.Label('Sélectionnez une variable disponible : '),
        # dcc.Dropdown(list(indic.keys()), id='cb_data', placeholder='Sélection de la donnée'),
        # html.Br(),
        # html.Label('Sélectionnez un département : '),
        # dcc.Dropdown(liste_dpts, id='cb_dpts', placeholder='Sélection du département'),
        # html.Button('Télécharger la donnée', id='button', n_clicks=0),
        # dcc.Loading(
        #     id="loading-2",
        #     children=[dcc.Download(id='dl_link')],
        #     type="circle",
        # ),
    ]),
    html.Div(style={'margin':'100px'}),
    html.Footer(html.Img(src='assets/cerema.png')),
], id = 'main')


@app.callback(
    Output('variables', 'options'),
    Input('upload-netcdf', 'filename'),
    State('upload-netcdf', 'contents'),
)
def on_load_netcdf(uploaded_filenames, uploaded_file_contents):
    if uploaded_filenames is not None and uploaded_file_contents is not None:
        for name, data in zip(uploaded_filenames, uploaded_file_contents):
            d = data.encode("utf8").split(b";base64,")[1]
            f = f"netcdf/{name}"
            with open(os.path.join(f), "wb") as fp:
                fp.write(base64.decodebytes(d))

            
    return "list_variables(f)"



# ###########
# ## Callback
# @app.callback(
#     Output('dl_link', 'data'),
#     Input('button', 'n_clicks'),
#     State('cb_data', 'value'), 
#     State('cb_dpts', 'value'),
#     prevent_initial_call=True
# )
# def update_output(n_clicks, cb_data_value, cb_dpts_value):

#     numero_dpt = cb_dpts_value.split('-')[0].strip()
#     nom = f"{cb_data_value} du {cb_dpts_value}"

#     # On rajoute la géométrie du département séléctionné pour intersection lors de l'appel d'UrbanSimul
#     geom_dpt = [dpt for dpt in departements if dpt["properties"]["INSEE_DEP"] == numero_dpt][0]["geometry"]
#     payload = indic[cb_data_value]
#     payload["geometry"] = geom_dpt

#     rep = requests.post(api_us_url, data=geojson.dumps(payload), headers=headers).json()
#     try:
#         url_callback = rep["url"]
#         print(url_callback)
#     except:
#         print(rep["message"])
#         exit()

#     r = requests.get(url_callback, headers=headers, stream=True).json()
#     return dict(content=geojson.dumps(r), filename=f"{nom}.geojson")


#######
## Main
if __name__ == '__main__':
    app.run_server(debug=True)
