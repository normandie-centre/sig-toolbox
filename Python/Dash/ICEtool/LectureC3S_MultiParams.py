# -*- coding: utf-8 -*-
"""
Created on Thu Mar 21 11:35:12 2019

@author: Teo
"""
###############################################################################
import numpy as np
from netcdf import read_C3S_MultiParam
import calendar
###############################################################################

textfinal = open("final.txt","w")
textfinal.write("%s\n" % ('year\tmonth\tday\thour\t' + '\tTemp(°C)\tSurface solar radiation downwards(Wh/m²)'))

for mois in ('Janvier','Fevrier','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','Decembre'):

    #### Déclaration vecteurs ####
    date, Njour = [], []
    tp_conv = []

    #### Déclaration des coordonnées [lon, lat]####
    # PI_coord = [0, 0]
    PI_coord = [1.10459, 49.44441]
    readfile = f'/home/thomas/depots/sig-toolbox/Python/ICEtool/Donnees/{mois}_2022.nc'

    val_PI = np.array([])

    #### Lecture fichier _ compléter les chemins de lectures et d'écritures####
    #### Ajouter nom des fichiers####
    # readpath = ('C:\\') 
    # readfile = '.nc'
    readfichier = readfile
    writepath = ''
    writefilename = f'./output/{mois}_2022.txt'

    lat, lon, dates, T2m_tmp, netsolarrad_tmp = read_C3S_MultiParam(readfichier)
    date = date + dates                                                            

    ###### Création des points d'extractions ######   
    r = abs(lon - PI_coord[0]).argmin()
    s = abs(lat - PI_coord[1]).argmin()
    val_PI_temp = T2m_tmp[:, s, r]
    val_PI_flux = netsolarrad_tmp[:, s, r]

        
    #### Création fichier texte ####
    textfile = open(writepath + writefilename,"w")
    textfile.write("%s\n" % ('year\tmonth\tday\thour\t'
                            + '\tTemp(°C)\tSurface solar radiation downwards(Wh/m²)'))
    data_str = ''
    date_str = ''
    for i, _ in enumerate(val_PI_temp):
        date_str = (str(date[i][0]) + '\t' + str(date[i][1]) + '\t' + str(date[i][2]) + '\t') + str(date[i][3] + 1) + '\t'
        data_str += (date_str + str(val_PI_temp[i]) + '\t' 
                    + str(val_PI_flux[i]) + '\n' )
    # data_str = data_str[:-2]
    # data_str2 = data_str[:-1]
    textfile.write(data_str)
    textfinal.write(data_str)
    textfile.close()

textfinal.close()
