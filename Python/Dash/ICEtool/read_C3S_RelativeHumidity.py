# -*- coding: utf-8 -*-
"""
Created on Mon Dec  7 09:40:16 2020
    
    fonction lecture fichier C3S, dans ce cas: relative humidity

@author: Teodolina
"""
##############################################################################
import netCDF4 as nc
import numpy as np
from datetime import datetime, timedelta
##############################################################################

def read_C3S_RH(filepath, filename):
    dates = []

    f = nc.Dataset(filepath + filename, 'r') #lecture du fichier

    # glo_attr = f.ncattrs() #attributs globaux
    attr_list = f.variables #attributs des variables. Toujours vérifier dans la fenêtre de commande 
                       #avec la ligne de commande : ncdump -h nom du fichier.
    # var_list = f.variables.keys()
    
    lat = np.array(attr_list['latitude']) #créer une matrice avec les valeurs de long
    lon = np.array(attr_list['longitude']) #créer une matrice aves les valeurs de lat
    temps = np.array(attr_list['time']) #créer une matrice avec les valeurs de temps
    RH_tmp = np.array(attr_list['r'])
    missing_value = attr_list['r']._FillValue                            
    RH_tmp[RH_tmp == missing_value] = np.nan

    #Permet de convertir les valeurs de temps en date
    base = datetime(1900, 1, 1,0,0,0)
    for i in temps:
        tmp = base + timedelta(hours=i.item())
        dates.append([tmp.year,tmp.month,tmp.day,tmp.hour,0,0])

    return lat, lon, dates, RH_tmp
