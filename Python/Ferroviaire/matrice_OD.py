###SCRIPT
#pour calculer les temps de parcours routiers et ferroviaires de gare à gare sur une région

from Gare import *
import csv
import pprint

#Ici on définit les régions

region = {'PACA':['ALPES-DE-HAUTE-PROVENCE','HAUTES-ALPES','ALPES-MARITIMES','BOUCHES-DU-RHONE','VAR','VAUCLUSE'],
'AUVERGNE-RHONE-ALPES':['AIN','ALLIER','ARDECHE','CANTAL','DROME','ISERE','LOIRE','HAUTE-LOIRE','PUY-DE-DOME','RHONE','SAVOIE','HAUTE-SAVOIE'],
'OCCITANIE':['ARIEGE','AUDE','AVEYRON','GARD','HAUTE-GARONNE','GERS','HERAULT','LOT','LOZERE','HAUTES-PYRENEES','PYRENEES-ORIENTALES','TARN','TARN-ET-GARONNE'],
'BOURGOGNE-FRANCHE-COMTE':['COTE-D-OR','DOUBS','JURA','NIEVRE','HAUTE-SAONE','SAONE-ET-LOIRE','YONNE','TERRITOIRE-DE-BELFORT'],
'GRAND-EST':['ARDENNES','AUBE','MARNE','HAUTE-MARNE','MEURTHE-ET-MOSELLE','MEUSE','MOSELLE','BAS-RHIN','HAUT-RHIN','VOSGES'],
'HAUTS-DE-FRANCE':['AISNE','NORD','OISE','PAS-DE-CALAIS','SOMME'],
'BRETAGNE':['COTES-D-ARMOR','FINISTERE','ILLE-ET-VILAINE','MORBIHAN'],
'PAYS-DE-LA-LOIRE':['LOIRE-ATLANTIQUE','MAINE-ET-LOIRE','MAYENNE','SARTHE','VENDEE'],
'NORMANDIE':['SEINE-MARITIME','EURE','ORNE','CALVADOS','MANCHE'],
'ILE-DE-FRANCE':['VAL-D-OISE','VAL-DE-MARNE','SEINE-ET-MARNE','ESSONNE','YVELINES','SEINE-SAINT-DENIS','HAUTS-DE-SEINE','PARIS'],
'NOUVELLE-AQUITAINE':['CHARENTE','CHARENTE-MARITIME','CORRÈZE','CREUSE','DORDOGNE','LOT-ET-GARONNE','GIRONDE','LANDES','PYRENEES-ATLANTIQUES','DEUX-SEVRES','VIENNE','HAUTE-VIENNE']}          
liste_region = [x for x in region]

#Ici on définit une liste de token. On en a créé 6, de telle sorte que ça devrait suffire à générer l'ensemble des fichiers de temps de parcours par région, soit 11 régions.

liste_token = ['9fb7dbb1-e5a4-4bd1-a937-8c156fe19e09','3b09108d-4aac-453d-a7f9-da4e44ad90d8','65c6788b-abec-470d-862d-8ac8a86d76ff','149585f3-50a5-4346-a4cf-a0acf217f7e2','e51f4e28-5c0b-46f2-ae55-c4b8dbee05ec','9fb7dbb1-e5a4-4bd1-a937-8c156fe19e09','8b0e5f4b-8adc-4e0c-9167-fcfe84a8db3b','3b09108d-4aac-453d-a7f9-da4e44ad90d8','65c6788b-abec-470d-862d-8ac8a86d76ff','149585f3-50a5-4346-a4cf-a0acf217f7e2','e51f4e28-5c0b-46f2-ae55-c4b8dbee05ec']

with open('C:/Users/alexis-m.vernier/Desktop/depot/sig-toolbox/Python/Ferroviaire/gare_origine.csv',encoding='utf-8') as srce:
    srce=csv.reader(srce,delimiter=',')
    for gare_origine in srce:
        gare_csv.append(gare_origine)

##On importe le fichier des gares principales comme destination
liste_gare_principale=[]
with open('C:/Users/alexis-m.vernier/Desktop/depot/sig-toolbox/Python/Ferroviaire/test.csv',encoding='utf-8') as srce:
    srce = csv.reader(srce, delimiter=',')
    for x in srce:
        liste_gare_principale.append(x)

##Comme c'est très lourd de faire vers toutes les sous-préfectures, on peut aussi faire le script seulement vers les préfectures
liste_prefecture = [x for x in liste_gare_principale if x[4] in["Préfecture","Préfecture de région"]]

compteur = 0

for region_choisie in liste_region:
    numero_token = int(compteur/2000)
    token = liste_token[numero_token]


    ###Ensuite, on va faire, pour simplifier, une sélection simplement des gares de la région considérée.
            
    liste_gare_origine=[]
    for gare_origine in gare_csv:
        if gare_origine[6].upper() in region[region_choisie]:
            liste_gare_origine.append(gare_origine)

            

    ##Pareil pour la destination, en utilisant les gares principales comme gare de destination

    liste_gare_destination=[]
    for gare_destination in liste_prefecture:
        if gare_destination[5].upper() in region[region_choisie]:
            liste_gare_destination.append(gare_destination)
                



    ##Maintenant, il faut faire la boucle sur liste_gare_origine et liste_gare_destination, sachant qu'il faut mettre en argument de la méthode "Gare" le code UIC des gares.

    ##Déjà, on crée une liste "résultats" pour les stocker, ça peut servir...
            
    resultat = []

    file = open('C:/Users/alexis-m.vernier/Desktop/depot/sig-toolbox/Python/Ferroviaire/matrice_od'+region_choisie+'.csv', 'w+', newline ='', encoding = 'utf-8') 
    write = csv.writer(file)
    write.writerow(["geom","Code UIC de la gare dorigine","Nom de la gare dorigine","Département de la gare d'origine","Code UIC de la gare de destination","Nom de la gare de destination","Département de la gare de destination","temps de trajet en mode ferroviaire","nombre de correspondances","distance par mode ferroviaire","temps dattente lors des correspondances","temps de parcours par mode routier","distance par mode routier"])
    with file:
        for gare_origine_ligne in liste_gare_origine:        ###Il s'agit des gares en origine
            numero_token = int(compteur/2000)                  ### Choix du token
            token = liste_token[numero_token]

            geom = gare_origine_ligne[0]
            code_uic_origine = gare_origine_ligne[2]
            nom_gare_origine = gare_origine_ligne[3]
            nom_departement_origine = gare_origine_ligne[6]
            code_commune_origine = gare_origine_ligne[5]
            gare_origine = Gare( nom = nom_gare_origine, code_commune = code_commune_origine, code_uic = code_uic_origine,token_sncf = token)
            for gare_destination_ligne in liste_gare_destination: ###Il s'agit des gares en destination
                if gare_destination_ligne[2] == code_uic_origine:
                    print("origine et destination identiques")
                else:
                    
                    nom_gare_destination = gare_destination_ligne[2]
                    nom_departement_destination = gare_destination_ligne[7]
                    code_commune_destination = gare_destination_ligne[5]
                    code_uic_destination = gare_destination_ligne[0]
                    print(code_uic_destination)
                    gare_destination = Gare(code_commune = code_commune_destination, code_uic = code_uic_destination,token_sncf = token)

                    tps_ferroviaire = gare_destination.temps_ferroviaire(gare_origine)
                    if tps_ferroviaire[0] in ["indisponible",'',None,'p']:
                        tps_ferroviaire_heure = "indisponible"
                    else:
                        tps_ferroviaire_heure = f"{int(int(tps_ferroviaire[0])/3600)}h{int(((int(tps_ferroviaire[0])/3600)-int(int(tps_ferroviaire[0])/3600))*60)}min"
                    

                    tps_routier = gare_destination.temps_routier(gare_origine)
                    if tps_routier[0] in ["indisponible",'',None,'p']:
                        tps_routier_heure = "indisponible"
                    else:
                        tps_routier_heure = f"{int(int(tps_routier[0])/3600)}h{int(((int(tps_routier[0])/3600)-int(int(tps_routier[0])/3600))*60)}min"


                    if tps_ferroviaire == None:
                        f = [geom,code_uic_origine,nom_gare_origine,nom_departement_origine,code_uic_destination,nom_gare_destination,nom_departement_destination,'','','','','', tps_routier[0],tps_routier[1]]
                    else:
                        f = [geom,code_uic_origine,nom_gare_origine,nom_departement_origine,code_uic_destination,nom_gare_destination,nom_departement_destination,tps_ferroviaire[0],tps_ferroviaire_heure, tps_ferroviaire[1], tps_ferroviaire[2], tps_ferroviaire[3], tps_routier[0],tps_routier_heure,tps_routier[1]]
                        resultat.append(f)

                    if tps_ferroviaire == None:
                        print(code_uic_origine,",",nom_gare_origine,",",nom_departement_origine,",",code_uic_destination,",",nom_gare_destination,",",nom_departement_destination,",",0,",",",",0,",",0,",",0,",",0,",",0,",",tps_routier[0],",",tps_routier_heure,",",tps_routier[1])
                    else:
                        print(code_uic_origine,",",nom_gare_origine,",",nom_departement_origine,",",code_uic_destination,",",nom_gare_destination,",",nom_departement_destination,",",tps_ferroviaire[0],",",tps_ferroviaire_heure,",",tps_ferroviaire[1],",",tps_ferroviaire[2],",",tps_ferroviaire[3],",",",",tps_routier[0],",",tps_routier_heure,",",tps_routier[1])
                    print(f)     

                    
                    write.writerow(f) 
            compteur = compteur + 1


