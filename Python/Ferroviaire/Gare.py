"""Classe Python de création d'une gare SNCF

Usage:
======
    python gare.py

Exemples:
=========
    Création d'un objet :
        hirson=Gare(nom='hirson')
        vesoul = Gare('vesoul','70000')
        lille = Gare(code_uic=87223263)
    Calcul d'un temps entre deux gares :
        temps_sncf = vesoul.temps_ferroviaire(lille)
        temps_routier = hirson.temps_routier(vesoul)

"""

__authors__ = ("Alexis Vernier", "Thomas Escrihuela")
__contact__ = ("Alexis.Vernier@cerema.fr", "Thomas.Escrihuela@cerema.fr")
__date__ = "2020/08"
__version__ = "1.0"


import requests
import csv
from urllib.parse import urlencode
from datetime import datetime as dt
import datetime
from dateutil import tz
import pprint

proxies = {'http':'','https':''}
"""proxies = {
    'http': 'http://pfrie-std.proxy.e2.rie.gouv.fr:8080',
    'https': 'http://pfrie-std.proxy.e2.rie.gouv.fr:8080'
}"""

# SNCF
#token = '65c6788b-abec-470d-862d-8ac8a86d76ff'
#token = '149585f3-50a5-4346-a4cf-a0acf217f7e2'
#token = 'e51f4e28-5c0b-46f2-ae55-c4b8dbee05ec'

#Fichier des gares
gare_csv=[]
with open("C:/Users/alexis-m.vernier/Desktop/depot/sig-toolbox/Python/Ferroviaire/gare.csv", encoding='utf-8') as srce:
    srce=csv.reader(srce)
    for x in srce:
        gare_csv.append(x)
    
# Calcule de la date au format Google Maps API & SNCF API
date = '2023-01-26 07:30'
from_zone = tz.tzlocal()
to_zone = tz.tzutc()
local_date = dt.strptime(date, '%Y-%m-%d %H:%M')
local_date = local_date.replace(tzinfo=from_zone)
utc_date = local_date.astimezone(to_zone)
sncf_date = dt.strftime(local_date, '%Y%m%dT%H%M00')


class Gare:
    """Définit une gare, par son code uic, son nom ou son code postal.
    
        Attributs :
            - nom
            - code_postal
            - code_uic
            - is_closed : indique si une gare est fermée (true) ou ouverte (false). Une gare est considérée fermée quand le code UIC n'est plus répertorié par l'open data SNCF.
            - token_sncf : token d'entrée pour les requêtes sncf
        Méthodes :
            - geoloc_uic(code_uic) : géolocalise une gare avec le code UIC
            - geoloc_look4(nom, code_postal) : géolocalise une gare par son nom ou son code postal
            - temps_ferroviaire(<gare>) : calcule le temps de parcours SNCF et le nombre de correspondances à l'objet <gare>
            - temps_routier(<gare>) : calcule le temps de parcours routier à l'objet <gare>

    """

    def __init__(self, nom=None, code_postal=None, code_commune=None, code_uic=None, token_sncf = None, *args, **kwargs):
        """Le constructeur de Gare par son nom, code postal ou code uic."""

        self.code_postal = code_postal
        self.code_uic = code_uic
        self.code_commune = code_commune
        self.token_sncf = token_sncf
        self.is_closed = False
        if self.code_uic:
            try:
                (self.lat, self.lon,self.nom) = self.geoloc_uic()
                
            except:
                (self.lat, self.lon, self.nom) = self.geoloc_look4()

        else:  
          (self.lat, self.lon,self.nom) = self.geoloc_look4()      
             
        
    def geoloc_uic(self):
        """Géolocalise une gare par son code UIC."""

        req_geoloc = 'https://api.sncf.com/v1/coverage/sncf/stop_areas/stop_area:SNCF:'+str(self.code_uic)
        try:
            r = requests.get(req_geoloc, proxies=proxies, auth=(self.token_sncf,''))
            r.raise_for_status()

            response_geoloc = r.json()      
            lat = response_geoloc['stop_areas'][0]['coord']['lat']
            lon = response_geoloc['stop_areas'][0]['coord']['lon']
            nom = response_geoloc['stop_areas'][0]['label']
            return lat,lon,nom

        except requests.exceptions.HTTPError as err:
            #raise SystemExit(err)
            self.is_closed = True
            return (None, None, None)

    
        

    # def geoloc_look4(self):
    #     """Géolocalise une gare par look4."""
    #     if self.nom:
    #         params['q'] = self.nom

    #     if self.code_postal:
    #         params['filters[postalCode]'] = self.code_postal
        
    #     if self.code_commune:
    #         params['filters[inseeCode]'] = self.code_commune
        
    #     if self.is_closed == False:
    #         params['filters[type]'] = 'gare voyageurs et fret'


    #     req_geoloc = 'https://geocodage.ign.fr/look4/poi/search?%s' % urlencode(params)
    #     try:
    #         r = requests.get(req_geoloc, proxies=proxies)
    #         r.raise_for_status()
    #     except requests.exceptions.HTTPError as err:
    #         #raise SystemExit(err)
    #         return (None, None, None)

    #     response_geoloc = r.json()
    #     lat = response_geoloc['features'][0]['geometry']['coordinates'][1]
    #     lon = response_geoloc['features'][0]['geometry']['coordinates'][0]
    #     return lat,lon,nom

    def temps_routier(self, x):
        """Calcule le temps routier entre l'objet et un autre objet Gare avec l'API OSRM."""
        req_itineraire_routier='http://router.project-osrm.org/route/v1/driving/{},{};{},{}'.format(self.lon,self.lat,x.lon,x.lat)
        
        try:
            r=requests.get(req_itineraire_routier, proxies=proxies)
            r.raise_for_status()
        except requests.exceptions.HTTPError as err:
            #raise SystemExit(err)
            return "problème dans temps_routier"
        response_itineraire_routier=r.json()
        (temps_routier_seconde, distance_routier) = [int(response_itineraire_routier['routes'][0]['legs'][0]['duration']), int(response_itineraire_routier['routes'][0]['legs'][0]['distance'])]

        return temps_routier_seconde, distance_routier

    def temps_ferroviaire(self,x):
        """Calcule le temps ferroviaire minimal entre l'objet et un autre objet Gare avec l'API SNCF, ainsi que le nombre de correspondances."""
        params = {
                    'from':str(self.lon)+';'+str(self.lat),
                    'to':str(x.lon)+';'+str(x.lat),
                    'datetime':sncf_date,
                    'count':100
        }
        req_temps_ferroviaire = 'https://api.sncf.com/v1/coverage/sncf/journeys?%s' % urlencode(params)
        #print(req_temps_ferroviaire)
        try:
            r = requests.get(req_temps_ferroviaire, proxies=proxies, auth=(self.token_sncf, ''))
            r.raise_for_status()
        except requests.exceptions.HTTPError as err:
            #raise SystemExit(err)
            return ("indisponible","indisponible","indisponible","indisponible")
        response_ferroviaire = r.json()
        try:
            response_ferroviaire['journeys']
            (sncf_time, sncf_correspondance, sncf_distance, sncf_temps_correspondance) = sorted([(f['duration'], f['nb_transfers'], sum([section['geojson']['properties'][0]['length'] for section in f['sections'] if 'geojson' in section]),sum([section['duration'] for section in f['sections'] if 'waiting' in section['type']])) for f in response_ferroviaire['journeys']], key=lambda tup: tup[0])[0]    # Tri sur le tuple (durée, nb_correspondaces, distance) qui a la plus petite durée
            #print(sncf_temps_correspondance)

            return sncf_time, sncf_correspondance, sncf_distance, sncf_temps_correspondance
        except:
            (sncf_time, sncf_correspondance, sncf_distance, sncf_correspondance) = (0,0,0,0)
            pass           



if __name__ == "__main__":
    """Quelques valeurs tests"""
    #gare_1 = Gare(code_uic = 87756387)
    #gare_2 = Gare(code_uic = 87753418)
    #temps_ferroviaire_gare_1_vers_gare_2 = gare_1.temps_ferroviaire(gare_2)

    # print("Geoloc le code uic {} : {}".format(gare_1.code_uic,gare_1.nom))
    # print("lat / lon de {}, {}: {} / {}".format(gare_1.code_uic,gare_1.nom,gare_1.lat,gare_1.lon))
    # print("lat / lon de {}, {}: {} / {}".format(gare_2.code_uic,gare_2.nom,gare_2.lat,gare_2.lon))
    # print("tps ferroviaire entre {} et {}: {}".format(gare_1.nom,gare_2.nom,temps_ferroviaire_gare_1_vers_gare_2))


    