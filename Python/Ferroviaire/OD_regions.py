###SCRIPT
#pour calculer les temps de parcours routiers et ferroviaires de gare à gare

from Gare import *
import csv

#au départ on essaie juste avec le GRAND EST
#Ici on définit les régions

region = {'PACA':['ALPES-DE-HAUTE-PROVENCE','HAUTES-ALPES','ALPES-MARITIMES','BOUCHES-DU-RHONE','VAR','VAUCLUSE'],
'AUVERGNE-RHONE-ALPES':['AIN','ALLIER','ARDECHE','CANTAL','DROME','ISERE','LOIRE','HAUTE-LOIRE','PUY-DE-DOME','RHONE','SAVOIE','HAUTE-SAVOIE'],
'OCCITANIE':['ARIEGE','AUDE','AVEYRON','GARD','HAUTE-GARONNE','GERS','HERAULT','LOT','LOZERE','HAUTES-PYRENEES','PYRENEES-ORIENTALES','TARN','TARN-ET-GARONNE'],
'BOURGOGNE-FRANCHE-COMTE':['COTE-D-OR','DOUBS','JURA','NIEVRE','HAUTE-SAONE','SAONE-ET-LOIRE','YONNE','TERRITOIRE-DE-BELFORT'],
'GRAND-EST':['ARDENNES','AUBE','MARNE','HAUTE-MARNE','MEURTHE-ET-MOSELLE','MEUSE','MOSELLE','BAS-RHIN','HAUT-RHIN','VOSGES'],
'HAUTS-DE-FRANCE':['AISNE','NORD','OISE','PAS-DE-CALAIS','SOMME'],
'BRETAGNE':['COTES-D\'ARMOR','FINISTERE','ILLE-ET-VILAINE','MORBIHAN'],
'PAYS-DE-LA-LOIRE':['LOIRE-ATLANTIQUE','MAINE-ET-LOIRE','MAYENNE','SARTHE','VENDÉE'],
'NORMANDIE':['SEINE-MARITIME','EURE','ORNE','CALVADOS','MANCHE'],
'ILE-DE-FRANCE':['VAL-D-OISE','VAL-DE-MARNE','SEINE-ET-MARNE','ESSONNE','YVELINES','SEINE-SAINT-DENIS','HAUTS-DE-SEINE','PARIS'],
'NOUVELLE-AQUITAINE':['CHARENTE','CHARENTE-MARITIME','CORREZE','CREUSE','DORDOGNE','LOT-ET-GARONNE','GIRONDE','LANDES','PYRENEES-ATLANTIQUES','DEUX-SEVRES','VIENNE','HAUTE-VIENNE']}          

##On importe le fichier de toutes les gares de France
gare_csv=[]
with open('gare_origine.csv',encoding='utf-8') as srce:
    srce=csv.reader(srce,delimiter=',')
    for x in srce:
        gare_csv.append(x)

##On importe le fichier des gares principales, ici dans C:/test.csv (à modifier..)
gare_principale=[]
with open('test.csv',encoding='utf-8') as srce:
    srce = csv.reader(srce, delimiter=',')
    for x in srce:
        gare_principale.append(x)

###Ensuite, on va faire, pour simplifier, une sélection simplement des gares du Grand Est.
        ###Ici, toutes les gares du Grand Est. Elles seront nos origines.
        
ge_orig=[]
for i in gare_csv:
    if i[6].upper() in region['PAYS-DE-LA-LOIRE']:
        ge_orig.append(i)
        

##Pareil pour la destination

ge_dest=[]
for i in gare_principale:
    if i[5] in region['PAYS-DE-LA-LOIRE']:
        ge_dest.append(i)


##Maintenant, il faut faire la boucle sur ge_orig et ge_dest, sachant qu'il faut mettre en argument de la méthode "Gare" le code UIC des gares.

##Déjà, on crée une liste "résultats" pour les stocker, ça peut servir...
        
resultat=[]

for gare_est in ge_orig:        ###Il s'agit des gares en origine
    code_uic_orig = gare_est[2]
    nom_gare_orig = gare_est[3]
    nom_departement_orig = gare_est[6]
    
    for v in ge_dest: ###Il s'agit des gares en destination
        if v[0] == gare_est[2]:
            print("origine et destination identiques")
        else:
            
            nom_gare_dest = v[1]
            nom_departement_dest = v[5]
            code_uic_dest = v[0]
            gare_origine = Gare(code_uic = code_uic_orig, nom = nom_gare_orig)
            gare_destination = Gare(code_uic=code_uic_dest, nom = nom_gare_dest)

            tps_ferroviaire = gare_destination.temps_ferroviaire(gare_origine)

            if tps_ferroviaire in ["problème dans temps_ferroviaire",("indisponible","indisponible","indisponible","indisponible"),'',None]:
                tarif_ferroviaire = 0
            else:
                if tps_ferroviaire[2] < 200:
                    tarif_ferroviaire = ((tps_ferroviaire[2] // 25000)+1)*4
                else:
                    tarif_ferroviaire = (((tps_ferroviaire[2]-200000)//20000)+1)*2 + 32



            tps_routier = gare_destination.temps_routier(gare_origine)


            f = [code_uic_orig,nom_gare_orig,nom_departement_orig,code_uic_dest,nom_gare_dest,nom_departement_dest,tps_ferroviaire,tps_routier]
            resultat.append(f)
            if tps_ferroviaire in ["problème dans temps_ferroviaire",("indisponible","indisponible","indisponible","indisponible"),'',None]:
                print(code_uic_orig,",",nom_gare_orig,",",nom_departement_orig,",",code_uic_dest,",",nom_gare_dest,",",nom_departement_dest,",","",",","",",","",",",",",tarif_ferroviaire,",",tps_routier[0],",",tps_routier[1])
            else:
                print(code_uic_orig,",",nom_gare_orig,",",nom_departement_orig,",",code_uic_dest,",",nom_gare_dest,",",nom_departement_dest,",",tps_ferroviaire[0],",",tps_ferroviaire[1],",",tps_ferroviaire[2],",",tps_ferroviaire[3],",",tarif_ferroviaire,",",tps_routier[0],",",tps_routier[1])
                
        

