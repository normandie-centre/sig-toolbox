from bs4 import BeautifulSoup 
import requests
import json
from pprint import pprint
import pandas as pd
import os

url_ressource = "https://www.data.gouv.fr/fr/datasets/trajets-realises-en-covoiturage-registre-de-preuve-de-covoiturage/#resources"
HEADERS = {
            "Content-Type": "application/json",
        }
soup = BeautifulSoup(requests.get(url_ressource,  headers = HEADERS).content, 'html.parser')
# print(soup.prettify()[:100000])

fichier_json = soup.find('script', id="json_ld").next_element
fichier_json = json.loads(fichier_json)

# for distribution in fichier_json['distribution']:
liste_url = [distri['contentUrl'] for distri in fichier_json['distribution']]
# pprint(liste_url)

## Boucle pour vérifier si le fichier existe déjà dans le dossier

# url_a_telecharger
# for nom_fichier in 


### On va commencer par concaténer tous les fichiers csv qui se trouvent dans data

liste_fichier_existant = os.listdir("./data")
nouveau_df =pd.DataFrame()
for url in liste_url:
    # pprint(url[-11:])
    if url[-11:] in liste_fichier_existant:
        pass
    else:
        print(url)

# export_csv = nouveau_df.to_csv('export_csv')

    # path = str(str("./data/")+fichier)
    # print(path)
    # df_csv = pd.read_csv(path, on_bad_lines='warn')
    # print(df_csv.count())
    # df_final = pd.concat([df_csv,df_final])