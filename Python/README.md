# Outils en Python

Arborescence :
```
.
├── Dash : Les tableaux de bord réalisés avec la librairie Dash
├── Ferroviaire : Des scripts traitant de données ferroviaires
├── Valise : Une interface de filtrage de documents à partir d'une bdd les référençant
└── stations_services : Un script de parsing de georisques pour récupérer toutes les ICPE avec une nomenclature IC "Station Service"
```
