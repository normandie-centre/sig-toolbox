import os
import psycopg2
from psycopg2 import sql
import pandas as pd

from sqlalchemy import create_engine

# Paramètres de connexion à la base de données PostgreSQL
db_params = {
    'dbname': 'accessibilite',
    'user': 'guillaume.chretien',
    'password': 'thomasparleklingoncouramment',
    'host': 'northmaen.cerema.fr', 
    'port': 5432
}

schema = 'geostandard'

# Répertoire contenant les fichiers CSV
csv_directory = os.path.join(os.path.dirname(__file__),'files')
print(os.path.abspath(csv_directory))

# Connexion à la base de données
try:
    conn = psycopg2.connect(**db_params)
    cursor = conn.cursor()
    
    #engine = create_engine('postgresql://username:password@localhost:5432/mydatabase')
    engine = create_engine('postgresql://guillaume.chretien:thomasparleklingoncouramment@northmaen.cerema.fr:5432/accessibilite')

    # Liste des fichiers CSV dans le répertoire
    csv_files = [f for f in os.listdir(csv_directory) if f.endswith('.csv')]

    # Importation de chaque fichier CSV dans la table
    for csv_file in csv_files:
        table_name = os.path.splitext(csv_file)[0]  # Nom de la table sans l'extension .csv
        table_name_sans_prefixe = table_name[55:].lower().replace("é", "e").replace("ô", "o")
        print(table_name_sans_prefixe)
        csv_path = os.path.join(csv_directory, csv_file)

        if table_name_sans_prefixe=='type_erp':
            code_length = 3
        else:
            code_length = 2

        # Création de la table (si elle n'existe pas déjà)
               
        drop_table_query = f"DROP TABLE IF EXISTS {schema}.{table_name_sans_prefixe} CASCADE;"
        cursor.execute(drop_table_query)
        
        create_table_query = f"CREATE TABLE IF NOT EXISTS {schema}.{table_name_sans_prefixe} ( code character varying({code_length}), libelle character varying, definition character varying );"
        cursor.execute(create_table_query)

        pk_table_query = f"ALTER TABLE {schema}.{table_name_sans_prefixe} ADD CONSTRAINT pk_{schema}_{table_name_sans_prefixe}_code PRIMARY KEY (code);"
        cursor.execute(pk_table_query)

        conn.commit()

        # Importation des données en passant par un dataframe
        df = pd.read_csv(csv_path)
        #print(df)        
        df = df.rename(columns={'Code': 'code', 'Libellé': 'libelle', 'Définition': 'definition'}) 
        df = df._append({"code": "00", "libelle": "valeurs inconnues, non renseignées, ou information non disponible", "definition": None}, ignore_index=True)
        df = df._append({"code": "99", "libelle": "sans objet", "definition": None}, ignore_index=True)

        if table_name_sans_prefixe !='type_erp':
            df['code'] = df['code'].apply(lambda x: str(x).zfill(2))
        #print(df)
        df.to_sql(name = table_name_sans_prefixe, schema= schema, con = engine, if_exists='append', index=False)

    # Valider les modifications et fermer la connexion
    cursor.close()
    conn.close()


except psycopg2.Error as e:
    print(f"Erreur lors de la connexion à la base de données : {e}")
