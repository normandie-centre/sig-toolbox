Le script *import_data.sh* est celui utilisé par Jenkins pour importer les données IGN BDTopo & BDCarto sur le serveur postgres.

Le script *import_csv_into_postgres_exemple_accessibilite.py* est un exemple d'utilisation d'exécution de SQL et de pandas pour importer en masse des csv similaires. Dans ce cas, le script est lancé depuis visual studio code. Ce code pourrait être généralisé pour importer tout type de csv.
