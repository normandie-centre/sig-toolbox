#!/bin/bash
source ~/.jenkins_env

echo -e "Import de $folder"

data=$(echo $folder | rev | cut -d/ -f1 | rev | cut -d'_' -f1 | tr '[:upper:]' '[:lower:]')   # premier mot du nom du dossier ("BDTopo", "BDCarto", ...)
annee=$(echo $folder | grep -Eo "[0-9]{4}-[0-9]{2}-[0-9]{2}" | head -n1 | grep -Eo "[0-9]{4}")  # l'année depuis une substring YYYY-MM-DD dans le nom
dpt=$(echo $folder | grep -Eo "D[0-9]{3}" | head -n1 | tr '[:upper:]' '[:lower:]')
bdd="${data}_${annee}"
schema="${data}_${dpt}_${annee}"

echo -e "Création de la $bdd et du schéma $schema si ils n'existent pas"
psql -h $PGSERVER -p $PGPORT -U $PGUSER -d postgres -c "create database $bdd" && psql -h $PGSERVER -p $PGPORT -U $PGUSER -d $bdd -c "create extension postgis"
psql -h $PGSERVER -p $PGPORT -U $PGUSER -d $bdd -c "create schema if not exists $schema" 

for shp in $(find $folder -name *.shp)
do
	tab=$(echo $shp | sed 's!.*/\(.*\).shp!\1!g')
    shp2pgsql -I -s 2154 $shp ${schema}.${tab} | psql -h $PGSERVER -p $PGPORT -U $PGUSER -d ${bdd} -q
done

