---------------------------------------------
--				Haropa 				---------
---------------------------------------------

---------------------------------------------
--	Ajout des colonnes géométriques et index 
-- 	aux données initiales
select DropGeometryColumn('haropa', 'domaine_haropa', 'geom_lae');
select AddGeometryColumn('haropa', 'domaine_haropa', 'geom_lae', 3035, 'MULTIPOLYGON', 2);
update domaine_haropa set geom_lae = st_multi(st_simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(st_transform(geom, 3035))),3)),0));;
create index on domaine_haropa using gist(geom);
create index on domaine_haropa using gist(geom_lae);

select DropGeometryColumn('haropa', 'oscom_2012_in_haropa', 'geom_lae');
select AddGeometryColumn('haropa', 'oscom_2012_in_haropa', 'geom_lae', 3035, 'MULTIPOLYGON', 2);
update oscom_2012_in_haropa set geom_lae = st_multi(st_simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(st_transform(geom, 3035))),3)),0));
create index on oscom_2012_in_haropa using btree(id);
create index on oscom_2012_in_haropa using gist(geom);
create index on oscom_2012_in_haropa using gist(geom_lae);

select DropGeometryColumn('haropa', 'oscom_2012_in_haropa', 'geom_decoup');
select AddGeometryColumn('haropa', 'oscom_2012_in_haropa', 'geom_decoup', 3035, 'MULTIPOLYGON', 2);

select DropGeometryColumn('haropa', 'oscom_2018_in_haropa', 'geom_lae');
select AddGeometryColumn('haropa', 'oscom_2018_in_haropa', 'geom_lae', 3035, 'MULTIPOLYGON', 2);
update oscom_2018_in_haropa set geom_lae = st_multi(st_simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(st_transform(geom, 3035))),3)),0));
create index on oscom_2018_in_haropa using btree(id);
create index on oscom_2018_in_haropa using gist(geom);
create index on oscom_2018_in_haropa using gist(geom_lae);

select DropGeometryColumn('haropa', 'oscom_2018_in_haropa', 'geom_decoup');
select AddGeometryColumn('haropa', 'oscom_2018_in_haropa', 'geom_decoup', 3035, 'MULTIPOLYGON', 2);


-------------------------------------------------
-- 	Découpage des OSCOM par le territoire d'étude

with oscom_2012_decoupee as (
	select oscom.id,
	st_union(case 
		when st_within(oscom.geom_lae, dom.geom_lae) then oscom.geom_lae
		else st_multi(st_simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(st_intersection(oscom.geom_lae, dom.geom_lae))),3)),0))
	end) as geom_decoup
	from oscom_2012_in_haropa as oscom, domaine_haropa as dom
	where ST_Intersects(oscom.geom_lae, dom.geom_lae)
	group by oscom.id
)
update oscom_2012_in_haropa as osc12 set geom_decoup = st_multi(osc_dec.geom_decoup) from oscom_2012_decoupee osc_dec where osc_dec.id = osc12.id;
create index on oscom_2012_in_haropa using gist(geom_decoup);

with oscom_2018_decoupee as (
	select oscom.id,
	st_union(case 
		when st_within(oscom.geom_lae, dom.geom_lae) then oscom.geom_lae
		else st_multi(st_simplify(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(st_intersection(oscom.geom_lae, dom.geom_lae))),3)),0))
	end) as geom_decoup
	from oscom_2018_in_haropa as oscom, domaine_haropa as dom
	where ST_Intersects(oscom.geom_lae, dom.geom_lae)
	group by oscom.id
)
update oscom_2018_in_haropa as osc18 set geom_decoup = st_setsrid(st_multi(osc_dec.geom_decoup), 3035) from oscom_2018_decoupee osc_dec where osc_dec.id = osc18.id;
create index on oscom_2018_in_haropa using gist(geom_decoup);


--------------------------------------
--			COPERNICUS				--
--------------------------------------

-- Suppression des pixels blancs dans la mer
drop table if exists clc_2012_ca_haropa_no_255;
SELECT ST_MapAlgebra(rast, 1, null, '(case when [rast1]< 255 then [rast1] end)') AS rast
INTO clc_2012_ca_haropa_no_255
FROM clc_2012_ca_haropa;

drop table if exists clc_2012_imd_haropa_no_255;
SELECT ST_MapAlgebra(rast, 1, null, '(case when [rast1]< 255 then [rast1] end)') AS rast
INTO clc_2012_imd_haropa_no_255
FROM clc_2012_ca_haropa;

drop table if exists clc_2018_ca_haropa_no_255;
SELECT ST_MapAlgebra(rast, 1, null, '(case when [rast1]< 255 then [rast1] end)') AS rast
INTO clc_2018_ca_haropa_no_255
FROM clc_2018_ca_haropa;

drop table if exists clc_2018_imd_haropa_no_255;
SELECT ST_MapAlgebra(rast, 1, null, '(case when [rast1]< 255 then [rast1] end)') AS rast
INTO clc_2018_imd_haropa_no_255
FROM clc_2018_imd_haropa;


--	Ajout des contraintes sur les raster
select DropRasterConstraints('haropa'::name, 'clc_2012_ca_haropa_no_255'::name, 'rast'::name);
select AddRasterConstraints('haropa'::name, 'clc_2012_ca_haropa_no_255'::name, 'rast'::name);

select DropRasterConstraints('haropa'::name, 'clc_2012_imd_haropa_no_255'::name, 'rast'::name);
select AddRasterConstraints('haropa'::name, 'clc_2012_imd_haropa_no_255'::name, 'rast'::name);

select DropRasterConstraints('haropa'::name, 'clc_2018_ca_haropa_no_255'::name, 'rast'::name);
select AddRasterConstraints('haropa'::name, 'clc_2018_ca_haropa_no_255'::name, 'rast'::name);

select DropRasterConstraints('haropa'::name, 'clc_2018_imd_haropa_no_255'::name, 'rast'::name);
select AddRasterConstraints('haropa'::name, 'clc_2018_imd_haropa_no_255'::name, 'rast'::name);



---------------------------------
-- Éclatage des polygones d'OSCOM 
-- => pour ne pas avoir un seul polygone par commune

drop table if exists oscom_2012_in_haropa_dumped;
create table oscom_2012_in_haropa_dumped as (
	select fid, insee_comm, code_oscom, nom_tabref, (st_dump(geom_decoup)).geom as geom
	from oscom_2012_in_haropa oih 
);
alter table oscom_2012_in_haropa_dumped add column id serial primary key;
create index on oscom_2012_in_haropa_dumped using gist(geom);

drop table if exists oscom_2018_in_haropa_dumped;
create table oscom_2018_in_haropa_dumped as (
	select fid, insee_comm, code_oscom, nom_tabref, (st_dump(geom_decoup)).geom as geom
	from oscom_2018_in_haropa oih 
);
alter table oscom_2018_in_haropa_dumped add column id serial primary key;
create index on oscom_2018_in_haropa_dumped using gist(geom);


---------------------------------------------
-- Enrichissement des OSCOM avestc Copernicus
 
-- Enrichissement 2012
drop table if exists haropa.oscom_2012_enrichie;
create table haropa.oscom_2012_enrichie as
with stats_ca as (
	select osc.id, (st_summarystats(st_union(st_clip(ca.rast, osc.geom)),1)).*
	from oscom_2012_in_haropa_dumped osc, clc_2012_ca_haropa_no_255 as ca
	where st_intersects(ca.rast, osc.geom)
	group by osc.id
),
stats_imperm as (
	select osc.id, (st_summarystats(st_union(st_clip(imd.rast,osc.geom)),1)).*
	from oscom_2012_in_haropa_dumped osc, clc_2012_imd_haropa_no_255 imd
	where st_intersects(imd.rast, osc.geom)
	group by osc.id
)
select 
	osc.id, osc.code_oscom, osc.insee_comm, osc.nom_tabref origine,
	ca.count as count_veg, ca.sum as sum_veg, ca.mean as mean_veg, ca.stddev as stddev_veg, ca.min as min_veg, ca.max as max_veg,
	imd.count as count_imp, imd.sum as sum_imp, imd.mean as mean_imp, imd.stddev as stddev_imp, imd.min as min_imp, imd.max as max_imp,
	osc.geom
from oscom_2012_in_haropa_dumped osc, stats_ca ca, stats_imperm imd
where ca.id = osc.id
and imd.id = osc.id;


-- Enrichissement 2018
drop table if exists haropa.oscom_2018_enrichie;
create table haropa.oscom_2018_enrichie as
with stats_ca as (
	select osc.id, (st_summarystats(st_union(st_clip(ca.rast, osc.geom)),1)).*
	from oscom_2018_in_haropa_dumped osc, clc_2018_ca_haropa_no_255 as ca
	where st_intersects(ca.rast, osc.geom)
	group by osc.id
),
stats_imperm as (
	select osc.id, (st_summarystats(st_union(st_clip(imd.rast,osc.geom)),1)).*
	from oscom_2018_in_haropa_dumped osc, clc_2018_imd_haropa_no_255 imd
	where st_intersects(imd.rast, osc.geom)
	group by osc.id
)
select 
	osc.id, osc.code_oscom, osc.insee_comm, osc.nom_tabref origine,
	ca.count as count_veg, ca.sum as sum_veg, ca.mean as mean_veg, ca.stddev as stddev_veg, ca.min as min_veg, ca.max as max_veg,
	imd.count as count_imp, imd.sum as sum_imp, imd.mean as mean_imp, imd.stddev as stddev_imp, imd.min as min_imp, imd.max as max_imp,
	osc.geom
from oscom_2018_in_haropa_dumped osc, stats_ca ca, stats_imperm imd
where ca.id = osc.id
and imd.id = osc.id;


----------------------------------------------------
--	Traitement des polygones qui n'intersectent aucun centre de pixel de Copernicus : 
-- => ajout d'un buffer de 14m à la geom (car résolution de 20m)

-- Traitement 2012
with stats_ca as (
	select osc.id, (st_summarystats(st_union(st_clip(ca.rast, st_buffer(osc.geom, 14.2))),1)).*
	from oscom_2012_in_haropa_dumped osc, clc_2012_ca_haropa_no_255 as ca
	where st_intersects(ca.rast, osc.geom)
	group by osc.id
),
stats_imperm as (
	select osc.id, (st_summarystats(st_union(st_clip(imd.rast, st_buffer(osc.geom, 14.2))),1)).*
	from oscom_2012_in_haropa_dumped osc, clc_2012_imd_haropa_no_255 imd
	where st_intersects(imd.rast, osc.geom)
	group by osc.id
)
update oscom_2012_enrichie osc
set 
	count_veg = ca.count,
	sum_veg = ca.sum, 
	mean_veg = ca.mean,
	stddev_veg = ca.stddev,
	min_veg = ca.min,
	max_veg = ca.max,
	count_imp = imd.count, 
	sum_imp = imd.sum, 
	mean_imp = imd.mean, 
	stddev_imp = imd.stddev, 
	min_imp = imd.min, 
	max_imp = imd.max
from stats_ca ca, stats_imperm imd
where ca.id = osc.id
and imd.id = osc.id
and count_veg = 0; --si count_veg = 0, count_imp = 0 aussi (même maillage)

-- Ajout de la colonne <in_estuaire> pour passage en caté 8 si polygone non agri dans estuaire
alter table oscom_2012_enrichie 
drop column if exists in_estuaire,
add column in_estuaire boolean default false;

update oscom_2012_enrichie osc
set in_estuaire = case when st_within(st_centroid(osc.geom), re.geom_lae) then true else false end
from reserve_estuaire re
where st_within(st_centroid(osc.geom), re.geom_lae);

alter table oscom_2012_enrichie add constraint pk_oscom_2012_enrichie primary key(id);
create index on oscom_2012_enrichie using gist (geom);
select populate_geometry_columns('oscom_2012_enrichie'::regclass);
comment on table haropa.oscom_2012_enrichie is 'infos du CLC de couverture arborée 2012 et du CLC imperméabilisation 2012 rapportées à l''OSCOM 2012 pour la mission haropa ZAN 2022';


-- Traitement 2018
-- Traitement des polygones qui n'intersectent aucun centre de pixel de Copernicus : 
-- => ajout d'un buffer de 7m à la geom (car résolution de 10m)
with stats_ca as (
	select osc.id, (st_summarystats(st_union(st_clip(ca.rast, st_buffer(osc.geom, 14.2))),1)).*
	from oscom_2018_in_haropa_dumped osc, clc_2018_ca_haropa_no_255 as ca
	where st_intersects(ca.rast, osc.geom)
	group by osc.id
),
stats_imperm as (
	select osc.id, (st_summarystats(st_union(st_clip(imd.rast, st_buffer(osc.geom, 14.2))),1)).*
	from oscom_2018_in_haropa_dumped osc, clc_2018_imd_haropa_no_255 imd
	where st_intersects(imd.rast, osc.geom)
	group by osc.id
)
update oscom_2018_enrichie osc
set 
	count_veg = ca.count,
	sum_veg = ca.sum, 
	mean_veg = ca.mean,
	stddev_veg = ca.stddev,
	min_veg = ca.min,
	max_veg = ca.max,
	count_imp = imd.count, 
	sum_imp = imd.sum, 
	mean_imp = imd.mean, 
	stddev_imp = imd.stddev, 
	min_imp = imd.min, 
	max_imp = imd.max
from stats_ca ca, stats_imperm imd
where ca.id = osc.id
and imd.id = osc.id
and count_veg = 0; --si count_veg = 0, count_imp = 0 aussi (même maillage)


-- Ajout de la colonne <in_estuaire> pour passage en caté 8 si polygone non agri dans estuaire
alter table oscom_2018_enrichie 
drop column if exists in_estuaire,
add column in_estuaire boolean default false;

update oscom_2018_enrichie osc
set in_estuaire = case when st_within(st_centroid(osc.geom), re.geom_lae) then true else false end
from reserve_estuaire re
where st_within(st_centroid(osc.geom), re.geom_lae);

alter table haropa.oscom_2018_enrichie add constraint pk_oscom_2018_enrichie primary key(id);
create index on oscom_2018_enrichie using GIST (geom);
select populate_geometry_columns('oscom_2018_enrichie'::regclass);
comment on table haropa.oscom_2018_enrichie is 'infos du CLC de couverture arborée 2018 et du CLC imperméabilisation 2018 rapportées à l''OSCOM 2018 pour la mission haropa ZAN 2022';


-----------------------------------
-- Calcul de la catégorie du décret

alter table haropa.oscom_2012_enrichie 
drop column if exists cat_surf_decret,
add column cat_surf_decret varchar(10);

-- Décret 2012
with oscom_2012_decret as (
	select id,
	case
		-- catégories absolues
		when code_oscom in ('51') then '6'
		when code_oscom in ('31', '32') then '8'
		when code_oscom not in ('20', '21', '22', '23', '24') and in_estuaire is true then '8' -- les espaces non agricoles de l'estuaire vont en NAF
--		when code_oscom in ('20', '21', '22', '23', '24') and mean_imp > 20 then '3 ou 4'
		when code_oscom in ('20', '21', '22', '23', '24') then '7'
		-- catégories sous condition de couverture arborée puis sous condition d'imperméabilisation
		when origine like '%bati%' or origine like '%reservoir%' then '1'
		when origine like '%route%' then '2'
		when origine like '%ferre%' then '3 ou 4'
		-- espaces indéterminés par l'oscom
		when code_oscom in ('00', '11', '12', '13', '14', '15') and mean_veg > 30 then '8'
		when code_oscom in ('00', '11', '12', '13', '14', '15') and mean_imp >= 90 then '2'
		when code_oscom in ('00', '11', '12', '13', '14', '15') and (mean_imp >= 20 and mean_imp < 90)  then '3 ou 4'
		when code_oscom in ('00', '11', '12', '13', '14', '15') and mean_imp < 20 then '5'
		-- hors catégories (pour le débug, objectif = 0 hors catégories !)
		else 'hors cat'
	end as case_col -- ne pas mettre le même nom que la colonne qui est update sinon erreur sql
	from oscom_2012_enrichie
	--	group by id
)
update oscom_2012_enrichie as oscom set cat_surf_decret = od.case_col from oscom_2012_decret as od where od.id = oscom.id;

alter table oscom_2012_enrichie
drop column if exists artif,
add column artif boolean default false;

update oscom_2012_enrichie 
set artif = true
where cat_surf_decret in ('1', '2', '3 ou 4', '5');


-- Décret 2018
alter table oscom_2018_enrichie
drop column if exists cat_surf_decret,
add column cat_surf_decret varchar(10);

with oscom_2018_decret as (
	select id,
	case
		-- catégories absolues
		when code_oscom in ('51') then '6'
		when code_oscom in ('31', '32') then '8'
		when code_oscom not in ('20', '21', '22', '23', '24') and in_estuaire is true then '8' -- les espaces non agricoles de l'estuaire vont en NAF
--		when code_oscom in ('20', '21', '22', '23', '24') and mean_imp > 20 then '3 ou 4'
		when code_oscom in ('20', '21', '22', '23', '24') then '7'
		-- catégories sous condition de couverture arborée puis sous condition d'imperméabilisation
		when origine like '%bati%' or origine like '%reservoir%' then '1'
		when origine like '%route%' then '2'
		when origine like '%ferre%' then '3 ou 4'
		-- espaces indéterminés par l'oscom
		when code_oscom in ('00', '11', '12', '13', '14', '15') and mean_veg > 30 then '8'
		when code_oscom in ('00', '11', '12', '13', '14', '15') and mean_imp >= 90 then '2'
		when code_oscom in ('00', '11', '12', '13', '14', '15') and (mean_imp >= 20 and mean_imp < 90)  then '3 ou 4'
		when code_oscom in ('00', '11', '12', '13', '14', '15') and mean_imp < 20 then '5'
		-- hors catégories (pour le débug, objectif = 0 hors catégories !)
		else 'hors cat'
	end as case_col -- ne pas mettre le même nom que la colonne qui est update sinon erreur sql
	from oscom_2018_enrichie
	--	group by id
)
update oscom_2018_enrichie as oscom set cat_surf_decret = od.case_col from oscom_2018_decret as od where od.id = oscom.id;

alter table oscom_2018_enrichie
drop column if exists artif,
add column artif boolean default false;

update oscom_2018_enrichie 
set artif = true
where cat_surf_decret in ('1', '2', '3 ou 4', '5');


------------------
--		Verif 2012

-- calcul du pourcentage des surfaces des catégories de l'oscom dans la surface totale de l'emprise Haropa 
with somme_surfaces as (
		select sum(st_area(geom)) as tot_surf 
		from oscom_2012_enrichie
	),
	somme_surface_par_code_oscom as (
		select code_oscom, sum(st_area(geom)::real) as surf
		from haropa.oscom_2012_enrichie
		group by code_oscom
	),
	pourcentage_surface as	(
		select code_oscom, round((surf / tot_surf::real * 100)::integer, 0) || ' %' as pc_surf
		from somme_surfaces, somme_surface_par_code_oscom
	)
select code_oscom, pc_surf
from pourcentage_surface
--where pc_surf != '0 %'
order by code_oscom;


-- calcul du pourcentage des surfaces des catégories du décret ZAN dans la surface totale de l'emprise Haropa 
with 
	somme_surfaces as (
		select sum(st_area(geom)) as tot_surf 
		from oscom_2012_enrichie
	),
	somme_surface_par_cat_decret as (
		select cat_surf_decret, sum(st_area(geom)::real) as surf
		from haropa.oscom_2012_enrichie
		group by cat_surf_decret 
	),
	pourcentage_surface as	(
		select cat_surf_decret , round((surf / tot_surf::real * 100)::integer, 0) || ' %' as pc_surf
		from somme_surfaces, somme_surface_par_cat_decret
	)
select cat_surf_decret, pc_surf
from pourcentage_surface
--where pc_surf != '0 %'
order by cat_surf_decret;


------------------
--		Verif 2018

-- calcul du pourcentage des surfaces des catégories de l'oscom dans la surface totale de l'emprise Haropa 
with 
	somme_surfaces as (
		select sum(st_area(geom)) as tot_surf 
		from oscom_2018_enrichie
	),
	somme_surface_par_code_oscom as (
		select code_oscom, sum(st_area(geom)::real) as surf
		from haropa.oscom_2018_enrichie
		group by code_oscom
	),
	pourcentage_surface as	(
		select code_oscom, round((surf / tot_surf::real * 100)::integer, 0) || ' %' as pc_surf
		from somme_surfaces, somme_surface_par_code_oscom
	)
select code_oscom, pc_surf
from pourcentage_surface
--where pc_surf != '0 %'
order by code_oscom;


-- calcul du pourcentage des surfaces des catégories du décret ZAN dans la surface totale de l'emprise Haropa 
with 
	somme_surfaces as (
		select sum(st_area(geom)) as tot_surf 
		from oscom_2018_enrichie
	),
	somme_surface_par_cat_decret as (
		select cat_surf_decret, sum(st_area(geom)::real) as surf
		from haropa.oscom_2018_enrichie
		group by cat_surf_decret 
	),
	pourcentage_surface as	(
		select cat_surf_decret , round((surf / tot_surf::real * 100)::integer, 0) || ' %' as pc_surf
		from somme_surfaces, somme_surface_par_cat_decret
	)
select cat_surf_decret, pc_surf
from pourcentage_surface
--where pc_surf != '0 %'
order by cat_surf_decret;


-------------------------------------------
-----	ARTIFICIALISATION		-----------
-------------------------------------------

-------------------------------------------
----		Méthode vectorielle		-------

-- Création de la couche de différence 2012 <-> 2018 
-- ATTENTION création de oscom_artificialisation a pris 12h de nuit (sans sollicitation autre sur la base)
drop table if exists oscom_artificialisation;
create table oscom_artificialisation as (
	select oscom2018.id id_2018, oscom2018.code_oscom code_oscom2018, oscom2018.cat_surf_decret cat_2018, oscom2018.artif artif_2018, 
	oscom2012.id id_2012, oscom2012.code_oscom code_oscom2012, oscom2012.cat_surf_decret cat_2012, oscom2012.artif artif_2012, 
	st_multi(st_collectionextract(st_intersection(st_buffer(oscom2012.geom, 0.001), st_buffer(oscom2018.geom, 0.001)), 3)) geom
	from oscom_2012_enrichie oscom2012 , oscom_2018_enrichie oscom2018 
	where st_intersects(st_buffer(oscom2012.geom, 0.001), st_buffer(oscom2018.geom, 0.001))
);
create index on oscom_artificialisation using gist(geom);

-- Ajout du champ "changement" 
alter table oscom_artificialisation 
drop column if exists changement,
add column changement varchar(20) default null;

update oscom_artificialisation 
set changement = 'artificialisé'
where artif_2012 is false and artif_2018 is true;
update oscom_artificialisation 
set changement = 'désartificialisé'
where artif_2012 is true and artif_2018 is false;

-- Ajout de la surface
alter table oscom_artificialisation 
drop column if exists surface,
add column surface integer;

update oscom_artificialisation 
set surface = st_area(geom);

-- Ajout d'un identifiant
alter table oscom_artificialisation
drop column if exists id,
add column id serial primary key;


-------------------------------------------
----		Méthode raster			-------
--

-- Création des champs integer pour rasterisation
alter table oscom_2012_enrichie
drop column if exists cat,
add column cat integer default 0;
update oscom_2012_enrichie 
set cat = case when cat_surf_decret = '3 ou 4' then 3 else cat_surf_decret::integer end
where cat_surf_decret != 'hors cat';

alter table oscom_2018_enrichie
drop column if exists cat,
add column cat integer default 0;
update oscom_2018_enrichie 
set cat = case when cat_surf_decret = '3 ou 4' then 3 else cat_surf_decret::integer end
where cat_surf_decret != 'hors cat';


----
----	Rasterisation à 100m
----


-- Création d'un raster vide de résolution 100m
-- from https://nronnei.github.io/blog/2017/03/creating-rasters-from-scratch-in-postgis/ :

drop table if exists emptyraster_100m_resolution;
create table emptyraster_100m_resolution(rid SERIAL primary key,rast raster);
insert into emptyraster_100m_resolution(rast) 
SELECT ST_AddBand(
  -- Make empty raster
  ST_MakeEmptyRaster(
      1500, -- Raster width x (in pixels)
      1500, -- Raster width y (in pixels)
      3024310, -- Upper left X coordinate
      3595358, -- Upper left Y coordinate
      100, -- X Cell Resolution (in degrees) (~250m)
      100, -- Y Cell Resolution (in degrees) (~250m)
      0, -- X skew
      0, -- Y skew
      3035 -- SRID (WGS 84)
    ),
    -- We're making a single band raster, but you can add
    -- as many bands as you like by adding additional rows
    -- to the array.
    ARRAY [
      ROW(
        1,  -- Band index: sets this as the first band
        '8BUI'::text,  -- Pixel Type (string rep of ST_BandPixelType types)
        0,  -- Initialized pixel value
        0  -- Nodata Value
      )
    ]::addbandarg[]
);

-- Rasterisation des oscom enrichies
drop table if exists rast_100m_oscom_2012;
CREATE TABLE rast_100m_oscom_2012 AS
SELECT ST_Union(ST_AsRaster(
	geom, 
	rast, 
	'8BUI', 
	cat, 
	0)
) rast
FROM (select (case when cat_surf_decret = '3 ou 4' then '3' else cat_surf_decret end)::integer cat, geom from oscom_2012_enrichie where cat_surf_decret != 'hors cat') a, (SELECT rast FROM emptyraster_100m_resolution LIMIT 1) rast;

drop table if exists rast_100m_oscom_2018;
CREATE TABLE rast_100m_oscom_2018 AS
SELECT ST_Union(ST_AsRaster(
	geom, 
	rast, 
	'8BUI', 
	cat, 
	0)
) rast
FROM (select (case when cat_surf_decret = '3 ou 4' then '3' else cat_surf_decret end)::integer cat, geom from oscom_2018_enrichie where cat_surf_decret != 'hors cat') a, (SELECT rast FROM emptyraster_100m_resolution LIMIT 1) rast;



----
----	Rasterisation à 20m
----


-- Rasterisation des oscom enrichies
drop table if exists rast_20m_oscom_2012;
CREATE TABLE rast_20m_oscom_2012 AS
SELECT ST_Union(ST_AsRaster(
	geom, 
	rast, 
	'8BUI', 
	cat, 
	0)
) rast
FROM (select (case when cat_surf_decret = '3 ou 4' then '3' else cat_surf_decret end)::integer cat, geom from oscom_2012_enrichie where cat_surf_decret != 'hors cat') a, (SELECT rast FROM clc_2012_ca_haropa LIMIT 1) rast;

drop table if exists rast_20m_oscom_2018;
CREATE TABLE rast_20m_oscom_2018 AS
SELECT ST_Union(ST_AsRaster(
	geom, 
	rast, 
	'8BUI', 
	cat, 
	0)
) rast
FROM (select (case when cat_surf_decret = '3 ou 4' then '3' else cat_surf_decret end)::integer cat, geom from oscom_2018_enrichie where cat_surf_decret != 'hors cat') a, (SELECT rast FROM clc_2012_ca_haropa LIMIT 1) rast;


------------------------------------------------
--		Calcul de l'artificialisation des pixels

-- À 100m de résolution 
drop table if exists rast_100m_artificialisation;
create table rast_100m_artificialisation as (
	SELECT
	    ST_MapAlgebra(
	        r2012.rast,
	        r2018.rast,
	        'case when [rast2] <= 5 and [rast1] > 5 then 2 when [rast2] > 5 and [rast1] <= 5 then 1 else 0 end'
	    ) AS rast
	FROM rast_100m_oscom_2012 r2012 CROSS JOIN rast_100m_oscom_2018 r2018
);

-- À 20m de résolution 
drop table if exists rast_20m_artificialisation;
create table rast_20m_artificialisation as (
	SELECT
	    ST_MapAlgebra(
	        r2012.rast,
	        r2018.rast,
	        'case when [rast2] <= 5 and [rast1] > 5 then 2 when [rast2] > 5 and [rast1] <= 5 then 1 else 0 end'
	    ) AS rast
	FROM rast_20m_oscom_2012 r2012 CROSS JOIN rast_20m_oscom_2018 r2018
);






----- Vérif

select changement, count(*)
from oscom_artificialisation oa 
group by changement;

select changement, round(sum(st_area(geom))/10000) surf_ha
from oscom_artificialisation oa 
group by changement;