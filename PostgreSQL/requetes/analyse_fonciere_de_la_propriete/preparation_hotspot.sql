------------------------------------------------------------------------------------------------------------
-- 					Pr�traitement de la table des HOTSPOTS        
--  Les hotspots sont �clat�s en ilots constitu�s des polygones voisins de moins de 1500m.
--  Un nom est calcul� pour les ilots (par ex "Pays Fort - Ilot 4")
------------------------------------------------------------------------------------------------------------

-------------------------------------------
-- Cluster de polygones de 1500m
-------------------------------------------
drop table if exists hotspot_clustered cascade;
create table hotspot_clustered as (
	with hotspot_dumped as (   -- Les hotspots �clat�s
		select *, (st_dump(geom)).geom geom_dumped
		from hotspot_complet hs 
	),
	clusters as (    -- Affectation d'un cluster_id selon la distance de clustering par hotspot
		SELECT id, cid, ST_Collect(geom_dumped) AS cluster_geom
		FROM (
			SELECT id, ST_ClusterDBSCAN(geom_dumped, 1500, 1) over (partition by id) + 1 AS cid, geom_dumped
			from hotspot_dumped
		) t 
		group by id, cid
	)
	select cid ilot_id, '' ilot_nom, hs.id hotspot_id, "INTITULE" hotspot_nom, cluster_geom geom, "SITE CEN", "SITE ENS", "ZNIEFF 1", "FD", "SURFACE"
	from hotspot_complet hs, clusters c
	where hs.id = c.id
);


----------------------------------------------
-- Calcul du ilot_nom avec le num�ro de l'ilot
----------------------------------------------
with nb_ilots_par_hotspot as (
	select hotspot_id, count(*) c
	from hotspot_clustered
	group by hotspot_id
)
update hotspot_clustered hc
set ilot_nom = 
	case 
		when c = 1 then hotspot_nom
		else hotspot_nom || ' - Ilot ' || ilot_id
	end
from nb_ilots_par_hotspot n
where hc.hotspot_id = n.hotspot_id



