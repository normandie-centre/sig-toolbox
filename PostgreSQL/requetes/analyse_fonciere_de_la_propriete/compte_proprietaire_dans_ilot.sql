------------------------------------------------------------
-- 		Calcul de la table de r�sultats compt_prop_in_ilot
--  1. Calcul des propri�t�s de l'ilot (nb prop, ...)
--  2. Cr�ation de la table de r�sultats pour laquelle on agr�ge toutes les tup d'un m�me compte-prop par ilot
--  3. R�cup�ration des 3 plus gros prop par ilot
------------------------------------
---- Calcul des propri�t�s de l'ilot
alter table hotspot_clustered 
drop column if exists nb_prop_physique, add column nb_compte_prop integer,
drop column if exists nb_compte_prop, add column nb_prop_physique integer,
drop column if exists nb_uf, add column nb_uf integer,
drop column if exists moyenne_uf, add column moyenne_uf integer,
drop column if exists somme_uf, add column somme_uf integer;
--drop column if exists pourc_surf_arti, add column pourc_surf_arti integer;

with t1 as (
	select h.ilot_nom, count(distinct(pd.idpersonne)) nb_pers
	from hotspot_clustered h, tup_computed t, proprietaires_droits_cvdl pd 
	where h.ilot_nom = t.ilot_nom
	and t.idprocpte = pd.idprocpte
	group by h.ilot_nom
),
t2 as (
	select hc.ilot_nom, count(distinct(tc.idprocpte)) nb_comptprop, count(distinct(tc.idtup)) nb_uf, avg(st_area(tc.geom)) moyenne_uf, sum(st_area(tc.geom)) somme_uf --, sum(dcntarti) somme_surf_arti 
	from hotspot_clustered hc, tup_computed tc 
	where tc.ilot_nom = hc.ilot_nom
	group by hc.ilot_nom
)
update hotspot_clustered hc
set nb_prop_physique = t1.nb_pers,
	nb_compte_prop = t2.nb_comptprop,
	nb_uf = t2.nb_uf,
	moyenne_uf = t2.moyenne_uf,
	somme_uf = t2.somme_uf
--	pourc_surf_arti = round(t2.somme_surf_arti * 100 / st_area(hc.geom))
from t1, t2
where hc.ilot_nom = t1.ilot_nom
and hc.ilot_nom = t2.ilot_nom;


---------------------------------------------
---- Cr�ation de la table compte_prop_in_ilot 
drop table if exists compte_prop_in_ilot;
create table compte_prop_in_ilot as (
	with cpte_prop_identification as (
		select t.idprocpte, string_agg(distinct(ddenom), ' et ') nom_prop_anonym, string_agg(distinct(nom_prop), ' et ') nom_prop, count(distinct(idpersonne)) nb_prop
		from proprietaires_droits_cvdl pd, tup_computed t
		where t.idprocpte = pd.idprocpte
		group by t.idprocpte
	)
	select 	h.ilot_id, h.ilot_nom, 
			t.idprocpte, t.type_de_proprietaire, 
			 cpi.nb_prop, cpi.nom_prop_anonym, cpi.nom_prop, 
			round(st_area(st_union(t.geom))) surf,
			(case when st_area(st_union(t.geom)) > 0.05*st_area(h.geom) then true else false end) is_sup_5_pourcent,
			count(distinct(t.idtup)) nb_uf, 
			array_agg(distinct(idtup)) id_uf, 
			row_number() over(partition by h.ilot_nom order by st_area(st_union(t.geom)) desc) rang,
			st_union(t.geom) geom
	from hotspot_clustered h, tup_computed t, cpte_prop_identification cpi 
	where h.ilot_nom = t.ilot_nom
	and t.idprocpte = cpi.idprocpte
	group by h.ilot_id, h.ilot_nom, h.geom, t.idprocpte, t.type_de_proprietaire, cpi.nom_prop_anonym, cpi.nom_prop, cpi.nb_prop
	order by h.ilot_nom, st_area(st_union(t.geom)) desc
);



-------------------------------------------------
---- Calcul des plus grands comptes-proprietaires
alter table hotspot_clustered
drop column if exists un_compte_idprocpte, add column un_compte_idprocpte varchar(15),
drop column if exists un_compte_prop, add column un_compte_prop varchar(200),
drop column if exists un_compte_prop_nom, add column un_compte_prop_nom varchar(1000),
drop column if exists un_compte_prop_type, add column un_compte_prop_type varchar(200),
drop column if exists un_compte_prop_nb_uf, add column un_compte_prop_nb_uf integer,
drop column if exists un_compte_prop_surf, add column un_compte_prop_surf integer,
drop column if exists un_compte_prop_pourcent, add column un_compte_prop_pourcent integer,

drop column if exists deux_compte_idprocpte, add column deux_compte_idprocpte varchar(15),
drop column if exists deux_compte_prop, add column deux_compte_prop varchar(200),
drop column if exists deux_compte_prop_nom, add column deux_compte_prop_nom varchar(1000),
drop column if exists deux_compte_prop_type, add column deux_compte_prop_type varchar(200),
drop column if exists deux_compte_prop_nb_uf, add column deux_compte_prop_nb_uf integer,
drop column if exists deux_compte_prop_surf, add column deux_compte_prop_surf integer,
drop column if exists deux_compte_prop_pourcent, add column deux_compte_prop_pourcent integer,

drop column if exists trois_compte_idprocpte, add column trois_compte_idprocpte varchar(15),
drop column if exists trois_compte_prop, add column trois_compte_prop varchar(200),
drop column if exists trois_compte_prop_nom, add column trois_compte_prop_nom varchar(1000),
drop column if exists trois_compte_prop_type, add column trois_compte_prop_type varchar(200),
drop column if exists trois_compte_prop_nb_uf, add column trois_compte_prop_nb_uf integer,
drop column if exists trois_compte_prop_surf, add column trois_compte_prop_surf integer,
drop column if exists trois_compte_prop_pourcent, add column trois_compte_prop_pourcent integer;

update hotspot_clustered hc
set un_compte_idprocpte = cpi.idprocpte,
	un_compte_prop = cpi.nom_prop_anonym,
	un_compte_prop_nom = cpi.nom_prop,
	un_compte_prop_type = cpi.type_de_proprietaire,
	un_compte_prop_nb_uf = cpi.nb_uf,
	un_compte_prop_surf = cpi.surf,
	un_compte_prop_pourcent = round(100 * cpi.surf / st_area(hc.geom))
from compte_prop_in_ilot cpi
where hc.ilot_nom = cpi.ilot_nom 
and cpi.rang = 1;

update hotspot_clustered hc 
set deux_compte_idprocpte = cpi.idprocpte,
	deux_compte_prop = cpi.nom_prop_anonym,
	deux_compte_prop_nom = cpi.nom_prop,
	deux_compte_prop_type = cpi.type_de_proprietaire,
	deux_compte_prop_nb_uf = cpi.nb_uf,
	deux_compte_prop_surf = cpi.surf,
	deux_compte_prop_pourcent = round(100 * cpi.surf / st_area(hc.geom))
from compte_prop_in_ilot cpi
where hc.ilot_nom = cpi.ilot_nom 
and cpi.rang = 2;

update hotspot_clustered hc
set trois_compte_idprocpte = cpi.idprocpte,
	trois_compte_prop = cpi.nom_prop_anonym,
	trois_compte_prop_nom = cpi.nom_prop,
	trois_compte_prop_type = cpi.type_de_proprietaire,
	trois_compte_prop_nb_uf = cpi.nb_uf,
	trois_compte_prop_surf = cpi.surf,
	trois_compte_prop_pourcent = round(100 * cpi.surf / st_area(hc.geom))
from compte_prop_in_ilot cpi
where hc.ilot_nom = cpi.ilot_nom 
and cpi.rang = 3;


