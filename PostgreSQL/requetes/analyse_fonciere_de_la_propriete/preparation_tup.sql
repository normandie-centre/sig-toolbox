----------------------------------------------------------------------------------------
-- 						 Calcul de la couche des TUP  
--  Calcul de la couche des tup intersect�es avec les hotspots.
--  En effet, parfois une grande tup borde un hotspot. L'intersection est n�cessaire pour �viter de consid�rer la tup dans son ensemble.
-- 		1. Calcul de la typologie des propri�taires
-- 		2. Agr�gation des propri�taires non anonymis�es 
----------------------------------------------------------------------------------------

-------------------------------------------------------------------------
-- Cr�ation de la couche des TUP crois�es avec les hotspot
--- => Prise en compte de cas particuliers : tup sur plusieurs ilots, ...
drop table if exists tup_computed;
create table tup_computed as (
	select idtup, idprocpte, hotspot_id, hotspot_nom, ilot_nom, st_area(st_union(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(st_intersection(tc.geom, hc.geom))), 3)))) surf_uf_dans_hotspot, st_union(ST_Multi(ST_CollectionExtract(ST_ForceCollection(ST_MakeValid(st_intersection(tc.geom, hc.geom))), 3))) geom,
		case
			when catpro2txt in ('COMMUNE','COMMUNE ET STRUCTURE SOCIALE', 'COMMUNE ET PROPRIETE DIVISEE EN LOT', 'COMMUNE ET STRUCTURE DU FONCIER ENVIRONNEMENTAL', 'DEPARTEMENT', 'STRUCTURE INTERCOMMUNALE') then 'Collectivit�'
			when catpro2txt in ('ETAT', 'ETAT ET STRUCTURE FORESTIERE', 'ETAT ET ETABLISSEMENT PUBLIC D ETUDE OU DE RECHERCHE', 'ETAT ET PERSONNE MORALE AUTRE') then '�tat'
			when catpro2txt in ('ACTIVITE EXTRACTIVE', 'ASSOCIATION FONCIERE DE REMEMBREMENT', 'INVESTISSEUR PROFESSIONNEL', 'PERSONNE MORALE AUTRE','ETABLISSEMENT D ENSEIGNEMENT DU PRIMAIRE ET SECONDAIRE', 'ETABLISSEMENT DE SANTE', 'PROMOTEUR', 'INVESTISSEUR PROFESSIONNEL ET PERSONNE PHYSIQUE', 'PERSONNE MORALE AUTRE ET PERSONNE PHYSIQUE') then 'Personne morale'
			when catpro2txt in ('PROPRIETE DIVISEE EN LOT', 'SOCIETE CIVILE A VOCATION IMMOBILIERE', 'PROPRIETE DIVISEE EN LOT ET PERSONNE PHYSIQUE', 'SOCIETE CIVILE A VOCATION IMMOBILIERE ET PERSONNE PHYSIQUE') then 'Copropri�t� ou SCI'
			when catpro2txt in ('STRUCTURE FORESTIERE', 'STRUCTURE FORESTIERE ET PERSONNE PHYSIQUE') then 'Structure foresti�re'
			when catpro2txt in ('RESEAU ELECTRIQUE OU GAZ', 'RESEAU EAU OU ASSAINISSEMENT', 'RESEAU FERRE', 'SAFER', 'STRUCTURE DU FONCIER ENVIRONNEMENTAL', 'STRUCTURE DU FONCIER ENVIRONNEMENTAL ET SOCIETE CIVILE A VOCATION IMMOBILIERE', 'STRUCTURE FLUVIALE OU MARITIME', 'STRUCTURE SOCIALE', 'STRUCTURE SOCIALE ET STRUCTURE LIEE AUX CULTES') then 'Structure parapublique'
			when catpro2txt in ('PERSONNE PHYSIQUE') then 'Particulier'
			when catpro2txt in ('STRUCTURE AGRICOLE', 'STRUCTURE AGRICOLE ET PERSONNE PHYSIQUE', 'STRUCTURE AGRICOLE ET SOCIETE CIVILE A VOCATION IMMOBILIERE') then 'Structure agricole'
			else 'inconnu'
		end as type_de_proprietaire 
	from tup_cvdl_non_inter tc, hotspot_clustered hc
	where st_intersects(tc.geom, hc.geom)
	group by idtup, idprocpte, hotspot_id, hotspot_nom, ilot_nom, tc.catpro2txt 
);


-- ---------------------------------------------------------
-- -- Suppression des r�sidus de TUP issues de l'intersection
-- delete from tup_computed 
-- where surf_uf_dans_hotspot < 100;


-----------------------------------------
-- Agr�gation des donn�es non anonymis�es
alter table tup_computed
drop column if exists nom_prop, add column nom_prop varchar(1000);

with prop_nom as (
	select "IDPROPRIO" , string_agg(distinct("NOM"), ' et ') liste_noms
	from "SAP_non_anonymisees" sna 
	group by "IDPROPRIO" 
)
update tup_computed tc
set nom_prop = liste_noms
from prop_nom pn
where tc.idprocpte = pn."IDPROPRIO"
