Les utilitaires basés sur PostgreSQL.

Contient :
  - raster2pgsql : une interface pour intégrer un raster en base Postgres
  - PG Linker : un projet d'application node js pour créer des foreigns servers sans se fatiguer et avec des noms normalisés
