"""
Outil qui crée une interface basique d'import de données raster dans une base de données PostgreSQL via la commande _raster2pgsql_ 

Usage :
    python import_raster_into_pgdb.py
"""

__author__ = "Thomas Escrihuela <Thomas.Escrihuela@cerema.fr>"
__version__ = "1.0"


import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
import subprocess

raster2pgsql_cmd = "raster2pgsql.exe"
psql_cmd = "psql.exe"

class Interface(tk.Tk):
    
    """Notre fenêtre principale.
    Tous les widgets sont stockés comme attributs de cette fenêtre."""
    
    def __init__(self):
        """Constructeur de la classe
        Crée les attributs de l'objets (fichier et informations de connexion à la bdd)"""
        tk.Tk.__init__(self)
        self.fichier = ""
        self.host = "172.28.0.82"
        self.port = "5432"
        self.username = "postgres"
        self.password = "postgres"
        self.database = "postgres"
        self.table = "public.maTable"

        self.creer_widgets()

    def creer_widgets(self):
        
        """Permet de créer tous les widgets de l'objet"""
        # Widgets du chargement de fichier source et de lancement du l'import
        self.load = tk.Button(self, text="Import du raster", command=self.load_file, width=20)
        self.fichier_source = tk.Label(width=100)
        self.run = tk.Button(self, text="Lancer l'import", command=self.import_raster)

        # Widgets de base de données
        self.text_hostname_label = tk.Label(self, text="Nom de l'hote")
        self.text_hostname = tk.Entry(self)
        self.text_hostname.insert(0,self.host)

        self.text_port_label = tk.Label(self, text="Port")
        self.text_port = tk.Entry(self)
        self.text_port.insert(0,self.port)

        self.text_username_label = tk.Label(self, text="Utilisateur")
        self.text_username = tk.Entry(self)
        self.text_username.insert(0,self.username)

        self.text_password_label = tk.Label(self, text="Mot de passe")
        self.text_password = tk.Entry(self)
        self.text_password.insert(0,self.password)

        self.text_database_label = tk.Label(self, text="Base de données")
        self.text_database = tk.Entry(self)
        self.text_database.insert(0,self.database)

        self.text_table_label = tk.Label(self, text="Nom de la table en sortie")
        self.text_table = tk.Entry(self)
        self.text_table.insert(0,self.table)

        # Placement des widgets
        self.load.grid(row=0, column=0)
        self.fichier_source.grid(row=0, column=1)
        self.text_hostname_label.grid(row=1, column=0)
        self.text_hostname.grid(row=1, column=1)
        self.text_port_label.grid(row=2, column=0)
        self.text_port.grid(row=2, column=1)
        self.text_username_label.grid(row=3, column=0)
        self.text_username.grid(row=3, column=1)
        self.text_password_label.grid(row=4, column=0)
        self.text_password.grid(row=4, column=1)
        self.text_database_label.grid(row=5, column=0)
        self.text_database.grid(row=5, column=1)
        self.text_table_label.grid(row=6, column=0)
        self.text_table.grid(row=6, column=1)
        self.run.grid(row=7, column=2)


    def load_file(self):
        """Charge un fichier raster (Geotiff)"""
        file = filedialog.askopenfilename(filetypes=[("Geotiff", "*.tif"),("Tous les fichiers","*.*")])
        self.fichier = file
        self.fichier_source["text"] = file


    def import_raster(self):
        """Lance l'import de raster dans postgres via raster2pgsql"""
        # On actualise la valeur des variables avec ce qui a été rentré par l'utilisateur
        self.password = self.text_password.get()
        self.table = self.text_table.get()
        self.host = self.text_hostname.get()
        self.port = self.text_port.get()
        self.username = self.text_username.get()
        self.database = self.text_database.get()

        # Teste l'existence du fichier
        if not self.fichier:
            messagebox.showerror(message="Veuillez sélectionner un fichier raster en entrée",title='Absence de données raster')
            return

        command = u'set \"PGPASSWORD={}\" & {} {} {} | {} -h {} -p {} -U {} -d {}'.format(self.password, raster2pgsql_cmd, self.fichier, self.table, psql_cmd, self.host, self.port, self.username, self.database)
        
        p = subprocess.Popen(command,shell=True,stdin=None,stdout=subprocess.PIPE,stderr=subprocess.PIPE,encoding='latin-1')
        (out,err) = p.communicate()

        if p.returncode != 0:
            messagebox.showerror(message="Erreur lors de l'import : \n{}".format(err),title='Erreur de l\'import')
        else:
            messagebox.showwarning(message="Import réussi !", title='Succès de l\'intégration en base')


if __name__ == "__main__":
    app = Interface()
    app.title("Import de raster dans base de données Postgresql")
    app.mainloop()
