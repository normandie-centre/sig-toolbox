# Import de données raster dans PostgreSQL

## Execution du script

Le script **import_raster_into_pgdb.py** peut être lancé par python pour créer une interface graphique afin d'importer dans une base PostgreSQL des données raster.

L'interface propose de sélectionner un fichier Geotiff puis de rentrer les informations de la base de données telles que :
- l'hôte
- le port
- l'utilisateur / mot de passe
- le schéma et la table en sortie

L'extension PostGIS de gestion des rasters (*postgis_raster*) doit être au préalable chargée dans la base de données cible.

## Compilation d'un exécutable

Le script peut être également compilé en exécutable. Pour ceci, il faut avoir python3 et le module *pyinstaller* et lancer la commande suivante :
`pyinstaller.exe --clean --onefile --add-binary 'raster2pgsql.exe;.' --add-binary 'psql.exe;.' import_raster_into_pgdb.py`

Le dossier *dist* est alors créé et contiendra l'exécutable **import_raster_into_pgdb**.

## Lancer l'éxecutable

Pour une utilisation immédiate, l'éxecutable du dépôt peut directement être lancé (éxecutable dans **dist/**).
