Les utilitaires basés sur PostgreSQL.

Contient :
  - dba : des requêtes pour l'administration des données en base postgres
  - outils : recense les outils développés autour de postgres
  - requetes : des requêtes SQL d'études pouvant être réutilisées au besoin
