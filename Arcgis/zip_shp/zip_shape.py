# zip_shape
# juillet 2021
# Sébastien BOULAND, Cerema NC
# But : à partir d'un dossier regroupant un ensemble de couches shp (et ses sous-fichiers)
#       créer automatique des zip contenant les 4 à 8 fichiers d'une couche, dans le dossier spécifié

import arcgisscripting, os, glob, zipfile, arcpy                   # librairies arcgis (arcgisscripting et arcpy), gestion de fichiers (os, glob) et zip
from os import path as p                                           # partie d'une librairie de l'explorateur W10 gérant les chemins, avec un alias

gp = arcgisscripting.create()                                      # Create the Geoprocessor object
gp.overwriteoutput=1                                               # arcpy.overwriteOutput = True

def ZipShapes(in_path, out_path):                                  # définition de la sous-fonction, utilisée dans la partie principale
    arcpy.env.workspace = in_path                                  # relocalisation dans le dossier où sont les couches à traiter
    featureclasses = arcpy.ListFeatureClasses()                    # listage des couches d'entités (shp) présentes dans le dossier
    for shape in featureclasses :                                  # pour chaque couche d'entités (en s'assurant que c'est un shp)
        nom_shp = shape[0:shape.find('.')]                         # extraction du nom de la couche, sans extension
        pref = ""                                                  # en cas d'utilisation d'un prefixe, suivant le traitement
        nom_zip = pref + nom_shp                                   # creation du nom du zip
        zip_path = p.join(out_path, nom_zip + '.zip')              # creation du chemin du zip
        zip = zipfile.ZipFile(zip_path, 'w', zipfile.ZIP_DEFLATED) # ouverture en écriture du zip 
        champs_recherche = in_pat + "\\" + nom_shp                 # filtrage des fichiers se nommant comme le SHP dans le dossier
        for files in glob.glob('%s.*' %champs_recherche):          # pour chaque fichier constituant la couche portant le même nom
            test_zip = files.endswith('.zip')                      # vérification que le fichier n'est pas un zip (boucle infinie)
            testlock = files.endswith('.lock')                     # vérification que le fichier n'est pas un fichier temporaire qui verrouille un shp
            test_non_zip_lock = not (test_zip or testlock)         # test pour vérifier si le fichier n'est pas un zip, ni un '.lock'
            if test_non_zip_lock:                                  # si le fichier réussi le test précédent ...
                name_file = files[files.rindex('\\')+1:len(files)] # extraction du nom du fichier, pour eviter des arborescences imbriquées
                zip.write(p.join(out_path,files), name_file)       # écriture du fichier dans le zip
        zip.close()                                                # fermeture du zip pour passer au suivant
    print('Tous les fichiers ont été ajoutés aux zips appropriés')

if __name__ == '__main__':                                         # définition de la partie principale
    in_pat = arcpy.GetParameterInfo(0)                             # recueil du 1er paramètre d'entrée, au format "boites à outils"
    outpat = arcpy.GetParameterInfo(1)                             # recueil du 2em paramètre d'entrée, au format "boites à outils"
    ZipShapes(in_pat, outpat)                                      # appel de la fonction secondaire définie
