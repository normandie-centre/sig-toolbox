# script python

"""-----------------------------------------------------------------------------
  Script Name: Near_pk_from_photo
  Description: à partir d'une couche de pt (localisation de photos), 
               trouver le points le plus proche (par exemple le pk sur le Seine) 
               et ajouter une info, par ex la hauteur d'eau associée à la photo 
  Created By:  Sébastien Bouland, Cerema Normandie Centre / DADT / GEEL
  Date:        Novembre 2019
-----------------------------------------------------------------------------"""
""" !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
/!\ nécessite l'outil "Proche" dans l'ArcToolBox 'Boîte à outils d'analyse 
    / Jeu d'outils Proximité', a priori disponible qu'avec la licence 'Advanced'
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! """ 
# Import des modules 
import arcpy # fonction d'arcgis
import os    # gestion des fichiers

# Définition de l'environnement de travail
arcpy.env.workspace = "C:/Users/sebastien.bouland/Desktop/test_hauteur_eau/" 

# definition des couches utilisées /!\ BIEN VERIFIER LES CHEMINS D'ACCES !!!!!!!
shp_pt_photo = 'C:/Users/sebastien.bouland/Desktop/test_hauteur_eau/SHP_source/20180201-02_P_positions_drone.shp'
shp_pt_seine = 'C:/Users/sebastien.bouland/Desktop/test_hauteur_eau/pk_pt_100m_ZE.shp'
shp_pt_PhtHo = 'C:/Users/sebastien.bouland/Desktop/test_hauteur_eau/SHP_produite/photos_correspondance_avec_pk_bezier.shp'
tab_corr_pkH = 'C:/Users/sebastien.bouland/Desktop/test_hauteur_eau/assemblage_v2_pas100m_pas1min_4.csv'


# fonction de calcul des distances entre les photos et les pk
"""-----------------------------------------------------------------------------
    ajoute les chps 
        NEAR_FID  (Identifiant d'objet de l'entité de proximité la plus proche. 
                   Si aucune entité en entrée n'est trouvée, la valeur est -1)
        NEAR_DIST (Distance entre l'entrée et l'entité de proximité. 
                   Si aucune entité en entrée n'est trouvée, la valeur est -1)
        NEAR_FC   (Chemin d'accès du catalogue à la classe d'entités qui 
                   contient l'entité Proche.
                   Ce champ est ajouté à la table en sortie uniquement lorsque
                   plusieurs entités de proximité sont spécifiées. 
                   Si aucune entité de proximité n'est détectée, la valeur sera
                   une chaîne vide ou Null.)
-----------------------------------------------------------------------------"""
def near_pk(shp_photo, shp_numPK):
    arcpy.Near_analysis(shp_photo,shp_numPK,'1000 meters','LOCATION','ANGLE','PLANAR')
    """ '1 kilometers' : rayon max de recherche
        'LOCATION' :     Les infos liées aux emplacements sont écrites 
        'ANGLE' :        Les valeurs d'angle de proximité sont ds NEAR_ANGLE
        'PLANAR' :       Méthode de calcul des distances <> 'GEODESIC'
        /!\ crée une couche temporaire
    """
    
near_pk(shp_pt_photo, shp_pt_seine)


# À cette étape, pour chaque cliché, il y a son 'pk' le plus proche.
# il reste donc à récupérer l'heure du cliché et avec le pk le plus proche pour,
# avec le CSV servant de table de correspondance, retrouver la hauteur d'eau

# 1- on met le csv et les données des photo dans un tableau

# a. données des pk issues du csv en utilisant un curseur, et liste des horodatages
chps_csv_pkH = [f.name for f in arcpy.ListFields(tab_corr_pkH)]
list_nom_champs_csv = []
list_interpolat_csv = []
list_horodatage_csv = []
list_heure_csv = []
list_date__csv = []
with arcpy.da.SearchCursor(tab_corr_pkH, chps_csv_pkH) as cursor_csv:
    for row_csv in cursor_csv :
        list_interpolat_csv.append(row_csv) # on prend tout
        list_horodatage_csv.append(row_csv[0])
        list_heure_csv.append(row_csv[1])
        list_date__csv.append(row_csv[2])
del cursor_csv # sinon la 'couche' reste verrouillée
list_nom_champs_csv = list_interpolat_csv[0]

# b. données des clichés en utilisant un curseur (Id_Heau, PK_near, PhotHeur, PhotDate)
chps_cliches_avec_pk = [f.name for f in arcpy.ListFields(shp_pt_PhtHo)]
ind_Id_Heau = int(chps_cliches_avec_pk.index('Id_Heau'));
ind_PhotDat = int(chps_cliches_avec_pk.index('STRdate'));
ind_PhotHeu = int(chps_cliches_avec_pk.index('PHOTHEUR'));
ind_Pk_Near = int(chps_cliches_avec_pk.index('PK_near'));
list_nom_champs_PhotPK = []
list_interpolat_PhotPK = []
with arcpy.da.SearchCursor(shp_pt_PhtHo, chps_cliches_avec_pk) as cursor_PhotPK:
    for row_photPK in cursor_PhotPK :
        list_interpolat_PhotPK.append([row_photPK[ind_Id_Heau],row_photPK[ind_PhotDat],row_photPK[ind_PhotHeu],row_photPK[ind_Pk_Near]]) # on prend tout
del cursor_PhotPK # sinon la 'couche' reste verrouillée
list_nom_champs_PhotPK = chps_cliches_avec_pk


# correspondante des hauteurs d'eau
list_idPhot_Heau = []
for cliche_pk in list_interpolat_PhotPK:
    #print ("est")
    # à partir du pk associé au cliché, on repère dans la table de correspondance la bonne colonne
    clich_pk_float = float(cliche_pk[3])
    pk_clich = list_nom_champs_csv.index(clich_pk_float)
    # on recherche maintenant la 'ligne' correspondant à l'horodatage du cliché dans le csv
    Dat_clich = cliche_pk[1];       Heu_clich = cliche_pk[2][:5] + ":00"; # troncature de la minute inf du cliché pour correspondre au csv
    PK_clich  = cliche_pk[3];       id_clich = cliche_pk[0]
    hauteur = 0;                    num_lig = 1;        
    #print(Dat_clich + " - " + Heu_clich)
    for data_csv in list_interpolat_csv :
        # on parcours la table du csv
        #print(data_csv[2]+"-"+Heu_clich+" et "+data_csv[1]+"-"+Dat_clich)
        if ((data_csv[2]==Heu_clich) and (data_csv[1]==Dat_clich)) :
            #print(id_clich)
            # si le cliché a la meme date et la meme heure que la ligne parcouru
            hauteur = data_csv[pk_clich] # on retient la hauteur d'eau
            list_idPhot_Heau.append([id_clich, Dat_clich, Heu_clich, clich_pk_float, hauteur])
            break # pour interrompre le parcours du csv
        else :
            num_lig += 1 # sinon on incrémente
    
    #list_idPhot_Heau.append([id_clich, Dat_clich, Heu_clich, clich_pk_float, hauteur])

# on met à jours la couche des photos_correspondance_avec_pk_bezier grâce à l'identifiant unique

chps_cliches_avec_pk = [f.name for f in arcpy.ListFields(shp_pt_PhtHo)]
ind_Id_Heau = int(chps_cliches_avec_pk.index('Id_Heau'));
ind_Heau_LH = int(chps_cliches_avec_pk.index('Heau_LH'));
ind_verific = int(chps_cliches_avec_pk.index('Verif'));
with arcpy.da.UpdateCursor(shp_pt_PhtHo, chps_cliches_avec_pk) as cursor_PhotPK:
    for row_photPK in cursor_PhotPK :
        Hautor = 0
        for photo_pk_h in list_idPhot_Heau:
            if photo_pk_h[0]==row_photPK[ind_Id_Heau]:
                Hautor = photo_pk_h[4]
                row_photPK[ind_Heau_LH] = Hautor
                row_photPK[ind_verific] = "Id_Heau : " + photo_pk_h[0] + " date: " + photo_pk_h[1] + " heure: " + photo_pk_h[2] + " pk: " + str(photo_pk_h[3]) + " HeauLH: " + str(photo_pk_h[4])
                break
        cursor_PhotPK.updateRow(row_photPK)      
del cursor_PhotPK # sinon la 'couche' reste verrouillée


# attention : il faut fermer la table attributaire et la rouvrir pour voir les résultats