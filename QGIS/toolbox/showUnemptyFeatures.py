# -*- coding: utf-8 -*-
"""
    Outil permettant d'afficher les informations d'une couche dans les champs sont non vides non nuls
    
    Usage :
    -------
        Input : 
            1. Sélectionner une ou plusieurs entités de la couche active
            2. Lancer le script
        Output:
            Affiche pour chaque entité une popup avec ses champs et valeurs non nulles non vides
    
    Developer : Thomas Escrihuela <Thomas.Escrihuela@cerema.fr>
"""

import qgis
from qgis.utils import iface
from PyQt5.QtWidgets import QMessageBox
from qgis.core import (QgsProcessingException,
                       QgsProcessingAlgorithm)

class showFeaturesWithUnemptyValues(QgsProcessingAlgorithm):
    
    def createInstance(self):
        return showFeaturesWithUnemptyValues()

    def name(self):
        return 'showFeaturesWithUnemptyValues'

    def displayName(self):
        return 'showFeaturesWithUnemptyValues'

    def group(self):
        return 'Cerema Normandie-Centre'

    def groupId(self):
        return 'cerema-nc'
        
    def shortHelpString(self):
        return 'Itère sur les entités de la couche active (zoomer et infos).'

    def initAlgorithm(self, config=None):
        return None

    def processAlgorithm(self, parameters, context, feedback):
        
        lay = iface.activeLayer()
        if lay is None:
            msg = QMessageBox()
            msg.setText('Aucune couche sélectionnée. Arrêt de l\'algorithme.')
            msg.exec()
            return (dict(result=False))
            
        features = lay.selectedFeatures()
        if features is None:
            msg = QMessageBox()
            msg.setText('Aucune entité sélectionnée. Arrêt de l\'algorithme.')
            msg.exec()
            return (dict(result=False))

        fields = [field.name() for field in lay.dataProvider().fields()]
        for f in features:
            res = ''
            for fi in fields:
                if 'NULL' not in str(f[fi]) and f[fi] != 0:
                    res += str(fi) + ': ' + str(f[fi]) + '#'
            msg = QMessageBox()
            msg.setText(res.replace('#','\n'))
            msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Abort)
            ret = msg.exec()
            if ret == QMessageBox.Abort:
                return (dict(result=False))

        return (dict(result=True))
