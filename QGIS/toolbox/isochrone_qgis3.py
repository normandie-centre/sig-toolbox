# -*- coding: utf-8 -*-

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessing,
                       QgsFeature,
                       QgsGeometry,
                       QgsFeatureSink,
                       QgsProcessingParameterEnum,
                       QgsMessageLog,
                       QgsProcessingException,
                       QgsProcessingAlgorithm,
                       QgsProcessingParameterFeatureSource,
                       QgsProcessingParameterString,
                       QgsProcessingParameterFeatureSink)
import processing
from urllib.request import urlopen, HTTPError
import ssl
import xml.etree.ElementTree as ET
import requests
import json
from pprint import pprint

graphes = ['Pieton','Velo','Velo electrique','Voiture']

class calculisochrone(QgsProcessingAlgorithm):
    
    INPUT = 'INPUT'
    OUTPUT = 'OUTPUT'
    TEMPS = 'TEMPS'
    GRAPHE = 'GRAPHE'

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return calculisochrone()

    def name(self):
        return 'calculisochrone'

    def displayName(self):
        return self.tr('Calcul d\'Isochrone')

    def group(self):
        return self.tr('Cerema Normandie-Centre')

    def groupId(self):
        return 'cerema-nc'
        
    def shortHelpString(self):
        return self.tr('Calcule un isochrone à partir du service d\'isochrone du Géoportail (OSR pour le vélo).')

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT,
                self.tr('Couche vecteur en entrée'),
                [QgsProcessing.TypeVectorPoint],
            )
        )

        self.addParameter(
            QgsProcessingParameterString(
                self.TEMPS,
                self.tr('Temps en secondes pour l\'isochone'),
                defaultValue = '900'
            )
        )

        self.addParameter(
            QgsProcessingParameterEnum(
                self.GRAPHE,
                self.tr("Choix du graphe"),
                graphes,
                defaultValue = 0
            )
        )

        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT,
                self.tr('Isochrone')
            )
        )
    
    def processAlgorithm(self, parameters, context, feedback):
        
        log = feedback.setProgressText

        source = self.parameterAsSource(
            parameters,
            self.INPUT,
            context
        )
        
        if source is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.INPUT))

        (sink, dest_id) = self.parameterAsSink(
            parameters,
            self.OUTPUT,
            context,
            source.fields(),
            Type.Polygon,
            source.sourceCrs()
        )

        if sink is None:
            raise QgsProcessingException(self.invalidSinkError(parameters, self.OUTPUT))
            
        temps = self.parameterAsString(
            parameters,
            self.TEMPS,
            context
        )
        
        grapheindex = self.parameterAsEnum(
            parameters,
            self.GRAPHE,
            context
        )

        total = 100.0 / source.featureCount()
        features = source.getFeatures()

        for current, feature in enumerate(features):
            if feedback.isCanceled():
                break

            geom = feature.geometry()
            if not geom:
                feedback.pushInfo('[WARN] L\'entité d\'id {} n\'a pas de géométrie associée'.format(feature.id()))
                feedback.pushInfo('Détails de l\'entité : {} \n'.format(feature.attributes()))
                continue
            
            coordX = geom.asPoint().x()
            coordY = geom.asPoint().y()
            xy = str(coordX) + "," + str(coordY)            
            if graphes[grapheindex] == 'Velo':
                req = 'https://api.openrouteservice.org/v2/isochrones/cycling-regular'
                
                body = {"locations":[[coordX,coordY]],"range":[temps]}
                print(body)

                headers = {
                    'Accept': 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8',
                    'Authorization': '5b3ce3597851110001cf62486e9a2a52c53f4405b666ce81b443965b',
                    'Content-Type': 'application/json; charset=utf-8'
                }
                call = requests.post(req, json=body, headers=headers)
                js = call.json()
                isochrone_geom = 'POLYGON (('+str(js['features'][0]['geometry']['coordinates']).replace('], [',';').replace(',','').replace(';',', ').replace('[','').replace(']','')+'))'
            elif graphes[grapheindex] == 'Velo electrique':
                req = 'https://api.openrouteservice.org/v2/isochrones/cycling-electric'
                
                body = {"locations":[[coordX,coordY]],"range":[temps]}
                print(body)

                headers = {
                    'Accept': 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8',
                    'Authorization': '5b3ce3597851110001cf62486e9a2a52c53f4405b666ce81b443965b',
                    'Content-Type': 'application/json; charset=utf-8'
                }
                call = requests.post(req, json=body, headers=headers)
                js = call.json()
                isochrone_geom = 'POLYGON (('+str(js['features'][0]['geometry']['coordinates']).replace('], [',';').replace(',','').replace(';',', ').replace('[','').replace(']','')+'))'
            else:
                req = "https://wxs.ign.fr/choisirgeoportail/isochrone/isochrone.xml?location=" + xy + "&method=time&graphName=" + graphes[grapheindex] + "&exclusions=&time=" + temps + "&holes=false&smoothing=true&srs=" + source.sourceCrs().authid()
                feedback.pushInfo(req)
                context = ssl._create_unverified_context()
                
                c = 0
                ok = False
                while (not ok) and (c < 5):
                    c+=1
                    try:
                        response = urlopen(req,context=context).read()
                        ok = True
                    except HTTPError as e:
                        feedback.pushInfo("Erreur : " + e.reason + ". Essai " + str(c) + "...")

                root = ET.fromstring(response)
                isochrone_geom = root.find('wktGeometry').text

            feat = QgsFeature(feature)
            feat.setGeometry(QgsGeometry.fromWkt(isochrone_geom))
                
            sink.addFeature(feat, QgsFeatureSink.FastInsert)
            feedback.setProgress(int(current * total))

        return {self.OUTPUT: dest_id}
