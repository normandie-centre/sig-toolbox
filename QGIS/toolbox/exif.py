# -*- coding: utf-8 -*-

"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (
    QgsCoordinateReferenceSystem,
    QgsProcessingAlgorithm,
    QgsProcessingException,
    QgsProcessingParameterFile,
    QgsProcessingUtils,
    QgsProject,
    QgsVectorLayer
)
from qgis import processing
from qgis.utils import iface

import os
from exif import Image
import csv


#############
## Constantes
photos_format = ('jpg', 'png')
infobulle = "<img src='file:///[%path%]' alt='pas de photo' width=500>"
csv_columns = ['path', 'lat', 'lon']


#############
##  Fonctions

# from https://medium.com/spatial-data-science/how-to-extract-gps-coordinates-from-images-in-python-e66e542af354
def decimal_coords(coords, ref):
    decimal_degrees = coords[0] + coords[1] / 60 + coords[2] / 3600
    if ref == 'S' or ref == 'W':
        decimal_degrees = -decimal_degrees
        
    return decimal_degrees

def image_coordinates(image_path, feedback):
    with open(image_path, 'rb') as src:
        img = Image(src)
    if img.has_exif:
        try:
            coords = (
                decimal_coords(img.gps_latitude, img.gps_latitude_ref),
                decimal_coords(img.gps_longitude, img.gps_longitude_ref)
            )
        except AttributeError:
            feedback.pushInfo(f'Pas de coordonnées GPS dans la photo {image_path}.')
            return
    else:
        feedback.pushInfo(f"La photo {image_path} n'a pas d'informations EXIF")
        return

    return image_path, *coords


#######################
##  Classe du programme
class Importphotos(QgsProcessingAlgorithm):
    """Boîte à outils pour importer des données photos dans QGIS."""

    INPUT = 'INPUT'
    OUTPUT = 'OUTPUT'


    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return Importphotos()

    def name(self):
        return 'import_photos'

    def displayName(self):
        return self.tr('Import photos')

    def group(self):
        return self.tr('Cerema')

    def groupId(self):
        return 'cerema'

    def shortHelpString(self):
        return self.tr("Import de photos géoréférencées depuis un dossier")

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT,
                self.tr('Dossier photos'),
                behavior=QgsProcessingParameterFile.Folder
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
            Fonctionnement de l'algo : 
                1. Extraction de la géolocalisation des photos par les tags EXIF
                2. Création d'une couche de ponctuelle avec infobulles des photos
        """

        coords = []

        photos_folder = self.parameterAsString(
            parameters,
            self.INPUT,
            context
        )
        if photos_folder is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.INPUT))


        #############################
        ## Extraction des coordonnées
        for image in os.listdir(photos_folder):
            if image.lower().endswith(photos_format):
                coord = image_coordinates(os.path.abspath(f"{photos_folder}/{image}"), feedback)
                coords.append(coord) if coord else None
        
        if not coords:
            raise QgsProcessingException("Le dossier [{}] ne contient pas de photos au format [{}]. Archive photos non détectée, fin du programme...".format(photos_folder, ', '.join(photos_format)))

        temp_csv = f"{QgsProcessingUtils.tempFolder()}/coords.csv"
        with open(temp_csv , 'w', newline='', encoding = 'utf-8') as f:
            wr = csv.writer(f)
            wr.writerow(csv_columns)
            wr.writerows(coords)
        
        
        ###################################################
        ##  Création de la couche de photos avec infobulles
        out = processing.run("native:createpointslayerfromtable", {
            'INPUT': temp_csv ,
            'XFIELD': 'lon',
            'YFIELD': 'lat',
            'ZFIELD':'',
            'MFIELD':'',
            'TARGET_CRS': QgsCoordinateReferenceSystem('EPSG:4326'),
            'OUTPUT': 'TEMPORARY_OUTPUT'}
        )
        out['OUTPUT'].setMapTipTemplate(infobulle)
        iface.actionMapTips().setChecked(True)  
        QgsProject.instance().addMapLayer(out["OUTPUT"])
        
        return {self.OUTPUT: "Données photos importées"}
