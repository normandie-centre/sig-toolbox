# -*- coding: utf-8 -*-
"""
Outil permettant de télécharger par flux WFS et d'afficher les données de l'INPN découpées sur une couche.

Usage :
-------
    Input : 
        1. Lancer la boîte à outils
        2. Sélectionner une couche de masque
        3. <Optionnel> Choisir un répertoire de sortie où télécharger la donnée.
    Output:
        pour chaque entité une popup avec ses cha

Developer : Thomas Escrihuela <Thomas.Escrihuela@cerema.fr>
"""

from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import ( QgsProcessing,
                        QgsVectorLayer,
                        QgsLayerTreeLayer,
                        QgsLayerTreeGroup,
                        QgsProcessingAlgorithm,
                        QgsProcessingContext,
                        QgsProcessingException,
                        QgsProcessingOutputMultipleLayers,
                        QgsProcessingParameterFeatureSource,
                        QgsProcessingParameterFolderDestination)

import processing
import xml.etree.ElementTree as ET
import requests
import re
import os
import zipfile


###############
#### Paramètres
zip_directory_name = "zip"
shape_directory_name = "shape"
getcap_inpn_wfs = "http://ws.carmencarto.fr/WFS/119/fxx_inpn?request=GetCapabilities"
getfeature_inpn_wfs_template = "http://ws.carmencarto.fr/WFS/119/fxx_inpn?SERVICE=WFS&REQUEST=GetFeature&VERSION=2.0.0&TYPENAME=<layer>&SRSNAME=EPSG:2154&BBOX=<bbox>"


##############
#### Fonctions
def download_url(url, save_path, chunk_size=128):
    r = requests.get(url, stream=True)
    with open(save_path, 'wb') as fd:
        for chunk in r.iter_content(chunk_size=chunk_size):
            fd.write(chunk)

def create_directory(absolute_path_to_directory):
    if not os.path.exists(absolute_path_to_directory):
        os.makedirs(absolute_path_to_directory)
    
def substitute_tag(tag, value, string):
    return re.sub(tag, value, string)


######################
#### Classe principale
class getINPNdata(QgsProcessingAlgorithm):
    
    INPUT = 'INPUT'
    OUTPUT_FOLDER = 'OUTPUT_FOLDER'
    OUTPUT_LAYERS = 'OUTPUT_LAYERS'

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT,
                self.tr('Emprise sur laquelle importer les données INPN'),
                [QgsProcessing.TypeVectorPolygon],
            )
        )

        self.addParameter(
            QgsProcessingParameterFolderDestination(
                self.OUTPUT_FOLDER,
                self.tr('Répertoire de destination')
            )
        )
    
        self.addOutput(
            QgsProcessingOutputMultipleLayers(
                self.OUTPUT_LAYERS,
                self.tr('Output layers')
            )
        )
        
        
    def processAlgorithm(self, parameters, context, feedback):
        
        log = feedback.setProgressText

        source = self.parameterAsSource(
            parameters,
            self.INPUT,
            context
        )
        if source is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.INPUT))

        output_directory_name = self.parameterAsString(
            parameters,
            self.OUTPUT_FOLDER,
            context
        )
        
        
        ## Définition de la projection et de l'emprise de la couche source
        feedback.pushInfo("..................................................................")
        feedback.pushInfo("Définition de la couche source :")
        
        proj = source.sourceCrs().authid()
        ex = source.sourceExtent()
        (xmin, ymin, xmax, ymax) = (ex.xMinimum(), ex.yMinimum(), ex.xMaximum(), ex.yMaximum())
        bbox = "%s,%s,%s,%s,%s" %(xmin,ymin,xmax,ymax,proj)

        feedback.pushInfo("Répertoire de sortie : " + output_directory_name)
        feedback.pushInfo("Projection de la couche d'entrée : " + proj)
        feedback.pushInfo("Emprise de la couche d'entrée : %s, %s, %s, %s" %(xmin,ymin,xmax,ymax))


        ## Création des répertoires de sortie
        zip_directory = os.path.join(output_directory_name, zip_directory_name)
        shape_directory = os.path.join(output_directory_name, shape_directory_name)
        create_directory(zip_directory)
        create_directory(shape_directory)


        ## Liste les données dispo dans le GetCap
        feedback.pushInfo("..................................................................")
        feedback.pushInfo("Appel du GetCapabilities : ")
        feedback.pushInfo(getcap_inpn_wfs)
        
        r = requests.get(getcap_inpn_wfs)
        root = ET.fromstring(r.text)
        layers = root.findall(".//{http://www.opengis.net/wfs}FeatureType//{http://www.opengis.net/wfs}Name")
        layers = [l.text for l in layers]
        nb_layers = len(layers)
        total = 100.0 / nb_layers
        feedback.pushInfo("Nombre de couches trouvées dans le GetCap : %s" %(nb_layers))

        ## Téléchargement et import des données
        output_layers = []
        for i, l in enumerate(layers):
            if feedback.isCanceled():
                break
            
            feedback.pushInfo("..................................................................")
            feedback.pushInfo("%s/%s : Récupération des données de %s" %(i+1, nb_layers, l))
        
            ## Récupération du nombre d'objets dans l'emprise source
            getfeature_inpn_wfs_temp = substitute_tag('<bbox>', bbox, getfeature_inpn_wfs_template)
            getfeature_inpn_wfs = substitute_tag('<layer>', l, getfeature_inpn_wfs_temp)
            r = requests.get(getfeature_inpn_wfs)
            root = ET.fromstring(r.text)
            nb_features = root.get('numberReturned')

            if nb_features == '0':
                feedback.pushInfo("    Pas d'entité trouvée. On passe...")
            else:
                feedback.pushInfo("    " + nb_features + " entitées trouvées.")
                
                ## Télécharge la donnée dans le répertoire 'zip'
                download_request = getfeature_inpn_wfs + "&outputformat=shape"
                zip = os.path.join(zip_directory, l + ".zip")
                download_url(download_request, zip)

                ## Extraction du zip dans le dossier 'shapefile'
                with zipfile.ZipFile(zip, 'r') as zip_ref:
                    zip_ref.extractall(shape_directory)
                
                ## Ajout du shapefile au projet
                s = QgsVectorLayer(os.path.join(shape_directory, l + ".shp"), l, "ogr")
                if not s.isValid():
                    feedback.reportError("Erreur de chargement de " + os.path.join(shape_directory, l + ".shp"))
                else:
                    output_layers.append(s)
                    context.temporaryLayerStore().addMapLayer(s)
                    context.addLayerToLoadOnCompletion(
                        s.id(),
                        QgsProcessingContext.LayerDetails(
                            l,
                            context.project(),
                            self.OUTPUT_LAYERS
                        )
                    )

            feedback.setProgress(int(i*total))
        
        return {self.OUTPUT_LAYERS: output_layers}



    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return getINPNdata()

    def name(self):
        return 'Récupère les données de l\'INPN à partir de flux wfs.'

    def displayName(self):
        return self.tr('Récupérer toutes les données de l\'INPN sur une emprise')

    def group(self):
        return self.tr('Cerema Normandie-Centre')

    def groupId(self):
        return 'cerema-nc'
        
    def shortHelpString(self):
        return self.tr('Récupère les données de l\'INPN en utilisant les flux wfs. Prend en paramètre une couche de polygone (de préférence en 2154) et récupère et affiche les données de l\'INPN, stockées au format shapefile dans le répertoire de destination.')
