# -*- coding: utf-8 -*-

"""
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
***************************************************************************
"""

from qgis.PyQt.QtCore import QCoreApplication
from qgis.gui import QgsMapCanvas
from qgis.core import (
    QgsProcessing,
    QgsProcessingAlgorithm,
    QgsProcessingException,
    QgsProcessingParameterEnum,
    QgsProcessingParameterFile,
    QgsProcessingParameterFolderDestination,
    QgsProject,
    QgsVectorLayer
)
from qgis import processing
from qgis.utils import iface

from sys import argv
import os
import simplejson as json 


#############
## Constantes
orientation = ['Portrait', 'Paysage']
infobulle_body = """<h2>Relevé n°[% "NumeroReleve" %]</h2>
<strong>Horodatage : </strong>[% "Horodatage" %]<br>
<strong>Commentaire texte : </strong>[% "CommentaireTexte" %]<br>
<div style="width:500px;">
	<img src='file:///[%concat(file_path(layer_property(@layer, 'path')), '/Photos/', "NomFichierPhoto")%]' alt='pas de photo' style="max-width:100%;height:auto;">
</div>"""


#############
##  Fonctions
def jsonscout2geojson(in_file, out_file):
    """Convertit un json Scout en geojson."""
    
    longitude_champs = "Longitude"
    latitude_champs = "Latitude"
    data = json.load(open(in_file))

    geojson = {
        "type": "FeatureCollection",
        "features": [
        {
            "type": "Feature",
            "geometry" : {
                "type": "Point",
                "coordinates": [d[longitude_champs], d[latitude_champs]],
                },
            "properties" : d,
        } for d in data]
    }

    output = open(out_file, 'w')
    json.dump(geojson, output)
    output.close()

    return None


#######################
##  Classe du programme
class ImportScout(QgsProcessingAlgorithm):
    """Boîte à outils pour importer des données Scout dans QGIS."""

    INPUT = 'INPUT'
    OUTPUT = 'OUTPUT'
    ORIENTATION = 'ORIENTATION'
    RELEVES = 'Releves.json'
    ITINERAIRES = 'Itineraires.json'


    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return ImportScout()

    def name(self):
        return 'import_scout'

    def displayName(self):
        return self.tr('Import SCOUT')

    def group(self):
        return self.tr('Cerema')

    def groupId(self):
        return 'cerema'

    def shortHelpString(self):
        return self.tr("Import de données depuis un dossier Scout")

    def initAlgorithm(self, config=None):
        self.addParameter(
            QgsProcessingParameterFile(
                self.INPUT,
                self.tr('Dossier Scout'),
                behavior=QgsProcessingParameterFile.Folder
            )
        )
        
        self.addParameter(
            QgsProcessingParameterEnum(
                self.ORIENTATION,
                self.tr('Orientation générale des photos'),
                orientation,
                defaultValue = 0
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        """
            Fonctionnement de l'algo : 
                1. Calcul du style des images de l'infobulle en fonction de l'orientation globale des photos
                2. Récupération du json des itinéraires, conversion en un geojson temporaire, transformation en une couche linéaire dans un geojson, suppression du geojson temporaire
                3. Récupération du json des relevés, conversion en geojson, association de l'infobulle, activation globale des infobulles niveau QGIS puis ajout de la couche
        """


        scout_folder = self.parameterAsString(
            parameters,
            self.INPUT,
            context
        )
        
        if scout_folder is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.INPUT))
        elif not os.path.isfile(os.path.join(scout_folder, self.RELEVES)):
            raise QgsProcessingException("Le dossier [{}] ne contient pas de fichier [{}]. Archive Scout non détectée, fin du programme...".format(scout_folder, self.RELEVES))

        orientation_index = self.parameterAsEnum(
            parameters,
            self.ORIENTATION,
            context
        )
        
        
        ###############
        ##  Itinéraires
        in_itineraires = os.path.join(scout_folder, self.ITINERAIRES)
        temp_out_itineraires = os.path.join(scout_folder, self.ITINERAIRES.replace('.json', '_temp.geojson'))
        out_itineraires = os.path.join(scout_folder, self.ITINERAIRES.replace('.json', '.geojson'))

        # Conversion en geojson
        jsonscout2geojson(in_itineraires, temp_out_itineraires)

        # Transformation en linéaire
        temp_itineraires = QgsVectorLayer(temp_out_itineraires, "temp", "ogr")
        if not temp_itineraires.isValid():
            raise QgsProcessingException("La couche [{}] n'a pas pu être chargée.".format(temp_out_itineraires))
        else:
            out_iti_path = processing.run("native:pointstopath", {
                'INPUT': temp_itineraires,
                'OUTPUT': out_itineraires,
            }, context=context, feedback=feedback)['OUTPUT']

            # Import dans QGIS
            out_iti = QgsVectorLayer(out_iti_path, "Itinéraires", "ogr")
            if not out_iti.isValid():
                raise QgsProcessingException("La couche [{}] n'a pas pu être chargée.".format(out_iti_path))
            else:
                QgsProject.instance().addMapLayer(out_iti)
        
        #os.remove(temp_out_itineraires)
        
        feedback.pushInfo('====> Création du geojson des itinéraires')
        
        
        ###########
        ##  Relevés
        in_releves = os.path.join(scout_folder, self.RELEVES) 
        out_releves = os.path.join(scout_folder, self.RELEVES.replace('.json', '.geojson')) 

        # Conversion en geojson
        jsonscout2geojson(in_releves, out_releves)
        
        # Import dans QGIS
        releves = QgsVectorLayer(out_releves, "Relevés", "ogr")
        if not releves.isValid():
            raise QgsProcessingException("La couche [{}] n'a pas pu être chargée.".format(out_releves))
        else:
            # Association de l'infobulle à la couche
            releves.setMapTipTemplate(infobulle_body)
            iface.actionMapTips().setChecked(True)      # Activation des infobulles dans le menu Vue
            QgsProject.instance().addMapLayer(releves)

        feedback.pushInfo('====> Création du geojson des relevés')
    
        
        return {self.OUTPUT: "Données Scout importées"}
