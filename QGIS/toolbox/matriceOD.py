
from qgis.PyQt.QtCore import QCoreApplication, QVariant
from qgis.core import (
    QgsFeature,
    QgsField,
    QgsFields,
    QgsFeatureSink,
    QgsMessageLog,
    QgsProcessing, 
    QgsProcessingParameterEnum,
    QgsProcessingException,
    QgsProcessingAlgorithm,
    QgsProcessingParameterFeatureSource,
    QgsProcessingParameterString,
    QgsProcessingParameterFeatureSink
)

import processing
from urllib.parse import urlencode
import requests
from datetime import datetime
from dateutil import tz
from random import *


################
## Paramètres ##
################

# Commun
proxies = {
    'http': 'http://pfrie-std.proxy.e2.rie.gouv.fr:8080',
    'https': 'http://pfrie-std.proxy.e2.rie.gouv.fr:8080'
}
defaultdate = '2019-11-09 07:30'
from_zone = tz.tzlocal()
to_zone = tz.tzutc()

# Google Maps
is_gmaps = False # A passer à "True" pour utilisation de l'API Google Maps
cle = 'AIzaSyCIVBf6mEG2qvQ-0PhLpVniq9CiNzxQor4'
traffic_model = ['best_guess', 'pessimistic', 'optimistic']

# SNCF
token = '96ecb230-7767-4e32-b5e8-f712bdcecfaa'

# Champs à ajouter dans la couche en sortie
gmaps_field_name = 'Temps routier'
sncf_field_name = 'Temps ferroviaire'
sncf_correspondance_field_name = 'Nombre de correspondances'
gmaps_field = QgsField(gmaps_field_name, QVariant.Int)
sncf_field = QgsField(sncf_field_name, QVariant.Int)
sncf_correspondance_field = QgsField(sncf_correspondance_field_name, QVariant.Int)


##########
## Main ##
##########

class matriceOD(QgsProcessingAlgorithm):
    
    INPUT = 'INPUT'
    OUTPUT = 'OUTPUT'
    DATE = 'DATE'
    MODEL = 'MODEL'

    def tr(self, string):
        
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        
        return matriceOD()

    def name(self):
        
        return 'matriceOD'

    def displayName(self):
        
        return self.tr('Calcul de temps de parcours routier + ferroviaire')

    def group(self):
        
        return self.tr('Cerema Normandie-Centre')

    def groupId(self):
        
        return 'cerema-nc'

    def shortHelpString(self):
        
        return self.tr('Crée une matrice Origine-Destination avec les temps de parcours routier (gmaps) et ferroviaires (sncf)')

    # Définit les entrées de l'algorithme
    def initAlgorithm(self, config=None):
        
        # Couche en entrée
        self.addParameter(
            QgsProcessingParameterFeatureSource(
                self.INPUT,
                self.tr('Tableau en entrée. Doit comprendre a minima les colonnes [origine], [x_orig], [y_orig], [destination], [x_dest], [y_dest].'),
                [QgsProcessing.TypeVector]
            )
        )
        
        # Date du calcul des estimations de temps
        self.addParameter(
            QgsProcessingParameterString(
                self.DATE,
                self.tr('Date pour le calcul du temps de parcours routier (au format yyyy-mm-dd hh:mm:ss)'),
                defaultValue=defaultdate
            )
        )
        
        # Choix du modèle de congestion de Google Maps
        self.addParameter(
            QgsProcessingParameterEnum(
                self.MODEL,
                self.tr('Choix du modèle'),
                traffic_model,
                defaultValue=0
            )
        )
        
        # Couche en sortie
        self.addParameter(
            QgsProcessingParameterFeatureSink(
                self.OUTPUT,
                self.tr('matriceOD')
            )
        )

    def processAlgorithm(self, parameters, context, feedback):
        
        log = feedback.setProgressText
        
        # Récupération de la couche en entrée
        source = self.parameterAsSource(
            parameters,
            self.INPUT,
            context
        )
        
        if source is None:
            raise QgsProcessingException(self.invalidSourceError(parameters, self.INPUT))
            
        # Création des champs supplémentaires pour la couche en sortie
        sink_fields = QgsFields(source.fields())
        sink_fields.append(gmaps_field)
        sink_fields.append(sncf_field)
        sink_fields.append(sncf_correspondance_field)
        
        # Définition de la couche en sortie
        sink, dest_id = self.parameterAsSink(
            parameters,
            self.OUTPUT,
            context,
            sink_fields
        )
        
        if sink is None:
            raise QgsProcessingException(self.invalidSinkError(parameters, self.OUTPUT))
        
        # Récupération de la date en entrée
        date = self.parameterAsString(
            parameters,
            self.DATE,
            context
        )

        # Calcule de la date au format Google Maps API & SNCF API
        local_date = datetime.strptime(date, '%Y-%m-%d %H:%M')
        local_date = local_date.replace(tzinfo=from_zone)
        utc_date = local_date.astimezone(to_zone)
        gmaps_date = int(datetime.timestamp(utc_date))
        sncf_date = datetime.strftime(local_date, '%Y%m%dT%H%M%S')
        
        # Récupération du modèle de congestion en entrée
        traffic_model_index = self.parameterAsEnum(
            parameters,
            self.MODEL,
            context
        )
        
        total = 100.0 / source.featureCount()
        
        # Itération sur chaque entité en entrée
        features = source.getFeatures()
        for current, feature in enumerate(features):
            
            if feedback.isCanceled():
                break
            
            # Ajout à la couche en sortie des attributs de la couche en entrée
            feat = QgsFeature(sink_fields)
            [feat.setAttribute(k, feature[k]) for k in feature.fields().names()]
            
            
            ###########
            ## GMAPS ##
            ###########
            
            # Si "is_maps" est vrai, calcul du temps route via API Google Maps, sinon valeur affectée au hasard
            if is_gmaps:
                # Récupération des coordonnées au format Google Maps API
                yx_orig = str(feature['y_orig']) + ',' + str(feature['x_orig'])
                yx_dest = str(feature['y_dest']) + ',' + str(feature['x_dest'])
                
                # Préparation de la requête web
                params = {
                    'origin':yx_orig,
                    'destination':yx_dest,
                    'mode':'driving',
                    'traffic_model':traffic_model[traffic_model_index],
                    'departure_time':gmaps_date,
                    'key':cle
                }                
                req = 'https://maps.googleapis.com/maps/api/directions/json?%s' % urlencode(params)
                
                # Execution de la requête + parsing de la réponse
                response = requests.get(req, proxies=proxies).json()                
                gmaps_time = response['routes'][0]['legs'][0]['duration_in_traffic']['value']
            
            else:
                gmaps_time = randint(1, 10000)
                

            ##########
            ## SNCF ##
            ##########
            
            # Récupération des coordonnées au format SNCF API
            xy_orig = str(feature['x_orig']) + ';' + str(feature['y_orig'])
            xy_dest = str(feature['x_dest']) + ';' + str(feature['y_dest'])
            
            # Préparation de la requête web            
            params = {
                'from':xy_orig,
                'to':xy_dest,
                'datetime':sncf_date,
                'datetime_represents':'departure'
            }
            req = 'https://api.sncf.com/v1/coverage/sncf/journeys?%s' % urlencode(params)
            
            # Execution de la requête + parsing de la réponse
            response = requests.get(req, proxies=proxies, auth=(token, '')).json()
            sncf_time = response['journeys'][0]['duration']
            sncf_correspondance = response['journeys'][0]['nb_transfers']
            
            #feedback.pushInfo('Le temps de trajet entre {} et {} est de : {} secondes par la route, {} secondes par le train\n'.format(feature['origine'], feature['destination'], gmaps_time, sncf_time))
            
            # Ajout des valeurs calculées aux champs de la couche en sortie
            feat.setAttribute(gmaps_field_name, gmaps_time)
            feat.setAttribute(sncf_field_name, sncf_time)
            feat.setAttribute(sncf_correspondance_field_name, sncf_correspondance)
            
            # Ajout de l'entité dans la couche de sortie
            sink.addFeature(feat, QgsFeatureSink.FastInsert)
            
            feedback.setProgress(int(current * total))

        return {self.OUTPUT: dest_id}