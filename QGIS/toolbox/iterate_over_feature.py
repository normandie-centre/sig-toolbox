import qgis
from qgis.utils import iface
from PyQt5.QtWidgets import QMessageBox
from qgis.PyQt.QtCore import QCoreApplication
from qgis.core import (QgsProcessingException,
                       QgsProcessingAlgorithm)

class iterateOverFeatures(QgsProcessingAlgorithm):

    def tr(self, string):
        return QCoreApplication.translate('Processing', string)

    def createInstance(self):
        return iterateOverFeatures()

    def name(self):
        return 'iterateOverFeatures'

    def displayName(self):
        return self.tr('Itération sur les entités de la couche active')

    def group(self):
        return self.tr('Cerema Normandie-Centre')

    def groupId(self):
        return 'cerema-nc'
        
    def shortHelpString(self):
        return self.tr('Itère sur les entités de la couche active (zoomer et infos).')

    def initAlgorithm(self, config=None):
        return None

    def processAlgorithm(self, parameters, context, feedback):
        
        try:
            lay = iface.activeLayer()
        except QgsProcessingException:
            msg = QMessageBox()
            msg.setText('Aucune couche sélectionnée. Arrêt de l\'algorithme.')
            msg.exec()
            
        fields = [field.name() for field in lay.dataProvider().fields()]

        for feat in lay.getFeatures():
            iface.mapCanvas().setExtent(feat.geometry().boundingBox())
            iface.mapCanvas().refresh()
            msg = QMessageBox()
            msg.setText(str(dict(zip(fields,feat.attributes()))).replace(',','\n'))
            msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Abort)
            ret = msg.exec()
            if ret == QMessageBox.Abort:
                return {}
            
        return {}
