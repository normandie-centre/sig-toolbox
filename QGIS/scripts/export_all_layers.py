dir = '.'

for vLayer in QgsProject.instance().mapLayers().values():
    print(f"-- {vLayer.name()}")
    try:
        QgsVectorFileWriter.writeAsVectorFormat( vLayer, dir + vLayer.name() + ".shp", "utf-8", vLayer.crs(),driverName='ESRI Shapefile' )
    except:
        print(f"ERROR : {vLayer.name()} non exporté")
