##############################################
##  Recherche sur différents moteurs de recherche le nom associé à un code SIREN et l'affiche dans la messageBar, en se basant sur le site web societe.com.
##  Est utile lorsqu'on utilise Geosirene de Christian Quest dont les dénominations d'entreprise ne sont pas toujours présentes dans la table.
##############################################

from qgis.utils import iface
from qgis.core import Qgis

from search_engines import Google, Yahoo, Duckduckgo, Startpage, Aol

engines = [Google(), Duckduckgo(), Startpage(), Yahoo(), Aol()]		# ordre dans lequel on va procéder à la recherche
siret = [%siret%]							# le champ s'appelle 'siret' dans les données Geosirene
site = "societe.com"
req = f"siren {siret} site:{site}"

def find_siret_name(engine):
    for response in engine.search(req, pages=1):
	# on passe si ce n'est pas le site 'societe.com'
        if site not in response['host']:
            continue

        nom, siren = ' '.join(response['link'].split('/')[-1].split('-')[0:-1]).upper(), response['link'].split('/')[-1].split('-')[-1].split('.')[0]
        return nom, siren
        
    return None		# si le moteur de recherche ne trouve pas de liens sur le site 'societe.com'


## Main
for engine in engines:
    res = find_siret_name(engine)
    if res is not None:
        nom, siren = res[0], res[1]
        iface.messageBar().pushInfo("Nom de l'entreprise", f"Entreprise {nom} et SIREN {siren} trouvé grâce à {engine.__class__.__name__}")
        break	# on s'arrête dès qu'un moteur de recherche a trouvé

